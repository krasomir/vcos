@extends('layouts.head')
@section('title', __('Izvještaji'))
@section('meta-description', __('DKolektiv usmjeren je prema viziji društva u kojem su građani motivirani činiti pozitivne promjene u svojim zajednicama'))
@section('meta-keywords', __('ciljevi, vizija, misija, vizija društva, motivacija'))

@section('og_url', config('app.url') . '/' . app()->getLocale() . '/reports')
@section('og_title', __('Izvještaji'))
@section('og_description', __('DKolektiv usmjeren je prema viziji društva u kojem su građani motivirani činiti pozitivne promjene u svojim zajednicama'))
@section('og_image', asset('storage/images/dkolektiv_logo_hd.jpg'))
@section('og_type', 'article')
@section('og_locale', 'hr_HR')

@section('content')
<!-- BANNER -->
 <x-banner-component :request-path="Request::path()" :name="__('Izvještaji')" />

<!-- downloads -->
<div class="section">
    <div class="content-wrap">
        <div class="container">
            <div class="row">
                @foreach($reports as $report)
                <!-- download item -->
                <div class="col-sm-12 col-md-6 mb-4">
                    <div class="rs-box-download block">
                        <div class="icon">
                            <i class="fa fa-file-{{ $report->report_type === 'pdf' ? 'pdf-o' : 'powerpoint-o'}}"></i>
                        </div>
                        <div class="body">
                            <a href="{{ $report->report_type === 'pdf' ? asset($report->report_url) : $report->report_url }}" target="_blank">
                                <h3>{{ $report->title }}</h3>
                                klikni za {{ $report->report_type === 'pdf' ? 'preuzimanje .' . strtoupper($report->report_type) : 'piktochart' }} 
                            </a>
                        </div>
                    </div>
                </div>
                <!-- end download item -->
                @endforeach
            </div>
        </div>
    </div>
</div>  

<!-- OUR PATRONS -->
<x-our-patrons-component />                  
@endsection