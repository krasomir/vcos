@extends('layouts.head')
@section('title', $post->name)
@section('meta-description', Str::limit(strip_tags($post->preview), 160, '...'))
@section('meta-keywords', $post->keywords)
@section('og_url', config('app.url') . '/' . app()->getLocale() . '/posts/' . $post->slug)
@section('og_title', $post->name)
@section('og_description', Str::limit(strip_tags($post->text), 160, '...'))
@section('og_image', asset('storage/images/news/' . $post->image_name))
@section('og_type', 'article')
@section('og_locale', 'hr_HR')

@section('content')
<!-- BANNER -->
 <x-banner-component :request-path="Request::path()" 
    :name="$post->name" 
    :extra-breadcrumb="route('news', app()->getLocale())" 
    extra-breadcrumb-name="Novosti"
/>

<!-- news -->
<div class="section">
    <div class="content-wrap">
        <div class="container">
            <div class="row">
                @if($post->preview || $post->keywords)
                <div class="col-sm-5 col-md-4 col-lg-3">

                    @if($post->preview)
                    <div class="widget widget-text">
                        <div class="widget-title">
                            Uvod
                        </div>
                        <p>{!! $post->preview !!}</p>
                    </div>
                    @endif

                    @if($post->keywords)
                    <!-- invisible on small devices -->
                    <div class="widget tags d-none d-md-block">
                        <div class="widget-title">
                            Tagovi
                        </div>
                        <x-tags-component :tags="$post->keywords" />
                    </div>
                    @endif

                    <!-- social network share -->
                    <div class="social-network-share mt-4">
                        <p class="mb-2">
                            <i class="fa fa-share-alt"></i>
                            Podijeli na društvenim mrežama:
                        </p>
                        {!! $socialShare !!}
                    </div>
                </div>
                @endif
                <!-- main content -->
                <div class="@if($post->preview || $post->keywords) col-sm-7 col-md-8 col-lg-9 @else col-sm-12 @endif">
                    <div class="single-news">
                        @if($post->image_name)
                            <div class="image">
                                <img src="{{ asset('storage/images/news/' . $post->image_name) }}" alt="{{ $post->slug }}" class="img-fluid">
                            </div>
                        @elseif($post->id < config('app.last_old_post_id') && $oldPostImagePath)
                            <div class="image">
                                <img src="{{ asset('storage' . $oldPostImagePath->crop) }}" alt="{{ $post->slug }}" class="img-fluid">
                            </div>
                        @endif
                        <div class="meta mt-2">
                            <div class="meta-date">
                                <i class="fa fa-clock-o"></i> 
                                {{ $post->datetime_on->isoFormat('DD. MMMM Y.') }}
                            </div>
                        </div>

                        <!-- content -->
                        {!! $post->text !!}
                        <!-- end content -->
                    </div>

                    <!-- social network share -->
                    <div class="social-network-share mt-6">
                        <p class="mb-2">
                            <i class="fa fa-share-alt"></i>
                            Podijeli na društvenim mrežama:
                        </p>
                        {!! $socialShare !!}
                    </div>
                </div>
            </div>

            @if($post->keywords)
            <div class="row">
                <div class="col-sm-12 mt-5">
                    <!-- visible only on small devices -->
                    <div class="widget tags d-block d-sm-none">
                        <div class="widget-title">
                            Tagovi
                        </div>
                        <x-tags-component :tags="$post->keywords" />
                    </div>
                </div>
            </div>
            @endif
        </div>
    </div>
</div>


<!-- OUR PATRONS -->
<x-our-patrons-component />
@endsection