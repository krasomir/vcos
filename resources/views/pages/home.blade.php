@extends('layouts.head')
@section('title', __('DKolektiv - organizacija za društveni razvoj'))
@section('meta-description', __('DKolektiv - organizacija za društveni razvoj'))
@section('meta-keywords', __('Humano, otvoreno, solidarno, održivo i uključivo društvo'))

@section('og_url', config('app.url') . '/' . app()->getLocale())
@section('og_title', __('DKolektiv - organizacija za društveni razvoj'))
@section('og_description', __('Biti inspiracija i podrška demokratskoj kulturi, razvoju civilnog društva i volonterstva, stvaranju prilika za jednake mogućnosti i aktivno sudjelovanje građana'))
@section('og_image', asset('storage/images/d_kolektiv_logo_hd.jpg'))
@section('og_type', 'article')
@section('og_locale', 'hr_HR')

@section('content')
<!-- BANNER -->
@if(count($sliders) > 2)
	<div id="oc-fullslider" class="banner owl-carousel" style="height:596px;">
		@foreach($sliders as $slider)
			<div class="owl-slide">
				<div class="item">
					<a href="{{ ($slider->link_to_page) ? url(app()->getLocale() . $slider->link_to_page) : $slider->custom_link }}">
						<img src="{{ asset('storage/images/slider/' . $slider->image_name) }}" alt="{{ $slider->title }}">
						<div class="slider-pos">
							<div class="container">
								<div class="wrap-caption right">
									<!-- <h1 class="caption-heading bg">
										<span>{{ __('#Solidarnost') }}</span>
										{{ __('Nije toliko bitno što je na stolu, već tko je na stolicama.') }}
									</h1>
									<p class="bg">
										{{ __('Nemojte se truditi da samo vi budete dobri, već da i svijet što ga iza sebe ostavljate postane dobar.') }}
									</p>
									<a href="{{ ($slider->link_to_page) ? url(app()->getLocale() . $slider->link_to_page) : '#' }}" class="btn btn-primary">
										{{ strtoupper(__('Volontiraj')) }}
									</a> -->
								</div>
							</div>
						</div>
					</a>
				</div>
			</div>
		@endforeach
	</div>
@endif

<div class="clearfix"></div>

<!-- ongoing activities section -->
<div class="section">
	<div class="content-wrap pb-3">
		<div class="container p-0">
			<div class="row gutter-5 mb-3">
				<!-- ongoing activities -->
				<div id="ongoing-activities" class="col-sm-12 d-none d-xl-block">
					<div class="row gutter-5">
						<!-- vcos -->
						<div class="col-sm-12 col-md-6 col-lg-3 col-xl-3">
							<div class="primary-activity bkg-white h-158">
								<a href="{{ route('vcos', app()->getLocale()) }}">
									<img src="{{ asset('storage/images/programs/volonterski_centar_osijek_bkg.jpg') }}" alt="Volonterski centar Osijek">
								</a>
							</div>
							<div class="primary-activity h-150">
								<div class="h-150">
									<div class="activity-title">
										<a href="{{ route('vcos', app()->getLocale()) }}">
											<span class="list-icon list-icon-vcos"></span>
											Volonterski centar
										</a>
									</div>
									<ul class="ml-4 mb-0">
										<li>
											<div class="spacer-1"></div>
										</li>
										<li>
											<a href="{{ App\Constants\Volonteka\Volonteka::WANT_TO_VOLUNTEER_LINK }}" target="_blank">
												<span class="list-icon list-icon-vcos"></span> Želite volontirati?
											</a>
										</li>
										<li>
											<div class="spacer-1"></div>
										</li>
										<li>
											<a href="{{ App\Constants\Volonteka\Volonteka::NEED_VOLUNTEERS_LINK }}" target="_blank">
												<span class="list-icon list-icon-vcos"></span> Trebate volontere?
											</a>
										</li>
										<li>
											<div class="spacer-1"></div>
										</li>
										<li>
											<a href="{{ App\Constants\Volonteka\Volonteka::OPEN_VOLUNTEERING_OPPORTUNITIES_LINK }}" target="_blank">
												<span class="list-icon list-icon-vcos"></span> Otvorene mogućnosti za volontiranje
											</a>
										</li>
									</ul>
								</div>
							</div>
						</div>
						<!-- second column -->
						<div class="col-sm-12 col-md-6 col-lg-3 col-xl-3">
							<!-- demo academy -->
							<div class="other-activities demo-academy-block position-relative h-150 mb-2">
								<div class="h-150">
									<span class="block-corner"></span>
									<div class="activity-title">
										<a href="{{ route('demo-academy', app()->getLocale()) }}">
											Demo akademija
										</a>
									</div>
									<ul class="ml-4 mb-0 h-150 other-activities-sngle">
										<li>
											<!-- <a href="{{ route('demo-academy', app()->getLocale()) }}"> -->
												<span class="font-italic">Obrazovanje za demokraciju i aktivno sudjelovanje</span>
											<!-- </a> -->
										</li>
									</ul>
								</div>
							</div>
							<!-- society studio component -->
							<div class="other-activities society-studio position-relative h-150 mb-2">
								<div class="h-150">
									<span class="block-corner"></span>
									<div class="activity-title">
										<a href="{{ route('social-atelier', app()->getLocale()) }}">
											Društveni atelje
										</a>
									</div>
									<ul class="ml-4 mb-0 h-150 other-activities-sngle">
										<li>
											<!-- <a href="{{ route('social-atelier', app()->getLocale()) }}"> -->
												<span class="font-italic">Mjesto susreta različitosti i osnaživanja za doprinos uključivoj zajednici</span>
											<!-- </a> -->
										</li>
									</ul>
								</div>
							</div>
						</div>
						<!-- third column -->
						<div class="col-sm-12 col-md-6 col-lg-3 col-xl-3">
							<!-- podcast kolektiv -->
							<div class="other-activities podcast-block position-relative h-150 mb-2">
								<div class="h-150">
									<span class="block-corner"></span>
									<span class="podcast-icon">
										<i class="fas fa-microphone-alt"></i>
									</span>
									<div class="activity-title">
										<a href="{{ route('podcasts', app()->getLocale()) }}">
											PoDcast Kolektiv
										</a>
									</div>
									<ul class="ml-4 mb-0 h-150 other-activities-sngle">
										<li>
											<!-- <a href="#"> -->
											<span class="font-italic">Podcast civilnog društva o civilnom društvu</span>
											<!-- </a> -->
										</li>
									</ul>
								</div>
							</div>
							<!-- radius V -->
							<div class="other-activities radius-block position-relative h-150 mb-2">
								<div class="h-150">
									<span class="block-corner"></span>
									<div class="activity-title">
										<a href="{{ route('project', ['lang' => app()->getLocale(), 'project' => 'radius-v']) }}">
											Radius V
										</a>
									</div>
									<ul class="ml-4 mb-0 h-150 other-activities-sngle">
										<li>
											<!-- <a href="#"> -->
											<span class="font-italic">Projekt usmjeren na kreiranje poticajnog okruženja za razvoj volonterstva</span>
											<!-- </a> -->
										</li>
									</ul>
								</div>
							</div>
						</div>
						<!-- strength of participation -->
						<div class="col-sm-12 col-md-6 col-lg-3 col-xl-3">
							<div class="other-activities civil-society bkg-white h-158">
								<a href="{{ route('strength-of-participation', app()->getLocale()) }}">
									<img src="{{ asset('storage/images/programs/razvoj_civilnog_drustva_bkg.jpg') }}" alt="Razvoj civilnog društva: snaga sudjelovanja za društveni razvoj">
								</a>
							</div>
							<div class="other-activities civil-society h-150">
								<div class="h-150">
									<div class="activity-title">
										<a href="{{ route('strength-of-participation', app()->getLocale()) }}">
											<span class="list-icon list-icon-rcd"></span>
											Razvoj civilnog društva
										</a>
									</div>
									<ul class="ml-4 mb-0 mt-2">
										<li>
											<div class="spacer-1"></div>
										</li>
										<li>
											<a href="{{ route('strength-of-participation', app()->getLocale()) }}">
												<span class="list-icon list-icon-rcd"></span>
												<span class="font-italic">Snaga sudjelovanja za društveni razvoj</span>
											</a>
										</li>
										<li>
											<div class="spacer-1"></div>
										</li>
										<li>
											<a href="{{ route('strength-of-participation-application', app()->getLocale()) }}">
												<span class="list-icon list-icon-rcd"></span> POVEŽI SE!
											</a>
										</li>
									</ul>
								</div>
							</div>
						</div>
					</div>
				</div>
				<!-- ongoing activities mobile -->
				<div id="ongoing-activities" class="col-sm-12 d-md-block d-lg-block d-xl-none">
					<div class="row gutter-5">
						<!-- forst column -->
						<div class="col-sm-12 col-md-6 col-lg-4 col-xl-3">
							<!-- vcos -->
							<div class="primary-activity h-150 mb-2">
								<div class="h-150">
									<div class="activity-title">
										<a href="{{ route('vcos', app()->getLocale()) }}">
											<span class="list-icon list-icon-vcos"></span>
											Volonterski centar
										</a>
									</div>
									<ul class="ml-4 mb-0">
										<li>
											<div class="spacer-1"></div>
										</li>
										<li>
											<a href="https://vcos.hr/forma/prijava/volonteri" target="_blank">
												<span class="list-icon list-icon-vcos"></span> Želite volontirati?
											</a>
										</li>
										<li>
											<div class="spacer-1"></div>
										</li>
										<li>
											<a href="https://vcos.hr/forma/prijava/organizacije" target="_blank">
												<span class="list-icon list-icon-vcos"></span> Trebate volontere?
											</a>
										</li>
										<li>
											<div class="spacer-1"></div>
										</li>
										<li>
											<a href="{{ route('open-volunteering-opportunities', app()->getLocale()) }}">
												<span class="list-icon list-icon-vcos"></span> Otvorene mogućnosti za volontiranje
											</a>
										</li>
									</ul>
								</div>
							</div>
							<!-- strength of participation -->
							<div class="other-activities civil-society h-150 mb-2">
								<div class="h-150">
									<div class="activity-title">
										<a href="{{ route('strength-of-participation', app()->getLocale()) }}">
											<span class="list-icon list-icon-rcd"></span>
											Razvoj civilnog društva
										</a>
									</div>
									<ul class="ml-4 mb-0 mt-2">
										<li>
											<div class="spacer-1"></div>
										</li>
										<li>
											<a href="{{ route('strength-of-participation', app()->getLocale()) }}">
												<span class="list-icon list-icon-rcd"></span>
												<span class="font-italic">Snaga sudjelovanja za društveni razvoj</span>
											</a>
										</li>
										<li>
											<div class="spacer-1"></div>
										</li>
										<li>
											<a href="https://vcos.hr/forma/prijava/snagasudjelovanja" target="_blank">
												<span class="list-icon list-icon-rcd"></span> POVEŽI SE!
											</a>
										</li>
									</ul>
								</div>
							</div>
						</div>
						<!-- second column -->
						<div class="col-sm-12 col-md-6 col-lg-4 col-xl-3">
							<!-- demo academy -->
							<div class="other-activities demo-academy-block position-relative h-150 mb-2">
								<div class="h-150">
									<span class="block-corner"></span>
									<div class="activity-title">
										<a href="{{ route('demo-academy', app()->getLocale()) }}">
											Demo akademija
										</a>
									</div>
									<ul class="ml-4 mb-0 h-150 other-activities-sngle">
										<li>
											<a href="{{ route('demo-academy', app()->getLocale()) }}">
												<span class="font-italic">Obrazovanje za demokraciju i aktivno sudjelovanje</span>
											</a>
										</li>
									</ul>
								</div>
							</div>
							<!-- society studio component -->
							<div class="other-activities society-studio position-relative h-150 mb-2">
								<div class="h-150">
									<span class="block-corner"></span>
									<div class="activity-title">
										<a href="{{ route('social-atelier', app()->getLocale()) }}">
											Društveni atelje
										</a>
									</div>
									<ul class="ml-4 mb-0 h-150 other-activities-sngle">
										<li>
											<a href="{{ route('social-atelier', app()->getLocale()) }}">
												<span class="font-italic">Mjesto susreta različitosti i osnaživanja za doprinos uključivoj zajednici</span>
											</a>
										</li>
									</ul>
								</div>
							</div>
						</div>
						<!-- third column -->
						<div class="col-sm-12 col-md-12 col-lg-4 col-xl-3 d-none d-lg-block">
							<!-- podcast kolektiv -->
							<div class="other-activities podcast-block position-relative h-150 mb-2">
								<div class="h-150">
									<span class="block-corner"></span>
									<span class="podcast-icon">
										<i class="fas fa-microphone-alt"></i>
									</span>
									<div class="activity-title">
										<a href="{{ route('podcasts', app()->getLocale()) }}">
											PoDcast Kolektiv
										</a>
									</div>
									<ul class="ml-4 mb-0 h-150 other-activities-sngle">
										<li>
											<!-- <a href="#"> -->
											<span class="font-italic">Podcast civilnog društva o civilnom društvu</span>
											<!-- </a> -->
										</li>
									</ul>
								</div>
							</div>
							<!-- radius V -->
							<div class="other-activities radius-block position-relative h-150 mb-2">
								<div class="h-150">
									<span class="block-corner"></span>
									<div class="activity-title">
										<a href="{{ route('project', ['lang' => app()->getLocale(), 'project' => 'radius-v']) }}">
											Radius V
										</a>
									</div>
									<ul class="ml-4 mb-0 h-150 other-activities-sngle">
										<li>
											<!-- <a href="#"> -->
											<span class="font-italic">Projekt usmjeren na kreiranje poticajnog okruženja za razvoj volonterstva</span>
											<!-- </a> -->
										</li>
									</ul>
								</div>
							</div>
						</div>
						<!-- third column smaller -->
						<div class="col-md-6 d-lg-none">
							<!-- podcast kolektiv -->
							<div class="other-activities podcast-block position-relative h-150 mb-2">
								<div class="h-150">
									<span class="block-corner"></span>
									<span class="podcast-icon">
										<i class="fas fa-microphone-alt"></i>
									</span>
									<div class="activity-title">
										<a href="{{ route('podcasts', app()->getLocale()) }}">
											PoDcast Kolektiv
										</a>
									</div>
									<ul class="ml-4 mb-0 h-150 other-activities-sngle">
										<li>
											<!-- <a href="#"> -->
											<span class="font-italic">Podcast civilnog društva o civilnom društvu</span>
											<!-- </a> -->
										</li>
									</ul>
								</div>
							</div>
						</div>
						<div class="col-md-6 d-lg-none">
							<!-- radius V -->
							<div class="other-activities radius-block position-relative h-150 mb-2">
								<div class="h-150">
									<span class="block-corner"></span>
									<div class="activity-title">
										<a href="{{ route('project', ['lang' => app()->getLocale(), 'project' => 'radius-v']) }}">
											Radius V
										</a>
									</div>
									<ul class="ml-4 mb-0 h-150 other-activities-sngle">
										<li>
											<!-- <a href=""> -->
											<span class="font-italic">Projekt usmjeren na kreiranje poticajnog okruženja za razvoj volonterstva</span>
											<!-- </a> -->
										</li>
									</ul>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<!-- news -->
			<div class="row gutter-5 d-none d-lg-block">
				<div id="ongoing-activities" class="col-sm-12">
					<div class="row gutter-5">
						<!-- news box -->
						@foreach($latestPosts as $post)
							@if($loop->iteration <= 2)
								<div class="col-sm-12 col-md-4 col-lg-4">
									<div class="box-news-1">
										@if($post->image_name)
											<div class="media gbr">
												<img src="{{ asset('storage/images/news/thumb/' . $post->image_name) }}" alt="{{ $post->slug }}" class="img-fluid">
											</div>
											@elseif($post->id < config('app.last_old_post_id') && $post->getOldPostImagePath())
												<div class="media gbr">
													<img src="{{ asset('storage' . $post->getOldPostImagePath()->crop) }}" alt="{{ $post->slug }}" class="img-fluid">
												</div>
											@endif
										<div class="body">
											<div class="title">
												<a href="{{ route('news-details', ['lang' => app()->getLocale(), 'post' => $post]) }}" title="{{ $post->name }}">
													<h3>{{ $post->name }}</h3>
												</a>
											</div>
											<div>
												<p class="mt-2">
													{!! $post->preview !!}
													@if(!$post->preview)
														{!! $post->shortenText() !!}
													@endif
												</p>
											</div>
											<div class="meta">
												<span class="date"><i class="fa fa-clock-o"></i> {{ $post->datetime_on->format('d.m.Y.') }}</span>
											</div>
										</div>
									</div>
								</div>
							@endif
						@endforeach
						<!-- end news box -->
						<!-- facebook -->
						<div class="col-sm-12 col-md-4 col-lg-4">
							<div class="row">
								<div class="col-sm-12 mt-2 mt-md-4 mt-lg-0 text-center">
									<div id="fb-root"></div>
									<script async defer crossorigin="anonymous" src="https://connect.facebook.net/hr_HR/sdk.js#xfbml=1&version=v8.0&appId=1668544280031855&autoLogAppEvents=1" nonce="MdUnwd8T"></script>
									<div class="fb-page" data-href="https://www.facebook.com/dkolektiv" data-tabs="timeline" data-width="540" data-height="500" data-small-header="true" data-adapt-container-width="true" data-hide-cover="false" data-show-facepile="true">
										<blockquote cite="https://www.facebook.com/dkolektiv" class="fb-xfbml-parse-ignore">
											<a href="https://www.facebook.com/dkolektiv">Volonterski Centar Osijek</a>
										</blockquote>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>

		</div>
	</div>
</div>



<!-- NEWS on mobile -->
<div class="section">
	<div class="content-wrap pt-0">
		<div class="container p-0">
			<div class="row mx-2 gutter-5">
				<!-- news box -->
				@foreach($latestPosts as $post)
					<div class="col-sm-12 col-md-6 d-block d-lg-none d-xl-none">
						<div class="box-news-1">
							@if($post->image_name)
								<div class="media gbr">
									<img src="{{ asset('storage/images/news/thumb/' . $post->image_name) }}" alt="{{ $post->slug }}" class="img-fluid">
								</div>
								@elseif($post->id < config('app.last_old_post_id') && $post->getOldPostImagePath())
									<div class="media gbr">
										<img src="{{ asset('storage' . $post->getOldPostImagePath()->crop) }}" alt="{{ $post->slug }}" class="img-fluid">
									</div>
								@endif
							<div class="body">
								<div class="title">
									<a href="{{ route('news-details', ['lang' => app()->getLocale(), 'post' => $post]) }}" title="{{ $post->name }}">
										<h3>{{ $post->name }}</h3>
									</a>
								</div>
								<div>
									<p class="mt-2">
										{!! $post->preview !!}
										@if(!$post->preview)
											{!! $post->shortenText() !!}
										@endif
									</p>
								</div>
								<div class="meta">
									<span class="date"><i class="fa fa-clock-o"></i> {{ $post->datetime_on->format('d.m.Y.') }}</span>
								</div>
							</div>
						</div>
					</div>
				@endforeach
				<!-- end news box -->
			</div>
		</div>
	</div>
</div>

<!-- OUR PATRONS -->
<x-our-patrons-component />
@endsection
