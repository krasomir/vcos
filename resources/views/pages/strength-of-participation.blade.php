@extends('layouts.head')
@section('title', $program->title)
@section('meta-description', __('Biti aktivan i biti odgovoran važne su vrline civilnog društva za koje se DKolektiv zalaže od svoga osnivanja.'))
@section('meta-keywords', __('edukacija, informacija, poticanje, afirmacija, zagovaranje, umrežavanje, MOTUS'))

@section('og_url', config('app.url') . '/' . app()->getLocale() . '/strength-of-participation')
@section('og_title', $program->title)
@section('og_description', __('Biti aktivan i biti odgovoran važne su vrline civilnog društva za koje se DKolektiv zalaže od svoga osnivanja.'))
@section('og_image', asset('storage/images/razvoj_civilnog_drustva_logo_hd.jpg'))
@section('og_type', 'article')
@section('og_locale', 'hr_HR')

@section('content')
<!-- BANNER -->
 <x-banner-component :request-path="Request::path()" 
    :name="$program->title" 
/>

<!-- strentgh of participation -->
<div class="section strength-of-participation">
    <div class="content-wrap">
        <div class="container">
            <div class="row">
                <!--  -->
                <div class="col-sm-12">
                    {!! $program->content !!}
                </div>
            </div>
        </div>
    </div>
</div>
<!-- frame logos -->
<div class="section mt-n5">
    <div class="content-wrap">
        <div class="container">
            <div class="row gutter-1">
                <div class="col-12 col-sm-12 col-md-6 col-lg-6 text-center frame-container mb-4">
                    <a href="{{ route('osijek-to-goo', app()->getLocale()) }}">
                        <div id="osijekToGoo"></div>
                    </a>
                </div>
                <div class="col-12 col-sm-12 col-md-6 col-lg-6 text-center frame-container mb-4">
                    <a href="{{ route('demo-academy', app()->getLocale()) }}">
                        <div id="demoAkademija"></div>
                    </a>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- OUR PATRONS -->
<x-our-patrons-component />
@endsection