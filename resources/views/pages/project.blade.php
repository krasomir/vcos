@extends('layouts.head')
@section('title', $project->title)

@section('meta-description', Str::limit(strip_tags($project->preview), 160, '...'))
@section('meta-keywords', str_replace(' ', ', ', Str::words(strip_tags($project->preview), 10)))
@section('og_url', config('app.url') . '/' . app()->getLocale() . '/project/' . $project->slug)
@section('og_title', $project->title)
@section('og_description', Str::limit(strip_tags($project->description), 160, '...'))
@if($project->image_name)
    @section('og_image', asset('storage/images/projects/' . $project->image_name))
@else
@section('og_image', asset('storage/images/d_kolektiv_logo_hd.jpg'))
@endif
@section('og_type', 'article')
@section('og_locale', 'hr_HR')

@section('content')
<!-- BANNER -->
<x-banner-component :request-path="Request::path()" 
    :name="$project->title" 
    :extra-breadcrumb="($project->archived) ? route('archive-of-projects', app()->getLocale()) : route('active-projects', app()->getLocale())" 
    extra-breadcrumb-name="{{ ($project->archived) ? 'Arhiva projekata' : 'Projekti' }}"
/>

<!-- news -->
<div class="section">
    <div class="content-wrap">
        <div class="container">
            <div class="row">

                <!-- project info -->
                <div class="col-sm-5 col-md-4 col-lg-3">
                    <div class="widget-title">
                        Uvod
                    </div>
                    <div>{!! $project->preview !!}</div>
                </div>

                <!-- project details -->
                <div class="col-sm-7 col-md-8 col-lg-9">
                    <div class="single-news">
                        @if($project->image_name)
                            <div class="image">
                                <img src="{{ asset('storage/images/projects/' . $project->image_name) }}" alt="{{ $project->title }}" class="img-fluid">
                            </div>
                        @endif

                        <div class="meta">
                            <div class="meta-date"><i class="fa fa-clock-o"></i> {{ $project->start_date->isoFormat('DD. MMMM Y.') }}</div>
                        </div>

                        <!-- content -->
                        <div class="content">{!! $project->description !!}</div>
                        <!-- end content -->
                        
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection