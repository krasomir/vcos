@extends('layouts.head')
@section('title', $program->title)
@section('meta-description', __('Program Volonterskog centra Osijek dio je sustavnog i promišljenog djelovanja DKolektiva u cilju uspostavljanja kontinuirane, strukturirane i kvalitetne podrške volonterima, organizatorima volontiranja i drugim dionicima relevantnim za razvoj volonterstva.'))
@section('meta-keywords', __('promocija, razmjena, usluga, podrška, umrežavanje, provedba'))

@section('og_url', config('app.url') . '/' . app()->getLocale() . '/volonterski-centar-osijek')
@section('og_title', $program->title)
@section('og_description', __('Program Volonterskog centra Osijek dio je sustavnog i promišljenog djelovanja DKolektiva u cilju uspostavljanja kontinuirane, strukturirane i kvalitetne podrške volonterima, organizatorima volontiranja i drugim dionicima relevantnim za razvoj volonterstva.'))
@section('og_image', asset('storage/images/volonterski_centar_osijek_logo_hd.jpg'))
@section('og_type', 'article')
@section('og_locale', 'hr_HR')

@section('content')
<!-- BANNER -->
 <x-banner-component :request-path="Request::path()"
    :name="$program->title"
/>
<!-- SUB-NAVBAR SECTION -->
<section class="section sub-navbar">
    <div class="content-wrap pb-0">
        <div class="container">
            <div class="row">
                <div class="col-xs-6">
                    <a href="{{ App\Constants\Volonteka\Volonteka::WANT_TO_VOLUNTEER_LINK }}" target="_blank">
                        <button class="btn btn-outline-dark">
                            {{ strtoupper(__('Želite volontirati?')) }}
                        </button>
                    </a>
                </div>
                <div class="col-xs-6">
                    <a href="{{ App\Constants\Volonteka\Volonteka::NEED_VOLUNTEERS_LINK }}" target="_blank">
                        <button type="button" class="btn btn-outline-dark">
                            {{ strtoupper(__('Trebate volontere?')) }}
                        </button>
                    </a>
                </div>
            </div>
        </div>
    </div>
</section>

<!--  -->
<div class="section">
    <div class="content-wrap">
        <div class="container">
            <div class="row">
                <!--  -->
                <div class="col-sm-12">
                    {!! $program->content !!}
                </div>

                <!-- TABOVI -->
                <div class="col-sm-12 col-md-12">
                    <div class="row col-0 tabs">
                        <div class="col-sm-4 col-md-4 mb-3">
                            <!-- BOX 2 -->
                            <a href="{{ route('kd', app()->getLocale()) }}">
                                <div class="rs-feature-box-1" data-background="{{ asset('storage/images/korisni_dokumenti.jpg') }}"></div>
                            </a>
                        </div>
                        <div class="col-sm-4 col-md-4 mb-3">
                            <!-- BOX 3 -->
                            <a href="{{ route('unv', app()->getLocale()) }}">
                                <div class="rs-feature-box-1" data-background="{{ asset('storage/images/volonterska_nagrada.jpg') }}"></div>
                            </a>
                        </div>
                        <div class="col-sm-4 col-md-4 mb-3">
                            <!-- BOX 4 -->
                            <a href="{{ route('ess', app()->getLocale()) }}">
                                <div class="rs-feature-box-1" data-background="{{ asset('storage/images/europske_snage_solidarnosti.jpg') }}"></div>
                            </a>
                        </div>
                    </div>
                </div>

                <!-- icons -->
                <div class="col-sm-12 vcos-patrons-section">
                    <div class="row col-0 tabs">
                        <!--  -->
                        <div class="col-sm-4">
                            <a href="https://www.hcrv.hr/" target="_blank">
                                <img class="img-fluid" src="{{ asset('storage/images/hcrv_icon.jpg') }}" alt="hcrv" />
                            </a>
                        </div>
                        <div class="col-sm-4">
                            <a href="https://www.europeanvolunteercentre.org/" target="_blank">
                                <img class="img-fluid" src="{{ asset('storage/images/cev_icon.jpg') }}" alt="cev" />
                            </a>
                        </div>
                        <div class="col-sm-4">
                            <a href="https://mrosp.gov.hr/" target="_blank">
                                <img class="img-fluid" src="{{ asset('storage/images/mrmsosp_icon.jpg') }}" alt="mrmsosp" />
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
