@extends('layouts.head')
@section('title', __('Pretraga'))
@section('meta-description', __('Volonterski centar usmjeren je prema viziji društva u kojem su građani motivirani činiti pozitivne promjene u svojim zajednicama'))
@section('meta-keywords', __('Volonteri, centar, Osijek, organizacija, Volonterski, Publikacije'))

@section('og_url', config('app.url') . '/' . app()->getLocale() . '/search')
@section('og_title', __('Pretraga'))
@section('og_description', __('Volonterski centar usmjeren je prema viziji društva u kojem su građani motivirani činiti pozitivne promjene u svojim zajednicama'))
@section('og_image', asset('storage/images/dkolektiv_logo_hd.jpg'))
@section('og_type', 'article')
@section('og_locale', 'hr_HR')

@section('content')
    <!-- BANNER -->
    <x-banner-component :request-path="Request::path()" :name="__('Pretraga')" />

    <!--  -->
    <div class="section">
        <div class="content-wrap">
            <div class="container">
                @if($searchResults->count() > 0)
                    <x-search-result-info :search-results-count="$searchResults->count()" :search-term="$searchTerm" />
                @endif
                <div class="row">
                    @forelse ($searchResults as $result)
                        @if($result instanceof \App\Post)
                            <x-global-search-result-card
                                category="{{ __('NOVOST') }}"
                                link="{{ route('news-details', ['lang' => app()->getLocale(), 'post' => $result]) }}"
                                :title="$result->name"
                                :date="$result->created_at->isoFormat('DD. MMMM Y.')"
                                :text="$result->shortenText()"
                            />
                        @endif

                        @if($result instanceof \App\Project)
                            <x-global-search-result-card
                                category="{{ __('PROJEKT') }}"
                                link="{{ route('project', ['lang' => app()->getLocale(), 'project' => $result]) }}"
                                :title="$result->title"
                                :date="$result->created_at->isoFormat('DD. MMMM Y.')"
                                :text="$result->shortenDescription()"
                            />
                        @endif

                        @if($result instanceof \App\Podcast)
                            <x-global-search-result-card
                                category="{{ __('PODCAST') }}"
                                link="{{ route('podcast-show', ['lang' => app()->getLocale(), 'podcast' => $result]) }}"
                                :title="$result->title"
                                :date="$result->created_at->isoFormat('DD. MMMM Y.')"
                                :text="$result->shortenDescription()"
                            />
                        @endif
                    @empty
                        <div class="col-sm-12">
                            <div class="publication-container">
                                <div class="body-content">
                                    <p class="text-center">
                                        {!! __('Nema rezultata za pojam <span class="bold">:searchTerm</span>', ['searchTerm' => $searchTerm]) !!}
                                    </p>
                                </div>
                            </div>
                        </div>
                    @endforelse

                </div>
                @if($searchResults->count() > 0)
                    <x-search-result-info :search-results-count="$searchResults->count()" :search-term="$searchTerm" />
                @endif
            </div>
        </div>
    </div>

@endsection
