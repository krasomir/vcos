@extends('layouts.head')
@section('title', __('PoDcast Kolektiv'))
@section('meta-description', __('PoDcast Kolektiv dijeli vaše priče. Odgovara na vaša pitanja. Informira vas na vrijeme. Slušajte nas svake srijede.'))
@section('meta-keywords', __('podcast, priče, informacije, pitanja, odgovori, volontiranje'))

@section('og_url', config('app.url') . '/' . app()->getLocale() . '/podcasts')
@section('og_title', __('PoDcast Kolektiv'))
@section('og_description', __('PoDcast Kolektiv dijeli vaše priče. Odgovara na vaša pitanja. Informira vas na vrijeme. Slušajte nas svake srijede.'))
@section('og_image', asset('storage/images/dkolektiv_logo_hd.jpg'))
@section('og_type', 'article')
@section('og_locale', 'hr_HR')

@section('content')
<!-- BANNER -->
<x-banner-component :request-path="Request::path()" :name="__('PoDcast Kolektiv')" />

<!--  -->
<div class="section">
    <div class="content-wrap">
        <div class="container">
            <div class="row">
                <!-- kd -->
                <div class="col-sm-12 col-md-12">
                    <blockquote id="pL">
                        {!! $mainPodcastText->content !!}

                        <!-- accordion podcasts seasons -->
                        <div class="accordion rs-accordion" id="seasonList">
                        @foreach($seasons as $sKey => $season)
                            <div class="card mb-2">
                                <div class="card-header" id="heading_season{{ $sKey }}">
                                    <h4 class="title">
                                        <button class="btn btn-link btn-block @if(!$loop->first) collapsed @endif" type="button" data-toggle="collapse" data-target="#collapse_season{{ $sKey }}" aria-expanded="true" aria-controls="collapse_season{{ $sKey }}">
                                            <span class="float-left">
                                                {{ $season }}. sezona {{ \App\Podcast::getSeasonYears($season) }}
                                            </span>
                                        </button>
                                    </h4>
                                </div>
                            </div>
                            <div id="collapse_season{{ $sKey }}" class="collapse @if($loop->first) show @endif" aria-labelledby="heading_season{{ $sKey }}">
                                <div class="card-body">
                                    <!-- accordion podcasts -->
                                    <div class="accordion rs-accordion" id="podcastsList{{ $sKey }}">
                                        <!--  -->
                                        @foreach($podcasts as $key => $podcast)
                                            @if($podcast->season == $season)
                                                <div class="card mb-2">
                                                    <div class="card-header" id="heading{{ $key }}">
                                                        <h4 class="title">
                                                            <button class="btn btn-link btn-block @if(!$loop->first) collapsed @endif" type="button" data-toggle="collapse" data-target="#collapse{{ $key }}" aria-expanded="true" aria-controls="collapse{{ $key }}">
                                                                <span class="float-left">
                                                                    {{ $podcast->title }}
                                                                </span>
                                                                <span class="float-right meta-date">
                                                                    <i class="fa fa-clock-o" aria-hidden="true"></i>
                                                                    {{ $podcast->published_at->format('d.m.Y') }}
                                                                </span>
                                                            </button>
                                                        </h4>
                                                    </div>
                                                    <div id="collapse{{ $key }}" class="collapse @if($loop->first) show @endif" aria-labelledby="heading{{ $key }}" data-parent="#podcastsList{{ $sKey }}">
                                                        <div class="card-body">
                                                            <p>{!! $podcast->description !!}</p>
                                                            <div class="spacer mt-2"></div>
                                                            <x-podcast-player-component :fileName="$podcast->file_name" :title="$podcast->title" />
                                                            <!-- download -->
                                                            <a class="pcast-download" href="{{ asset('storage/files/podcasts/' . $podcast->file_name) }}" download>
                                                                <div class="rs-box-download mt-4">
                                                                    <div class="icon">
                                                                        <i class="fas fa-download"></i>
                                                                    </div>
                                                                    <div class="body">
                                                                        <h4>Preuzmi epizodu</h4>
                                                                        <span class="small">klikni za preuzimanje</span>
                                                                    </div>
                                                                </div>
                                                            </a>
                                                        </div>
                                                    </div>
                                                </div>
                                            @endif
                                        @endforeach
                                    </div>
                                    <!-- accordion end -->
                                </div>
                            </div>
                        @endforeach
                        </div>


                    </blockquote>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('js')
<script>
    (function() {

        let speeds = [1, 1.5, 2, 2.5, 3];

        document.querySelectorAll('.pcast-player').forEach(function(player) {
            let audio = player.querySelector('audio');
            let play = player.querySelector('.pcast-play');
            let pause = player.querySelector('.pcast-pause');
            let rewind = player.querySelector('.pcast-rewind');
            let progress = player.querySelector('.pcast-progress');
            let speed = player.querySelector('.pcast-speed');
            let mute = player.querySelector('.pcast-mute');
            let currentTime = player.querySelector('.pcast-currenttime');
            let duration = player.querySelector('.pcast-duration');

            let currentSpeedIdx = 0;

            pause.style.display = 'none';

            let toHHMMSS = function(totalsecs) {
                let sec_num = parseInt(totalsecs, 10); // don't forget the second param
                let hours = Math.floor(sec_num / 3600);
                let minutes = Math.floor((sec_num - (hours * 3600)) / 60);
                let seconds = sec_num - (hours * 3600) - (minutes * 60);

                if (hours < 10) {
                    hours = "0" + hours;
                }
                if (minutes < 10) {
                    minutes = "0" + minutes;
                }
                if (seconds < 10) {
                    seconds = "0" + seconds;
                }

                let time = hours + ':' + minutes + ':' + seconds;
                return time;
            }

            audio.addEventListener('loadedmetadata', function() {
                setTimeout(() => {
                    progress.setAttribute('max', Math.floor(audio.duration));
                    duration.textContent = toHHMMSS(audio.duration);
                }, 500);

            });

            audio.addEventListener('timeupdate', function() {
                progress.setAttribute('value', audio.currentTime);
                currentTime.textContent = toHHMMSS(audio.currentTime);
            });

            play.addEventListener('click', function() {
                stopPlayingAll();
                this.style.display = 'none';
                pause.style.display = 'inline-block';
                audio.classList.add('active');
                audio.play();
                pause.focus();
            }, false);

            pause.addEventListener('click', function() {
                this.style.display = 'none';
                play.style.display = 'inline-block';
                audio.classList.remove('active');
                audio.pause();
                play.focus();
            }, false);

            rewind.addEventListener('click', function() {
                audio.currentTime -= 30;
            }, false);

            progress.addEventListener('click', function(e) {
                audio.currentTime = Math.floor(audio.duration) * (e.offsetX / e.target.offsetWidth);
            }, false);

            speed.addEventListener('click', function() {
                currentSpeedIdx = currentSpeedIdx + 1 < speeds.length ? currentSpeedIdx + 1 : 0;
                audio.playbackRate = speeds[currentSpeedIdx];
                this.textContent = speeds[currentSpeedIdx] + 'x';
                return true;
            }, false);

            mute.addEventListener('click', function() {
                if (audio.muted) {
                    audio.muted = false;
                    this.querySelector('.fa').classList.remove('fa-volume-off');
                    this.querySelector('.fa').classList.add('fa-volume-up');
                } else {
                    audio.muted = true;
                    this.querySelector('.fa').classList.remove('fa-volume-up');
                    this.querySelector('.fa').classList.add('fa-volume-off');
                }
            }, false);
        });
    })(this);

    // stop playing audio when click on accordion
    $('.card-header').on('click', function(e) {
        stopPlayingAll();
    });

    function stopPlayingAll() {
        document.querySelectorAll('.pcast-player').forEach(function(player) {
            let audio = player.querySelector('audio');
            let play = player.querySelector('.pcast-play');
            let pause = player.querySelector('.pcast-pause');
            if (audio.classList.contains('active')) {
                pause.style.display = 'none';
                play.style.display = 'inline-block';
                audio.pause();
            }
        });
    }
</script>
@endsection
