@extends('layouts.head')
@section('title', __('Voditelji kolegija i predavači Demo akademije'))
@section('meta-description', __('Demo akademija, jednogodišnji neformalni program obrazovanja, podržava razvoj znanja, jačanje vještina i oblikovanje stavova i ponašanja koji su osnova društvenog i političkog angažmana građana te su važni za realizaciju ljudskih i građanskih prava i odgovornosti.'))
@section('meta-keywords', __('Demokracija, Civilno društvo, Ljudska prava, Socijalna kohezija, Aktivni i odgovorni građani, Kultura mira, Održivi razvoj, Europska unija'))

@section('og_url', config('app.url') . '/' . app()->getLocale() . '/demo-academy-instructors')
@section('og_title', __('Voditelji kolegija i predavači Demo akademije'))
@section('og_description', __('Volonterski centar usmjeren je prema viziji društva u kojem su građani motivirani činiti pozitivne promjene u svojim zajednicama'))
@section('og_image', asset('storage/images/demo_akademija_logo_hd.jpg'))
@section('og_type', 'article')
@section('og_locale', 'hr_HR')

@section('content')
    <!-- BANNER -->
     <x-banner-component :request-path="Request::path()" 
        :name="__('Voditelji kolegija i predavači')" 
        :extra-breadcrumb="route('demo-academy', app()->getLocale())" 
        extra-breadcrumb-name="Demo akademija"
    />

    <!-- demo academy -->
    <div class="section demo-academy-instructors">
        <div class="content-wrap">
            <div class="container">
                <div class="row">
                    <!--  -->
                    <div class="col-sm-12">
                        <!--  -->
                        <div class="author-box mt-0">
                            <div class="media">
                                <img src="{{ asset('storage/images/demo_academy_instructors/berto_salaj.jpg') }}" alt="Dr. sc. Berto Šalaj" class="img-fluid">
                            </div>
                            <div class="body">
                                <h4 class="mt-0">Dr. sc. Berto Šalaj</h4>
                                <p>
                                    je izvanredni profesor na Fakultetu političkih znanosti Sveučilišta u Zagrebu gdje izvodi nastavu na kolegijima Demokracija i civilno društvo i Politička socijalizacija i političko obrazovanje. Znanstveni i istraživački interesi Berta Šalaja usmjereni su prema pitanjima demokracije, populizma, političke kulture, socijalnog kapitala, civilnog društva i političkog obrazovanja, o čemu je objavio više studija i članaka, od kojih se mogu izdvojiti knjiga Socijalni kapital (2007) i članak Suvremeni populizam (2012). Predstojnik je Centra za cjeloživotno obrazovanje Fakulteta političkih znanosti. Svoj znanstveno-istraživački rad povezuje s angažmanom u organizacijama civilnog društva, pa je od 2011. godine član Vijeća GONG-a.
                                </p>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                        
                        <div class="author-box">
                            <div class="media">
                                <img src="{{ asset('storage/images/demo_academy_instructors/srdjan_dvornik.jpg') }}" alt="Dr. sc. Berto Šalaj" class="img-fluid">
                            </div>
                            <div class="body">
                                <h4 class="mt-0">Mr. sc. Srđan Dvornik (1953.)</h4>
                                <p>
                                    neovisni istraživač, konzultant, publicist i prevoditelj iz Zagreba. Diplomirao filozofiju i sociologiju, magistrirao politologiju. Djeluje u istraživačkim i obrazovnim projektima o ljudskim pravima, civilnom društvu i demokratskim inicijativama. Autor je knjige Akteri bez društva, o ulozi civilnih aktera kao 'agenata promjena' u postkomunističkoj transformaciji u Hrvatskoj, Bosni i Hercegovini i Srbiji. Objavljuje političke komentare i analize za medije. Uredio i preveo dvadesetak knjiga te više publikacija i članaka. Aktivno sudjelovao u organizacijama kao što su Antiratna kampanja, Građanska inicijativa za slobodu javne riječi, Građanski odbor za ljudska prava i drugima. Radio u obrazovanju, izdavaštvu, zakladništvu (Open Society Institute, Heinrich Böll Stiftung) i zaštiti ljudskih prava. Član je Programskog vijeća Zelene akademije 2020, koju vodi Heinrich Böll Stiftung u Hrvatskoj.
                                </p>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                        
                        <div class="author-box">
                            <div class="media">
                                <img src="{{ asset('storage/images/demo_academy_instructors/marko_kovacic.jpg') }}" alt="Dr. sc. Berto Šalaj" class="img-fluid">
                            </div>
                            <div class="body">
                                <h4 class="mt-0">Marko Kovačić</h4>
                                <p>
                                    politolog, zaposlen je u Institutu za društvena istraživanja u Zagrebu gdje se bavi istraživanjima iz područja političke sociologije i javnih politika, prvenstveno mladih. Osim različitih istraživačkih projekata vezanih za participaciju mladih, njihovu građansku kompetenciju i rad s mladima, sudjelovao je u projektima koji se bave društvenom kohezijom i solidarnošću. Studirao je i radio na sveučilištima u Hrvatskoj, Mađarskoj, Sloveniji, Španjolskoj i SAD-u. Suradnik je brojnih organizacija civilnog društva u Hrvatskoj i inozemstvu vezano uz područje neformalnog obrazovanja. Ekspert je i nacionalni korespondent Vijeća Europe i Europske komisije za politike za mlade.
                                </p>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                        
                        <div class="author-box">
                            <div class="media">
                                <img src="{{ asset('storage/images/demo_academy_instructors/emina_buzinkic.jpg') }}" alt="Dr. sc. Berto Šalaj" class="img-fluid">
                            </div>
                            <div class="body">
                                <h4 class="mt-0">Emina Bužinkić</h4>
                                <p>
                                    po vokaciji je politologinja s dodatnim pedagoškim i psihološkim obrazovanjem, a po životnom izboru aktivistkinja. Rođena 1984. godine u Sisku u kojemu je provela većinu ratnog i postratnog odrastanja, a dio djetinjstva provela je u izbjeglištvu u Ljubljani i Zagrebu. Od srednje škole aktivna u kazališnim i političkim civilnim inicijativama poput Dosta je ratova i Ne u moje ime! Vise od 15 godina provela je u Centru za mirovne studije, Mreži mladih Hrvatske i Documenti – Centru za suočavanje s prošlošću povezujući rad na izgradnji mira i suočavanju s prošlošću s direktnim radom s društveno isključenim skupinama. Bavi se političkim obrazovanjem, malim koracima u ekonomskim alternativama i kulinarskim aktivizmom. Trenutno pohađa doktorski studij u polju kritičkog obrazovanja na Sveučilistu u Minnesoti na kojemu predaje kolegije u području liderstva, globalizacije i obrazovanja. Životnu inspiraciju crpi u odlascima u nepoznate joj krajeve, susretima s ljudima i vlastitim spiritualnim putovanjima. Vjeruje u promjene, ne vjeruje u granice.
                                </p>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                        
                        <div class="author-box">
                            <div class="media">
                                <img src="{{ asset('storage/images/demo_academy_instructors/teo_petricevic.jpg') }}" alt="Dr. sc. Berto Šalaj" class="img-fluid">
                            </div>
                            <div class="body">
                                <h4 class="mt-0">Teo Petričević</h4>
                                <p>
                                    je društveni poduzetnik i inovator, nezavisni savjetnik i trener. Osnivač je i inicijator dvadesetak društveno poduzetničkih organizacija u Hrvatskoj koje djeluju u području zelene ekonomije i pružanja socijalnih usluga u lokalnim zajednicama. Direktor je ACT Grupe, prvog konzorcija društvenih poduzeća u Hrvatskoj. Stručnjak je na području društvenog poduzetništva s više od 15 godina iskustva u jačanju kapaciteta organizacija civilnog društva i društvenih poduzetnika i više od 10 godina iskustva u razvoju društvenog poduzetništva u lokalnim zajednicama. Posjeduje zavidnu razinu ekspertize u području zakonodavstva vezanog uz radnu integraciju, zadrugarstvo i društveno poduzetništvo u Hrvatskoj i zemljama regije, te je aktivan u zagovaračkim procesima na nacionalnoj i EU razini.
                                </p>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                        
                        <div class="author-box">
                            <div class="media">
                                <img src="{{ asset('storage/images/demo_academy_instructors/senada_selo_sabic.jpg') }}" alt="Dr. sc. Berto Šalaj" class="img-fluid">
                            </div>
                            <div class="body">
                                <h4 class="mt-0">Dr. sc. Senada Šelo Šabić</h4>
                                <p>
                                    radi kao znanstvena suradnica na Institutu za razvoj i međunarodne odnose u  odjelu za međunarodne ekonomske i političke odnose. Njezini istraživački interesi uključuju hrvatsku vanjsku politiku, Jugoistočnu Europu, proširenje Europske unije, međunarodnu razvojnu suradnju i migracije. Senada ima doktorat političkih nauka s Europskog sveučilišnog instituta u Firenci  (2003.)  i dva magisterija – iz međunarodnih odnosa Sveučilišta u Zagrebu (1999.) i mirovnih studija Sveučilišta Notre Dame u SAD-u (1996.). Predaje na poslijediplomskom studiju na Sveučilištu u Zagrebu i glavna je urednica znanstvenog časopisa Croatian International Relations Review   (CIRR) koji pokriva teme iz područja društvenih znanosti s fokusom na politologiju, sociologiju, pravo i ekonomiju.
                                </p>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                        
                        <div class="author-box">
                            <div class="media">
                                <img src="{{ asset('storage/images/demo_academy_instructors/nives_ivelja.jpg') }}" alt="Dr. sc. Berto Šalaj" class="img-fluid">
                            </div>
                            <div class="body">
                                <h4 class="mt-0">Nives Ivelja (1961.)</h4>
                                <p>
                                    diplomirana psihologinja, dugogodišnja direktorica udruge „MI“ – Split i  konzultantica u području socijalnih usluga, razvoja zajednice i djelovanja organizacija civilnog društva. Identificira se s ulogom poduzetnice u neprofitnom civilnom sektoru i stručnjakinjom u području psiho-socijalnih intervencija u zajednici. Školovala se u Splitu i Zadru i dodatno obrazovala za psihosocijalne intervencije i rad u zajednici u međunarodnim programima izobrazbe (Realitetna terapija, Procesno orijentirana psihologija, Sistemska obiteljska terapija, Bihevioralna terapija, rad s traumom, upravljanje projektima i organizacijama civilnog društva, društveno poduzetništvo). Angažirala se u društvenoj revitalizaciji ratom pogođenih područja i uključivanju građana u zajednicu putem volontiranja. Volontirala je u procesima uspostavljanja i djelovanja infrastrukture za djelovanje civilnog društva, volonterstva i međusektorske suradnje. Sudjelovala je u osnivanju i vođenju osam organizacija civilnoga društva. Vjeruje u važnost ulaganja u osobne, organizacijske kapacitete i suradnju.
                                </p>
                            </div>
                            <div class="clearfix"></div>
                        </div>

                    </div>

                    <!--  -->
                    <div class="col-sm-12 mt-6">
                        <blockquote>
                            <h4>Ostali predavači</h4>
                            <p>
                                U obrazovni program bit će uključeni predavači iz različitih sektora te stručnjaci i praktičari s višegodišnjim iskustvom u razvoju demokracije i civilnog društva.
                            </p>
                        </blockquote>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- OUR PATRONS -->
    <x-our-patrons-component />
@endsection