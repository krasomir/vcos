@extends('layouts.head')
@section('title', $program->title)
@section('meta-description', __('Demo akademija, jednogodišnji neformalni program obrazovanja, podržava razvoj znanja, jačanje vještina i oblikovanje stavova i ponašanja koji su osnova društvenog i političkog angažmana građana te su važni za realizaciju ljudskih i građanskih prava i odgovornosti.'))
@section('meta-keywords', __('Demokracija, Civilno društvo, Ljudska prava, Socijalna kohezija, Aktivni i odgovorni građani, Kultura mira, Održivi razvoj, Europska unija'))

@section('og_url', config('app.url') . '/' . app()->getLocale() . '/demo-academy')
@section('og_title', $program->title)
@section('og_description', __('Demo akademija, jednogodišnji neformalni program obrazovanja, podržava razvoj znanja, jačanje vještina i oblikovanje stavova i ponašanja koji su osnova društvenog i političkog angažmana građana te su važni za realizaciju ljudskih i građanskih prava i odgovornosti.'))
@section('og_image', asset('storage/images/demo_akademija_logo_hd.jpg'))
@section('og_type', 'article')
@section('og_locale', 'hr_HR')

@section('content')
    <!-- BANNER -->
     <x-banner-component :request-path="Request::path()" 
        :name="$program->title" 
    />

    <!-- demo academy -->
    <div class="section">
        <div class="content-wrap">
            <div class="container">
                <div class="row">
                    <!--  -->
                    <div class="col-sm-12">
                        {!! $program->content !!}                        
                    </div>
                </div>
            </div>
        </div>
    </div>



    <!-- testimonials -->
    <div class="section demo-academy-wrapper mt-n5">
        <div class="content-wrap">
            <div class="container">
                <div class="row">
                    <div class="col-sm-6 col-md-6">
						<div class="testimonial-1 font-italic">
			              	<div class="body ml-2">
			                	<p>Dobiti mogućnost raditi s ovakvim predavačima, s ovakvim timom ljudi koji se bave doista time što je bitno - razvojem društva kakvo želimo -  je nešto neprocijenjivo.</p>
			                	<div class="title">Viktor Škoflek</div>
			              	</div>
			            </div>
                    </div>
                    <div class="col-sm-6 col-md-6">
						<div class="testimonial-1 font-italic">
			              	<div class="body ml-2">
			                	<p><span class="font-weight-bold">Na Demo akademiji</span> sam dobila mogućnost postati inicijator i nositelj dobrih promjena u svojoj zajednici.</p>
			                	<div class="title">Mirna Grbec</div>
			              	</div>
			            </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- demo academy publications -->
    @if($publications->count() > 0)
        <div class="section mt-n5">
            <div class="content-wrap">
                <div class="container">
                    <div class="row">
                        <!-- publications -->
                        <div class="col-sm-12 mb-3">
                            <h4>Publikacije</h4>
                        </div>
                        @foreach($publications as $publication)
                            <div class="col-sm-12 col-md-6 col-lg-4 mb-4">
                                <div class="publication-container">
                                    <a target="_blank" href="{{ asset('storage/documents/publications/' . $publication->publication_name) }}">
                                        @if((int)$publication->is_demo_academy === 1)
                                            <div class="publication-box" data-background="{{ asset('storage/images/publications/demo_academy/' . $publication->image_name) }}"></div>
                                        @else
                                            <div class="publication-box" data-background="{{ asset('storage/images/publications/' . $publication->image_name) }}"></div>
                                        @endif
                                    </a>
                                    <div class="body-content">
                                        <p class="title">
                                            <a target="_blank" href="{{ asset('storage/documents/publications/' . $publication->publication_name) }}">{{ $publication->title }}</a>
                                        </p>
                                    </div>
                                </div>
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    @endif

    <!-- movie - first generation of demo academy -->
    <div class="section mt-n5">
        <div class="content-wrap">
            <div class="container">
                <div class="row">
                    <div class="col-sm-12">
                        <h4>Film o prvoj generaciji polaznika "Demo akademije"</h4>
                        <!-- movie -->
                        <div class="video-container">
                            <iframe 
                                src="https://www.youtube.com/embed/1wDj9H2AMVg" 
                                frameborder="0" 
                                allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" 
                                allowfullscreen>
                            </iframe>
                        </div>
                        
                    </div>
                </div>
            </div>
        </div>
    </div>



    <!-- OUR PATRONS -->
    <x-our-patrons-component />
@endsection