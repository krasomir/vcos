@extends('layouts.head')
@section('title', __('Trebate volontere?'))
@section('meta-description', __('Volonterski centar usmjeren je prema viziji društva u kojem su građani motivirani činiti pozitivne promjene u svojim zajednicama'))
@section('meta-keywords', __('ciljevi, vizija, misija, vizija društva, motivacija'))

@section('content')
    <!-- BANNER -->
     <x-banner-component :request-path="Request::path()" 
        :name="__('Trebate volontere?')" 
    />

    <!-- need volunteer -->
    <div class="section">
        <div class="content-wrap">
            <div class="container mt-n2">
                <div class="row application-success-message d-none m-5">
                    <div class="col-sm-12">
                        <div class="alert alert-success text-center m-0">
                            <p class="m-0">Vaša potreba za volonterima je uspješno zaprimljena.</p>
                        </div>
                    </div>
                </div>
                <div class="row progressbar-container">
                    <ul class="progressbar m-0">
                        <li class="step active">Opći podaci</li>
                        <li class="step">
                            <span class="d-sm-none">Način prijave</span>
                            <span class="d-none d-sm-block">Način prijave zainteresiranih volontera</span>
                        </li>
                        <li class="step">
                            <span class="d-sm-none">Podaci o poziciji</span>
                            <span class="d-none d-sm-block">Podaci o volonterskoj poziciji</span>
                        </li>
                        <li class="step">
                            <span class="d-sm-none">Ostale info</span>
                            <span class="d-none d-sm-block">Ostale informacije</span>
                        </li>
                    </ul>
                </div>
                <div class="row application-wrapper">
                    <div class="col-sm-12">
                        <!-- form -->
                        <form method="POST" action="{{ route('save-need-volunteers', app()->getLocale()) }}" class="form-contact need-volunteers" id="needVolunteersForm" novalidate="true">
                            @csrf
                            <!-- form step one -->
                            <div id="step_1" class="step row mb-3 ">
                                <div class="col-sm-6 col-sm-6 col-md-6">
                                    <div class="form-group">
                                        <label for="organisation_type">{{ __('Vrsta organizacije') }}*</label>
                                        <select class="form-control" name="organisation_type" id="organisation_type" required>
                                            <option value=""></option>
                                            <option value="udruga">udruga</option>
                                            <option value="zaklada">zaklada</option>
                                            <option value="ustanova">ustanova</option>
                                            <option value="jedinica lokalne samouprave">jedinica lokalne samouprave</option>
                                            <option value="jedinica regionalne samouprave">jedinica regionalne samouprave</option>
                                            <option value="tijelo državne uprave">tijelo državne uprave</option>
                                        </select>
                                        <div class="help-block with-errors"></div>
                                    </div>
                                </div>
                                <div class="col-sm-6 col-md-6">
                                    <div class="form-group">
                                        <label for="organisation_name">{{ __('Naziv organizacije') }}*</label>
                                        <input type="text" class="form-control" name="organisation_name" id="organisation_name" required>
                                        <div class="help-block with-errors"></div>
                                    </div>
                                </div>
                                <div class="col-sm-6 col-md-6">
                                    <div class="form-group">
                                        <label for="address">{{ __('Sjedište organizacije (ulica i kućni broj)') }}*</label>
                                        <input type="text" class="form-control" name="address" id="address" required>
                                        <div class="help-block with-errors"></div>
                                    </div>
                                </div>
                                <div class="col-sm-6 col-md-6">
                                    <div class="form-group">
                                        <label for="city">{{ __('Mjesto') }}*</label>
                                        <input type="text" class="form-control" name="city" id="city" required>
                                        <div class="help-block with-errors"></div>
                                    </div>
                                </div>
                                <div class="col-sm-6 col-md-6">
                                    <div class="form-group">
                                        <label for="telephone">{{ __('Telefon') }}*</label>
                                        <input type="tel" class="form-control" name="telephone" id="telephone" required>
                                        <div class="help-block with-errors"></div>
                                    </div>
                                </div>
                            </div>
                            <!-- form step two -->
                            <div id="step_2" class="step row mb-3 d-none">
                                <div class="col-sm-12">
                                    <div class="alert alert-info">(Ukoliko navedete osobne podatke, koristit ćemo ih kako bismo potencijalne volontere informirali o načinu prijave te će oni biti javno dostupni na našim internetskim stranicama te društvenim mrežama. Ne preuzimamo odgovornost za dijeljenje osobnih podataka od strane trećih osoba niti za njihovu zloupotrebu.)</div>
                                </div>
                                <div class="col-sm-12 col-md-12">
                                    <div class="form-group">
                                        <label for="contact_email">{{ __('Kontakt e-mail') }}</label>
                                        <input type="email" class="form-control" name="contact_email" id="contact_email">
                                        <div class="help-block with-errors"></div>
                                    </div>
                                </div>
                                <div class="col-sm-6 col-md-6">
                                    <div class="form-group">
                                        <label for="contact_telephone">{{ __('Kontakt telefon') }}</label>
                                        <input type="tel" class="form-control" name="contact_telephone" id="contact_telephone">
                                        <div class="help-block with-errors"></div>
                                    </div>
                                </div>
                                <div class="col-sm-6 col-md-6">
                                    <div class="form-group">
                                        <label for="contact_person">{{ __('Kontakt osoba') }}</label>
                                        <input type="text" class="form-control" name="contact_person" id="contact_person">
                                        <div class="help-block with-errors"></div>
                                    </div>
                                </div>
                            </div>
                            <!-- form step three -->
                            <div id="step_3" class="step row mb-3 d-none">
                                <div class="col-sm-12 col-md-6">
                                    <div class="form-group">
                                        <label for="volunteer_engagement">{{ __('Volontere trebate') }}*</label>
                                        <select name="volunteer_engagement" id="volunteer_engagement" class="form-control" required>
                                            <option value=""></option>
                                            <option value="povremeni">{{ __('jednokratno') }}</option>
                                            <option value="u kontinuetu manje od 3 mjeseca">{{ __('u kontinuetu manje od 3 mjeseca') }}</option>
                                            <option value="u kontinuetu duže od 3 mjeseca">{{ __('u kontinuetu duže od 3 mjeseca') }}</option>
                                        </select>
                                        <div class="help-block with-errors"></div>
                                    </div>
                                </div>
                                <div class="col-sm-12 col-md-6">
                                    <div class="form-group">
                                        <label for="volunteer_number">{{ __('Broj potrebnih volontera') }}*</label>
                                        <input type="number" name="volunteer_number" id="volunteer_number" placeholder="" class="form-control" required>
                                        <div class="help-block with-errors"></div>
                                    </div>
                                </div>
                                <div class="col-sm-12 col-md-6">
                                    <div class="form-group">
                                        <label for="position_name">{{ __('Naziv volonterske pozicije') }}*</label>
                                        <input type="text" name="position_name" id="position_name" placeholder="" class="form-control" required>
                                        <div class="help-block with-errors"></div>
                                    </div>
                                </div>
                                <div class="col-sm-12 col-md-6">
                                    <div class="form-group">
                                        <label for="purpose_of_position">{{ __('Svrha volonterske pozicije') }}*</label>
                                        <input type="text" name="purpose_of_position" id="purpose_of_position" placeholder="" class="form-control" required>
                                        <div class="help-block with-errors"></div>
                                    </div>
                                </div>
                                <div class="col-sm-12 col-md-6">
                                    <div class="form-group">
                                        <label for="tasks_description">{{ __('Opis zadatka koje bi volonter obavljao') }}*</label>
                                        <input type="text" name="tasks_description" id="tasks_description" placeholder="" class="form-control" required>
                                        <div class="help-block with-errors"></div>
                                    </div>
                                </div>
                                <div class="col-sm-12 col-md-6">
                                    <div class="form-group">
                                        <label for="volunteer_qualifications">{{ __('Koje kvalifikacije, vještine očekujete od volontera') }}*</label>
                                        <input type="text" name="volunteer_qualifications" id="volunteer_qualifications" placeholder="" class="form-control" required>
                                        <div class="help-block with-errors"></div>
                                    </div>
                                </div>
                                <div class="col-sm-12 col-md-6">                               
                                    <div class="row">
                                        <div class="col">
                                            <div class="form-group">
                                                <label for="volunteer_period_from">{{ __('Razdoblje volontiranja - od') }}*</label>
                                                <input type="text" name="volunteer_period_from" id="volunteer_period_from" placeholder="" class="col-xs-6 form-control" required>
                                                <div class="help-block with-errors"></div>
                                            </div>
                                        </div>
                                        <div class="col">
                                            <div class="form-group">
                                                <label for="volunteer_period_to">{{ __('Razdoblje volontiranja - do') }}*</label>
                                                <input type="text" name="volunteer_period_to" id="volunteer_period_to" placeholder="" class="col-xs-6 form-control" required>
                                                <div class="help-block with-errors"></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-12 col-md-6">
                                    <div class="form-group">
                                        <label for="hour_number">{{ __('Broj sati') }}*</label>
                                        <input type="number" name="hour_number" id="hour_number" placeholder="" class="form-control" required>
                                        <div class="help-block with-errors"></div>
                                    </div>
                                </div>
                                <div class="col-sm-12 col-md-6">
                                    <div class="form-group">
                                        <label for="volunteering_place">{{ __('Mjesto volontiranja (pobliže odredite prostor/lokaciju)') }}*</label>
                                        <input type="text" name="volunteering_place" id="volunteering_place" placeholder="" class="form-control" required>
                                        <div class="help-block with-errors"></div>
                                    </div>
                                </div>
                                <div class="col-sm-12 col-md-6">
                                    <div class="form-group">
                                        <label for="application_deadline">{{ __('Rok za prijavu') }}*</label>
                                        <input type="text" name="application_deadline" id="application_deadline" placeholder="" class="form-control" required>
                                        <div class="help-block with-errors"></div>
                                    </div>
                                </div>
                                <div class="col-sm-12 col-md-6">
                                    <label class="mb-3">{{ __('Može li organizacija pružiti specifično dodatno obrazovanje (poduku) za obavljanje volonterskog angažmana') }}*</label>
                                    <div id="additional_education" class="form-group row mb-0 text-center">
                                        <div class="checkboxes col-6">
                                            <input type="radio" name="additional_education" id="additional_education_1" value="1">
                                            <label for="additional_education_1">{{ __('Da') }}</label>
                                        </div>
                                        <div class="checkboxes col-6">
                                            <input type="radio" name="additional_education" id="additional_education_3" value="0">
                                            <label for="additional_education_3">{{ __('Ne') }}</label>
                                        </div>
                                    </div>
                                    <div class="help-block with-errors"></div>
                                </div>
                                <div class="col-sm-12 col-md-6">
                                    <label class="mb-3">{{ __('Može li organizacija volonteru osigurati pokrivanje određenih troškova ako je potrebno') }}?*</label>
                                    <div id="expense_refund" class="form-group row mb-0">
                                        <div class="checkboxes col-xs-12 col-sm-6">
                                            <input type="checkbox" name="expense_refund[]" id="expense_refund_1" value="1">
                                            <label for="expense_refund_1">{{ __('Troškovi prijevoza') }}</label>
                                        </div>
                                        <div class="checkboxes col-xs-12 col-sm-6">
                                            <input type="checkbox" name="expense_refund[]" id="expense_refund_2" value="2">
                                            <label for="expense_refund_2">{{ __('Prehrana') }}</label>
                                        </div>
                                        <div class="checkboxes col-xs-12 col-sm-6">
                                            <input type="checkbox" name="expense_refund[]" id="expense_refund_3" value="3">
                                            <label for="expense_refund_3">{{ __('Smještaj') }}</label>
                                        </div>
                                        <div class="checkboxes col-xs-12 col-sm-6">
                                            <input type="checkbox" name="expense_refund[]" id="expense_refund_0" value="0">
                                            <label for="expense_refund_0">{{ __('Nismo u mogućnosti pokriti navedene troškove') }}</label>
                                        </div>
                                    </div>
                                    <div class="help-block with-errors"></div>
                                </div>
                            </div>
                            <!-- form step four -->
                            <div id="step_4" class="step row mb-3 d-none">
                                <div class="col-sm-12">
                                    <label class="mb-3">{{ __('Jesu li članovi vaše organizacije završili edukaciju za koordinatora volontera (menadžment volontera)?') }}*</label>
                                    <div id="volunteer_menagement" class="form-group row mb-0">
                                        <div class="checkboxes col-xs-6 col-sm-4">
                                            <input type="radio" name="volunteer_menagement" id="volunteer_menagement_1" value="1" data-linked-element="#leadership_training_1">
                                            <label for="volunteer_menagement_1">{{ __('Da') }}</label>
                                        </div>
                                        <div class="checkboxes col-xs-6 col-sm-4">
                                            <input type="radio" name="volunteer_menagement" id="volunteer_menagement_0" value="0" data-linked-element="#leadership_training_1">
                                            <label for="volunteer_menagement_0">{{ __('Ne') }}</label>
                                        </div>
                                        <div class="checkboxes col-xs-12 col-sm-4">
                                            <input type="radio" name="volunteer_menagement" id="volunteer_menagement_2" value="2" data-linked-element="#leadership_training_1">
                                            <label for="volunteer_menagement_2">{{ __('Ne, ali smo zainteresirani za edukaciju') }}</label>
                                        </div>
                                    </div>
                                    <div class="help-block with-errors"></div>
                                </div>
                                <div id="leadershipTrainingContainer" class="col-sm-12 d-none">
                                    <label class="mb-3">{{ __('Jeste li zainteresirani za treninge upravljanju kvalitetnim volonterskim programima koje organizira Volonterski centar Osijek?') }}*</label>
                                    <div class="form-group row mb-0 text-center">
                                        <div class="checkboxes col-sm-6">
                                            <input type="radio" name="leadership_training" id="leadership_training_1" value="1">
                                            <label for="leadership_training_1">{{ __('Da') }}</label>
                                        </div>
                                        <div class="checkboxes col-sm-6">
                                            <input type="radio" name="leadership_training" id="leadership_training_0" value="0">
                                            <label for="leadership_training_0">{{ __('Ne') }}</label>
                                        </div>
                                    </div>
                                    <div class="help-block with-errors"></div>
                                </div>
                                <div class="col-sm-12">
                                    <label class="mb-3">{{ __('Jeste li zainteresirani redovito primati informacije putem info biltena DKolektiva - organizacije za društveni razvoj') }}?*</label>
                                    <div id="newsletter" class="form-group row mb-0">
                                        <div class="checkboxes col-sm-6">
                                            <input type="radio" name="newsletter" id="newsletter_1" value="1" data-linked-element="#newsletterAlert">
                                            <label for="newsletter_1">{{ __('Da (registrirajte se)') }}</label>
                                        </div>
                                        <div class="checkboxes col-sm-6">
                                            <input type="radio" name="newsletter" id="newsletter_0" value="0" data-linked-element="#newsletterAlert">
                                            <label for="newsletter_0">{{ __('Ne') }}</label>
                                        </div>
                                    </div>
                                    <div class="help-block with-errors"></div>
                                </div>
                            </div>
                            <!-- newsletter info -->
                            <div id="newsletterAlert" class="alert alert-info d-none">
                                <p class="font-weight-bold">{{ __('Napomena') }}!</p class="bold">
                                <p>{{ __('Odabirom "Da (registrirajte se)" nakon slanja obrasca bit ćete preusmjereni na obrazac za registraciju "Snaga sudjelovanja - newsletter"') }}</p>
                            </div>

                            <hr class="mb-2">
                            <div class="row">
                                <div class="col-12 mb-2">
                                    <div class="small">{{ __('Polja označena zvjezdicom (*) je potrebno ispuniti/odabrati') }}</div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-6 col-sm-6">
                                    <div class="form-group">
                                        <button id="btnPrev" type="button" data-step="-1" class="btn btn-light w-100 d-none">{{ __('Predhodni korak') }}</button>
                                    </div>
                                </div>
                                <div class="col-6 col-sm-6">
                                    <div class="form-group">
                                        <button id="btnNext" type="button" data-step="1" class="btn btn-primary w-100">{{ __('Sljedeći korak') }}</button>
                                        <button id="btnSubmit" type="submit" class="btn btn-primary w-100 d-none">{{ __('Spremi prijavu') }}</button>
                                    </div>
                                </div>
                            </div>                   
                        </form>
                    </div>
                </div>
                <div class="jumbotron jumbotron-fluid py-2 mt-2">
                    <div class="container">
                        <h3 class="">Izjava o povjerljivosti</h3>
                        <p>Popunjavanjem obrasca dajete svoju suglasnost da podatke koje ste dostavili, uključujući i osobne podatke, Volonterski centar Osijek može koristiti kako bi informirao potencijalne volontere o mogućnosti volontiranja u vašoj organizaciji. Podatci će biti javno dostupni na našim internetskim stranicama te društvenim mrežama. Ne preuzimamo odgovornost za dijeljenje osobnih podataka od strane trećih osoba niti za njihovu zloupotrebu.</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('js')
<script>
    $(document).ready(function() {

        // datepicker init
        $("#volunteer_period_from, #volunteer_period_to, #application_deadline").daterangepicker({
            singleDatePicker: true,
            autoUpdateInput: false,
            locale: {
                format: 'DD.MM.YYYY',
                daysOfWeek: ['Ne', 'Po', 'Ut', 'Sr', 'Če', 'Pe', 'Su'],
                monthNames: ['Siječanj', 'Veljača', 'Ožujak', 'Travanj', 'Svibanj', 'Lipanj', 'Srpanj', 'Kolovoz', 'Rujan', 'Listopad', 'Studeni', 'Prosinac'],
                firstDay: 1
            }
        });

        $("#volunteer_period_from, #volunteer_period_to, #application_deadline").on('apply.daterangepicker', function(ev, picker) {
            $(this).val(picker.startDate.format('DD.MM.YYYY'));
        });

        // validation step routes
        const VALIDATION_STEP_ROUTES = @json($validation_step_routes);

        let currentStep = 0;
        showStep(currentStep);

        // submit form
        $("#needVolunteersForm").on('submit', function(e) {
            e.preventDefault();
            let formData = $("#needVolunteersForm :input").serialize();
            let route = $(this).attr('action');
            $("#needVolunteersForm .error-border").removeClass('error-border');
            $("#needVolunteersForm .with-errors").empty();

            $.ajax({
                type:'POST',
                url: route,
                data: formData,
                cache: false,
                timeout: 10000,
                success: function(data){
                    if (data.errors === false) {
                        if (data.redirect) {
                            document.location.replace(data.redirect);
                        } else {
                            $('.application-wrapper, .progressbar-container').addClass('d-none');
                            $('.application-success-message').removeClass('d-none');
                        }
                    }
                },
                error: function(xhr, statusText, error){
                    $.each(xhr.responseJSON.errors, (_key, val) => {
                        $("#" + _key).addClass('error-border');
                        $("#" + _key + "+ .with-errors").text(val);
                    });
                    console.log("Error: " + xhr.status + ' ' + error);
                }
            });
        });
        
        // navigate to next form step
        $("#btnNext").on('click', function(e) {
            nexPrevStep(1);
        });

        // navigate to previous form step
        $("#btnPrev").on('click', function(e) {
            nexPrevStep(-1);
        });

        // toogle value of leadership training container
        $('input[name="volunteer_menagement"]').on('change', function(e) {
            let value = $(this).val();
            let linkedElement = $(this).data('linked-element');
            value == 2 ? $(linkedElement).prop('checked', true): $(linkedElement).prop('checked', false);

            if (value != 2) {                
                $('input[name="leadership_training"]').each(function(idx) {
                    $(this).prop('checked', false);
                });
            }
        });

        // toggle/validate oposite values
        $('input[name="expense_refund[]"]').on('change', function(e) {
            if ($(this).is(':checked')) {
                let value = $(this).val();
                
                if (value == 0) {
                    $('input[name="expense_refund[]"]').each(function(idx) {
                        if ($(this).val() != value) {
                            $(this).prop('checked', false);
                        }                        
                    });
                } else {
                    $('#expense_refund_0').prop('checked', false);
                }
            }                
        });

        // toogle newsletter alert
        $('input[name="newsletter"]').on('change', function(e) {
            toggle($(this).data('linked-element'), $(this).val());
        });

        // toggle show/hide element
        function toggle(element, value) {
            value == 1 ? $(element).removeClass('d-none'): $(element).addClass('d-none');
        }

        // display next or previous form step
        function nexPrevStep(value) {
            // get all steps in form
            let steps = $(".step");
            // validate step data
            if (value == 1 && !validated(currentStep, value)) {
                return false;
            }
            // hide current step
            $('form.need-volunteers').find('.step').eq(currentStep).addClass('d-none');
            // increase or decrease current step number by 1
            currentStep = currentStep + value;
            // display current step
            showStep(currentStep);
        }

        // display the specified form step
        function showStep(step) {
            
            let steps = $("form.need-volunteers .step");
            scrollToElement('.progressbar-container');

            $('form.need-volunteers').find('.step').addClass('d-none');
            $('form.need-volunteers').find('.step').eq(step).removeClass('d-none');
            // displaying of Previous/Next buttons
            if (step == 0) {
                $("#btnPrev").addClass('d-none');
            } else {
                $("#btnPrev").removeClass('d-none');
            }

            // toggle between next step and submit form button
            if (step == (steps.length - 1)) {
                $("#btnNext").addClass('d-none');
                $("#btnSubmit").removeClass('d-none');
            } else {
                $("#btnNext").removeClass('d-none');
                $("#btnSubmit").addClass('d-none');
            }
            // display correct step in progress bar
            fixStepIndicator(step);
        };

        // display correct step number in progress bar
        function fixStepIndicator(currentStep) {
            // get all steps
            let steps = $(".progressbar .step");
            steps.each(function(i, val) {
                if (i <= currentStep) {
                    // if step is left from current step & current step
                    $('.progressbar').find('.step').eq(i).addClass('active');
                } else {
                    // if step is right from current step
                    $('.progressbar').find('.step').eq(i).removeClass('active');
                }
            });
        }

        // validate all steps individualy
        function validated(step, value) {

            let stepToValidate = step + 1;
            let routeKey = 'step_' + stepToValidate;
            let route = VALIDATION_STEP_ROUTES[routeKey];
            let token = $("#needVolunteersForm input[name=_token]").val();
            let stepDataToValidate = $("#step_"+ stepToValidate +" :input, #needVolunteersForm input[name=_token]").serialize();
            $("#needVolunteersForm .error-border").removeClass('error-border');
            $("#needVolunteersForm .with-errors").empty();

            $.ajax({
                type:'POST',
                url: route,
                data: stepDataToValidate,
                cache: false,
                timeout: 10000,
                success: function(data){                    
                    // hide current step
                    $('form.need-volunteers').find('.step').eq(currentStep).addClass('d-none');
                    // increase or decrease current step number by 1
                    currentStep = currentStep + value;
                    // display current step
                    showStep(currentStep);
                },
                error: function(xhr, statusText, error){
                    $.each(xhr.responseJSON.errors, (_key, val) => {
                        
                        $("#" + _key).addClass('error-border');
                        $("#" + _key + "+ .with-errors").text(val);
                    });
                    console.log("Error: " + xhr.status + ' ' + error);
                }
            });
        }
        
    });
</script>
@endsection