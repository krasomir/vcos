@extends('layouts.head')
@section('title', __('Izvori financiranja'))
@section('meta-description', __('DKolektiv usmjeren je prema viziji društva u kojem su građani motivirani činiti pozitivne promjene u svojim zajednicama'))
@section('meta-keywords', __('Europska komisija, EACEA, EIDHR, IPA 2012, Interreg - IPA CBC, Lifelong Learning Programme, Erasmus+, ESF Operativni program, Europe for Citizens, British Council, UNICEF'))

@section('og_url', config('app.url') . '/' . app()->getLocale() . '/sources-of-funding')
@section('og_title', __('Izvori financiranja'))
@section('og_description', __('DKolektiv usmjeren je prema viziji društva u kojem su građani motivirani činiti pozitivne promjene u svojim zajednicama'))
@section('og_image', asset('storage/images/dkolektiv_logo_hd.jpg'))
@section('og_type', 'article')
@section('og_locale', 'hr_HR')

@section('content')
<!-- BANNER -->
 <x-banner-component :request-path="Request::path()" :name="__('Izvori financiranja')" />

<!-- sources of funding -->
<div class="section">
    <div class="content-wrap">
        <div class="container">
            <div class="row">
                <!--  -->
                <div class="col-sm-12">
                    <blockquote>
                        <ul>
                            <li><span class="font-weight-bold">Europska komisija</span></li>
                            <li>EACEA</li>
                            <li>EIDHR</li>
                            <li>IPA 2012</li>
                            <li>Interreg - IPA CBC</li>
                            <li>Lifelong Learning Programme</li>
                            <li>Erasmus+ KA1</li>
                            <li>Erasmus+ KA2</li>
                            <li>Erasmus+ KA3</li>
                            <li>ESF Operativni program</li>
                            <li>Europe for Citizens</li>
                            <li><span class="font-weight-bold">Academy for educational development-AED/USAID</span></li>
                            <li><span class="font-weight-bold">Veleposlanstvo kraljevine Norveške</span></li>
                            <li><span class="font-weight-bold">Veleposlanstvo Ujedinjene Kraljevine Velike Britanije i Sjeverne Irske</span></li>
                            <li><span class="font-weight-bold">British Council</span></li>
                            <li><span class="font-weight-bold">Veleposlanstvo Sjedinjenih Američkih država</span></li>
                            <li><span class="font-weight-bold">EEA Grants - Norway Grants</span></li>
                            <li><span class="font-weight-bold">Švicarske-hrvatski program suradnje</span></li>
                            <li><span class="font-weight-bold">Open Society Foundations</span></li>
                            <li><span class="font-weight-bold">UNICEF</span></li>
                            <li><span class="font-weight-bold">CARE International</span></li>
                            <li><span class="font-weight-bold">Nacionalna zaklada za razvoj civilnoga društva</span></li>
                            <li><span class="font-weight-bold">Ured Vlade Republike Hrvatske za udruge</span></li>
                            <li><span class="font-weight-bold">Ministarstvo za demografiju, obitelj, mlade i socijalnu politiku</span></li>
                            <li><span class="font-weight-bold">Ministarstvo znanosti, obrazovanja i sporta RH</span></li>
                            <li><span class="font-weight-bold">Ministarstvo zdravstva RH</span></li>
                            <li><span class="font-weight-bold">Ured za suzbijanje zlouporabe droga</span></li>
                            <li><span class="font-weight-bold">Grad Osijek</span></li>
                            <li><span class="font-weight-bold">Osječko-baranjska županija</span></li>
                        </ul>
                    </blockquote>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- OUR PATRONS -->
<x-our-patrons-component />
@endsection