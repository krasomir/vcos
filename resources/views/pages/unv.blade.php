@extends('layouts.head')
@section('title', $program->title)
@section('meta-description', __('Potencijal volontiranja prepoznat je u aktivnom građanskom sudjelovanju, društvenoj uključenosti, kvaliteti međuljudskih odnosa, stvaranju otpornih i prilagodljivih zajednica i izgradnji društvenog kapitala.'))
@section('meta-keywords', __('volonterska nagrada, dobra praksa, doprinos razvoju volonterstva, angažman pojedinca'))

@section('og_url', config('app.url') . '/' . app()->getLocale() . '/uciniti-nevidljivo-vidljivim')
@section('og_title', $program->title)
@section('og_description', __('Potencijal volontiranja prepoznat je u aktivnom građanskom sudjelovanju, društvenoj uključenosti, kvaliteti međuljudskih odnosa, stvaranju otpornih i prilagodljivih zajednica i izgradnji društvenog kapitala.'))
@section('og_image', asset('storage/images/volonterski_centar_osijek_logo_hd.jpg'))
@section('og_type', 'article')
@section('og_locale', 'hr_HR')

@section('content')
<!-- BANNER -->
 <x-banner-component :request-path="Request::path()" 
    :name="$program->title" 
    :extra-breadcrumb="route('vcos', app()->getLocale())" 
    extra-breadcrumb-name="Volonterski centar Osijek"
/>

<!--  -->
<div class="section">
    <div class="content-wrap">
        <div class="container">
            <div class="row">

                <!-- unv -->
                <div class="col-sm-12 col-md-12">
                    {!! $program->content !!}                    
                </div>

                <!-- link to document -->
                @if($program->link_to_pdf)
                <div class="col-sm-12 col-md-12">
                    <blockquote>
                        <div class="rs-box-download">
                            <div class="icon">
                                <i class="fa fa-file-pdf-o" aria-hidden="true"></i>
                            </div>
                            <div class="body">
                                <a href="{{ $program->link_to_pdf }}" target="_blank">
                                    <h4>{{ $program->link_title }}</h4>
                                    <span class="small">klik za preuzimanje dokumenta</span>
                                </a>
                            </div>
                        </div>
                    </blockquote>
                </div>
                @endif
            </div>
        </div>
    </div>
</div>
@endsection