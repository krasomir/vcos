@extends('layouts.head')
@section('title', $program->title)
@section('meta-description', __('Društveni atelje jedan je od glavnih strateških razvojnih pravaca DKolektiva usmjeren na razvoj uključivog društva'))
@section('meta-keywords', __('društvena otuđenost, poticanje društvene integracije građana, pojedinih zajednica, osnaživanje tih grupa'))

@section('og_url', config('app.url') . '/' . app()->getLocale() . '/social-atelier')
@section('og_title', $program->title)
@section('og_description', __('Društveni atelje jedan je od glavnih strateških razvojnih pravaca DKolektiva usmjeren na razvoj uključivog društva'))
@section('og_image', asset('storage/images/drustveni_atelje_logo_hd.jpg'))
@section('og_type', 'article')
@section('og_locale', 'hr_HR')

@section('content')
    <!-- BANNER -->
     <x-banner-component :request-path="Request::path()" 
        :name="$program->title" 
    />

    <!-- social atelier -->
    <div class="section">
        <div class="content-wrap">
            <div class="container">
                <div class="row">
                    <!--  -->
                    <div class="col-sm-12">
                        {!! $program->content !!}                        
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- OUR PATRONS -->
    <x-our-patrons-component />
@endsection