@extends('layouts.head')
@section('title', __('Misija, Vizija, Ciljevi'))
@section('meta-description', __('Biti inspiracija i podrška demokratskoj kulturi, razvoju civilnog društva i volonterstva, stvaranju prilika za jednake mogućnosti i aktivno sudjelovanje građana'))
@section('meta-keywords', __('ciljevi, vizija, misija, vizija društva, motivacija'))

@section('og_url', config('app.url') . '/' . app()->getLocale() . '/mision-vision-goals')
@section('og_title', __('Misija, Vizija, Ciljevi'))
@section('og_description', __('Biti inspiracija i podrška demokratskoj kulturi, razvoju civilnog društva i volonterstva, stvaranju prilika za jednake mogućnosti i aktivno sudjelovanje građana'))
@section('og_image', asset('storage/images/d_kolektiv_logo_hd.jpg'))
@section('og_type', 'article')
@section('og_locale', 'hr_HR')

@section('content')

<!-- BANNER -->
 <x-banner-component :request-path="Request::path()" :name="__('Misija, Vizija, Ciljevi')" />

<!-- HOW TO HELP US -->
<div class="section">
	<div class="content-wrap">
		<div class="container">

			<div class="row">

				<!-- mission -->
				<div class="col-sm-6 col-md-6">
					<blockquote>
						<div class="body-content">
							<p class="title">{{ __('MISIJA') }}</p>
							<div class="text">Biti inspiracija i podrška demokratskoj kulturi, razvoju civilnog društva i volonterstva, stvaranju prilika za jednake mogućnosti i aktivno sudjelovanje građana.</div>
						</div>
					</blockquote>
				</div>
				<!-- vision -->
				<div class="col-sm-6 col-md-6">
					<blockquote>
						<div class="body-content">
							<p class="title">{{ __('VIZIJA') }}</p>
							<div class="text">Humano, otvoreno, solidarno, održivo i uključivo društvo.</div>
						</div>
					</blockquote>
				</div>
				<!-- goals -->
				<div class="col-sm-12 col-md-12">
					<blockquote>
						<div class="body-content">
							<div class="text">
								<span class="vision-accent">DKolektiv – organizacija za društveni razvoj</span> razvila se na postignućima 15-godišnjeg djelovanja Volonterskog centra Osijek u području razvoja volonterstva, civilnog društva i demokratske kulture.
								DKolektiv počiva na načelima solidarnosti humanosti, uvažavanja različitosti, nenasilja, tolerancije, razumijevanja, odgovornosti i suradnje. Organizacija smo koja je društveno osjetljiva, koja njeguje dijalog i suradnju, štiti ljudsko dostojanstvo i ljudska prava, doprinosi solidarnosti i društvenoj koheziji, aktivno sluša i argumentirano zagovara održivi razvoj, humano, otvoreno i demokratsko društvo.
							</div>
							<div class="text mt-2">
								Svojim djelovanjem želimo doprinijeti razvoju demokratske kulture, aktivnom građanstvu, razvoju volonterstva, jakom i progresivnom civilnom društvu, dobrom upravljanju, uključivom društvu i boljem položaju manjinskih, ranjivih i marginaliziranih skupina. DKolektiv karakterizira neovisnost djelovanja i otvorenog istupanja, razmjena znanja i kapaciteta, otvorenost prema različitim mišljenjima i stavovima te dijeljenje spoznaja i ekspertiza u području djelovanja. DKolektiv surađuje s organizacijama civilnog društva, inicijativama, građanima, javnim institucijama, javnim i privatnim  ustanovama, medijima, gospodarstvom kao i europskim i međunarodnim organizacijama.
							</div>
							<div class="text mt-2">
								Posebnu pažnju posvećujemo kreiranju ciljanih i održivih intervencija kroz projekte i programe koji se provode u suradnji s drugim organizacijama i djeluju sinergijski. Nastojimo biti inovativni u svom djelovanju, promovirati pozitivne promjene, kreirati prilike za učenje i razvoj, njegovati dijalog te stvarati veze među ljudima, organizacijama i zajednicama.
							</div>
						</div>
					</blockquote>
				</div>
			</div>

		</div>
	</div>
</div>

<!-- OUR PATRONS -->
<x-our-patrons-component />
@endsection