@extends('layouts.head')
@section('title', $program->title)
@section('meta-description', __('DKolektiv - organizacija za društveni razvoj'))
@section('meta-keywords', __('DKolektiv - organizacija za društveni razvoj'))

@section('og_url', config('app.url') . '/' . app()->getLocale() . '/korisni-dokumenti')
@section('og_title', $program->title)
@section('og_description', __('DKolektiv - organizacija za društveni razvoj'))
@section('og_image', asset('storage/images/volonterski_centar_osijek_logo_hd.jpg'))
@section('og_type', 'article')
@section('og_locale', 'hr_HR')

@section('content')
<!-- BANNER -->
 <x-banner-component :request-path="Request::path()" 
    :name="$program->title" 
    :extra-breadcrumb="route('vcos', app()->getLocale())" 
    extra-breadcrumb-name="Volonterski centar Osijek"
/>

<!--  -->
<div class="section">
    <div class="content-wrap">
        <div class="container">
            <div class="row">
                <!-- kd -->
                <div class="col-sm-12 col-md-12">
                    <!-- new -->
                    {!! $program->content !!}
                </div>
                
            </div>
        </div>
    </div>
</div>
@endsection