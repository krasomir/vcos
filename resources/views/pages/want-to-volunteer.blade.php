@extends('layouts.head')
@section('title', __('Želim volontirati'))
@section('meta-description', __('Volonterski centar usmjeren je prema viziji društva u kojem su građani motivirani činiti pozitivne promjene u svojim zajednicama'))
@section('meta-keywords', __('ciljevi, vizija, misija, vizija društva, motivacija'))

@section('content')
    <!-- BANNER -->
     <x-banner-component :request-path="Request::path()" 
        :name="__('Želim volontirati')" 
    />

    <!-- want to volunteer -->
    <div class="section">
        <div class="content-wrap">
            <div class="container mt-n2">
                <div class="row application-success-message d-none m-5">
                    <div class="col-sm-12">
                        <div class="alert alert-success text-center m-0">
                            <p class="m-0">Vaša prijava za volontiranjem je uspješno zaprimljena.</p>
                        </div>
                    </div>
                </div>
                <div class="row progressbar-container">
                    <ul class="progressbar m-0">
                        <li class="step active">Osobni podaci</li>
                        <li class="step">Vještine i interesi</li>
                        <li class="step">Područje interesa</li>
                        <li class="step">Dostupnost</li>
                    </ul>
                </div>
                <div class="row application-wrapper">
                    <div class="col-sm-12">
                        <!-- form -->
                        <form method="POST" action="{{ route('save-volunteer-application', app()->getLocale()) }}" class="form-contact want-to-volunteer" id="wantToVolunteerForm" novalidate="true">
                            @csrf
                            <!-- form step one -->
                            <div id="step_1" class="step row mb-3 d-none">
                                <div class="col-sm-6 col-sm-6 col-md-6">
                                    <div class="form-group">
                                        <label for="first_last_name">{{ __('Ime i prezime') }}*</label>
                                        <input type="text" class="form-control" name="first_last_name" id="first_last_name" required>
                                        <div class="help-block with-errors"></div>
                                    </div>
                                </div>
                                <div class="col-sm-6 col-md-6">
                                    <div class="form-group">
                                        <label for="birth_year">{{ __('Godina rođenja') }}*</label>
                                        <input type="text" class="form-control" name="birth_year" id="birth_year" required>
                                        <div class="help-block with-errors"></div>
                                    </div>
                                </div>
                                <div class="col-sm-6 col-md-6">
                                    <div class="form-group">
                                        <label for="city">{{ __('Mjesto stanovanja') }}*</label>
                                        <input type="text" class="form-control" name="city" id="city" required>
                                        <div class="help-block with-errors"></div>
                                    </div>
                                </div>
                                <div class="col-sm-6 col-md-6">
                                    <div class="form-group">
                                        <label for="telephone">{{ __('Telefon') }}</label>
                                        <input type="text" class="form-control" name="telephone" id="telephone">
                                        <div class="help-block with-errors"></div>
                                    </div>
                                </div>
                                <div class="col-sm-6 col-md-6">
                                    <div class="form-group">
                                        <label for="email">{{ __('E-mail') }}*</label>
                                        <input type="email" class="form-control" name="email" id="email" required>
                                        <div class="help-block with-errors"></div>
                                    </div>
                                </div>
                                <div class="col-sm-6 col-md-6">
                                    <div class="form-group">
                                        <label for="gender">{{ __('Spol') }}*</label>
                                        <select name="gender" id="gender" class="form-control">
                                            <option value=""></option>
                                            <option value="1">{{ __('Muški') }}</option>
                                            <option value="2">{{ __('Ženski') }}</option>
                                            <option value="2">{{ __('Drugo') }}</option>
                                        </select>
                                        <div class="help-block with-errors"></div>
                                    </div>
                                </div>
                            </div>
                            <!-- form step two -->
                            <div id="step_2" class="step row mb-3 d-none">
                                <div class="col-sm-12 col-md-12">
                                    <div class="form-group">
                                        <label for="profession">{{ __('Zanimanje') }}*</label>
                                        <input type="text" class="form-control" name="profession" id="profession" required>
                                        <div class="help-block with-errors"></div>
                                    </div>
                                </div>
                                <div class="col-sm-6 col-md-6">
                                    <div class="form-group">
                                        <label for="additional_skills">{{ __('Dodatne vještine, interesi, hobiji') }}</label>
                                        <textarea class="form-control" name="additional_skills" id="additional_skills"></textarea>
                                        <div class="help-block with-errors"></div>
                                    </div>
                                </div>
                                <div class="col-sm-6 col-md-6">
                                    <div class="form-group">
                                        <label for="volunteer_experience">{{ __('Dosadašnje volontersko iskustvo') }}</label>
                                        <textarea class="form-control" name="volunteer_experience" id="volunteer_experience"></textarea>
                                        <div class="help-block with-errors"></div>
                                    </div>
                                </div>
                            </div>
                            <!-- form step three -->
                            <div id="step_3" class="step row mb-3 d-none">
                                <div class="col-sm-12 col-md-12">
                                    <div class="form-group">
                                        <label for="volunteer_engagement">{{ __('Vaš volonterski angažman može biti') }}*</label>
                                        <select name="volunteer_engagement" id="volunteer_engagement" class="form-control">
                                            <option value=""></option>
                                            <option value="povremeni">{{ __('povremeni') }}</option>
                                            <option value="u kontinuitetu manje od 3 mjeseca">{{ __('u kontinuitetu manje od 3 mjeseca') }}</option>
                                            <option value="u kontinuitetu duže od 3 mjeseca">{{ __('u kontinuitetu duže od 3 mjeseca') }}</option>
                                        </select>
                                        <div class="help-block with-errors"></div>
                                    </div>
                                </div>
                                <div class="col-sm-12 col-md-12">
                                    <label class="mb-3">{{ __('U kojim ste od navedenih područja zainteresirani volontirati? (Možete odabrati više područja.)') }}*</label>
                                    <div id="work_area" class="form-group row">
                                        <div class="checkboxes col-sm-3">
                                            <input type="checkbox" class="form-control" name="work_area[]" id="work_area_1" value="aktivnosti za djecu i mlade">
                                            <label for="work_area_1">aktivnosti za djecu i mlade</label>
                                        </div>
                                        <div class="checkboxes col-sm-3">
                                            <input type="checkbox" class="form-control" name="work_area[]" id="work_area_2" value="prevencija ovisnosti">
                                            <label for="work_area_2">prevencija ovisnosti</label>
                                        </div>
                                        <div class="checkboxes col-sm-3">
                                            <input type="checkbox" class="form-control" name="work_area[]" id="work_area_3" value="programi za osobe sa invaliditetom">
                                            <label for="work_area_3">programi za osobe sa invaliditetom</label>
                                        </div>
                                        <div class="checkboxes col-sm-3">
                                            <input type="checkbox" class="form-control" name="work_area[]" id="work_area_4" value="skrb o starijima">
                                            <label for="work_area_4">skrb o starijima</label>
                                        </div>
                                        <div class="checkboxes col-sm-3">
                                            <input type="checkbox" class="form-control" name="work_area[]" id="work_area_5" value="kultura i umjetnost">
                                            <label for="work_area_5">kultura i umjetnost</label>
                                        </div>
                                        <div class="checkboxes col-sm-3">
                                            <input type="checkbox" class="form-control" name="work_area[]" id="work_area_6" value="zaštita okoliša i održivi razvoj">
                                            <label for="work_area_6">zaštita okoliša i održivi razvoj</label>
                                        </div>
                                        <div class="checkboxes col-sm-3">
                                            <input type="checkbox" class="form-control" name="work_area[]" id="work_area_7" value="zaštita i zbrinjavanje životinja">
                                            <label for="work_area_7">zaštita i zbrinjavanje životinja</label>
                                        </div>
                                        <div class="checkboxes col-sm-3">
                                            <input type="checkbox" class="form-control" name="work_area[]" id="work_area_8" value="skrb o siromašnima">
                                            <label for="work_area_8">skrb o siromašnima</label>
                                        </div>
                                        <div class="checkboxes col-sm-3">
                                            <input type="checkbox" class="form-control" name="work_area[]" id="work_area_9" value="zaštita i briga o zdravlju">
                                            <label for="work_area_9">zaštita i briga o zdravlju</label>
                                        </div>
                                        <div class="checkboxes col-sm-3">
                                            <input type="checkbox" class="form-control" name="work_area[]" id="work_area_10" value="zaštita ljudskih prava">
                                            <label for="work_area_10">zaštita ljudskih prava</label>
                                        </div>
                                        <div class="checkboxes col-sm-3">
                                            <input type="checkbox" class="form-control" name="work_area[]" id="work_area_11" value="izgradnja mira i nenasilja">
                                            <label for="work_area_11">izgradnja mira i nenasilja</label>
                                        </div>
                                        <div class="checkboxes col-sm-3">
                                            <input type="checkbox" class="form-control" name="work_area[]" id="work_area_12" value="sport">
                                            <label for="work_area_12">sport</label>
                                        </div>
                                    </div>
                                    <div class="help-block with-errors"></div>
                                </div>
                            </div>
                            <!-- form step four -->
                            <div id="step_4" class="step row mb-3 d-none">
                                <div class="col-sm-12 col-md-12">
                                    <label class="mb-3">{{ __('Na kojem geografskom području želite volontirati? (Možete odabrati više područja).') }}*</label>
                                    <div class="form-group row mb-0">
                                        <div class="checkboxes col-sm-4">
                                            <input type="checkbox" name="geo_area[]" id="geo_area_1" value="Osijek i okolica">
                                            <label for="geo_area_1">{{ __('Osijek i okolica') }}</label>
                                        </div>
                                        <div class="checkboxes col-sm-4">
                                            <input type="checkbox" name="geo_area[]" id="geo_area_3" value="Hrvatska">
                                            <label for="geo_area_3">{{ __('Hrvatska') }}</label>
                                        </div>
                                        <div class="checkboxes col-sm-4">
                                            <input type="checkbox" name="geo_area[]" id="geo_area_4" value="Inozemstvo">
                                            <label for="geo_area_4">{{ __('Inozemstvo') }}</label>
                                        </div>
                                    </div>
                                    <div id="geo_area" class="form-group row mb-0 d-none">
                                        <div class="checkboxes col-sm-12">
                                            <label for="geo_area_2">{{ __('Slavonija i Baranja (pobliže odredite mjesto/grad)') }}</label>
                                            <input type="text" name="geo_area_sib" id="geo_area_sib" class="form-control">
                                        </div>
                                    </div>
                                    <div class="help-block with-errors"></div>
                                </div>
                                <div class="col-sm-12 col-md-12">
                                    <div class="form-group row">
                                        <div class="checkboxes col-sm-12">
                                            <label for="geo_area_2">{{ __('Navedite ukoliko postoje područja u kojima nikako ne biste volontirali ili nas želite upoznati s drugim ograničenjima za koje smatrate da bismo s njima trebali biti upoznati.') }}</label>
                                            <textarea name="exclude_area" id="exclude_area" class="form-control"></textarea>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-12 col-md-12">
                                    <label class="mb-3">{{ __('Kako ste saznali za Volonterski centar Osijek') }}*</label>
                                    <div id="source_info" class="form-group row mb-0">
                                        <div class="checkboxes col-sm-3">
                                            <input type="checkbox" name="source_info[]" id="source_info_1" value="vidjeli ste oglas ili promotivni materijal">
                                            <label for="source_info_1">{{ __('vidjeli ste oglas ili promotivni materijal') }}</label>
                                        </div>
                                        <div class="checkboxes col-sm-3">
                                            <input type="checkbox" name="source_info[]" id="source_info_3" value="od prijatelja ili volontera">
                                            <label for="source_info_3">{{ __('od prijatelja ili volontera') }}</label>
                                        </div>
                                        <div class="checkboxes col-sm-3">
                                            <input type="checkbox" name="source_info[]" id="source_info_4" value="od organizacije ili škole">
                                            <label for="source_info_4">{{ __('od organizacije ili škole') }}</label>
                                        </div>
                                        <div class="checkboxes col-sm-3">
                                            <input type="checkbox" name="source_info[]" id="source_info_5" value="putem interneta">
                                            <label for="source_info_5">{{ __('putem interneta') }}</label>
                                        </div>
                                        <div class="checkboxes col-sm-12">
                                            <label for="source_info_other">{{ __('Ostalo') }}</label>
                                            <input type="text" name="source_info_other" id="source_info_other" class="form-control">
                                        </div>
                                    </div>
                                    <div class="help-block with-errors"></div>
                                </div>
                                <div class="col-sm-12 col-md-12">
                                    <label class="mb-3">{{ __('Newsletter DKolektiva') }}</label>
                                    <div class="form-group row">
                                        <div class="checkboxes col-sm-12">
                                            <input type="checkbox" name="newsletter" id="newsletter" value="1">
                                            <label for="newsletter">{{ __('Da, želim primati newsletter DKolektiva') }}</label>
                                        </div>
                                    </div>
                                    <!-- newsletter info -->
                                    <div class="alert alert-info d-none">
                                        <p class="font-weight-bold">{{ __('Napomena') }}:</p class="bold">
                                        <p>{{ __('Poslat ćemo Vam e-mail s linkom za potvrdu Vaše adrese, molimo provjerite Vaš e-mail sandučić.') }}</p>
                                    </div>
                                </div>
                            </div>

                            <hr class="mb-2">
                            <div class="row">
                                <div class="col-12 mb-2">
                                    <div class="small">{{ __('Polja označena zvjezdicom (*) je potrebno ispuniti/odabrati') }}</div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-6 col-sm-6">
                                    <div class="form-group">
                                        <button id="btnPrev" type="button" data-step="-1" class="btn btn-light w-100 d-none">{{ __('Predhodni korak') }}</button>
                                    </div>
                                </div>
                                <div class="col-12 col-sm-6">
                                    <div class="form-group">
                                        <button id="btnNext" type="button" data-step="1" class="btn btn-primary w-100">{{ __('Sljedeći korak') }}</button>
                                        <button id="btnSubmit" type="submit" class="btn btn-primary w-100 d-none">{{ __('Spremi prijavu') }}</button>
                                    </div>
                                </div>
                            </div>                   
                        </form>
                    </div>
                </div>
                <div class="jumbotron jumbotron-fluid py-2 mt-2">
                    <div class="container">
                        <h3 class="">Izjava o povjerljivosti</h3>
                        <p>Popunjavanjem ovog obrasca dajete svoj pristanak DKolektivu - organizaciji za društveni razvoj za prikupljanje i obradu Vaših osobnih podataka. Vaše osobne podatke koristit ćemo u svrhu slanja informacija i obavijesti (na dnevnoj i tjednoj osnovi) koje zaprimamo od različitih dionika vezane uz volonterstvo i društveni razvoj kao što su informacije o mogućnostima volontiranja u zajednici i inozemstvu, otvorenim prilikama za sudjelovanjem u raznim projektima, pozivima na besplatne edukacije te druge relevantne informacije vezane uz volonterstvo.</p>
                        <p>Ovim potvrđujete da su osobni podatci navedeni u ovom upitniku točni te da ste suglasni s prikupljanjem, obradom i korištenjem podataka u navedene svrhe.</p>
                        <p>U svakom trenutku imate pravo zatražiti uvid u podatke koje smo o Vama prikupili, pravo na ispravak ili promjenu osobnih podataka, povlačenje suglasnosti i brisanje svih prikupljenih podataka.</p>
                        <p>Ukoliko želite ostvariti neko od prethodno navedenih prava, molimo da nas kontaktirate putem e-maila <a href="mailto:dkolektiv@dkolektiv.hr">dkolektiv@dkolektiv.hr</a>, pozivom na <a href="tel:+38531211306">031 211 306</a> ili osobnim dolaskom u DKolektiv - Volonterski centar Osijek, Stjepana Radića 16, radnim danom od ponedjeljka do petka od 8 do 16 sati.</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('js')
<script>
    $(document).ready(function() {

        const VALIDATION_STEP_ROUTES = @json($validation_step_routes);

        // submit form
        $("#wantToVolunteerForm").on('submit', function(e) {
            e.preventDefault();
            let formData = $("#wantToVolunteerForm :input").serialize();
            let route = $(this).attr('action');
            $("#wantToVolunteerForm .error-border").removeClass('error-border');
            $("#wantToVolunteerForm .with-errors").empty();

            $.ajax({
                type:'POST',
                url: route,
                data: formData,
                cache: false,
                timeout: 10000,
                success: function(data){
                    if (data.errors === false) {
                        toggle('.application-wrapper, .application-success-message, .progressbar-container');
                    }
                },
                error: function(xhr, statusText, error){
                    $.each(xhr.responseJSON.errors, (_key, val) => {
                        console.log(_key + ' - log - ' + val);
                        $("#" + _key).addClass('error-border');
                        $("#" + _key + "+ .with-errors").text(val);
                    });
                    console.log("Error: " + xhr.status + ' ' + error);
                }
            });
            
            //
        });

        // navigate to next form step
        $("#btnNext").on('click', function(e) {
            nexPrevStep(1);
        });

        // navigate to previous form step
        $("#btnPrev").on('click', function(e) {
            nexPrevStep(-1);
        });

        // geo area toggle
        $("#geo_area_1").on('click', function(e) {
            let isChecked = $(this).is(':checked');

            toggle("#geo_area");
        });

        // newsletter info toggle
        $("#newsletter").on('click', function(e) {
            let isChecked = $(this).is(':checked');

            toggle(".alert-info");
        });

        // toggle show/hide element
        function toggle(element) {
            $(element).toggleClass('d-none');
        }

        let currentStep = 0;
        showStep(currentStep);

        // display the specified form step
        function showStep(step) {
            
            let steps = $("form.want-to-volunteer .step");
            scrollToElement('.progressbar-container');

            $('form.want-to-volunteer').find('.step').addClass('d-none');
            $('form.want-to-volunteer').find('.step').eq(step).removeClass('d-none');
            // displaying of Previous/Next buttons
            if (step == 0) {
                $("#btnPrev").addClass('d-none');
            } else {
                $("#btnPrev").removeClass('d-none');
            }

            // toggle between next step and submit form button
            if (step == (steps.length - 1)) {
                $("#btnNext").addClass('d-none');
                $("#btnSubmit").removeClass('d-none');
            } else {
                $("#btnNext").removeClass('d-none');
                $("#btnSubmit").addClass('d-none');
            }
            // display correct step in progress bar
            fixStepIndicator(step);
        };

        // display next or previous form step
        function nexPrevStep(value) {
            // get all steps in form
            let steps = $(".step");
            // validate step data
            if (value == 1 && !validated(currentStep, value)) {
                return false;
            }
            // hide current step
            $('form.want-to-volunteer').find('.step').eq(currentStep).addClass('d-none');
            // increase or decrease current step number by 1
            currentStep = currentStep + value;
            // display current step
            showStep(currentStep);
        }

        // display correct step number in progress bar
        function fixStepIndicator(currentStep) {
            // get all steps
            let steps = $(".progressbar .step");
            steps.each(function(i, val) {
                if (i <= currentStep) {
                    // if step is left from current step & current step
                    $('.progressbar').find('.step').eq(i).addClass('active');
                } else {
                    // if step is right from current step
                    $('.progressbar').find('.step').eq(i).removeClass('active');
                }
            });

        }

        // validate all steps individualy
        function validated(step, value) {
            let stepToValidate = step + 1;
            let routeKey = 'step_' + stepToValidate;
            let route = VALIDATION_STEP_ROUTES[routeKey];
            //console.log(route + ' - log - ' + routeKey);
            let token = $("#wantToVolunteerForm input[name=_token]").val();
            let stepDataToValidate = $("#step_"+ stepToValidate +" :input, #wantToVolunteerForm input[name=_token]").serialize();
            $("#wantToVolunteerForm .error-border").removeClass('error-border');
            $("#wantToVolunteerForm .with-errors").empty();

            $.ajax({
                type:'POST',
                url: route,
                data: stepDataToValidate,
                cache: false,
                timeout: 10000,
                success: function(data){                    
                    // hide current step
                    $('form.want-to-volunteer').find('.step').eq(currentStep).addClass('d-none');
                    // increase or decrease current step number by 1
                    currentStep = currentStep + value;
                    // display current step
                    showStep(currentStep);
                },
                error: function(xhr, statusText, error){
                    $.each(xhr.responseJSON.errors, (_key, val) => {
                        $("#" + _key).addClass('error-border');
                        $("#" + _key + "+ .with-errors").text(val);
                    });
                    console.log("Error: " + xhr.status + ' ' + error);
                }
            });
        }
    });
</script>
@endsection