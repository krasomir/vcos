@extends('layouts.head')
@section('title', __('Upravni odbor'))
@section('meta-description', __('Volonterski centar usmjeren je prema viziji društva u kojem su građani motivirani činiti pozitivne promjene u svojim zajednicama'))
@section('meta-keywords', __('ciljevi, vizija, misija, vizija društva, motivacija'))

@section('og_url', config('app.url') . '/' . app()->getLocale() . '/board-of-directors')
@section('og_title', __('Upravni odbor'))
@section('og_description', __('Volonterski centar usmjeren je prema viziji društva u kojem su građani motivirani činiti pozitivne promjene u svojim zajednicama'))
@section('og_image', asset('storage/images/d_kolektiv_logo_hd.jpg'))
@section('og_type', 'article')
@section('og_locale', 'hr_HR')

@section('content')

<!-- BANNER -->
 <x-banner-component :request-path="Request::path()" :name="__('Upravni odbor')" />

<!-- board of directors -->
<div class="section">
    <div class="content-wrap">
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <blockquote>
                        DKolektivom upravlja Upravni odbor. Svojim angažmanom osiguravaju da organizacija ostane na tragu misije i postavljenih strateških ciljeva te da posluje stručno i transparentno.
                    </blockquote>		              			
                </div>
                <div class="col-sm-12 mt-3">
                    <h2>{{ __('Članovi upravnog odbora') }}</h2>
                </div>

                <!-- <div class="col-sm-4 col-md-4">
                    <div class="rs-team-1">
                        <div class="media">
                            <img src="{{ asset('storage/images/our_team/default-user.jpg') }}" alt="Sandra Jakelić">
                        </div>
                        <div class="body">
                            <div class="title">Sandra Jakelić</div>
                            <div class="position">predsjednica</div>
                        </div>
                        <div class="media-overlay">
                            <h3>Sandra Jakelić</h3>
                        </div>
                    </div>
                </div>

                <div class="col-sm-4 col-md-4">
                    <div class="rs-team-1">
                        <div class="media"><img src="{{ asset('storage/images/our_team/default-user.jpg') }}" alt=""></div>
                        <div class="body">
                            <div class="title">Sanja Vuković</div>
                            <div class="position"></div>
                        </div>
                        <div class="media-overlay">
                            <h3>Sanja Vuković</h3>
                        </div>
                    </div>
                </div>

                <div class="col-sm-4 col-md-4">
                    <div class="rs-team-1 position-relative">
                        <div class="media">
                            <img src="{{ asset('storage/images/board_of_directors/denis-cosic.jpg') }}" alt="Denis Ćosić">
                        </div>
                        <div class="body">
                            <div class="title">Denis Ćosić</div>
                            <div class="position"></div>
                        </div>
                        <div class="media-overlay">
                            <h3>Denis Ćosić</h3>
                            <p>
                                Ravnatelj Gradskog društva Crvenog križa Osijek od 2017. godine, u kojem je aktivan što kroz
                                volonterski, što kroz profesionalni angažman još od 1981. godine. Diplomirao je na smjeru Proizvodno
                                strojarstvo na Strojarskom fakultetu Slavonski Brod gdje je stekao zanimanje diplomiranog inženjera
                                strojarstva. Na Fakultetu za odgojne i obrazovne znanosti u Osijeku završio je 2016. godine Pedagoško-
                                psihološku i didaktičko-metodičku izobrazbu. Član je Skupštine i Glavnog odbora Hrvatskog Crvenog
                                križa, Skupštine i Odbora Hrvatskog Crvenog križa Društva Crvenog križa Osječko-baranjske županije,
                                Nacionalnog odbora za dobrovoljno davanje krvi Hrvatskog Crvenog križa, Povjerenstva za razvoj
                                volonterstva Hrvatskog Crvenog križa i Savjeta za prvu pomoć Hrvatskog Crvenog križa. Član je Savjeta za
                                razvoj volonterstva Slavonije i Baranje i jedan je od osnivača i članova Upravnog odbora DKolektiva.
                                Član je radnih tijela Grada Osijeka - Odbor za socijalnu skrb i zdravstvo, Odbor za organizacije civilnog
                                društva Grada Osijeka te Osječko vijeće za razvoj socijalnih usluga te Povjerenstva Osječko-baranjske
                                županije za provedbu nacionalne strategije izjednačavanja mogućnosti za osobe s invaliditetom na
                                području Osječko-baranjske županije.
                            </p>
                        </div>
                    </div>
                </div>

                <div class="col-sm-4 offset-sm-2 col-md-4">
                    <div class="rs-team-1">
                        <div class="media"><img src="{{ asset('storage/images/our_team/default-user.jpg') }}" alt=""></div>
                        <div class="body">
                            <div class="title">Gordana Kovačević</div>
                            <div class="position"></div>
                        </div>
                        <div class="media-overlay">
                            <h3>Gordana Kovačević</h3>
                        </div>
                    </div>
                </div>

                <div class="col-sm-4 col-md-4">
                    <div class="rs-team-1">
                        <div class="media"><img src="{{ asset('storage/images/our_team/default-user.jpg') }}" alt=""></div>
                        <div class="body">
                            <div class="title">Silvio Ambruš</div>
                            <div class="position"></div>
                        </div>
                        <div class="media-overlay">
                            <h3>Silvio Ambruš</h3>
                        </div>
                    </div>
                </div> -->

                <!-- temporary -->
                <div class="col-sm-12 rs-team-1">
                    <blockquote>
                        <ul>
                            <li class="text-left"><span class="title">Sandra Jakelić</span>, predsjednica</li>
                            <li class="text-left"><span class="title">Sanja Vuković</span></li>
                            <li class="text-left"><span class="title">Denis Ćosić</span></li>
                            <li class="text-left"><span class="title">Gordana Kovačević</span></li>
                            <li class="text-left"><span class="title">Silvio Ambruš</span></li>
                        </ul>
                    </blockquote>
                </div>

            </div>

        </div>
    </div>
</div>

<!-- OUR PATRONS -->
<x-our-patrons-component />
@endsection