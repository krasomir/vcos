@extends('layouts.head')
@section('title', __('Publikacije'))
@section('meta-description', __('Volonterski centar usmjeren je prema viziji društva u kojem su građani motivirani činiti pozitivne promjene u svojim zajednicama'))
@section('meta-keywords', __('Volonteri, centar, Osijek, organizacija, Volonterski, Publikacije'))

@section('og_url', config('app.url') . '/' . app()->getLocale() . '/publications')
@section('og_title', __('Publikacije'))
@section('og_description', __('Volonterski centar usmjeren je prema viziji društva u kojem su građani motivirani činiti pozitivne promjene u svojim zajednicama'))
@section('og_image', asset('storage/images/dkolektiv_logo_hd.jpg'))
@section('og_type', 'article')
@section('og_locale', 'hr_HR')

@section('content')
<!-- BANNER -->
 <x-banner-component :request-path="Request::path()" :name="__('Publikacije')" />

<!--  -->
<div class="section">
    <div class="content-wrap">
        <div class="container">
            <div class="row">
                <div class="col-sm-12 col-md-6 col-lg-6 col-xl-6">
                    <h3 class="w-full color-primary">
                        {{ ucfirst(__(\App\Enums\PublicationCategoriesEnum::PROFESSIONAL_MANUALS->value)) }}
                    </h3>
                    <div class="row">
                        @forelse ($professionalManualsPublications as $professionalManualsPublication)
                            <div class="col-sm-12 col-md-12 col-lg-6 col-xl-6 mb-4">
                                <div class="publication-container">
                                    <a target="_blank" href="{{ asset('storage/documents/publications/' . $professionalManualsPublication->publication_name) }}">
                                        <div class="publication-box" data-background="{{ asset('storage/images/publications/' . $professionalManualsPublication->image_name) }}">
                                        </div>
                                    </a>
                                    <div class="body-content">
                                        <p class="title">
                                            <a target="_blank" href="{{ asset('storage/documents/publications/' . $professionalManualsPublication->publication_name) }}">{{ $professionalManualsPublication->title }}</a>
                                        </p>
                                    </div>
                                </div>
                            </div>
                        @empty
                            <div class="col-sm-12">
                                <div class="publication-container">
                                    <div class="body-content">
                                        <p class="text-center">Uskoro!!</p>
                                    </div>
                                </div>
                            </div>
                        @endforelse
                    </div>
                </div>
                <div class="col-sm-12 col-md-6 col-lg-6 col-xl-6">
                    <h3 class="w-full color-primary">
                        {{ ucfirst(__(\App\Enums\PublicationCategoriesEnum::RESEARCH_AND_ANALYSIS->value)) }}
                    </h3>
                    <div class="row">
                        @forelse ($researchAndAnalysisPublications as $researchAndAnalysisPublication)
                            <div class="col-sm-12 col-md-12 col-lg-6 col-xl-6 mb-4">
                                <div class="publication-container">
                                    <a target="_blank" href="{{ asset('storage/documents/publications/' . $researchAndAnalysisPublication->publication_name) }}">
                                        <div class="publication-box" data-background="{{ asset('storage/images/publications/' . $researchAndAnalysisPublication->image_name) }}">
                                        </div>
                                    </a>
                                    <div class="body-content">
                                        <p class="title">
                                            <a target="_blank" href="{{ asset('storage/documents/publications/' . $researchAndAnalysisPublication->publication_name) }}">{{ $researchAndAnalysisPublication->title }}</a>
                                        </p>
                                    </div>
                                </div>
                            </div>
                        @empty
                            <div class="col-sm-12">
                                <div class="publication-container">
                                    <div class="body-content">
                                        <p class="text-center">Uskoro!!</p>
                                    </div>
                                </div>
                            </div>
                        @endforelse
                    </div>
                </div>
            </div>
            {{--<div class="row">
                <h3 class="w-full color-primary">{{ ucwords(__(\App\Enums\PublicationCategoriesEnum::PROFESSIONAL_MANUALS->value)) }}</h3>
                @forelse ($professionalManualsPublications as $professionalManualsPublication)
                    <div class="col-sm-12 col-md-6 col-lg-4 col-xl-3 mb-4">
                        <div class="publication-container">
                            <a target="_blank" href="{{ asset('storage/documents/publications/' . $professionalManualsPublication->publication_name) }}">
                                <div class="publication-box" data-background="{{ asset('storage/images/publications/' . $professionalManualsPublication->image_name) }}">
                                </div>
                            </a>
                            <div class="body-content">
                                <p class="title">
                                    <a target="_blank" href="{{ asset('storage/documents/publications/' . $professionalManualsPublication->publication_name) }}">{{ $professionalManualsPublication->title }}</a>
                                </p>
                            </div>
                        </div>
                    </div>
                @empty
                    <div class="col-sm-12">
                        <div class="publication-container">
                            <div class="body-content">
                                <p class="text-center">Uskoro!!</p>
                            </div>
                        </div>
                    </div>
                @endforelse
            </div>
            <div class="row">
                <h3 class="w-full color-primary">{{ ucwords(__(\App\Enums\PublicationCategoriesEnum::RESEARCH_AND_ANALYSIS->value)) }}</h3>
                @forelse ($researchAndAnalysisPublications as $researchAndAnalysisPublication)
                    <div class="col-sm-12 col-md-6 col-lg-4 col-xl-3 mb-4">
                        <div class="publication-container">
                            <a target="_blank" href="{{ asset('storage/documents/publications/' . $researchAndAnalysisPublication->publication_name) }}">
                                <div class="publication-box" data-background="{{ asset('storage/images/publications/' . $researchAndAnalysisPublication->image_name) }}">
                                </div>
                            </a>
                            <div class="body-content">
                                <p class="title">
                                    <a target="_blank" href="{{ asset('storage/documents/publications/' . $researchAndAnalysisPublication->publication_name) }}">{{ $researchAndAnalysisPublication->title }}</a>
                                </p>
                            </div>
                        </div>
                    </div>
                @empty
                    <div class="col-sm-12">
                        <div class="publication-container">
                            <div class="body-content">
                                <p class="text-center">Uskoro!!</p>
                            </div>
                        </div>
                    </div>
                @endforelse
            </div>--}}
        </div>
    </div>
</div>

@endsection

@section('js')
    <script>
        $(document).ready(function() {
            // Function to check if element is in viewport with offset
            function isElementInViewport(el, offset = 0) {
                const rect = el.getBoundingClientRect();
                const windowHeight = window.innerHeight || document.documentElement.clientHeight;

                return (
                    rect.top >= -offset &&
                    rect.bottom <= windowHeight + offset
                );
            }

            // Initialize publications
            $('.publication-container').parent().each(function(index) {
                $(this).addClass('publication-hidden');
                // Add data attribute for position in row (0-3)
                $(this).attr('data-position', index % 4);
            });

            let lastScrollTop = 0; // Store the last scroll position

            // Function to handle scroll animation
            function handleScrollAnimation() {
                const st = window.pageYOffset || document.documentElement.scrollTop;
                const scrollingDown = st > lastScrollTop;

                $('.publication-container').parent().each(function() {
                    const $element = $(this);
                    const isVisible = isElementInViewport(this, 50);
                    const position = $element.attr('data-position');

                    if (isVisible && $element.hasClass('publication-hidden')) {
                        setTimeout(() => {
                            $element.removeClass('publication-hidden')
                                .addClass('publication-visible')
                                .removeClass('publication-exit-up publication-exit-down');
                        }, position * 150); // 150ms delay between each item
                    }
                    /*else if (!isVisible && $element.hasClass('publication-visible')) {
                        if (scrollingDown) {
                            $element.removeClass('publication-visible')
                                .addClass('publication-hidden publication-exit-up');
                        } else {
                            $element.removeClass('publication-visible')
                                .addClass('publication-hidden publication-exit-down');
                        }
                    }*/
                });

                lastScrollTop = st <= 0 ? 0 : st; // For Mobile or negative scrolling
            }

            // Handle initial state
            handleScrollAnimation();

            // Add scroll event listener with debounce for better performance
            let scrollTimeout;
            $(window).on('scroll', function() {
                if (scrollTimeout) {
                    clearTimeout(scrollTimeout);
                }
                scrollTimeout = setTimeout(handleScrollAnimation, 10);
            });

            // Handle resize events
            $(window).on('resize', handleScrollAnimation);
        });
    </script>
@endsection
