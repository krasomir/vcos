@extends('layouts.head')
@section('title', __('Arhiva projekata'))
@section('meta-description', __('Volonterski centar usmjeren je prema viziji društva u kojem su građani motivirani činiti pozitivne promjene u svojim zajednicama'))
@section('meta-keywords', __('ciljevi, vizija, misija, vizija društva, motivacija'))

@section('og_url', config('app.url') . '/' . app()->getLocale() . '/archive-of-projects')
@section('og_title', __('Arhiva projektata'))
@section('og_description', __('Volonterski centar usmjeren je prema viziji društva u kojem su građani motivirani činiti pozitivne promjene u svojim zajednicama'))
@section('og_image', asset('storage/images/d_kolektiv_logo_hd.jpg'))
@section('og_type', 'article')
@section('og_locale', 'hr_HR')

@section('content')
    <!-- BANNER -->
     <x-banner-component :request-path="Request::path()" :name="__('Arhiva projekata')" />

    <!-- archive projects -->
    <div class="section">
        <div class="content-wrap">
            <div class="container">
                <div class="row">

                    <!-- Item 1 -->
                    @foreach($projects as $project)
                    <div class="col-sm-6 col-md-4 col-lg-3">
                        <div class="box-fundraising">
                            @if($project->image_name)
                                <div class="media">
                                    <img src="{{ asset('storage/images/projects/thumb/' . $project->image_name) }}" alt="{{ $project->title }}">
                                </div>
                            @endif
                            <div class="body-content">
                                <p class="title">
                                    <a href="{{ route('project', ['lang' => app()->getLocale(), 'project' => $project->slug]) }}">
                                        {{ $project->title }}
                                    </a>
                                </p>
                                <div class="meta">
                                    <div class="date-archive">
                                        <i class="fa fa-clock-o"></i> {{ $project->start_date->isoFormat('DD. MMMM Y.') }}
                                    </div>
                                </div>
                                <div class="text font-italic">
                                    {!! $project->preview !!}
                                </div>                                
                            </div>
                        </div>
                    </div>
                    @endforeach

                </div>                
                <!-- pagination -->
				<div class="row">
					<div class="col-sm-12 col-md-12">						
						<nav aria-label="Page navigation">
                        {{ $projects->links('components.custom-pagination') }}
						</nav>
					</div>
				</div>
            </div>
        </div>
    </div>
@endsection