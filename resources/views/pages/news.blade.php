@extends('layouts.head')
@section('title', __('Novosti'))
@section('meta-description', __('DKolektiv - organizacija za društveni razvoj'))
@section('meta-keywords', __('ciljevi, vizija, misija, vizija društva, motivacija'))

@section('og_url', config('app.url') . '/' . app()->getLocale() . '/news')
@section('og_title', __('Novosti'))
@section('og_description', __('DKolektiv - organizacija za društveni razvoj'))
@section('og_image', asset('storage/images/d_kolektiv_logo_hd.jpg'))
@section('og_type', 'article')
@section('og_locale', 'hr_HR')

@section('content')
    <!-- BANNER -->
     <x-banner-component :request-path="Request::path()" :name="__('Novosti')" />

    <!-- news -->
    <div class="section">
        <div class="content-wrap">
            <div class="container">
                <div class="row">

                    @foreach($posts as $post)
                    <!-- news box -->
                    <div class="col-sm-6 col-md-4">
                        <div class="box-news-1">
                            <div class="media gbr">
                                @if($post->image_name)
                                    <img src="{{ asset('storage/images/news/thumb/' . $post->image_name) }}" alt="{{ $post->slug }}" class="img-fluid">
                                @elseif($post->id < config('app.last_old_post_id') && $post->getOldPostImagePath())
                                    <img src="{{ asset('storage' . $post->getOldPostImagePath()->crop_thumb) }}" alt="{{ $post->slug }}" class="img-fluid">
                                @endif
                            </div>
                            <div class="body">
                                <div class="title">
                                    <a href="{{ route('news-details', ['lang' => app()->getLocale(), 'post' => $post]) }}" title="{{ $post->name }}">
                                        <h3>{{ $post->name }}</h3>
                                    </a>
                                </div>
                                <div>
                                    <p class="mt-2">
                                    @if($post->preview)
                                        {!! $post->shortenPreview() !!}
                                    @else
                                        {!! $post->shortenText() !!}
                                    @endif
                                    </p>
                                </div>
                                <div class="meta">
                                    <span class="date"><i class="fa fa-clock-o"></i> {{ $post->datetime_on->format('d.m.Y.') }}</span>
                                </div>
                            </div>
                        </div>
                    </div>
                    @endforeach
                </div>             

                <!-- pagination -->
				<div class="row">
					<div class="col-sm-12 col-md-12">						
						<nav aria-label="Page navigation">
                        {{ $posts->links('components.custom-pagination') }}
						</nav>
					</div>
				</div>
            </div>
        </div>
    </div>

    
    <!-- OUR PATRONS -->
    <x-our-patrons-component />
@endsection