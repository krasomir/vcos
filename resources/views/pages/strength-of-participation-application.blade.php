@extends('layouts.head')
@section('title', __('Prijava u bazu Snaga sudjelovanja'))
@section('meta-description', __('Biti aktivan i biti odgovoran važne su vrline civilnog društva za koje se DKolektiv zalaže od svoga osnivanja.'))
@section('meta-keywords', __('edukacija, informacija, poticanje, afirmacija, zagovaranje, umrežavanje, MOTUS'))

@section('content')
    <!-- BANNER -->
    <x-banner-component :request-path="Request::path()" 
        :name="__('Prijava u bazu Snaga sudjelovanja')" 
    />

    <!-- strenght of participation -->
    <div class="section">
        <div class="content-wrap">
            <div class="container mt-n2">
                <div class="jumbotron jumbotron-fluid py-1">
                    <div class="container">
                        <h3 class="mb-0">SNAGA SUDJELOVANJA - INFO</h3>
                        <h4 class="mt-1">postanite korisnik</h4>
                        <p class="mb-1">Popunjavanjem prijavnog obrasca postajete korisnik SNAGE SUDJELOVANJA što će vam omogućiti redovito primanje obavijesti važnih za civilno društvo i razvoj demokracije te sve druge relevantne informacije koje zaprimamo od različitih dionika <em>(npr. obavijesti o različitim događanjima u zajednici, otvorenim natječajima, edukacijama, istraživanjima, strategijama i programima na nacionalnoj i lokalnoj razini i sl.)</em></p>
                    </div>
                </div>
                <div class="row application-success-message d-none m-5">
                    <div class="col-sm-12">
                        <div class="alert alert-success text-center m-0">
                            <p class="m-0">Vaša prijava je uspješno zaprimljena.</p>
                        </div>
                    </div>
                </div>
                <div class="row progressbar-container">
                    <ul class="progressbar progress-third m-0">
                        <li class="step active">Opći podaci</li>
                        <li class="step">Djelovanje organizacije</li>
                        <li class="step">Geo područja</li>
                    </ul>
                </div>
                <div class="row application-wrapper">
                    <div class="col-sm-12">
                        <!-- form -->
                        <form action="{{ route('save-strength-of-participation-application', app()->getLocale()) }}" method="post" class="form-contact need-volunteers" id="strengthOfParticipationForm" novalidate="true">
                            @csrf
                            <!-- step one -->
                            <div id="step_1" class="step row mb-3 ">
                                <div class="col-sm-6 col-sm-6 col-md-6">
                                    <div class="form-group">
                                        <label for="organisation_type">{{ __('Vrsta organizacije') }}*</label>
                                        <select class="form-control" name="organisation_type" id="organisation_type" required>
                                            <option value="" disabled selected></option>
                                            <option value="udruga" @if($organisation && $organisation->organisation_type === 'udruga') selected @endif>udruga</option>
                                            <option value="zaklada" @if($organisation && $organisation->organisation_type === 'zaklada') selected @endif>zaklada</option>
                                            <option value="ustanova" @if($organisation && $organisation->organisation_type === 'ustanova') selected @endif>ustanova</option>
                                            <option value="jedinica lokalne samouprave" @if($organisation && $organisation->organisation_type === 'jedinica lokalne samouprave') selected @endif>jedinica lokalne samouprave</option>
                                            <option value="jedinica regionalne samouprave" @if($organisation && $organisation->organisation_type === 'jedinica regionalne samouprave') selected @endif>jedinica regionalne samouprave</option>
                                            <option value="tijelo državne uprave" @if($organisation && $organisation->organisation_type === 'tijelo državne uprave') selected @endif>tijelo državne uprave</option>
                                        </select>
                                        <div class="help-block with-errors"></div>
                                    </div>
                                </div>
                                <div class="col-sm-6 col-md-6">
                                    <div class="form-group">
                                        <label for="organisation_name">{{ __('Naziv organizacije') }}*</label>
                                        <input type="text" value="{{ $organisation->organisation_name ?? '' }}" class="form-control" name="organisation_name" id="organisation_name" required>
                                        <div class="help-block with-errors"></div>
                                    </div>
                                </div>
                                <div class="col-sm-6 col-md-6">
                                    <div class="form-group">
                                        <label for="foundation_year">{{ __('Godina osnutka') }}*</label>
                                        <input type="number" value="" class="form-control" name="foundation_year" id="foundation_year" required>
                                        <div class="help-block with-errors"></div>
                                    </div>
                                </div>
                                <div class="col-sm-6 col-md-6">
                                    <div class="form-group">
                                        <label for="address">{{ __('Ulica i kućni broj') }}*</label>
                                        <input type="text" value="{{ $organisation->address ?? '' }}" class="form-control" name="address" id="address" required>
                                        <div class="help-block with-errors"></div>
                                    </div>
                                </div>
                                <div class="col-sm-6 col-md-6">
                                    <div class="form-group">
                                        <label for="zip_number">{{ __('Poštanski broj') }}*</label>
                                        <input type="number" value="" class="form-control" name="zip_number" id="zip_number" required>
                                        <div class="help-block with-errors"></div>
                                    </div>
                                </div>
                                <div class="col-sm-6 col-md-6">
                                    <div class="form-group">
                                        <label for="city">{{ __('Mjesto') }}*</label>
                                        <input type="text" value="{{ $organisation->city ?? '' }}" class="form-control" name="city" id="city" required>
                                        <div class="help-block with-errors"></div>
                                    </div>
                                </div>
                                <div class="col-sm-6 col-md-6">
                                    <div class="form-group">
                                        <label for="contact_person">{{ __('Kontakt osoba') }}*</label>
                                        <input type="text" value="{{ $organisation->contact_person ?? '' }}" class="form-control" name="contact_person" id="contact_person" required>
                                        <div class="help-block with-errors"></div>
                                    </div>
                                </div>
                                <div class="col-sm-6 col-md-6">
                                    <div class="form-group">
                                        <label for="telephone">{{ __('Telefon') }}*</label>
                                        <input type="text" value="{{ $organisation->contact_telephone ?? '' }}" class="form-control" name="telephone" id="telephone" required>
                                        <div class="help-block with-errors"></div>
                                    </div>
                                </div>
                                <div class="col-sm-6 col-md-6">
                                    <div class="form-group">
                                        <label for="email">{{ __('E-mail') }}*</label>
                                        <input type="email" value="{{ $organisation->contact_email ?? '' }}" class="form-control" name="email" id="email" required>
                                        <div class="help-block with-errors"></div>
                                    </div>
                                </div>
                                <div class="col-sm-6 col-md-6">
                                    <div class="form-group">
                                        <label for="web">{{ __('Web stranica') }}</label>
                                        <input type="text" class="form-control" name="web" id="web" required>
                                        <div class="help-block with-errors"></div>
                                    </div>
                                </div>
                            </div>
                            <!-- step two -->
                            <div id="step_2" class="step row mb-3 ">                                
                                <div class="col-sm-12">
                                    <div class="form-group">
                                        <label for="mission_goals">{{ __('Misija i ciljevi organizacije (max 250 znakova)') }}*</label>
                                        <textarea class="form-control" name="mission_goals" id="mission_goals" rows="5" required></textarea>
                                        <div class="help-block with-errors"></div>
                                    </div>
                                </div>
                                <div class="col-sm-12">
                                    <label class="mb-3">{{ __('Područje djelovanja (moguće izabrati više odgovora)') }}*</label>
                                    <div id="activity_area" class="form-group row mb-0">
                                        @foreach($acticvity_areas as $key => $activity_area)
                                        <div class="checkboxes col-sm-6 col-md-4">
                                            <input type="checkbox" name="activity_area[]" id="activity_area_{{ $key }}" value="{{ $activity_area }}">
                                            <label for="activity_area_{{ $key }}">{{ __($activity_area) }}</label>
                                        </div>
                                        @endforeach
                                    </div>
                                    <div class="help-block with-errors"></div>
                                </div>
                            </div>
                            <!-- step three -->
                            <div id="step_3" class="step row mb-3 ">
                                <div class="col-sm-12">
                                    <div class="form-group">
                                        <label for="target_group">{{ __('Ciljna skupina/korisnici organizacije') }}*</label>
                                        <input type="text" class="form-control" name="target_group" id="target_group" required>
                                        <div class="help-block with-errors"></div>
                                    </div>
                                </div>
                                <div class="col-sm-12">
                                    <label class="mb-3">{{ __('Zemljopisno područje djelovanja (moguće izabrati više odgovora)') }}*</label>
                                    <div id="geo_area" class="form-group row mb-0">
                                        @foreach($geo_areas as $key => $geo_area)
                                        <div class="checkboxes col-sm-6 col-md-4">
                                            <input type="checkbox" name="geo_area[]" id="geo_area_{{ $key }}" value="{{ $geo_area }}">
                                            <label for="geo_area_{{ $key }}">{{ __($geo_area) }}</label>
                                        </div>
                                        @endforeach
                                    </div>
                                    <div class="help-block with-errors"></div>
                                </div>
                                <div class="col-sm-12 mt-3">
                                    <label class="mb-3 bold">{{ __('Newsletter DKolektiva – organizacije za društveni razvoj') }}</label>                                  
                                    <div class="form-group row mb-0 highlight-accent">
                                        <div class="checkboxes col-sm-12">
                                            <input type="checkbox" @if($organisation && $organisation->newsletter === 'da') checked @endif name="newsletter" id="newsletter" value="1">
                                            <label for="newsletter">{{ __('Da, želim primati newsletter DKolektiva – organizacije za društveni razvoj, kao i newslettere iz odabranih zemljopisnih područja djelovanja.') }}</label>
                                        </div>
                                    </div>
                                    <div class="help-block with-errors"></div>
                                    <!-- newsletter info -->
                                    <div class="alert alert-info mt-3 @if(!$organisation || ($organisation && $organisation->newsletter === 'ne')) d-none @endif">
                                        <p class="font-weight-bold">{{ __('Napomena') }}:</p class="bold">
                                        <p>{{ __('Poslat ćemo Vam e-mail s linkom za potvrdu Vaše adrese, molimo provjerite Vaš e-mail sandučić.') }}</p>
                                    </div>
                                </div>
                            </div>

                            <hr id="divider" class="mb-2">
                            <div class="row">
                                <div class="col-12 mb-2">
                                    <div class="small">{{ __('Polja označena zvjezdicom (*) je potrebno ispuniti/odabrati') }}</div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-6 col-sm-6">
                                    <div class="form-group">
                                        <button id="btnPrev" type="button" data-step="-1" class="btn btn-light w-100 d-none">{{ __('Predhodni korak') }}</button>
                                    </div>
                                </div>
                                <div class="col-6 col-sm-6">
                                    <div class="form-group">
                                        <button id="btnNext" type="button" data-step="1" class="btn btn-primary w-100">{{ __('Sljedeći korak') }}</button>
                                        <button id="btnSubmit" type="submit" class="btn btn-primary w-100 d-none">{{ __('Spremi prijavu') }}</button>
                                    </div>
                                </div>
                            </div> 
                        </form>
                    </div>
                </div>
            
                <div class="jumbotron jumbotron-fluid py-2 mt-2">
                    <div class="container">
                        <h3 class="">Izjava o povjerljivosti</h3>
                        <p>Podatke koje ste dostavili popunjavanjem prijavnog obrasca DKolektiv čuva i postupa s njima sukladno Općoj uredbi o zaštiti podataka. Dostavljeni podatci neće biti dostupni niti ćemo ih dijeliti s trećim osobama.</p>
                    </div>
                </div>    
            </div>
        </div>
    </div>
@endsection
@section('js')
<script>
    $(document).ready(function() {

        // validation step routes
        const VALIDATION_STEP_ROUTES = @json($validation_step_routes);

        let currentStep = 0;
        showStep(currentStep);

        // submit form
        $("#strengthOfParticipationForm").on('submit', function(e) {
            e.preventDefault();
            let formData = $("#strengthOfParticipationForm :input").serialize();
            let route = $(this).attr('action');
            $("#strengthOfParticipationForm .error-border").removeClass('error-border');
            $("#strengthOfParticipationForm .with-errors").empty();

            $.ajax({
                type:'POST',
                url: route,
                data: formData,
                cache: false,
                timeout: 10000,
                success: function(data){
                    if (data.errors === false) {                        
                        $('.application-wrapper, .progressbar-container').addClass('d-none');
                        $('.application-success-message').removeClass('d-none');
                    }
                },
                error: function(xhr, statusText, error){
                    $.each(xhr.responseJSON.errors, (_key, val) => {
                        let tmpKy = _key.split('.');
                        _key = tmpKy[0];

                        $("#" + _key).addClass('error-border');
                        $("#" + _key + "+ .with-errors").text(val);
                    });
                    console.log("Error: " + xhr.status + ' ' + error);
                }
            });
        });

        // navigate to next form step
        $("#btnNext").on('click', function(e) {
            nexPrevStep(1);
        });

        // navigate to previous form step
        $("#btnPrev").on('click', function(e) {
            nexPrevStep(-1);
        });

        $("#newsletter").on('change', function(e) {
            let isChecked = $(this).is(':checked');

            $(".alert-info").toggleClass('d-none');
        });
        
        // display next or previous form step
        function nexPrevStep(value) {
            // get all steps in form
            let steps = $(".step");
            // validate step data
            if (value == 1 && !validated(currentStep, value)) {
                return false;
            }
            // hide current step
            $('form.need-volunteers').find('.step').eq(currentStep).addClass('d-none');
            // increase or decrease current step number by 1
            currentStep = currentStep + value;
            // display current step
            showStep(currentStep);
        }

        // display the specified form step
        function showStep(step) {
            
            let steps = $("form.need-volunteers .step");
            scrollToElement('.progressbar-container');

            $('form.need-volunteers').find('.step').addClass('d-none');
            $('form.need-volunteers').find('.step').eq(step).removeClass('d-none');
            // displaying of Previous/Next buttons
            if (step == 0) {
                $("#btnPrev").addClass('d-none');
            } else {
                $("#btnPrev").removeClass('d-none');
            }

            // toggle between next step and submit form button
            if (step == (steps.length - 1)) {
                $("#btnNext").addClass('d-none');
                $("#btnSubmit").removeClass('d-none');
            } else {
                $("#btnNext").removeClass('d-none');
                $("#btnSubmit").addClass('d-none');
            }
            // display correct step in progress bar
            fixStepIndicator(step);
        };

        // display correct step number in progress bar
        function fixStepIndicator(currentStep) {
            // get all steps
            let steps = $(".progressbar .step");
            steps.each(function(i, val) {
                if (i <= currentStep) {
                    // if step is left from current step & current step
                    $('.progressbar').find('.step').eq(i).addClass('active');
                } else {
                    // if step is right from current step
                    $('.progressbar').find('.step').eq(i).removeClass('active');
                }
            });
        }

        // validate all steps individualy
        function validated(step, value) {

            let stepToValidate = step + 1;
            let routeKey = 'step_' + stepToValidate;
            let route = VALIDATION_STEP_ROUTES[routeKey];
            let token = $("#strengthOfParticipationForm input[name=_token]").val();
            let stepDataToValidate = $("#step_"+ stepToValidate +" :input, #strengthOfParticipationForm input[name=_token]").serialize();
            $("#strengthOfParticipationForm .error-border").removeClass('error-border');
            $("#strengthOfParticipationForm .with-errors").empty();

            $.ajax({
                type:'POST',
                url: route,
                data: stepDataToValidate,
                cache: false,
                timeout: 10000,
                success: function(data){                    
                    // hide current step
                    $('form.need-volunteers').find('.step').eq(currentStep).addClass('d-none');
                    // increase or decrease current step number by 1
                    currentStep = currentStep + value;
                    // display current step
                    showStep(currentStep);
                },
                error: function(xhr, statusText, error){
                    if (xhr.status === 419) {
                        let message = 'Usljed duge neaktivnosti sesija je istekla. Molimo osvježite stranicu.';
                        let element = `<div class="help-block with-errors">${message}</div>`;
                        $(element).insertBefore('#divider');
                    } else {
                        $.each(xhr.responseJSON.errors, (_key, val) => {
                            let tmpKy = _key.split('.');
                            _key = tmpKy[0];
                            
                            $("#" + _key).addClass('error-border');
                            $("#" + _key + "+ .with-errors").text(val);
                        });
                        console.log("Error: " + xhr.status + ' ' + error);
                    }
                }
            });
        }


    });
</script>
@endsection