@extends('layouts.head')
@section('title', __('Potvrda e-mail adrese'))

@section('content')
    <!-- BANNER -->
    <x-banner-component :request-path="Request::path()" 
        :name="__('Potvrda e-mail adrese')" 
    />

    <!-- strenght of participation -->
    <div class="section">
        <div class="content-wrap">
            <div class="container mt-n2">
                <div class="row application-success-message m-5">
                    <div class="col-sm-12">
                        <div class="alert alert-{{ $alertType }} text-center m-0">
                            <p class="m-0">{{ $verificationMessage }}</p>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
@endsection