@extends('layouts.head')
@section('title', __('Radius V'))
@section('meta-description', __('Projekt „Radius V“ je usmjeren na kreiranje poticajnog okruženja za razvoj volonterstva'))
@section('meta-keywords', __('Radius V, poticajno okruženje,'))

@section('og_url', config('app.url') . '/' . app()->getLocale() . '/radius-v')
@section('og_title', __('Radius V'))
@section('og_description', __('Projekt „Radius V“ je usmjeren na kreiranje poticajnog okruženja za razvoj volonterstva'))
@section('og_image', asset('storage/images/dkolektiv_logo_hd.jpg'))
@section('og_type', 'article')
@section('og_locale', 'hr_HR')

@section('content')
    <!-- BANNER -->
     <x-banner-component :request-path="Request::path()" 
        :name="__('Radius V')" 
    />

    <!--  -->
    <h1>Radius V</h1>
@endsection