@extends('layouts.head')
@section('title', $program->title)
@section('meta-description', __('Osijek to GOO zajednički je program Grada Osijeka i četiri osječke organizacije civilnoga društva – Dkolektiva, Nansen dijalog centra, Udruge za rad s mladima “Breza” i Dječjeg osječkog kreativnog centra Dokkica'))
@section('meta-keywords', __('DKolektiv, Nansen dijalog centar, Udruga za rad s mladima “Breza”, Dječiji osječki kreativni centar Dokkica'))

@section('og_url', config('app.url') . '/' . app()->getLocale() . '/osijek-to-goo')
@section('og_title', $program->title)
@section('og_description', __('Osijek to GOO zajednički je program Grada Osijeka i četiri osječke organizacije civilnoga društva – Dkolektiva, Nansen dijalog centra, Udruge za rad s mladima “Breza” i Dječjeg osječkog kreativnog centra Dokkica'))
@section('og_image', asset('storage/images/razvoj_civilnog_drustva_logo_hd.jpg'))
@section('og_type', 'article')
@section('og_locale', 'hr_HR')

@section('content')
    <!-- BANNER -->
     <x-banner-component :request-path="Request::path()" 
        :name="$program->title" 
        :extra-breadcrumb="route('strength-of-participation', app()->getLocale())" 
        extra-breadcrumb-name="Snaga sudjelovanja"
    />

    <!-- osijek to goo -->
    <div class="section">
        <div class="content-wrap">
            <div class="container">
                <div class="row">
                    <!--  -->
                    {!! $program->content !!}
                </div>
            </div>
        </div>
    </div>
    
    <!-- OUR PATRONS -->
    <x-our-patrons-component />
@endsection