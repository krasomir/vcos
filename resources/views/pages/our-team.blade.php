@extends('layouts.head')
@section('title', __('Tko čini DKolektiv?'))
@section('meta-description', __('Naš tim okuplja predane i marljive pojedince čija se iskustva, stručna znanja, kvalitete i vrline međusobno nadopunjuju i čine tim DKolektiva'))
@section('meta-keywords', __('Lejla Šehić Relić, Nikoleta Poljak, Jelena Kamenko Mayer, Tatjana Vidaković Gal, Mirta Kovačević, Nikica Torbica, Bruno, Mirna Šostarko, Jelena Stefanović, Roberta Verner'))

@section('og_url', config('app.url') . '/' . app()->getLocale() . '/our-team')
@section('og_title', __('Tko čini DKolektiv?'))
@section('og_description', __('Naš tim okuplja predane i marljive pojedince čija se iskustva, stručna znanja, kvalitete i vrline međusobno nadopunjuju i čine tim DKolektiva'))
@section('og_image', asset('storage/images/dkolektiv_logo_hd.jpg'))
@section('og_type', 'article')
@section('og_locale', 'hr_HR')

@section('content')

<!-- BANNER -->
 <x-banner-component :request-path="Request::path()" :request-path="Request::path()" :name="__('Tko čini DKolektiv?')" />

<!-- our team -->
<div class="section">
    <div class="content-wrap">
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <blockquote>
                        Naš tim okuplja predane i marljive pojedince čija se iskustva, stručna znanja, kvalitete i vrline međusobno nadopunjuju i čine tim DKolektiva. Zajedničkim snagama stvaramo, razvijamo i provodimo aktivnosti usmjerene društvenom razvoju, uz dragocjenu podršku stručnih suradnika i volontera.
                    </blockquote>
                </div>

                @foreach($teams as $team)
                    <div
                        @class([
                            'col-sm-4 col-md-4 col-xl-3 offset-0',
                            'offset-xl-3' => ($loop->index == (count($teams) - 2)),
                            'offset-md-4 offset-sm-4 offset-xl-0' => ($loop->index == (count($teams) - 1)),
                        ])
                    >
                        <div
                            @class([
                                'team-member-container' => null !== $team->description
                            ])
                        >
                            <div class="rs-team-1">
                                <div class="media">
                                    <img src="{{ asset('storage/images/our_team/' . $team->image) }}" alt="{{ $team->first_name .' '. $team->last_name }}">
                                </div>
                                <div class="body">
                                    <div class="title">{{ $team->first_name .' '. $team->last_name }}</div>
                                    <div class="position">{{ $team->position }}</div>
                                    @if($team->linked_in)
                                        <a target="_new" href="{{ $team->linked_in }}">
                                            <div class="block">
                                                <span class="fa fa-linkedin"></span>
                                            </div>
                                        </a>
                                    @endif
                                    @if($team->email)
                                        <div>
                                            {{ $team->email }}
                                        </div>
                                    @endif
                                </div>
                            </div>
                            @if(null !== $team->description)
                                <div class="rs-team-1 team-member-description">
                                    <div class="vertical-centered">
                                    {!! $team->description !!}
                                    </div>
                                </div>
                            @endif
                        </div>
                    </div>
                @endforeach

                {{--<div class="col-sm-3 col-md-3">
                    <div class="rs-team-1">
                        <div class="media">
                            <img src="{{ asset('storage/images/our_team/sehic-relic.jpg') }}" alt="Lejla Šehić Relić">
                        </div>
                        <div class="body">
                            <div class="title">Lejla Šehić Relić</div>
                            <div class="position">izvršna direktorica</div>
                            <ul class="social-icon">
                                <li>
                                    <a target="_new" href="https://www.linkedin.com/in/lejla-%C5%A1ehi%C4%87-reli%C4%87-957b3668/">
                                        <span class="fa fa-linkedin"></span>
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>

                <div class="col-sm-3 col-md-3">
                    <div class="rs-team-1">
                        <div class="media"><img src="{{ asset('storage/images/our_team/nikoleta-poljak.jpg') }}" alt="Nikoleta Poljak"></div>
                        <div class="body">
                            <div class="title">Nikoleta Poljak</div>
                            <div class="position">voditeljica razvoja i programa demokratske kulture i civilnog društva</div>
                            <ul class="social-icon"></ul>
                        </div>
                    </div>
                </div>

                <div class="col-sm-3 col-md-3">
                    <div class="rs-team-1">
                        <div class="media"><img src="{{ asset('storage/images/our_team/jelena-kamenko.jpg') }}" alt="Jelena Kamenko"></div>
                        <div class="body">
                            <div class="title">Jelena Kamenko Mayer</div>
                            <div class="position">voditeljica razvoja i programa uključivog društva i volonterstva</div>
                            <ul class="social-icon">
                                <li>
                                    <a target="_new" href="https://www.linkedin.com/in/jelena-kamenko-mayer-001b1280/?originalSubdomain=hr">
                                        <span class="fa fa-linkedin"></span>
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>

                <div class="col-sm-3 col-md-3">
                    <div class="rs-team-1">
                        <div class="media"><img src="{{ asset('storage/images/our_team/tatjana-vidakovic.jpg') }}" alt="Tatjana Vidaković Gal"></div>
                        <div class="body">
                            <div class="title">Tatjana Vidaković Gal</div>
                            <div class="position">voditeljica financijskog poslovanja</div>
                            <ul class="social-icon">
                                <li>
                                    <a target="_new" href="https://www.linkedin.com/in/tatjana-vidakovic-gal-1a961a1b/">
                                        <span class="fa fa-linkedin"></span>
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>

                <div class="col-sm-3 col-md-3">
                    <div class="rs-team-1">
                        <div class="media"><img src="{{ asset('storage/images/our_team/mirta-kovacevic.jpg') }}" alt="Mirta Kovačević"></div>
                        <div class="body">
                            <div class="title">Mirta Kovačević</div>
                            <div class="position">voditeljica Volonterskog centra Osijek</div>
                            <ul class="social-icon"></ul>
                        </div>
                    </div>
                </div>

                <div class="col-sm-3 col-md-3">
                    <div class="rs-team-1">
                        <div class="media"><img src="{{ asset('storage/images/our_team/nikica-torbica.jpg') }}" alt="Nikica Torbica"></div>
                        <div class="body">
                            <div class="title">Nikica Torbica</div>
                            <div class="position">voditelj Društvenog ateljea</div>
                            <ul class="social-icon"></ul>
                        </div>
                    </div>
                </div>

                <div class="col-sm-3 col-md-3">
                    <div class="rs-team-1">
                        <div class="media"><img src="{{ asset('storage/images/our_team/bruno-koic.jpg') }}" alt="Bruno Koić"></div>
                        <div class="body">
                            <div class="title">Bruno Koić</div>
                            <div class="position">stručni suradnik</div>
                            <ul class="social-icon"></ul>
                        </div>
                    </div>
                </div>

                <div class="col-sm-3 col-md-3">
                    <div class="rs-team-1">
                        <div class="media"><img src="{{ asset('storage/images/our_team/mirna-sostarko.jpg') }}" alt="Mirna Šostarko"></div>
                        <div class="body">
                            <div class="title">Mirna Šostarko</div>
                            <div class="position">stručna suradnica</div>
                            <ul class="social-icon"></ul>
                        </div>
                    </div>
                </div>

                <div class="col-sm-3 offset-sm-3 col-md-3">
                    <div class="rs-team-1">
                        <div class="media"><img src="{{ asset('storage/images/our_team/jelena-stefanovic.jpg') }}" alt="Jelena Stefanović"></div>
                        <div class="body">
                            <div class="title">Jelena Stefanović</div>
                            <div class="position">voditeljica ureda</div>
                            <ul class="social-icon"></ul>
                        </div>
                    </div>
                </div>

                <div class="col-sm-3 col-md-3">
                    <div class="rs-team-1">
                        <div class="media"><img src="{{ asset('storage/images/our_team/roberta-verner.jpg') }}" alt="Roberta Verner"></div>
                        <div class="body">
                            <div class="title">Roberta Verner</div>
                            <div class="position">stručna suradnica na administrativnim i financijskim poslovima</div>
                            <ul class="social-icon"></ul>
                        </div>
                    </div>
                </div>--}}


            </div>

        </div>
    </div>
</div>

<!-- OUR PATRONS -->
<x-our-patrons-component />
@endsection
