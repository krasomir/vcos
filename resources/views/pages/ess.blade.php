@extends('layouts.head')
@section('title', $program->title)
@section('meta-description', __('Europske snage solidarnosti program je Europske komisije pokrenut 2018. godine kojim se želi omogućiti mladim osobama sudjelovanje u aktivnostima s naglašenom solidarnom dimenzijom'))
@section('meta-keywords', __('ciljevi, vizija, misija, vizija društva, motivacija'))

@section('og_url', config('app.url') . '/' . app()->getLocale() . '/ambasadori-europskih-snaga-solidarnosti')
@section('og_title', $program->title)
@section('og_description', __('Europske snage solidarnosti program je Europske komisije pokrenut 2018. godine kojim se želi omogućiti mladim osobama sudjelovanje u aktivnostima s naglašenom solidarnom dimenzijom'))
@section('og_image', asset('storage/images/volonterski_centar_osijek_logo_hd_logo_hd.jpg'))
@section('og_type', 'article')
@section('og_locale', 'hr_HR')

@section('content')
<!-- BANNER -->
 <x-banner-component :request-path="Request::path()" 
    :name="$program->title" 
    :extra-breadcrumb="route('vcos', app()->getLocale())" 
    extra-breadcrumb-name="Volonterski centar Osijek"
/>

<!--  -->
<div class="section">
    <div class="content-wrap">
        <div class="container">
            <div class="row">

                <!-- ess -->
                <div class="col-sm-12 col-md-12">
                {!! $program->content !!}
                </div>
            </div>
        </div>
    </div>
</div>
@endsection