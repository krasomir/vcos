@extends('layouts.head')
@section('title', __('Otvorene mogućnosti za volontiranje'))
@section('meta-description', __('Volonterski centar usmjeren je prema viziji društva u kojem su građani motivirani činiti pozitivne promjene u svojim zajednicama'))
@section('meta-keywords', __('Volonteri, centar, Osijek, organizacija, Volonterski'))

@section('og_url', config('app.url') . '/' . app()->getLocale() . '/open-volunteering-opportunities')
@section('og_title', __('Otvorene mogućnosti za volontiranje'))
@section('og_description', __('Volonterski centar usmjeren je prema viziji društva u kojem su građani motivirani činiti pozitivne promjene u svojim zajednicama'))
@section('og_image', asset('storage/images/volonterski_centar_osijek_logo_hd.jpg'))
@section('og_type', 'article')
@section('og_locale', 'hr_HR')

@section('content')
    <!-- BANNER -->
     <x-banner-component :request-path="Request::path()" 
        :name="__('Otvorene mogućnosti za volontiranje')" 
    />

    <!-- news for open volunteereng opportunities -->
    <div class="section">
        <div class="content-wrap">
            <div class="container">
                <div class="row">

                    @foreach($posts as $post)
                    <!-- news box -->
                    <div class="col-sm-6 col-md-4">
                        <div class="box-news-1">
                            <div class="media gbr">
                                @if($post->image_name)
                                    <img src="{{ asset('storage/images/news/thumb/' . $post->image_name) }}" alt="{{ $post->slug }}" class="img-fluid">
                                @elseif($post->id < config('app.last_old_post_id') && $post->getOldPostImagePath())
                                    <img src="{{ asset('storage' . $post->getOldPostImagePath()->crop_thumb) }}" alt="{{ $post->slug }}" class="img-fluid">
                                @endif
                            </div>
                            <div class="body">
                                <div class="title">
                                    <a href="{{ route('open-volunteering-opportunities-details', ['lang' => app()->getLocale(), 'post' => $post]) }}" title="{{ $post->name }}">
                                        <h3>{{ $post->name }}</h3>
                                    </a>
                                </div>
                                <div>
                                    <p class="mt-2">
                                    @if($post->preview)
                                        {!! $post->shortenPreview() !!}
                                    @else
                                        {!! $post->shortenText() !!}
                                    @endif
                                    </p>
                                </div>
                                <div class="meta">
                                    <span class="date"><i class="fa fa-clock-o"></i> {{ $post->datetime_on->format('d.m.Y.') }}</span>
                                </div>
                            </div>
                        </div>
                    </div>
                    @endforeach
                </div>             

                <!-- pagination -->
				<div class="row">
					<div class="col-sm-12 col-md-12">						
						<nav aria-label="Page navigation">
                        {{ $posts->links('components.custom-pagination') }}
						</nav>
					</div>
				</div>
            </div>
        </div>
    </div>

    
    <!-- OUR PATRONS -->
    <x-our-patrons-component />
@endsection