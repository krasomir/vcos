@extends('layouts.head-light')
@section('title', __('404 Not found'))

@section('content')
    <!-- main 404 -->
    <div class="section">
		<div class="content-wrap">
			<div class="container">
				<div class="row">
					<div class="col-sm-12 col-md-6">
						<img src="{{ asset('storage/images/404.jpg') }}" alt="404 not found" class="img-fluid img-404">
					</div>
					<div class="col-sm-12 col-md-4 text-center">
						<p class="title-404 color-primary">4<span class="color-secondary">0</span>4</p>
						<p class="subtitle-404 color-secondary"><strong>{{ __('Stranica') }}</strong> {{ __('nije pronađena') }}!!</p>
						<p class="uk18">{{ __('Stranica koju tražite premještena je, uklonjena ili je preimenovana ili možda čak nikad nije niti postojala.') }}</p>
						<div class="spacer-30"></div>
						<a href="{{ route('home', ['lang' => config('app.locale')]) }}" class="btn btn-secondary">{{ strtoupper(__('Povratak na naslovnicu')) }}</a>
					</div>
				</div>
			</div>
		</div>
    </div>
    
    <!-- CTA -->
	<x-our-patrons-component />

@endsection


