<div class="header header-1">
    	<!-- TOPBAR -->
    	<div class="topbar">
			<div class="container">
				<div class="row">
					<div class="col-sm-7 col-md-6">
						<!-- dynamic slogans -->
						<span>DKolektiv - </span>
						<span>
							<p class="mt-2 dynamic-slogans" style="display: inline-block;">
								<em>{{ __('Volonterski centar Osijek') }}</em>|
								<em>{{ __('Drugi na prvom mjestu') }}</em>|
								<em>{{ __('Mali kolektiv za veliki kolektiv') }}</em>|
								<em>{{ __('Vrline znanja') }}</em>|
								<em>{{ __('Energija srca') }}</em>
							</p>
						</span>
							
					</div>
					<div class="col-sm-5 col-md-6">
						<div class="sosmed-icon pull-right">
							<a href="https://www.facebook.com/dkolektiv" target="_blank">
								<i class="fa fa-facebook"></i>
							</a> 
							<a href="https://twitter.com/DKolektiv_Os" class="p-twitter" target="_blank">
								<i class="fa fa-twitter"></i>
							</a> 
							<a href="https://www.youtube.com/user/osvolonteri" class="p-youtube" target="_blank">
								<i class="fa fa-youtube"></i>
							</a> 
							<a href="https://www.linkedin.com/in/volonterski-centar-osijek-6137b2107/" class="p-linkedin" target="_blank">
								<i class="fa fa-linkedin"></i>
							</a> 
							<a href="https://www.instagram.com/dkolektiv_osijek/" class="p-instagram" target="_blank">
								<i class="fa fa-instagram"></i>
							</a> 
						</div>
					</div>
				</div>
			</div>
		</div>

    	<!-- MIDDLE BAR -->
		<div class="middlebar">
			<div class="container">				
				
				<div class="contact-info">
					<!-- INFO 1 -->
					<div class="box-icon-1">
						<div class="icon">
							<div class="fa fa-envelope-o"></div>
						</div>
						<div class="body-content">
							<div class="heading">{{ __('E-mail') }} :</div>
							<a href="mailto:{{ config('mail.from.address') }}">{{ config('mail.from.address') }}</a>
						</div>
					</div>
					<!-- INFO 3 -->
					<div class="box-icon-1">
						<div class="icon">
							<div class="fa fa-phone"></div>
						</div>
						<div class="body-content">
							<div class="heading">{{ __('Nazovite nas') }} :</div>
							<a href="tel:+385 31 211 306">+385 31 211 306</a>
						</div>
					</div>
					
				</div>
			</div>
		</div>

		<!-- NAVBAR SECTION -->
		<div class="navbar-main">
			<div class="container">
			    <nav class="navbar navbar-expand-lg">
			        <a class="navbar-brand" href="{{ route('home', app()->getLocale()) }}">
						<img src="{{ asset('storage/images/dkolektiv_logo_sml.png') }}" alt="dKolektiv" style="height:60px;" />
					</a>
			        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
			            <span class="navbar-toggler-icon"></span>
			        </button>
			        <div class="collapse navbar-collapse" id="navbarNavDropdown">
			            <ul class="navbar-nav">
			                <li class="nav-item dropdown">
			                    <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
						          {{ strtoupper(__('O nama')) }}
						        </a>
			                    <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
	          						<a class="dropdown-item" href="{{ route('mision-vision-goals', app()->getLocale()) }}">{{ __('Misija, vizija, ciljevi') }}</a>
	          						<a class="dropdown-item" href="{{ route('our-team', app()->getLocale()) }}">{{ __('Tko čini DKolektiv?') }}</a>
	          						<a class="dropdown-item" href="{{ route('board-of-directors', app()->getLocale()) }}">{{ __('Upravni odbor') }}</a>
	          						<a class="dropdown-item" href="{{ route('reports', app()->getLocale()) }}">{{ __('Izvještaji') }}</a>
	          						<a class="dropdown-item" href="{{ route('sources-of-funding', app()->getLocale()) }}">{{ __('Izvori financiranja') }}</a>
							    </div>
			                </li>
			                <li class="nav-item dropdown">
			                    <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
						          {{ strtoupper(__('Projekti')) }}
						        </a>
			                    <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
			                    	<a class="dropdown-item" href="{{ route('active-projects', app()->getLocale()) }}">{{ __('Aktivni projekti') }}</a>
	          						<a class="dropdown-item" href="{{ route('archive-of-projects', app()->getLocale()) }}">{{ __('Arhiva projekata') }}</a>
							    </div>
							</li>
							@if(config('app.locale') === 'hr')
			                <li class="nav-item">
			                    <a class="nav-link" href="{{ route('publications', app()->getLocale()) }}" role="button" >
						          {{ strtoupper(__('Publikacije')) }}
						        </a>
			                </li>
			                <li class="nav-item">
			                    <a class="nav-link" href="{{ route('news', app()->getLocale()) }}">
						          {{ strtoupper(__('Novosti')) }}
						        </a>
							</li>
							@endif
							
							@if(isset($_COOKIE['show-login-button']))
			                <li class="nav-item">
			                    <a class="nav-link" href="{{ route('admin-home', app()->getLocale()) }}">
						          {{ strtoupper(__('Login')) }}
						        </a>
							</li>
							@endif

			            </ul>
			        </div>
			    </nav> <!-- -->

			</div>
		</div>

    </div>