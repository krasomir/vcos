<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>@yield('title') - {{ config('app.name') }}</title>

    <!-- favicon -->
    <link rel="apple-touch-icon" sizes="180x180" href="{{ asset('apple-touch-icon.png') }}">
    <link rel="icon" type="image/png" sizes="32x32" href="{{ asset('favicon-32x32.png') }}">
    <link rel="icon" type="image/png" sizes="16x16" href="{{ asset('favicon-16x16.png') }}">
    <link rel="manifest" href="{{ asset('site.webmanifest') }}">

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>

    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">
    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">

    <!-- font awesome -->
    <link href="{{ asset('css/font-awesome-free.min.css') }}" rel="stylesheet">

    <!-- datetime picker -->
    <link href="{{ asset('css/vendor/daterangepicker.css') }}" rel="stylesheet">
    <link href="{{ asset('css/vendor/jquery-ui.css') }}" rel="stylesheet">

    <!-- datatable -->
    <link href="https://cdn.datatables.net/1.10.21/css/dataTables.bootstrap4.min.css" rel="stylesheet">

    <!-- livewire styles -->
    @livewireStyles

    <!-- admin lte https://adminlte.io/docs/3.0/index.html -->
    <link href="{{ asset('css/adminlte.altered.css?v=' . config('app.asset_version')) }}" rel="stylesheet">
    <link href="{{ asset('css/custom.css?v=' . config('app.asset_version')) }}" rel="stylesheet">

    <!-- switchery -->
    <link href="{{ asset('css/vendor/switchery.min.css') }}" rel="stylesheet">



</head>
<body class="sidebar-mini sidebar-open">
    <div id="app" class="wrapper">
        @if(Auth::check())
            @include('layouts.admin-header')

            @include('layouts.admin-sidebar')

            <main class="admin-section content-wrapper pt-5">
                <!-- error section for all screens -->
                    <div class="container">
                        <div class="row justify-content-center">
                            <div class="alert-messages col-md-12">
                                @if ($errors->any() && (Route::currentRouteName() === 'admin-users' || Route::currentRouteName() === 'media-library'))
                                    <div class="alert alert-danger alert-dismissible">
                                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                                        @foreach ($errors->all() as $error)
                                            <p class="mb-0">{{ $error }}</p>
                                        @endforeach
                                    </div>
                                @endif
                                @if(session('success'))
                                    <div class="alert alert-success alert-dismissible">
                                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                                        <p class="mb-0">{{ session('success') }}</p>
                                    </div>
                                @endif
                            </div>
                        </div>
                    </div>
                @yield('content')
            </main>

            @include('layouts.admin-footer')
        @else
            <main class="my-5">
                @yield('content')
            </main>
        @endif
    </div>
@yield('js')
</body>
</html>
