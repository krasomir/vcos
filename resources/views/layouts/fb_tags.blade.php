<meta property="og:url" content="@yield('og_url')" />
<meta property="og:title" content="@yield('og_title')" />
<meta property="og:description" content="@yield('og_description')" />
<meta property="og:image" content="@yield('og_image')" />
<meta property="fb:app_id" content="1668544280031855" />
<meta property="og:type" content="@yield('og_type')" />
<meta property="og:locale" content="@yield('og_locale')" />