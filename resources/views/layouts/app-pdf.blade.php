<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>@yield('title')</title>
    <!-- css -->
    <link href="{{ asset('css/custom-pdf.css?v=' . config('app.asset_version')) }}" rel="stylesheet">

</head>
<body>
    @yield('content')
</body>
</html>
