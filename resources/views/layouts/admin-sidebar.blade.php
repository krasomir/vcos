<aside class="main-sidebar sidebar-dark-teal elevation-4">
    <!-- Brand Logo -->
    <a href="{{ route('home', ['lang' => config('app.locale')]) }}" class="brand-link">
        <img src="{{ asset('storage/images/d_logo_box.png') }}" alt="{{ config('app.name') }} Logo" class="brand-image">
        <span class="brand-text font-weight-light text-teal">&nbsp;
        <img src="{{ asset('storage/images/kolektiv_logo_box.png') }}" alt="{{ config('app.name') }} Logo" class="brand-image-box">
        </span>
    </a>

    <!-- Sidebar -->
    <div class="sidebar">
        <!-- Sidebar user panel (optional) -->
        <div class="user-panel mt-3 pb-3 mb-3 d-flex">
            <div class="image">
                <img src="{{ asset('storage/images/users/default_user.png') }}" class="img-circle elevation-2" alt="User Image">
            </div>
            <div class="info">
                <a href="{{ route('admin-user-profile', app()->getLocale()) }}" class="d-block">{{ auth()->user()->name }}</a>
            </div>
        </div>

        <!-- Sidebar Menu -->
        <nav class="mt-2">
            <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
                <li class="nav-item">
                    <a href="{{ route('admin-home', app()->getLocale()) }}" class="nav-link @if(Route::currentRouteName() == 'admin-home') active @endif">
                        <i class="nav-icon fas fa-tachometer-alt"></i>
                        <p>Dashbord</p>
                    </a>
                </li>
                <!-- Add icons to the links using the .nav-icon class
               with font-awesome or any other icon font library -->
                <!-- <li class="nav-item has-treeview">
                    <a href="#" class="nav-link">
                        <i class="nav-icon fas fa-tachometer-alt"></i>
                        <p>
                            Starter Pages
                            <i class="right fas fa-angle-left"></i>
                        </p>
                    </a>
                    <ul class="nav nav-treeview">
                        <li class="nav-item">
                            <a href="#" class="nav-link">
                                <i class="far fa-circle nav-icon"></i>
                                <p>Active Page</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="#" class="nav-link">
                                <i class="far fa-circle nav-icon"></i>
                                <p>Inactive Page</p>
                            </a>
                        </li>
                    </ul>
                </li> -->
                <!-- korisnici -->
                <li class="nav-item">
                    <a href="{{ route('admin-users', app()->getLocale()) }}" class="nav-link @if(Request::is('hr/admin/user') || Request::is('hr/admin/user/*')) active @endif">
                        <i class="nav-icon fas fa-users"></i>
                        <p>Korisnici</p>
                    </a>
                </li>
                <!-- dkolektiv tim -->
                <li class="nav-item">
                    <a href="{{ route('teams.index', app()->getLocale()) }}" class="nav-link @if(Request::is('hr/admin/teams') || Request::is('hr/admin/teams/*')) active @endif">
                        <i class="nav-icon fas fa-user-circle"></i>
                        <p>DKolektiv Tim</p>
                    </a>
                </li>
                <!-- volonteri -->
                <!-- <li class="nav-item">
                    <a href="{{ route('volunteers', app()->getLocale()) }}" class="nav-link @if(Request::is('hr/admin/volunteers') || Request::is('hr/admin/volunteers/*')) active @endif">
                        <i class="nav-icon far fa-address-book"></i>
                        <p>Volonteri</p>
                    </a>
                </li> -->
                <!-- organizacije -->
                <!-- <li class="nav-item has-treeview
                    @if(
                            (Request::is('hr/admin/organisations') || Request::is('hr/admin/organisations/*')) ||
                            (Request::is('hr/admin/organisation-strength-of-participations') || Request::is('hr/admin/organisation-strength-of-participations/*'))
                        )
                        menu-open
                    @endif">
                    <a href="#" class="nav-link
                        @if(
                                (Request::is('hr/admin/organisations') || Request::is('hr/admin/organisations/*')) ||
                                (Request::is('hr/admin/organisation-strength-of-participations') || Request::is('hr/admin/organisation-strength-of-participations/*'))
                            )
                            active
                        @endif">
                        <i class="nav-icon fas fa-sitemap"></i>
                        <p>
                            Organizacije
                            <i class="right fas fa-angle-left"></i>
                        </p>
                    </a>
                    <ul class="nav nav-treeview">
                        <li class="nav-item">
                            <a href="{{ route('organisations', app()->getLocale()) }}" class="nav-link @if(Request::is('hr/admin/organisations') || Request::is('hr/admin/organisations/*')) active @endif">
                                <i class="nav-icon far fa-circle"></i>
                                <p>Organizacije</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="{{ route('organisation-strength-of-participations', app()->getLocale()) }}" class="nav-link @if(Request::is('hr/admin/organisation-strength-of-participations') || Request::is('hr/admin/organisation-strength-of-participations/*')) active @endif">
                                <i class="nav-icon far fa-circle"></i>
                                <p>Snaga sudjelovanja</p>
                            </a>
                        </li>
                    </ul>
                </li>  -->
                <!-- blog -->
                <li class="nav-item">
                    <a href="{{ route('admin-posts', app()->getLocale()) }}" class="nav-link  @if(Request::is('hr/admin/post') || Request::is('hr/admin/post/*')) active @endif">
                        <i class="nav-icon fas fa-newspaper"></i>
                        <p>Novosti</p>
                    </a>
                </li>
                <!-- projekti -->
                <li class="nav-item">
                    <a href="{{ route('admin-projects', app()->getLocale()) }}" class="nav-link @if(Request::is('hr/admin/projects') || Request::is('hr/admin/project/*')) active @endif">
                        <i class="nav-icon fas fa-project-diagram"></i>
                        <p>Projekti</p>
                    </a>
                </li>
                <!-- programi -->
                <li class="nav-item">
                    <a href="{{ route('programs.index', app()->getLocale()) }}" class="nav-link @if(Request::is('hr/admin/programs') || Request::is('hr/admin/programs/*')) active @endif">
                        <i class="nav-icon fas fa-columns"></i>
                        <p>Programi</p>
                    </a>
                </li>
                <!-- izvještaji -->
                <li class="nav-item">
                    <a href="{{ route('admin-reports', app()->getLocale()) }}" class="nav-link @if(Request::is('hr/admin/reports') || Request::is('hr/admin/report/*')) active @endif">
                        <i class="nav-icon fas fa-tasks"></i>
                        <p>Izvještaji</p>
                    </a>
                </li>
                <!-- publikacije -->
                <li class="nav-item">
                <a href="{{ route('publications.index', app()->getLocale()) }}" class="nav-link @if(Request::is('hr/admin/publications') || Request::is('hr/admin/publications/*')) active @endif">
                        <i class="nav-icon fas fa-book"></i>
                        <p>Publikacije</p>
                    </a>
                </li>
                <!-- aktivnosti -->
                <li class="nav-item">
                    <a href="{{ route('podcasts.index', app()->getLocale()) }}" class="nav-link @if(Request::is('hr/admin/podcasts') || Request::is('hr/admin/podcasts/*')) active @endif">
                        <i class="nav-icon fas fa-microphone-alt"></i>
                        <p>Podcast epizode</p>
                    </a>
                </li>
                <!-- slider -->
                <li class="nav-item">
                    <a href="{{ route('sliders.index', app()->getLocale()) }}" class="nav-link  @if(Request::is('hr/admin/sliders') || Request::is('hr/admin/sliders/*')) active @endif">
                        <i class="nav-icon far fa-images"></i>
                        <p>Slider</p>
                    </a>
                </li>
                <!-- banneri -->
                <li class="nav-item">
                    <a href="{{ route('banners.index', app()->getLocale()) }}" class="nav-link @if(Request::is('hr/admin/banners') || Request::is('hr/admin/banners/*')) active @endif">
                        <i class="nav-icon fas fa-pager"></i>
                        <p>Banneri na stranicama</p>
                    </a>
                </li>
                <!-- media library -->
                <li class="nav-item">
                    <a href="{{ route('media-library', app()->getLocale()) }}" class="nav-link @if(Request::is('hr/admin/media-library')) active @endif">
                        <i class="nav-icon fas fa-database"></i>
                        <p>Medijateka</p>
                    </a>
                </li>
                <!-- newsletter -->
                <li class="nav-item has-treeview
                    @if(
                            (Request::is('hr/admin/volunteers') || Request::is('hr/admin/volunteers/*')) ||
                            (Request::is('hr/admin/organisations') || Request::is('hr/admin/organisations/*')) ||
                            (Request::is('hr/admin/organisation-strength-of-participations') || Request::is('hr/admin/organisation-strength-of-participations/*')) ||
                            (Request::is('hr/admin/failed-subscriptions') || Request::is('hr/admin/failed-subscriptions/*'))
                        )
                        menu-open
                    @endif">
                    <a href="#" class="nav-link">
                        <i class="nav-icon fas fa-mail-bulk"></i>
                        <p>
                            Newsletter
                            <i class="right fas fa-angle-left"></i>
                        </p>
                    </a>
                    <ul class="nav nav-treeview">
                        <li class="nav-item">
                            <a href="{{ route('volunteers', app()->getLocale()) }}" class="nav-link @if(Request::is('hr/admin/volunteers') || Request::is('hr/admin/volunteers/*')) active @endif">
                                <i class="nav-icon far fa-circle"></i>
                                <p>Volonteri</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="{{ route('organisations', app()->getLocale()) }}" class="nav-link @if(Request::is('hr/admin/organisations') || Request::is('hr/admin/organisations/*')) active @endif">
                                <i class="nav-icon far fa-circle"></i>
                                <p>Organizacije</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="{{ route('organisation-strength-of-participations', app()->getLocale()) }}" class="nav-link @if(Request::is('hr/admin/organisation-strength-of-participations') || Request::is('hr/admin/organisation-strength-of-participations/*')) active @endif">
                                <i class="nav-icon far fa-circle"></i>
                                <p>Snaga sudjelovanja</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="{{ route('failed-subscriptions.index', app()->getLocale()) }}" class="nav-link @if(Request::is('hr/admin/failed-subscriptions') || Request::is('hr/admin/failed-subscriptions/*')) active @endif">
                                <i class="nav-icon far fa-circle"></i>
                                <p>Neuspjele pretplate</p>
                            </a>
                        </li>
                    </ul>
                </li>
                @if(config('app.env') === 'local')
                <li class="nav-item has-treeview
                    @if(
                        (Request::is('hr/admin/subscribe-to-subscribers-list') || Request::is('hr/admin/subscribe-to-subscribers-list/*')) ||
                        (Request::is('hr/admin/subscribe-to-druge-zupanije-list') || Request::is('hr/admin/subscribe-to-druge-zupanije-list/*')) ||
                        (Request::is('hr/admin/subscribe-to-osjecko-baranjska-list') || Request::is('hr/admin/subscribe-to-osjecko-baranjska-list/*')) ||
                        (Request::is('hr/admin/subscribe-to-vukovarsko-srijemska-list') || Request::is('hr/admin/subscribe-to-vukovarsko-srijemska-list/*')) ||
                        (Request::is('hr/admin/subscribe-to-pozesko-slavonska-list') || Request::is('hr/admin/subscribe-to-pozesko-slavonska-list/*')) ||
                        (Request::is('hr/admin/subscribe-to-brodsko-posavska-list') || Request::is('hr/admin/subscribe-to-brodsko-posavska-list/*'))
                    )
                        menu-open
                    @endif">
                    <a href="#" class="nav-link">
                        <i class="nav-icon fas fa-mail-bulk"></i>
                        <p>
                            MailChimp liste
                            <i class="right fas fa-angle-left"></i>
                        </p>
                    </a>
                    <ul class="nav nav-treeview">
                        <li class="nav-item">
                            <a href="{{ route('subscribe-to-subscribers-list', app()->getLocale()) }}" class="nav-link @if(Request::is('hr/admin/subscribe-to-subscribers-list') || Request::is('hr/admin/subscribe-to-subscribers-list/*')) active @endif">
                                <i class="nav-icon far fa-circle"></i>
                                <p>Volonteri</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="{{ route('subscribe-to-osjecko-baranjska-list', app()->getLocale()) }}" class="nav-link @if(Request::is('hr/admin/subscribe-to-osjecko-baranjska-list') || Request::is('hr/admin/subscribe-to-osjecko-baranjska-list/*')) active @endif">
                                <i class="nav-icon far fa-circle"></i>
                                <p>Osječko-baranjska</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="{{ route('subscribe-to-vukovarsko-srijemska-list', app()->getLocale()) }}" class="nav-link @if(Request::is('hr/admin/subscribe-to-vukovarsko-srijemska-list') || Request::is('hr/admin/subscribe-to-vukovarsko-srijemska-list/*')) active @endif">
                                <i class="nav-icon far fa-circle"></i>
                                <p>Vukovarsko-srijemska</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="{{ route('subscribe-to-pozesko-slavonska-list', app()->getLocale()) }}" class="nav-link @if(Request::is('hr/admin/subscribe-to-pozesko-slavonska-list') || Request::is('hr/admin/subscribe-to-pozesko-slavonska-list/*')) active @endif">
                                <i class="nav-icon far fa-circle"></i>
                                <p>Požeško-slavonska</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="{{ route('subscribe-to-brodsko-posavska-list', app()->getLocale()) }}" class="nav-link @if(Request::is('hr/admin/subscribe-to-brodsko-posavska-list') || Request::is('hr/admin/subscribe-to-brodsko-posavska-list/*')) active @endif">
                                <i class="nav-icon far fa-circle"></i>
                                <p>Brodsko-posavska</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="{{ route('subscribe-to-druge-zupanije-list', app()->getLocale()) }}" class="nav-link @if(Request::is('hr/admin/subscribe-to-druge-zupanije-list') || Request::is('hr/admin/subscribe-to-druge-zupanije-list/*')) active @endif">
                                <i class="nav-icon far fa-circle"></i>
                                <p>Druge županije</p>
                            </a>
                        </li>
                    </ul>
                @endif
            </ul>
        </nav>
        <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
</aside>
