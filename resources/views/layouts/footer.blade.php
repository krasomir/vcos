<!-- FOOTER SECTION -->
<div class="footer" >
    <div class="content-wrap">
        <div class="container">

            <div class="row">
                <div class="col-sm-12 col-md-12">
                    <div class="footer-item">
                        <x-logo-component />
                        <div class="spacer-30"></div>
                        <p class="mb-2">{{ __('Želimo biti inspiracija i podrška demokratskoj kulturi, razvoju civilnog društva i volonterstva, stvaranju prilika za jednake mogućnosti. DKolektiv je izrastao iz 15-godišnjeg djelovanja Volonterskog centra Osijek.') }}</p>
                        <a href="{{ route('mision-vision-goals', app()->getLocale()) }}"><i class="fa fa-angle-right"></i> {{ __('Pročitaj više') }}</a>
                    </div>
                </div>


                <div class="col-sm-6 col-md-6">
                    <div class="footer-item">
                        <div class="footer-title">
                            {{ strtoupper(__('O nama')) }}
                        </div>

                        <div class="row">
                            <div class="col col-sm-6 mw-210">
                                <ul class="list">
                                    <li><a href="{{ route('mision-vision-goals', app()->getLocale()) }}" title="{{ __('Misija, vizija, ciljevi') }}">{{ __('Vizija, misija, ciljevi') }}</a></li>
                                    <li><a href="{{ route('our-team', app()->getLocale()) }}" title="{{ __('Tim') }}">{{ __('Tko čini DKolektiv?') }}</a></li>
                                    <li><a href="{{ route('board-of-directors', app()->getLocale()) }}" title="{{ __('Upravni odbor') }}">{{ __('Upravni odbor') }}</a></li>
                                    <li><a href="{{ route('reports', app()->getLocale()) }}" title="{{ __('Izvještaji') }}">{{ __('Izvještaji') }}</a></li>
                                    <li><a href="{{ route('sources-of-funding', app()->getLocale()) }}" title="{{ __('Izvori financiranja') }}">{{ __('Izvori financiranja') }}</a></li>
                                </ul>
                            </div>
                            <div class="col col-sm-6 mw-210">
                                <ul class="list">
                                    <li><a href="{{ route('active-projects', app()->getLocale()) }}" title="{{ __('Aktivni projekti') }}">{{ __('Aktivni projekti') }}</a></li>
                                    <li><a href="{{ route('archive-of-projects', app()->getLocale()) }}" title="{{ __('Arhiva projekata') }}">{{ __('Arhiva projekata') }}</a></li>
                                    <li><a href="{{ route('publications', app()->getLocale()) }}" title="{{ __('Publikacije') }}">{{ __('Publikacije') }}</a></li>
                                    <li><a href="{{ route('news', app()->getLocale()) }}" title="{{ __('Novosti') }}">{{ __('Novosti') }}</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-sm-6 col-md-6">
                    <div class="footer-item">
                        <div class="footer-title">
                            {{ strtoupper(__('Gdje smo')) }}
                        </div>
                        <div class="row">
                            <div class="col col-sm-6 mw-210">
                                <ul class="list-info">
                                    <li>
                                        <div class="info-icon">
                                            <span class="fa fa-map-marker"></span>
                                        </div>
                                        <div class="info-text">Šetalište kardinala Franje Šepera 12,<br>31000 Osijek</div>
                                    </li>
                                    <li>
                                        <div class="info-icon">
                                            <span class="fa fa-phone"></span>
                                        </div>
                                        <div class="info-text">
                                            <a href="tel:+385 (31) 211 306">+385 (31) 211 306</a>
                                        </div>
                                    </li>
                                    <li>
                                        <div class="info-icon">
                                            <span class="fa fa-map-marker"></span>
                                        </div>
                                        <div class="info-text bold">Društveni Atelje</div>
                                        <div class="info-text">Županijska ulica 11,<br>31000 Osijek</div>
                                    </li>
                                </ul>
                            </div>
                            <div class="col col-sm-6 mw-210">
                                <ul class="list-info">
                                    <li>
                                        <div class="info-icon">
                                            <span class="fa fa-envelope"></span>
                                        </div>
                                        <div class="info-text">
                                            <a href="mailto:{{ config('mail.from.address') }}">{{ config('mail.from.address') }}</a>
                                        </div>
                                    </li>
                                    <li>
                                        <div class="info-icon">
                                            <span class="fa fa-id-card"></span>
                                        </div>
                                        <div class="info-text">OIB: 64735588578</div>
                                    </li>
                                    <li>
                                        <div class="info-icon">
                                            <span class="fa fa-credit-card"></span>
                                        </div>
                                        <div class="info-text">HR4723600001101885089, <br>kod Zagrebačke banke d.d.</div>
                                    </li>
                                    <li>
                                        <div class="info-icon">
                                            <span class="fa fa-credit-card"></span>
                                        </div>
                                        <div class="info-text">HR1824020061101042339, <br>kod Erste&Steiermärkische bank d.d</div>
                                    </li>
                                    <li>
                                        <div class="info-icon">
                                            <span class="fa fa-clock-o"></span>
                                        </div>
                                        <div class="info-text">{{ __('Pon - Pet') }} 08:00 - 16:00</div>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-sm-12 col-md-12">
                    <div class="footer-item text-center">
                        <div class="sosmed-icon primary">
                            <a href="https://www.facebook.com/dkolektiv" class="p-facebook" target="_blank">
								<i class="fa fa-facebook"></i>
							</a>
							<a href="https://twitter.com/DKolektiv_Os" class="p-twitter" target="_blank">
								<i class="fa fa-twitter"></i>
							</a>
							<a href="https://www.youtube.com/user/osvolonteri" class="p-youtube" target="_blank">
								<i class="fa fa-youtube"></i>
							</a>
							<a href="https://www.linkedin.com/in/volonterski-centar-osijek-6137b2107/" class="p-linkedin" target="_blank">
								<i class="fa fa-linkedin"></i>
							</a>
							<a href="https://www.instagram.com/dkolektiv_osijek/" class="p-instagram" target="_blank">
								<i class="fa fa-instagram"></i>
							</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="fcopy">
        <div class="container">
            <div class="row">
                <div class="col-sm-12 col-md-12">
                    <p class="ftex">
                        Copyright {{ date('Y') }} &copy; <span class="color-primary">DKolektiv</span>
                        <span class="float-right">by: <span class="color-primary">Codit</span></span>
                    </p>
                </div>
            </div>
        </div>
    </div>

</div>
<script src="https://kit.fontawesome.com/69a44be531.js" crossorigin="anonymous"></script>

<!-- JS VENDOR -->
<script src="{{ asset('js/vendor/jquery.min.js') }}"></script>
<script src="{{ asset('js/vendor/bootstrap.min.js') }}"></script>
<script src="{{ asset('js/vendor/owl.carousel.js') }}"></script>
<script src="{{ asset('js/vendor/jquery.magnific-popup.min.js') }}"></script>

<!-- Include jQuery plugin -->
<script src="{{ asset('js/vendor/jquery.auto-text-rotating.min.js')}}"></script>

<!-- datetime picker -->
<script src="{{ asset('js/vendor/moment.js') }}"></script>
<script src="{{ asset('js/vendor/daterangepicker.js') }}"></script>
<script src="{{ asset('js/vendor/jquery-ui.min.js') }}"></script>

<!-- SENDMAIL -->
<script src="{{ asset('js/vendor/validator.min.js') }}"></script>
<script src="{{ asset('js/vendor/form-scripts.js') }}"></script>
<script src="{{ asset('js/script.js?v='. config('app.asset_version')) }}"></script>

<!-- laravel share -->
<script src="{{ asset('js/share.js?v='. config('app.asset_version')) }}"></script>

<!-- custom -->
<script src="{{ asset('js/main.js?v='. config('app.asset_version')) }}"></script>
@yield('js')
