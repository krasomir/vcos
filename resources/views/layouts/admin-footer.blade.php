<footer class="main-footer">
    <!-- Default to the left -->
    <strong>Copyright © {{ date('Y') }} <a href="#" class="text-orange">dKolektiv</a>.</strong> Sva prava pridržana
</footer>
<div id="sidebar-overlay"></div>
<!-- js -->
<script src="{{ asset('js/vendor/jquery.min.js') }}"></script>
<!-- datetime picker -->
<script src="{{ asset('js/vendor/moment.js') }}"></script>
<script src="{{ asset('js/vendor/daterangepicker.js') }}"></script>
<script src="{{ asset('js/vendor/jquery-ui.min.js') }}"></script>

<script src="{{ asset('js/vendor/switchery.min.js') }}"></script>
<script src="{{ asset('js/vendor/datatables.min.js') }}" defer></script>

<script src="{{ asset('js/adminlte.js') }}"></script>
<script src="{{ asset('js/admin.js?v=' . config('app.asset_version')) }}"></script>
<!-- ckEditor scripts -->
<!-- <script src="https://cdn.ckeditor.com/4.14.0/full-all/ckeditor.js"></script> -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/ckeditor/4.14.0/ckeditor.js"></script>
<script type="text/javascript" src="{{ asset('/js/ckfinder/ckfinder.js') }}"></script>
<script>CKFinder.config( { connectorPath: "{{ asset('ckfinder/connector') }}" } );</script>
<!-- livewire scripts -->
@livewireScripts
<!-- Delete Modal -->
<div class="modal fade" id="confirmDelete" tabindex="-1" role="dialog" aria-labelledby="confirmDeleteLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header bg-danger">
                <h5 class="modal-title" id="confirmDeleteLabel">Modal title</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                ...
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Zatvori</button>
                <form id="deleteForm" action="" method="POST">
                    @csrf
                    @method('DELETE')
                    <input type="hidden" name="id" id="id">
                    <input type="hidden" name="mediaPath" id="mediaPath">
                    <button type="submit" class="btn btn-danger">Obriši</button>
                </form>
            </div>
        </div>
    </div>
</div>
