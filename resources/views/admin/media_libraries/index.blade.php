@extends('layouts.app')
@section('title', 'Medijateka')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    <h3 class="card-title">Medijateka</h3>
                </div>

                <div class="card-body">
                    <div class="row">
                        <div class="col">
                            <div class="text-bold">Dokumenti ({{ count($documents) }})</div>
                            @foreach ($documents as $key => $document)
                                <div title="{{ $document->name }}" class="thumbnail">
                                    <a href="{{ $document->file_path }}" target="_blank">
                                        <i class="far fa-file-alt"></i>
                                    </a>
                                    <div class="caption">
                                        <p class="mb-0">{{ $document->name }}</p>
                                        <small class="small">
                                            {{ $document->formated_filesize }} {{ $document->created_at }}
                                        </small>
                                    </div> 
                                    <a href="#" 
                                        data-toggle="modal" 
                                        data-target="#confirmDelete" 
                                        data-title="Brisanje dokumenta" 
                                        data-media-type="document"
                                        data-media-path="{{ $document->media_path }}" 
                                        data-message="Da li zaista želiš obrisati dokument - {{ $document->name }}?" 
                                        data-delete-route="{{ route('media-delete', [app()->getLocale(), $document->name]) }}" 
                                        class="hide btn btn-icon-only btn-danger red pull-right"
                                        >
                                        <i class="fa fa-trash"></i>
                                    </a>
                                </div>
                            @endforeach 
                        </div>
                    </div>
                    <div class="row mt-5">
                        <div class="col">
                            <div class="text-bold">Slike ({{ count($images) }})</div>
                            @foreach ($images as $key => $image)                                
                                <div title="{{ $image->name }}" class="thumbnail">
                                    <a href="{{ $image->file_path }}" target="_blank">
                                        <img src="{{ $image->file_path }}" alt="">
                                    </a>
                                    <div class="caption">
                                        <p class="mb-0">{{ $image->name }}</p>
                                        <small class="small">
                                            {{ $image->formated_filesize }} {{ $image->created_at }}
                                        </small>
                                    </div> 
                                    <a href="#" 
                                        data-toggle="modal" 
                                        data-target="#confirmDelete" 
                                        data-title="Brisanje slike" 
                                        data-media-type="image"
                                        data-media-path="{{ $image->media_path }}" 
                                        data-message="Da li zaista želiš obrisati sliku - {{ $image->name }}?" 
                                        data-delete-route="{{ route('media-delete', [app()->getLocale(), $image->name]) }}" 
                                        class="hide btn btn-icon-only btn-danger red pull-right"
                                        >
                                        <i class="fa fa-trash"></i>
                                    </a>
                                </div>
                            @endforeach
                        </div>
                    </div>
                    
                    
                </div>
            </div>
        </div>
    </div>
</div>
@endsection