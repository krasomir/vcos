@extends('layouts.app')
@section('title', 'Uredi izvještaj')


@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    <h3 class="card-title">Uredi izvještaj</h3>
                    <span class="float-right">
                        <a href="{{ route('admin-reports', app()->getLocale()) }}">
                            <button class="btn btn-primary">
                                <span class="fa fa-arrow-left"></span>
                            </button>
                        </a>
                    </span>
                </div>

                <div class="card-body">
                    
                    <form action="{{ route('report-update', [app()->getLocale(), $report]) }}" id="updateReportForm" method="post" enctype="multipart/form-data">
                        @csrf
                        @method('PATCH')
                        <!--  -->
                        <div class="row">
                            <!-- prvi stupac -->
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="title">Naslov izvještaja</label>
                                    <input type="text" id="title" name="title" class="form-control @error('title') is-invalid @enderror" value="{{ old('title') ?? $report->title }}">
                                    @error('title')
                                        <div class="small text-danger">{{ $message }}</div>
                                    @enderror
                                </div>
                                <div class="form-group">
                                    <label for="report_type">Vrsta izvještaja</label>
                                    <select type="text" id="report_type" name="report_type" class="form-control @error('report_type') is-invalid @enderror">
                                        <option value="">-- izaberi vrstu izvještaja --</option>
                                        <option value="pdf" @if(old('report_type') === 'pdf') selected @endif>PDF</option>
                                        <option value="piktochart" @if(old('report_type') === 'piktochart' || $report->report_type === 'piktochart') selected @endif>Piktochart</option>
                                    </select>
                                    @error('report_type')
                                        <div class="small text-danger">{{ $message }}</div>
                                    @enderror
                                </div>
                                <div id="reportUrlGroup" class="form-group">
                                    <label>Izvještaj</label>
                                    <input type="text" name="report_url" class="form-control @error('report_url') is-invalid @enderror d-none" value="{{ old('report_url') ?? $report->report_url }}">
                                    <input type="file" name="report_url" class="form-control @error('report_url') is-invalid @enderror d-none" value="{{ old('report_url') }}">
                                    @if($report->report_url && $report->report_type === 'pdf')
                                        <div class="small">
                                            <a href="{{ asset($report->report_url) }}" target="_blank" rel="noopener noreferrer">
                                                {{ asset($report->report_url) }}
                                            </a>
                                        </div>
                                    @endif
                                    @error('report_url')
                                        <div class="small text-danger">{{ $message }}</div>
                                    @enderror
                                </div>
                                <div class="form-group">
                                    <label for="published">Objavi</label>
                                    <input type="checkbox" id="published" @if(old('published') || (int)$report->published === 1) checked @endif name="published" class="js-switch" data-switchery="true" value="1">
                                    @error('published')
                                        <div class="small text-danger">{{ $message }}</div>
                                    @enderror
                                </div>
                            </div>
                        </div>
                        
                        <!--  -->
                        <div class="form-group">
                            <button type="submit" class="btn btn-primary float-right">Spremi</button>
                        </div>
                    </form>

                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('js')
<script>
$(document).ready(function() {
    // switchery init
    let elem = document.querySelector('.js-switch');
    options = {
        color: "#FAA51A",
        size: "normal"
    };
    let init = new Switchery(elem, options);

    // handle report type change and show proper input type for report
    $("#report_type").on("change", function(e) {
        let type = $(this).val();
        showProperReportInput(type);        
    });

    setTimeout(() => {
       $("#report_type").trigger("change"); 
    }, 100);
    

});

function showProperReportInput(type) {
    switch (type) {
        case "pdf":
            $("#reportUrlGroup label").text("Izaberi file izvještaja za upload");
            $("#reportUrlGroup input[type=file]").removeClass("d-none");
            if (!$("#reportUrlGroup input[type=text]").hasClass("d-none")) {
                $("#reportUrlGroup input[type=text]").addClass("d-none");
                $("#reportUrlGroup input[type=text]").val("");
            } 
            break;
        case "piktochart":
            $("#reportUrlGroup label").text("Zaljepi URL/link izvještaja");
            $("#reportUrlGroup input[type=text]").removeClass("d-none");
            if (!$("#reportUrlGroup input[type=file]").hasClass("d-none")) {
                $("#reportUrlGroup input[type=file]").addClass("d-none");
                $("#reportUrlGroup input[type=file]").val("");
            }      
            break;        
        default:
            $("#reportUrlGroup label").text("Izvještaj");
            $("#reportUrlGroup input[type=text], #reportUrlGroup input[type=file]")
                .addClass("d-none")
                .val("");
            break;
    }
}
</script>
@endsection