@extends('layouts.app')
@section('title', 'Izvještaji')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    <h3 class="card-title">Izvještaji</h3>
                    <span class="float-right">
                        <a href="{{ route('report-create', app()->getLocale()) }}">
                            <button class="btn btn-primary">Novi izvještaj</button>
                        </a>
                    </span>
                </div>

                <div class="card-body">
                    <table id="reports-table" class="table table-bordered table-striped">
                        <thead>
                            <th>#</th>
                            <th>Naziv izvještaja</th>
                            <th>Izvještaj url</th>
                            <th>Tip izvještaja</th>
                            <th>Objavljeno</th>
                            <th>Akcije</th>
                        </thead>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('js') 
<script>
$(document).ready(function() {
    let usersTable = $('#reports-table').DataTable({
        processing: true,
        serverSide: true,
        responsive: true,
        deferRender: true,
        stateSave: true,
        lengthMenu: [[10, 25, 50, -1], [10, 25, 50, "Sve"]],
        order: [[0, 'desc']],
        ajax: {
            url: "{{ route('report-list', app()->getLocale()) }}"
        },
        columns: [
            { data: 'id', name: 'id' },
            { data: 'title', name: 'title'},
            { 
                data: 'report_url', 
                name: 'report_url'
            },
            { 
                data: 'report_type', 
                name: 'report_type'
            },
            { 
                data: 'published', 
                name: 'published',
                orderable: false
            },
            {
                data: 'action',
                name: 'action',
                orderable: false,
                width: '135px'
            }
        ],
        language: {
            url: "{{ asset('datatable_languages/' . config('app.locale') .'.json') }}"
        }
    });
    
    // publish/unpublish report
    $("#reports-table").on('change', '.custom-control-input', function(e) {

        let published = $(this).is(':checked') ? 1:0;
        let route = $(this).data('route');
        $(".alert-messages").empty();

        $.ajax({
            method: "PATCH",
            url: route,
            data: {published},
            cache: false,
            timeout: 30000,
            success: function(data) {
                if (data.error === false) {
                    let alert = `
                        <div class="alert alert-success alert-dismissible">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                            ${data.message}
                        </div>
                    `;
                    $(".alert-messages").append(alert);
                }
            },
            error: function(xhr, statusText, error) {
                $(".alert-messages").append(`<div class="alert alert-danger">${xhr.status} ${error}</div>`);
            }
        });
        
    });
});
</script>
@endsection