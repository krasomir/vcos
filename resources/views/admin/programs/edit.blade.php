@extends('layouts.app')
@section('title', 'Uredi program')


@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    <h3 class="card-title">Uredi program</h3>
                    <span class="float-right">
                        <a href="{{ route('programs.index', app()->getLocale()) }}">
                            <button class="btn btn-primary">
                                <span class="fa fa-arrow-left"></span>
                            </button>
                        </a>
                    </span>
                </div>

                <div class="card-body">
                    
                    <form action="{{ route('programs.update', [app()->getLocale(), $program]) }}" id="updateProgramForm" method="post" enctype="multipart/form-data">
                        @csrf
                        @method('PATCH')
                        <!--  -->
                        <div class="row">
                            <!--  -->
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="program_page">Stranica programa</label>
                                    <input type="hidden" name="program_page" value="{{ $program->program_page }}">
                                    <x-base-programs-component :selected="old('program_page') ?? $program->program_page" />
                                    @error('program_page')
                                        <div class="small text-danger">{{ $message }}</div>
                                    @enderror
                                </div>
                                <div class="form-group">
                                    <label for="title">Naziv programa</label>
                                    <input type="text" id="title" name="title" class="form-control @error('title') is-invalid @enderror" value="{{ old('title') ?? $program->title }}">
                                    @error('title')
                                        <div class="small text-danger">{{ $message }}</div>
                                    @enderror
                                </div>
                                <div class="form-group">
                                    <label for="slug">Slug/SEO naziva</label>
                                    <input type="text" id="slug" name="slug" readonly class="form-control @error('slug') is-invalid @enderror" value="{{ old('slug') ?? $program->slug }}">
                                    @error('slug')
                                        <div class="small text-danger">{{ $message }}</div>
                                    @enderror
                                </div>
                                <div class="form-group">
                                    <label for="content">Sadržaj</label>
                                    <textarea id="content" name="content" class="form-control @error('content') is-invalid @enderror">
                                        {{ old('content') ?? $program->content }}
                                    </textarea>
                                    @error('content')
                                        <div class="small text-danger">{{ $message }}</div>
                                    @enderror
                                </div>
                                <div id="unvHidden" class="form-group hidden d-none">
                                    <div class="form-group">
                                        <label for="link_to_pdf">Link do (pdf) dokumenta volonterskih nagrada</label>
                                        <input type="text" id="link_to_pdf" name="link_to_pdf" class="form-control @error('link_to_pdf') is-invalid @enderror" value="{{ old('link_to_pdf') ?? $program->link_to_pdf }}">
                                        @error('link_to_pdf')
                                            <div class="small text-danger">{{ $message }}</div>
                                        @enderror
                                    </div>
                                    <div class="form-group">
                                        <label for="link_title">Naziv linka</label>
                                        <input type="text" id="link_title" name="link_title" class="form-control @error('link_title') is-invalid @enderror" value="{{ old('link_title') ?? $program->link_title }}">
                                        @error('link_title')
                                            <div class="small text-danger">{{ $message }}</div>
                                        @enderror
                                    </div>
                                </div>
                            </div>
                        </div>
                        
                        <!--  -->
                        <div class="form-group">
                            <button type="submit" class="btn btn-primary float-right">Spremi</button>
                        </div>
                    </form>

                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('js')
<script>
$(document).ready(function() {
    // init selected program page
    let selectedProgramPage = @json($program->program_page);
    // ck editors init
    let editorContent = CKEDITOR.replace('content', {
        language: 'hr',
        allowedContent: true,
        toolbarGroups: [
            { name: 'clipboard',   groups: [ 'clipboard', 'undo' ] },
            { name: 'links' },
            { name: 'others' },
            { name: 'basicstyles', groups: [ 'basicstyles', 'cleanup' ] },
            { name: 'paragraph',   groups: [ 'list', 'indent', 'blocks', 'align' ] },
            { name: 'colors' },
            { name: 'insert', groups: ['image', 'table'] },
            { name: 'document', groups: [ 'mode', 'document', 'doctools' ] },
            { name: 'styles', groups: ['styles'] },
        ] 
    });
    CKFinder.setupCKEditor( editorContent );

    // create slug from podcast title
    $("#title").on('blur', function(e) {
            
        let title = $(this).val();
        let modelId = '{{ $program->id }}';
        let type = 'program';

        if (title != '') {            
            $.ajax({
                method: 'GET',
                url: "{{ route('create-slug', app()->getLocale()) }}",
                data: {title, modelId, type},
                cache: false,
                timeout: 10000,
                success: function(data, textStatus, xhr){
                    if(xhr.status === 200){
                        $("#slug").val(data);
                    } else {
                        console.log(data.message);
                    }
                },
                error: function(xhr, statusText, error){
                    console.log('Error creating slug ' + xhr.status + ' ' + error);
                }
            });
        }
    });

    // toggle program page hidden elements
    $("#program_page").on("change", function(e) {
        let programPage = $(this).val();
        
        toggleProgramPageHiddenElements(programPage);
    });

    // toggle program page hidden elements on page load
    toggleProgramPageHiddenElements(selectedProgramPage);

    // toggle program page hidden elements
    function toggleProgramPageHiddenElements(programPage) {

        $(".form-group.hidden").addClass('d-none');
        $(`#${programPage}Hidden`).removeClass('d-none');
    }
});
</script>
@endsection