@extends('layouts.app')
@section('title', 'Programi')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    <h3 class="card-title">Programi</h3>
                    <span class="float-right">
                        <a href="{{ route('programs.create', app()->getLocale()) }}">
                            <button class="btn btn-primary">Novi program</button>
                        </a>
                    </span>
                </div>

                <div class="card-body">
                    <table id="program-table" class="table table-bordered table-striped">
                        <thead>
                            <th>#</th>
                            <th>Naslov</th>
                            <th>Sadržaj</th>
                            <th>Akcije</th>
                        </thead>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('js') 
<script>
$(document).ready(function() {
    let usersTable = $('#program-table').DataTable({
        processing: true,
        serverSide: true,
        responsive: true,
        deferRender: true,
        stateSave: true,
        lengthMenu: [[10, 25, 50, -1], [10, 25, 50, "Sve"]],
        order: [[0, 'desc']],
        ajax: {
            url: "{{ route('program-list', app()->getLocale()) }}"
        },
        columns: [
            { data: 'id', name: 'id' },
            { data: 'title', name: 'title', width: '330px'},
            { 
                data: 'content', 
                name: 'content',
                orderable: false,
                render: function(data, type, row) {
                    return $("<div/>").html(data).text();
                }
            },
            {
                data: 'action',
                name: 'action',
                orderable: false,
                width: '135px'
            }
        ],
        language: {
            url: "{{ asset('datatable_languages/' . config('app.locale') .'.json') }}"
        }
    });

});
</script>
@endsection