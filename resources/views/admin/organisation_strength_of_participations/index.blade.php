@extends('layouts.app')
@section('title', 'Organizacije - Snaga sudjelovanja')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    <h3 class="card-title">Organizacije - Snaga sudjelovanja</h3>
                </div>

                <div class="card-body">
                    <table id="organisations-table" class="table table-bordered table-striped">
                        <thead>
                            <th>#</th>
                            <th>Naziv organizacije</th>
                            <th>Kontakt osoba</th>
                            <th>Email</th>
                            <th>Telefon</th>
                            <th>Mjesto</th>
                            <th>Akcije</th>
                        </thead>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('js') 
<script>
$(document).ready(function() {
    let organisationsTable = $('#organisations-table').DataTable({
        processing: true,
        serverSide: true,
        responsive: true,
        deferRender: true,
        stateSave: true,
        lengthMenu: [[10, 25, 50, -1], [10, 25, 50, "Sve"]],
        order: [[0, 'desc']],
        ajax: {
            url: "{{ route('organisation-strength-of-participations-list', app()->getLocale()) }}"
        },
        columns: [
            { data: 'id', name: 'id' },
            { data: 'organisation_name', name: 'organisation_name'},
            { data: 'contact_person', name: 'contact_person'},
            { data: 'email', name: 'email'},
            { data: 'telephone', name: 'telephone'},
            { data: 'city', name: 'city'},
            {
                data: 'action',
                name: 'action',
                orderable: false,
                width: '135px'
            }
        ],
        language: {
            url: "{{ asset('datatable_languages/' . config('app.locale') .'.json') }}"
        }
    });
});
</script>
@endsection