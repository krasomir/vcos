@extends('layouts.app')
@section('title', 'Detalji organizacije')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    <h3 class="card-title">Detalji organizacije</h3>
                    <span class="float-right">
                        <a href="{{ route('organisation-sop-pdf', [app()->getLocale(), $organisation]) }}" target="_blank">
                            <button class="btn btn-primary">
                                <span class="fas fa-print"></span>
                            </button>
                        </a>
                        <a href="{{ route('organisation-strength-of-participations', app()->getLocale()) }}">
                            <button class="btn btn-primary">
                                <span class="fa fa-arrow-left"></span>
                            </button>
                        </a>
                    </span>
                </div>

                <div class="card-body">
                    <x-volunteer-organisation-sop-details :organisation="$organisation" /> 
                </div>
            </div>
        </div>
    </div>
</div>
@endsection