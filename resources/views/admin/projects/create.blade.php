@extends('layouts.app')
@section('title', 'Novi projekt')


@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    <h3 class="card-title">Novi projekt</h3>
                    <span class="float-right">
                        <a href="{{ route('admin-projects', app()->getLocale()) }}">
                            <button class="btn btn-primary">
                                <span class="fa fa-arrow-left"></span>
                            </button>
                        </a>
                    </span>
                </div>

                <div class="card-body">
                    
                    <form action="{{ route('project-store', app()->getLocale()) }}" id="storeProjectForm" method="post" enctype="multipart/form-data">
                        @csrf
                        <!--  -->
                        <div class="row">
                            <!-- prvi stupac -->
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="title">Naslov</label>
                                    <input type="text" id="title" name="title" class="form-control @error('title') is-invalid @enderror" value="{{ old('title') }}">
                                    @error('title')
                                        <div class="small text-danger">{{ $message }}</div>
                                    @enderror
                                </div>
                                <div class="form-group">
                                    <label for="slug">Slug/SEO naslov</label>
                                    <input type="text" id="slug" name="slug" readonly class="form-control @error('slug') is-invalid @enderror" value="{{ old('slug') }}">
                                    @error('slug')
                                        <div class="small text-danger">{{ $message }}</div>
                                    @enderror
                                </div>
                                <div class="form-group">
                                    <label for="image">Priloži sliku</label>
                                    <small class="small text-info">Slika ne smije biti dimenzija manjih od 1035x286px (omjer 1 : 3,62)</small>
                                    <input type="file" id="image" name="image" class="form-control @error('image') is-invalid @enderror" value="">
                                    @error('image')
                                        <div class="small text-danger">{{ $message }}</div>
                                    @enderror
                                </div>
                                <div class="form-group">
                                    <label for="preview">Pregled/Kratki sadržaj/Uvod</label>
                                    <textarea id="preview" name="preview" class="form-control @error('preview') is-invalid @enderror">
                                        {{ old('preview') }}
                                    </textarea>
                                    @error('preview')
                                        <div class="small text-danger">{{ $message }}</div>
                                    @enderror
                                </div>
                                <div class="form-group">
                                    <label for="description">Opis</label>
                                    <textarea id="description" name="description" class="form-control @error('description') is-invalid @enderror">
                                        {{ old('description') }}
                                    </textarea>
                                    @error('description')
                                        <div class="small text-danger">{{ $message }}</div>
                                    @enderror
                                </div>
                                <div class="form-group">
                                    <label for="start_date">Datum početka</label>
                                    <input type="text" id="start_date" name="start_date" class="form-control @error('start_date') is-invalid @enderror" value="{{ old('start_date') }}">
                                    @error('start_date')
                                        <div class="small text-danger">{{ $message }}</div>
                                    @enderror
                                </div>
                                <div class="form-group">
                                    <label for="end_date">Datum završetka</label>
                                    <input type="text" id="end_date" name="end_date" class="form-control @error('end_date') is-invalid @enderror" value="{{ old('end_date') }}">
                                    @error('end_date')
                                        <div class="small text-danger">{{ $message }}</div>
                                    @enderror
                                </div>
                                <div class="form-group">
                                    <label for="published">Objavi</label>
                                    <input type="checkbox" id="published" @if(old('published')) checked @endif name="published" class="js-switch" data-switchery="true" value="1">
                                    @error('published')
                                        <div class="small text-danger">{{ $message }}</div>
                                    @enderror
                                </div>
                            </div>
                        </div>
                        
                        <!--  -->
                        <div class="form-group">
                            <button type="submit" class="btn btn-primary float-right">Spremi</button>
                        </div>
                    </form>

                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('js')
<script>
$(document).ready(function() {
    // ck editors init
    let editorText = CKEDITOR.replace('description', {
        language: 'hr',
        toolbarGroups: [
            { name: 'clipboard',   groups: [ 'clipboard', 'undo' ] },
            { name: 'links' },
            { name: 'others' },
            { name: 'basicstyles', groups: [ 'basicstyles', 'cleanup' ] },
            { name: 'paragraph',   groups: [ 'list', 'indent', 'blocks', 'align' ] },
            { name: 'colors' },
            { name: 'insert', groups: ['image', 'table'] },
            { name: 'document', groups: [ 'mode', 'document', 'doctools' ] },
            { name: 'styles', groups: ['styles'] },
        ] 
    });
    CKFinder.setupCKEditor( editorText );

    let editorPreview = CKEDITOR.replace('preview', {
        language: 'hr',
        toolbarGroups: [
            { name: 'clipboard',   groups: [ 'clipboard', 'undo' ] },
            { name: 'links' },
            { name: 'others' },
            { name: 'basicstyles', groups: [ 'basicstyles', 'cleanup' ] },
            { name: 'paragraph',   groups: [ 'list', 'indent', 'blocks', 'align' ] },
            { name: 'colors' },
            { name: 'insert', groups: ['image', 'table'] },
            { name: 'document', groups: [ 'mode', 'document', 'doctools' ] },
            { name: 'styles', groups: ['styles'] },
        ] 
    });
    CKFinder.setupCKEditor( editorPreview );

    
    // switchery init
    let elem = document.querySelector('.js-switch');
    options = {
        color: "#FAA51A",
        size: "normal"
    };
    let init = new Switchery(elem, options);

    // create slug from project title
    $("#title").on('blur', function(e) {
            
        let title = $(this).val();
        let modelId = 0;
        let type = 'project';

        if (title != '') {            
            $.ajax({
                method: 'GET',
                url: "{{ route('create-slug', app()->getLocale()) }}",
                data: {title, modelId, type},
                cache: false,
                timeout: 10000,
                success: function(data, textStatus, xhr){
                    if(xhr.status === 200){
                        $("#slug").val(data);
                    } else {
                        console.log(data.message);
                    }
                },
                error: function(xhr, statusText, error){
                    console.log('Error creating slug ' + xhr.status + ' ' + error);
                }
            });
        }
    });
});

// init datepickers
$("#start_date, #end_date").daterangepicker({
    singleDatePicker: true,
    locale: {
        format: 'YYYY-MM-DD',
        daysOfWeek: ['Ne', 'Po', 'Ut', 'Sr', 'Če', 'Pe', 'Su'],
        monthNames: ['Siječanj', 'Veljača', 'Ožujak', 'Travanj', 'Svibanj', 'Lipanj', 'Srpanj', 'Kolovoz', 'Rujan', 'Listopad', 'Studeni', 'Prosinac'],
        firstDay: 1
    }
});
</script>
@endsection