@extends('layouts.app')
@section('title', 'Projekti')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    <h3 class="card-title">Projekti</h3>
                    <span class="float-right">
                        <a href="{{ route('project-create', app()->getLocale()) }}">
                            <button class="btn btn-primary">Novi projekt</button>
                        </a>
                    </span>
                </div>

                <div class="card-body">
                    <table id="projects-table" class="table table-bordered table-striped">
                        <thead>
                            <th>#</th>
                            <th>Naslov</th>
                            <th>Početni datum</th>
                            <th>Završni datum</th>
                            <th>Objavljeno</th>
                            <th>Arhivirano</th>
                            <th>Datum arhiviranja</th>
                            <th>Akcije</th>
                        </thead>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('js') 
<script>
$(document).ready(function() {
    let usersTable = $('#projects-table').DataTable({
        processing: true,
        serverSide: true,
        responsive: true,
        deferRender: true,
        stateSave: true,
        lengthMenu: [[10, 25, 50, -1], [10, 25, 50, "Sve"]],
        order: [[0, 'desc']],
        ajax: {
            url: "{{ route('project-list', app()->getLocale()) }}"
        },
        columns: [
            { data: 'id', name: 'id' },
            { data: 'title', name: 'title', width: '330px'},
            { 
                data: 'start_date', 
                name: 'start_date',
                orderable: false
            },
            { 
                data: 'end_date', 
                name: 'end_date',
                orderable: false
            },
            { 
                data: 'published', 
                name: 'published',
                orderable: false
            },
            { 
                data: 'archived', 
                name: 'archived',
                orderable: false
            },
            {
                data: 'archived_date',
                name: 'archived_date',
                orderable: true,
                className: 'archived-date'
            },
            {
                data: 'action',
                name: 'action',
                orderable: false,
                width: '135px'
            }
        ],
        language: {
            url: "{{ asset('datatable_languages/' . config('app.locale') .'.json') }}"
        },
        rowCallback: function (row, data, index) {
            let dateCell = data.archived_date;

            if (dateCell !== undefined && dateCell > 0) {
                let date = moment.unix(dateCell).format('DD.MM.YYYY HH:mm:ss');
                $("td:eq(6)", row).html(date);
            }
        }
    });
    
    // archive project
    $("#projects-table").on('change', '.custom-control-input, .custom-control-input-archive', function(e) {

        let published = $(this).is(':checked') ? 1:0;
        let archived = $(this).is(':checked') ? 1:0;
        let route = $(this).data('route');
        let id = $(this).data('id');
        $(".alert-messages").empty();

        $.ajax({
            method: "PATCH",
            url: route,
            data: {published, archived},
            cache: false,
            timeout: 30000,
            success: function(data) {
                if (data.error === false) {
                    let alert = `
                        <div class="alert alert-success alert-dismissible">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                            ${data.message}
                        </div>
                    `;
                    $(".alert-messages").append(alert);
                }
                $(`#archive_${id} .archived-date`).text(data.archivedDate);
            },
            error: function(xhr, statusText, error) {
                $(".alert-messages").append(`<div class="alert alert-danger">${xhr.status} ${error}</div>`);
            }
        });
        
    });
});
</script>
@endsection