@extends('layouts.app')
@section('title', 'Publikacije')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    <h3 class="card-title">Publikacije</h3>
                    <span class="float-right">
                        <a href="{{ route('publications.create', app()->getLocale()) }}">
                            <button class="btn btn-primary">Nova publikacija</button>
                        </a>
                    </span>
                </div>

                <div class="card-body">
                    <table id="publications-table" class="table table-bordered table-striped">
                        <thead>
                            <th>#</th>
                            <th>Naziv</th>
                            <th>Kategorija</th>
                            <th>Slika</th>
                            <th>Akcije</th>
                        </thead>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('js')
<script>
$(document).ready(function() {
    let publicationsTable = $('#publications-table').DataTable({
        processing: true,
        serverSide: true,
        responsive: true,
        deferRender: true,
        stateSave: true,
        lengthMenu: [[10, 25, 50, -1], [10, 25, 50, "Sve"]],
        order: [[0, 'desc']],
        ajax: {
            url: "{{ route('publication.list', app()->getLocale()) }}"
        },
        columns: [
            { data: 'id', name: 'id', orderable: true },
            { data: 'title', name: 'title', orderable: true},
            {
                data: 'publication_category',
                name: 'publication_category',
                orderable: true
            },
            {
                data: 'image_name',
                name: 'image_name',
                render: function(data) {
                    if (!data.image) {
                        return '';
                    }
                    let image = `
                        <img src="../..${data.image}" height="30" />
                        <small class="text-info">${data.demo_academy}</small>
                    `;
                    return image;
                },
                orderable: false,
                width: '150px'
            },
            {
                data: 'action',
                name: 'action',
                orderable: false,
                width: '135px'
            }
        ],
        language: {
            url: "{{ asset('datatable_languages/' . config('app.locale') .'.json') }}"
        }
    });


});
</script>
@endsection
