@extends('layouts.app')
@section('title', 'Uredi publikaciju')


@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    <h3 class="card-title">Uredi publikaciju</h3>
                    <span class="float-right">
                        <a href="{{ route('publications.index', app()->getLocale()) }}">
                            <button class="btn btn-primary">
                                <span class="fa fa-arrow-left"></span>
                            </button>
                        </a>
                    </span>
                </div>

                <div class="card-body">

                    <form action="{{ route('publications.update', [app()->getLocale(), $publication]) }}" id="updateBannerForm" method="post" enctype="multipart/form-data">
                        @csrf
                        @method ('PATCH')
                        <!--  -->
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="title">Naziv publikacije</label>
                                    <input type="text" id="title" name="title" class="form-control @error('title') is-invalid @enderror" value="{{ old('title') ?? $publication->title }}">
                                    @error('title')
                                        <div class="small text-danger">{{ $message }}</div>
                                    @enderror
                                </div>
                                <div class="form-group">
                                    <label for="slug">Slug/SEO naziva</label>
                                    <input type="text" id="slug" name="slug" readonly class="form-control @error('slug') is-invalid @enderror" value="{{ old('slug') ?? $publication->slug }}">
                                    @error('slug')
                                        <div class="small text-danger">{{ $message }}</div>
                                    @enderror
                                </div>
                                <div class="form-group">
                                    <label for="title">Kategorija publikacije</label>
                                    <select id="publication_category" name="publication_category"
                                            class="form-control @error('publication_category') is-invalid @enderror"
                                    >
                                        <option value="">-- izaberi --</option>
                                        @foreach(\App\Enums\PublicationCategoriesEnum::cases() as $category)
                                            <option value="{{ $category->value }}"
                                                @selected($category->value === $publication->publication_category || old('publication_category') === $category->value)
                                            >
                                                {{ __($category->value) }}
                                            </option>
                                        @endforeach
                                    </select>
                                    @error('publication_category')
                                    <div class="small text-danger">{{ $message }}</div>
                                    @enderror
                                </div>
                                <div class="form-group position-relative">
                                    <label for="image">Priloži sliku</label>
                                    <small class="small text-info">Slika ne smije biti dimenzija manjih od 576x800px (omjer 1 : 0,72)</small>
                                    <input type="file" id="image" name="image" class="form-control @error('image') is-invalid @enderror" value="">
                                    @if($publication->image_name && file_exists('storage/images/publications/' . $publication->image_name))
                                        <a href="{{ asset('storage/images/publications/' . $publication->image_name) }}" target="_blank" rel="noopener noreferrer">
                                            <img src="{{ asset('storage/images/publications/' . $publication->image_name) }}"
                                                height="80" alt="{{ $publication->image_name }}"
                                                class="img-preview" title="Pogledaj u novom prozoru"
                                            >
                                        </a>
                                    @endif
                                    @error('image')
                                        <div class="small text-danger">{{ $message }}</div>
                                    @enderror
                                </div>
                                <div class="form-group">
                                    <label for="publication">Priloži publikaciju</label>
                                    <small class="small text-info">Publikacija treba biti u *.pdf formatu i ne smije zauzimati prostor veći od 10MB</small>
                                    <input type="file" id="publication" name="publication" class="form-control @error('publication') is-invalid @enderror" value="">
                                    @if($publication->publication_name && file_exists('storage/documents/publications/' . $publication->publication_name))
                                        <a href="{{ asset('storage/documents/publications/' . $publication->publication_name) }}" target="_blank" class="pl-5" rel="noopener noreferrer">
                                            {{ $publication->publication_name }}
                                        </a>
                                    @endif
                                    @error('publication')
                                        <div class="small text-danger">{{ $message }}</div>
                                    @enderror
                                </div>
                                <div class="form-group">
                                    <label for="is_demo_academy">Publikacija je za Demo Akademiju</label><br>
                                    <input type="checkbox" id="is_demo_academy" @if(old('is_demo_academy') || (int)$publication->is_demo_academy === 1) checked @endif name="is_demo_academy" class="js-switch" data-switchery="true" value="1">
                                    @error('is_demo_academy')
                                        <div class="small text-danger">{{ $message }}</div>
                                    @enderror
                                </div>
                            </div>
                        </div>

                        <!--  -->
                        <div class="form-group">
                            <button type="submit" class="btn btn-primary float-right">Spremi</button>
                        </div>
                    </form>

                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('js')
<script>
$(document).ready(function() {

    // switchery init
    let elem = document.querySelector('.js-switch');
    options = {
        color: "#FAA51A",
        size: "normal"
    };
    let init = new Switchery(elem, options);


    // create slug from project title
    $("#title").on('blur', function(e) {

        let title = $(this).val();
        let modelId = '{{ $publication->id }}';
        let type = 'publication';

        if (title != '') {
            $.ajax({
                method: 'GET',
                url: "{{ route('create-slug', app()->getLocale()) }}",
                data: {title, modelId, type},
                cache: false,
                timeout: 10000,
                success: function(data, textStatus, xhr){
                    if(xhr.status === 200){
                        $("#slug").val(data);
                    } else {
                        console.log(data.message);
                    }
                },
                error: function(xhr, statusText, error){
                    console.log('Error creating slug ' + xhr.status + ' ' + error);
                }
            });
        }
    });
});
</script>
@endsection
