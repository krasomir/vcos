@extends('layouts.app')
@section('title', 'Podcast epizode')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    <h3 class="card-title">Podcast epizode</h3>
                    <span class="float-right">
                        <a href="{{ route('podcasts.create', app()->getLocale()) }}">
                            <button class="btn btn-primary">Nova epizoda</button>
                        </a>
                    </span>
                    <span class="float-right mr-2">
                        <a href="#">
                            <button class="btn btn-default copy-feed-link" 
                                data-feed-link="{{ config('app.url') . '/feed' }}"
                                >
                                <i class="far fa-clone"></i> 
                                Kopiraj feed link
                            </button>
                            <!-- success btn -->
                            <button class="btn btn-success copied-feed-link d-none" >
                                <i class="fas fa-check"></i> 
                                Kopirano
                            </button>
                        </a>
                    </span>
                </div>

                <div class="card-body">
                    <table id="podcasts-table" class="table table-bordered table-striped">
                        <thead>
                            <th>#</th>
                            <th>Naziv</th>
                            <th>Sezona</th>
                            <th>Sadržaj</th>
                            <th>Ime datoteke</th>
                            <th>Akcije</th>
                        </thead>
                    </table>
                </div>
            </div>
        </div>
        <!-- edit main podcast text -->
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    <h3 class="card-title">Početni opis sekcije</h3>
                </div>
                <form action="{{ route('save-main-podcast-text', ['lang' => app()->getLocale(), 'podcastMainText' => $mainPodcastText]) }}" method="post">
                    @csrf
                    <div class="card-body">
                        <div class="form-group">
                            <textarea name="content" id="content" cols="30" rows="20" class="form-control @error('content') is-invalid @enderror">
                                {{ old('description') ?? $mainPodcastText->content }}
                            </textarea>
                            <small class="small text-info">Za potpuni responsive design korištenih slika, desnim klikom na sliku -> 'Svojstva slika' -> 'Napredno' -> 'Klase stilova' - upisati 'imageWrapper'</small>
                            @error('description')
                                <div class="small text-danger">{{ $message }}</div>
                            @enderror
                        </div>
                    </div>
                    <div class="card-footer">
                        <button type="submit" class="btn btn-success float-right">Spremi</button>
                    </div>
                </form>
            </div>
        </div>
        <!-- feed link -->
        <div class="col-md-12">
            <div class="card">
                <div class="card-header feed-link">
                    <h3 class="card-title">Feed link/url: 
                        <span class="ml-5 blockquote-footer">
                            <span class="feed-link-text">
                                {{ config('app.url') . '/feed' }}
                            </span> 
                            <a href="{{ config('app.url') . '/feed' }}" target="_blank" class="ml-3">
                                <i class="fas fa-external-link-alt"></i>
                            </a>
                        </span>
                    </h3>
                    <span class="float-right">
                        <a href="#">
                            <button class="btn btn-default copy-feed-link" 
                                data-feed-link="{{ config('app.url') . '/feed' }}"
                                >
                                <i class="far fa-clone"></i> 
                                Kopiraj feed link
                            </button>
                            <!-- success btn -->
                            <button class="btn btn-success copied-feed-link d-none" >
                                <i class="fas fa-check"></i> 
                                Kopirano
                            </button>
                        </a>
                    </span>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('js') 
<script>
$(document).ready(function() {
    let slidersTable = $('#podcasts-table').DataTable({
        processing: true,
        serverSide: true,
        responsive: true,
        deferRender: true,
        stateSave: true,
        lengthMenu: [[10, 25, 50, -1], [10, 25, 50, "Sve"]],
        order: [[0, 'desc']],
        ajax: {
            url: "{{ route('podcast.list', app()->getLocale()) }}"
        },
        columns: [
            { data: 'id', name: 'id' },
            { data: 'title', name: 'title'},
            { data: 'season', name: 'season'},
            {
                data: 'description',
                name: 'description',
                width: '350px',
                render: function(data, type, row) {
                    return $("<div/>").html(data).text();
                }
            },
            { 
                data: 'file_name', 
                name: 'file_name',
                orderable: false
            },
            {
                data: 'action',
                name: 'action',
                orderable: false,
                width: '135px'
            }
        ],
        language: {
            url: "{{ asset('datatable_languages/' . config('app.locale') .'.json') }}"
        }
    });

    // ck editors init
    let editorContent = CKEDITOR.replace('content', {
        language: 'hr',
        toolbarGroups: [
            { name: 'clipboard',   groups: [ 'clipboard', 'undo' ] },
            { name: 'links' },
            { name: 'others' },
            { name: 'basicstyles', groups: [ 'basicstyles', 'cleanup' ] },
            { name: 'paragraph',   groups: [ 'list', 'indent', 'blocks', 'align' ] },
            { name: 'colors' },
            { name: 'insert', groups: ['image', 'table'] },
            { name: 'document', groups: [ 'mode', 'document', 'doctools' ] },
            { name: 'styles', groups: ['styles'] },
        ] 
    });
    CKFinder.setupCKEditor( editorContent );

    // copy feed link
    $(".copy-feed-link").on('click', function(e) {
        let feedLink = $(this).data('feed-link');
        let $temp = $("<input>");

        $("body").append($temp);
        $temp.val(feedLink).select();

        try {
            let success = document.execCommand('copy');
            if (success) {
                $(".copy-feed-link, .copied-feed-link").toggleClass('d-none');
                setTimeout(() => {
                    $(".copy-feed-link, .copied-feed-link").toggleClass('d-none');
                }, 2000);
            }                
        } catch (error) {
            console.log('Unable to copy.');
        }
        $temp.remove();
    });


});

</script>
@endsection