@extends('layouts.app')
@section('title', 'Uredi epizodu')


@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    <h3 class="card-title">Uredi epizodu</h3>
                    <span class="float-right">
                        <a href="{{ route('podcasts.index', app()->getLocale()) }}">
                            <button class="btn btn-primary">
                                <span class="fa fa-arrow-left"></span>
                            </button>
                        </a>
                    </span>
                </div>

                <div class="card-body">
                    
                    <form action="{{ route('podcasts.update', [app()->getLocale(), $podcast]) }}" id="updatePodcastForm" method="post" enctype="multipart/form-data">
                        @csrf
                        @method('PATCH')
                        <!--  -->
                        <div class="row">
                            <!--  -->
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="season">Broj sezone</label>
                                    <small class="small text-info">Bez interpunkcija i riječi, samo broj</small>
                                    <input type="number" id="season" name="season" class="form-control col-md-1 @error('season') is-invalid @enderror" value="{{ old('season') ?? $podcast->season }}">
                                    @error('season')
                                        <div class="small text-danger">{{ $message }}</div>
                                    @enderror
                                </div>
                                <div class="form-group">
                                    <label for="title">Naziv epizode</label>
                                    <input type="text" id="title" name="title" class="form-control @error('title') is-invalid @enderror" value="{{ old('title') ?? $podcast->title }}">
                                    @error('title')
                                        <div class="small text-danger">{{ $message }}</div>
                                    @enderror
                                </div>
                                <div class="form-group">
                                    <label for="slug">Slug/SEO naziva</label>
                                    <input type="text" id="slug" name="slug" readonly class="form-control @error('slug') is-invalid @enderror" value="{{ old('slug') ?? $podcast->slug }}">
                                    @error('slug')
                                        <div class="small text-danger">{{ $message }}</div>
                                    @enderror
                                </div>
                                <div class="form-group">
                                    <label for="file">Priloži datoteku</label>
                                    <small class="small text-info">Datoteka mora biti u .mp3 formatu</small>
                                    <input type="file" id="file" name="file" class="form-control @error('image') is-invalid @enderror">
                                    @error('file')
                                        <div class="small text-danger">{{ $message }}</div>
                                    @enderror
                                </div>
                                <div class="form-group">                                    
                                    <label>Postojeća datoteka: <small class="small">{{ asset('storage/files/podcasts/' . $podcast->file_name) }}</small></label>
                                    <input type="hidden" id="existing_file" name="existing_file" value="{{ $podcast->file_name }}">
                                    @error('existing_file')
                                        <div class="small text-danger">{{ $message }}</div>
                                    @enderror
                                </div>
                                <div class="form-group">
                                    <label for="link_to_page">Kratki opis epizode</label>
                                    <small class="small text-info">Za potrebe RSS feed-a</small>
                                    <textarea id="short_description" name="short_description" class="form-control @error('short_description') is-invalid @enderror">{{ old('short_description') ?? $podcast->short_description }}</textarea>
                                    @error('short_description')
                                        <div class="small text-danger">{{ $message }}</div>
                                    @enderror
                                </div>
                                <div class="form-group">
                                    <label for="link_to_page">Opis epizode</label>
                                    <textarea id="description" name="description" class="form-control @error('description') is-invalid @enderror">
                                        {{ old('description') ?? $podcast->description }}
                                    </textarea>
                                    @error('description')
                                        <div class="small text-danger">{{ $message }}</div>
                                    @enderror
                                </div>
                                <div class="form-group">
                                    <label for="published">Objavi</label>
                                    <input type="checkbox" id="published" @if(old('published') ?? $podcast->published_at) checked @endif name="published" class="js-switch" data-switchery="true" value="1">
                                    @if($podcast->published_at)
                                        <span title="{{ $podcast->published_at->format('H:i:s') }}">{{ $podcast->published_at->format('d.m.Y.') }}</span>
                                    @endif
                                    @error('published')
                                        <div class="small text-danger">{{ $message }}</div>
                                    @enderror
                                </div>
                            </div>
                        </div>
                        
                        <!--  -->
                        <div class="form-group">
                            <button type="submit" class="btn btn-primary float-right">Spremi</button>
                        </div>
                    </form>

                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('js')
<script>
$(document).ready(function() {
    // ck editors init
    let editorDescription = CKEDITOR.replace('description', {
        language: 'hr',
        toolbarGroups: [
            { name: 'clipboard',   groups: [ 'clipboard', 'undo' ] },
            { name: 'links' },
            { name: 'others' },
            { name: 'basicstyles', groups: [ 'basicstyles', 'cleanup' ] },
            { name: 'paragraph',   groups: [ 'list', 'indent', 'blocks', 'align' ] },
            { name: 'colors' },
            { name: 'insert', groups: ['image', 'table'] },
            { name: 'document', groups: [ 'mode', 'document', 'doctools' ] },
            { name: 'styles', groups: ['styles'] },
        ] 
    });
    CKFinder.setupCKEditor( editorDescription );

    // switchery init
    let elem = document.querySelector('.js-switch');
    options = {
        color: "#FAA51A",
        size: "normal"
    };
    let init = new Switchery(elem, options);

    // create slug from podcast title
    $("#title").on('blur', function(e) {
            
        let title = $(this).val();
        let modelId = '{{ $podcast->id }}';
        let type = 'podcast';

        if (title != '') {            
            $.ajax({
                method: 'GET',
                url: "{{ route('create-slug', app()->getLocale()) }}",
                data: {title, modelId, type},
                cache: false,
                timeout: 10000,
                success: function(data, textStatus, xhr){
                    if(xhr.status === 200){
                        $("#slug").val(data);
                    } else {
                        console.log(data.message);
                    }
                },
                error: function(xhr, statusText, error){
                    console.log('Error creating slug ' + xhr.status + ' ' + error);
                }
            });
        }
    });
});
</script>
@endsection