@extends('layouts.app')
@section('title', 'Novi član tima')


@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <h3 class="card-title">Novi član tima</h3>
                        <span class="float-right">
                            <a href="{{ route('teams.index', app()->getLocale()) }}">
                                <button class="btn btn-primary">
                                    <span class="fa fa-arrow-left"></span>
                                </button>
                            </a>
                        </span>
                    </div>

                    <div class="card-body">

                        <form action="{{ route('teams.store', app()->getLocale()) }}" id="storeTeamForm" method="post" enctype="multipart/form-data">
                            @csrf
                            <!--  -->
                            <div class="row">
                                <!-- prvi stupac -->
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="first_name">Ime</label>
                                        <input type="text" id="first_name" name="first_name" class="form-control @error('first_name') is-invalid @enderror" value="{{ old('first_name') }}">
                                        @error('first_name')
                                            <div class="small text-danger">{{ $message }}</div>
                                        @enderror
                                    </div>
                                    <div class="form-group">
                                        <label for="last_name">Prezime</label>
                                        <input type="text" id="last_name" name="last_name" class="form-control @error('last_name') is-invalid @enderror" value="{{ old('last_name') }}">
                                        @error('last_name')
                                            <div class="small text-danger">{{ $message }}</div>
                                        @enderror
                                    </div>
                                    <div class="form-group">
                                        <label for="position">Pozicija</label>
                                        <input type="text" id="position" name="position" class="form-control @error('position') is-invalid @enderror" value="{{ old('position') }}">
                                        @error('position')
                                            <div class="small text-danger">{{ $message }}</div>
                                        @enderror
                                    </div>
                                    <div class="form-group">
                                        <label for="email">E-mail</label>
                                        <input type="email" id="email" name="email" class="form-control @error('email') is-invalid @enderror" value="{{ old('email') }}">
                                        @error('email')
                                            <div class="small text-danger">{{ $message }}</div>
                                        @enderror
                                    </div>
                                    <div class="form-group">
                                        <label for="linked_in">LinkedIn</label>
                                        <input type="text" id="linked_in" name="linked_in" class="form-control @error('linked_in') is-invalid @enderror" value="{{ old('linked_in') }}">
                                        @error('linked_in')
                                            <div class="small text-danger">{{ $message }}</div>
                                        @enderror
                                    </div>
                                </div>

                                <!-- drugi stupac -->
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="description">Kratki tekst</label>
                                        <textarea id="description" name="description" class="form-control @error('description') is-invalid @enderror">
                                            {{ old('description') }}
                                        </textarea>
                                        @error('description')
                                            <div class="small text-danger">{{ $message }}</div>
                                        @enderror
                                    </div>
                                    <div class="form-group">
                                        <label for="image">Priloži sliku</label>
                                        <small class="small text-info">Slika ne smije biti dimenzija manjih od 200x200 (omjer 1 : 1)</small>
                                        <input type="file" id="image" name="image" class="form-control @error('image') is-invalid @enderror" value="">
                                        @error('image')
                                            <div class="small text-danger">{{ $message }}</div>
                                        @enderror
                                    </div>
                                </div>
                            </div>

                            <!--  -->
                            <div class="form-group">
                                <button type="submit" class="btn btn-primary float-right">Spremi</button>
                            </div>
                        </form>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('js')
    <script>
        $(document).ready(function() {
            // ck editors init
            let editorText = CKEDITOR.replace('description', {
                language: 'hr',
                toolbarGroups: [
                    { name: 'clipboard',   groups: [ 'clipboard', 'undo' ] },
                    { name: 'links' },
                    { name: 'others' },
                    { name: 'basicstyles', groups: [ 'basicstyles', 'cleanup' ] },
                    { name: 'paragraph',   groups: [ 'list', 'indent', 'blocks', 'align' ] },
                    { name: 'colors' },
                    { name: 'insert', groups: ['image', 'table'] },
                    { name: 'document', groups: [ 'mode', 'document', 'doctools' ] },
                    { name: 'styles', groups: ['styles'] },
                ]
            });
            CKFinder.setupCKEditor( editorText );
        });
    </script>
@endsection
