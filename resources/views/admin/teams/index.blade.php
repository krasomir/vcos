@extends('layouts.app')
@section('title', 'Tim')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <h3 class="card-title">Tim</h3>
                        <span class="float-right">
                        <a href="{{ route('teams.create', app()->getLocale()) }}">
                            <button class="btn btn-primary">Novi član tima</button>
                        </a>
                    </span>
                    </div>

                    <div class="card-body">
                        <table id="teams-table" class="table table-bordered table-striped">
                            <thead>
                                <th>#</th>
                                <th>Slika</th>
                                <th>Ime</th>
                                <th>Prezime</th>
                                <th>Pozicija</th>
                                <th>Datum kreiranja</th>
                                <th>Akcije</th>
                            </thead>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('js')
    <script>
        $(document).ready(function() {
            let teamsTable = $('#teams-table').DataTable({
                processing: true,
                serverSide: true,
                responsive: true,
                deferRender: true,
                stateSave: true,
                lengthMenu: [[10, 25, 50, -1], [10, 25, 50, "Sve"]],
                order: [[0, 'asc']],
                ajax: {
                    url: "{{ route('admin-team-list', app()->getLocale()) }}"
                },
                columns: [
                    { data: 'id', name: 'id' },
                    {
                        data: 'image',
                        name: 'image',
                        render: function(dataImage) {
                            console.log(dataImage);
                            if (!dataImage) {
                                return '';
                            }
                            let image = `
                                <img src="../../storage/images/our_team/${dataImage}" height="30" class="rounded-circle" />
                            `;
                            return image;
                        },
                        orderable: false,
                        width: '60px'
                    },
                    { data: 'first_name', name: 'first_name'},
                    { data: 'last_name', name: 'last_name'},
                    { data: 'position', name: 'position'},
                    {
                        data: 'created_at',
                        name: 'created_at',
                        orderable: false,
                        width: '125px'
                    },
                    {
                        data: 'action',
                        name: 'action',
                        orderable: false,
                        width: '195px'
                    }
                ],
                language: {
                    url: "{{ asset('datatable_languages/' . config('app.locale') .'.json') }}"
                }
            });
        });
    </script>
@endsection
