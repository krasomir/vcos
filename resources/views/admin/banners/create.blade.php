@extends('layouts.app')
@section('title', 'Novi banner')


@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    <h3 class="card-title">Novi banner</h3>
                    <span class="float-right">
                        <a href="{{ route('banners.index', app()->getLocale()) }}">
                            <button class="btn btn-primary">
                                <span class="fa fa-arrow-left"></span>
                            </button>
                        </a>
                    </span>
                </div>

                <div class="card-body">
                    
                    <form action="{{ route('banners.store', app()->getLocale()) }}" id="storeBannerForm" method="post" enctype="multipart/form-data">
                        @csrf
                        <!--  -->
                        <div class="row">
                            <!-- prvi stupac -->
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="title">Naziv bannera</label>
                                    <input type="text" id="title" name="title" class="form-control @error('title') is-invalid @enderror" value="{{ old('title') }}">
                                    @error('title')
                                        <div class="small text-danger">{{ $message }}</div>
                                    @enderror
                                </div>
                                <div class="form-group">
                                    <label for="slug">Slug/SEO naziva</label>
                                    <input type="text" id="slug" name="slug" readonly class="form-control @error('slug') is-invalid @enderror" value="{{ old('slug') }}">
                                    @error('slug')
                                        <div class="small text-danger">{{ $message }}</div>
                                    @enderror
                                </div>
                                <div class="form-group">
                                    <label for="image">Priloži sliku</label>
                                    <small class="small text-info">Slika ne smije biti dimenzija manjih od 2030x190px (omjer 1 : 10,68)</small>
                                    <input type="file" id="image" name="image" class="form-control @error('image') is-invalid @enderror" value="">
                                    @error('image')
                                        <div class="small text-danger">{{ $message }}</div>
                                    @enderror
                                </div>
                                <div class="form-group">
                                    <label for="show_on_page">Prikaži na stranici</label>
                                    <small class="small text-info">Izborom prikaza na svim stranica ta slika se postavlja kao osnovna i neće se prikazivati kada za određenu stranicu postoji druga slika</small>
                                    <select name="show_on_page" id="show_on_page" class="form-control @error('show_on_page') is-invalid @enderror" value="{{ old('show_on_page') }}">
                                        <option value="">-- izaberi stranicu --</option>
                                        @foreach ($routes as $route)
                                            <option 
                                                value="{{ $route['uri'] }}" 
                                                @if(old('show_on_page') == $route['uri']) selected @endif
                                                @if(in_array($route['uri'], $exist_on_pages)) disabled @endif
                                            >{{ $route['name'] }}</option>
                                        @endforeach
                                    </select>
                                    @error('show_on_page')
                                        <div class="small text-danger">{{ $message }}</div>
                                    @enderror
                                </div>
                                <div class="form-group">
                                    <label for="bkg_color">Pozadniska boja</label>
                                    <small class="small text-info">Pozadinska boja će biti vidljiva samo u slučaju da ne postoji postavka/izbor za tu stranicu i ako nije definirana osnovna slika za sve stranice</small>
                                    <input type="color" id="bkg_color" name="bkg_color" class="form-control @error('bkg_color') is-invalid @enderror" value="{{ old('bkg_color') ?? '#000000' }}">
                                    @error('bkg_color')
                                        <div class="small text-danger">{{ $message }}</div>
                                    @enderror
                                </div>
                            </div>
                        </div>
                        
                        <!--  -->
                        <div class="form-group">
                            <button type="submit" class="btn btn-primary float-right">Spremi</button>
                        </div>
                    </form>

                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('js')
<script>
$(document).ready(function() {


    // create slug from project title
    $("#title").on('blur', function(e) {
            
        let title = $(this).val();
        let modelId = 0;
        let type = 'banner';

        if (title != '') {            
            $.ajax({
                method: 'GET',
                url: "{{ route('create-slug', app()->getLocale()) }}",
                data: {title, modelId, type},
                cache: false,
                timeout: 10000,
                success: function(data, textStatus, xhr){
                    if(xhr.status === 200){
                        $("#slug").val(data);
                    } else {
                        console.log(data.message);
                    }
                },
                error: function(xhr, statusText, error){
                    console.log('Error creating slug ' + xhr.status + ' ' + error);
                }
            });
        }
    });
});
</script>
@endsection