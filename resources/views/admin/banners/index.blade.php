@extends('layouts.app')
@section('title', 'Banneri')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    <h3 class="card-title">Banneri</h3>
                    <span class="float-right">
                        <a href="{{ route('banners.create', app()->getLocale()) }}">
                            <button class="btn btn-primary">Novi banner</button>
                        </a>
                    </span>
                </div>

                <div class="card-body">
                    <table id="banners-table" class="table table-bordered table-striped">
                        <thead>
                            <th>#</th>
                            <th>Naziv</th>
                            <th>Stranica na kojoj se prikazuje</th>
                            <th>Slika</th>
                            <th>Akcije</th>
                        </thead>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('js') 
<script>
$(document).ready(function() {
    let usersTable = $('#banners-table').DataTable({
        processing: true,
        serverSide: true,
        responsive: true,
        deferRender: true,
        stateSave: true,
        lengthMenu: [[10, 25, 50, -1], [10, 25, 50, "Sve"]],
        order: [[0, 'desc']],
        ajax: {
            url: "{{ route('banner.list', app()->getLocale()) }}"
        },
        columns: [
            { data: 'id', name: 'id' },
            { data: 'title', name: 'title'},
            { data: 'show_on_page', name: 'show_on_page'},
            { 
                data: 'image_name', 
                name: 'image_name',
                render: function(data) {
                    if (!data) {
                        return '';
                    }
                    let image = `<img src="../..${data}" height="30" />`;
                    return image;
                },
                orderable: false,
                width: '350px'
            },
            {
                data: 'action',
                name: 'action',
                orderable: false,
                width: '135px'
            }
        ],
        language: {
            url: "{{ asset('datatable_languages/' . config('app.locale') .'.json') }}"
        }
    });


});
</script>
@endsection