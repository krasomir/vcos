@extends('layouts.app')
@section('title', 'Neuspjele pretplate')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    <h3 class="card-title">Neuspjele pretplate</h3>
                </div>

                <div class="card-body">
                    <table id="failed-subscriptions-table" class="table table-bordered table-striped">
                        <thead>
                            <th>#</th>
                            <th>Email</th>
                            <th>Mailing lista</th>
                            <th>Ime</th>
                            <th>Komentar</th>
                            <th>Akcije</th>
                        </thead>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('js')
<script>
$(document).ready(function() {
    let organisationsTable = $('#failed-subscriptions-table').DataTable({
        processing: true,
        serverSide: true,
        responsive: true,
        deferRender: true,
        stateSave: true,
        lengthMenu: [[10, 25, 50, -1], [10, 25, 50, "Sve"]],
        order: [[1, 'asc']],
        ajax: {
            url: "{{ route('failed-subscriptions-list', app()->getLocale()) }}"
        },
        columns: [
            { data: 'id', name: 'id' },
            { data: 'email', name: 'email'},
            { data: 'mailing_list_name', name: 'mailing_list_name'},
            { data: 'names', name: 'names'},
            { data: 'comment', name: 'comment'},
            {
                data: 'action',
                name: 'action',
                orderable: false,
                width: '135px'
            }
        ],
        language: {
            url: "{{ asset('datatable_languages/' . config('app.locale') .'.json') }}"
        }
    });

    // subscription retry
    $('#failed-subscriptions-table').on('click', '.subscription-retry', function(e) {

        e.preventDefault();
        let route = $(this).data('retry-route');
        let id = $(this).data('id');
        $(`#subscription_retry_${id} .spinner`).removeClass('d-none');
        $(`#subscription_retry_${id} .button`).addClass('d-none');
        $(".alert-messages").empty();

        $.ajax({
            method: 'GET',
            url: route,
            data: {},
            cache: false,
            timeout: 10000,
            success: function(data, textStatus, xhr){
                $(`#subscription_retry_${id} .spinner`).addClass('d-none');
                $(`#subscription_retry_${id} .button`).removeClass('d-none');
                if(xhr.status === 200){
                    let alert = `
                        <div class="alert alert-success alert-dismissible">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                            ${data.message}
                        </div>
                    `;
                    $(".alert-messages").append(alert);                    
                } else {
                    
                }
            },
            error: function(xhr, statusText, error){
                $(`#subscription_retry_${id} .spinner`).addClass('d-none');
                $(`#subscription_retry_${id} .button`).removeClass('d-none');
                let alert = `
                    <div class="alert alert-danger alert-dismissible">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                        ${xhr.responseJSON.message}
                    </div>
                `;
                $(".alert-messages").append(alert);
                console.log('Error retrying to subscribe ' + xhr.status + ' ' + error);
            }
        });
    });
});
</script>
@endsection