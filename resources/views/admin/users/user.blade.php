@extends('layouts.app')
@section('title', 'Novi korisnik')


@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    <h3 class="card-title">Novi korisnik</h3>
                    <span class="float-right">
                        <a href="{{ route('admin-users', app()->getLocale()) }}">
                            <button class="btn btn-primary">
                                <span class="fa fa-arrow-left"></span>
                            </button>
                        </a>
                    </span>
                </div>

                <div class="card-body">
                    
                    <form action="{{ route('admin-user-store', app()->getLocale()) }}" id="storeUserForm" method="post">
                        @csrf
                        <div class="form-group">
                            <label for="name">Ime</label>
                            <input type="text" id="name" name="name" class="form-control @error('name') is-invalid @enderror" value="{{ old('name') }}">
                            @error('name')
                                <div class="small text-danger">{{ $message }}</div>
                            @enderror
                        </div>
                        <div class="form-group">
                            <label for="email">E-mail</label>
                            <input type="text" id="email" name="email" class="form-control @error('email') is-invalid @enderror" value="{{ old('email') }}">
                            @error('email')
                                <div class="small text-danger">{{ $message }}</div>
                            @enderror
                        </div>
                        <div class="form-group">
                            <label for="password">Lozinka</label>
                            <input type="password" id="password" name="password" class="form-control @error('password') is-invalid @enderror">
                            @error('password')
                                <div class="small text-danger">{{ $message }}</div>
                            @enderror
                        </div>
                        <div class="form-group">
                            <label for="password_confirm">Potvrdi lozinku</label>
                            <input type="password" id="password_confirmation" name="password_confirmation" class="form-control @error('password-confirm') is-invalid @enderror">
                            @error('password_confirmation')
                                <div class="small text-danger">{{ $message }}</div>
                            @enderror
                        </div>
                        <div class="form-group">
                            <label>Dozvole/dopuštenja</label>
                            <!-- select all = 26 -->
                            <div class="row">
                                <div class="col-sm-3">
                                    <div class="custom-control custom-checkbox">
                                        <input 
                                            class="custom-control-input" 
                                            value="1" 
                                            name="check_all" 
                                            type="checkbox" 
                                            id="customCheckbox_all"
                                            @if(old('check_all') == 1)
                                                checked
                                            @endif
                                        >
                                        <label for="customCheckbox_all" class="custom-control-label">Selektiraj sve</label>
                                    </div>
                                </div>
                            </div>
                            <hr>
                            <!--  -->
                            <div class="row">                                
                                @foreach($permissions as $permission)
                                <div class="col-sm-3">
                                    <div class="custom-control custom-checkbox permissions-container">
                                        <input 
                                            class="custom-control-input" 
                                            value="{{ $permission->id }}" 
                                            name="permission_ids[]" 
                                            type="checkbox" 
                                            id="customCheckbox_{{ $permission->id }}"
                                            @if(is_array(old('permission_ids')) && in_array($permission->id, old('permission_ids')))
                                                checked
                                            @endif
                                        >
                                        <label for="customCheckbox_{{ $permission->id }}" class="custom-control-label">{{ $permission->name }}</label>
                                    </div>
                                </div>
                                @endforeach
                            </div>
                            <!--  -->
                            @error('permission_ids')
                                <div class="small text-danger">{{ $message }}</div>
                            @enderror
                        </div>
                        <div class="form-group">
                            <button type="submit" class="btn btn-primary float-right">Spremi</button>
                        </div>
                    </form>

                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('js') 
<script>
$(document).ready(function() {
    let numberOfPermissions = $(".permissions-container input[type=checkbox]").length;
    // select all
    $("input[name=check_all]").on("change", function() {
        if ($(this).is(":checked")) {
            $(".permissions-container .custom-control-input").prop("checked", true);
        } else {
            $(".permissions-container .custom-control-input").prop("checked", false);
        }
    });

    // change select all if all not selected
    $(".permissions-container input[type=checkbox]").on("change", function() {
        if (numberOfChecked() == numberOfPermissions) {
            $("input[name=check_all]").prop("checked", true);
        } else {
            $("input[name=check_all]").prop("checked", false);
        }
    });
});

/**
 * count of checked permissions
 *
 * @return integer
 */
function numberOfChecked() {
    return $(".permissions-container input[type=checkbox]:checked").length;
}
</script>
@endsection