@extends('layouts.app')
@section('title', 'Korisnički profil')


@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <!--  -->
            <div class="card">
                <div class="card-header">
                    <h3 class="card-title">Korisnički profil</h3>
                    <span class="float-right">
                        <a href="{{ route('admin-users', app()->getLocale()) }}">
                            <button class="btn btn-primary">
                                <span class="fa fa-arrow-left"></span>
                            </button>
                        </a>
                    </span>
                </div>
                <ul class="list-group list-group-flush">
                    <li class="list-group-item">
                        <span>Prikaži 'login' dugme na glavnom izborniku</span>
                        <span id="storeUserForm" class="float-right">
                            <div class="custom-control custom-checkbox">
                                <input @if(isset($_COOKIE['show-login-button'])) checked @endif class="js-switch" value="1" name="showLoginButton" type="checkbox" id="showLoginButton">
                            </div>
                        </span>
                    </li>
                    <li class="list-group-item">
                        <span>Obriši predmemoriju (clear cache)</span>
                        <span class="float-right">
                            <a href="{{ route('clear-cache', app()->getLocale()) }}">
                                <button class="btn btn-primary px-06">
                                    <span class="fas fa-broom"></span>
                                </button>
                            </a>
                        </span>
                    </li>
                </ul>
            </div>

        </div>
    </div>
</div>
@endsection
@section('js')
<script>
    $(function() {
        // switchery init
        let elem = document.querySelector('.js-switch');
        options = {
            color: "#008b8b",
            size: "normal"
        };
        let init = new Switchery(elem, options);
        
        // show login button checkbox
        $("#showLoginButton").on('change', function(e) {
            let checked = $(this).is(':checked');

            if (checked == true) {
                createCookie('show-login-button', 1, 1000);
            } else {
                removeCookie('show-login-button');
            }

        });
    });

    /**
     * Creates cookie
     * @param {string} name 
     * @param {integer} value 
     * @param {integer} days 
     */
    function createCookie(name, value, days) {
        var expires;

        if (days) {
            var date = new Date();
            date.setTime(date.getTime() + (days * 24 * 60 * 60 * 1000));
            expires = "; expires=" + date.toGMTString();
        } else {
            expires = "";
        }
        document.cookie = encodeURIComponent(name) + "=" + encodeURIComponent(value) + expires + "; path=/";
    }

    /**
     * Read cookie
     * @param {string} name 
     */
    function readCookie(name) {
        var nameEQ = encodeURIComponent(name) + "=";
        var ca = document.cookie.split(';');
        for (var i = 0; i < ca.length; i++) {
            var c = ca[i];
            while (c.charAt(0) === ' ')
                c = c.substring(1, c.length);
            if (c.indexOf(nameEQ) === 0)
                return decodeURIComponent(c.substring(nameEQ.length, c.length));
        }
        return null;
    }

    /**
     * Removes cookie
     * @param {string} name 
     */
    function removeCookie(name) {
        createCookie(name, "", -1);
    }
</script>
@endsection