@extends('layouts.app')
@section('title', 'Korisnici')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    <h3 class="card-title">Korisnici</h3>
                    <span class="float-right">
                        <a href="{{ route('admin-user-create', app()->getLocale()) }}">
                            <button class="btn btn-primary">Novi korisnik</button>
                        </a>
                    </span>
                </div>

                <div class="card-body">
                    <table id="users-table" class="table table-bordered table-striped">
                        <thead>
                            <th>#</th>
                            <th>Ime</th>
                            <th>Email</th>
                            <th>Datum kreiranja</th>
                            <th>Akcije</th>
                        </thead>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('js') 
<script>
$(document).ready(function() {
    let usersTable = $('#users-table').DataTable({
        processing: true,
        serverSide: true,
        responsive: true,
        deferRender: true,
        stateSave: true,
        lengthMenu: [[10, 25, 50, -1], [10, 25, 50, "Sve"]],
        ajax: {
            url: "{{ route('admin-users-list', app()->getLocale()) }}"
        },
        columns: [
            { data: 'id', name: 'id' },
            { data: 'name', name: 'name'},
            { data: 'email', name: 'email'},
            { data: 'created_at', name: 'created_at'},
            {
                data: 'action',
                name: 'action',
                orderable: false
            }
        ],
        language: {
            url: "{{ asset('datatable_languages/' . config('app.locale') .'.json') }}"
        }
    });
});
</script>
@endsection