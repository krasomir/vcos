@extends('layouts.app')
@section('title', 'Uredi post')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    <h3 class="card-title">Uredi post</h3>
                    <span class="float-right">
                        <a href="{{ route('admin-posts', app()->getLocale()) }}">
                            <button class="btn btn-primary">
                                <span class="fa fa-arrow-left"></span>
                            </button>
                        </a>
                    </span>
                </div>

                <div class="card-body">
                    <!--  -->
                    <form action="{{ route('admin-post-update', [app()->getLocale(), $post]) }}" id="updatePostForm" method="post" enctype="multipart/form-data">
                        @csrf
                        @method('PATCH')
                        <!--  -->
                        <div class="row">
                            <!-- prvi stupac -->
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="name">Naslov</label>
                                    <input type="text" id="name" name="name" class="form-control @error('name') is-invalid @enderror" value="{{ old('name') ?? $post->name }}">
                                    @error('name')
                                        <div class="small text-danger">{{ $message }}</div>
                                    @enderror
                                </div>
                                <div class="form-group">
                                    <label for="preview">Pregled/Kratki sadržaj/Uvod</label>
                                    <textarea id="preview" name="preview" class="form-control @error('preview') is-invalid @enderror">
                                        {{ old('preview') ?? $post->preview }}
                                    </textarea>
                                    @error('preview')
                                        <div class="small text-danger">{{ $message }}</div>
                                    @enderror
                                </div>
                                <div class="form-group">
                                    <label for="datetime_on">Datum početka</label>
                                    <input type="text" id="datetime_on" name="datetime_on" class="form-control @error('datetime_on') is-invalid @enderror" value="{{ old('datetime_on') ?? $post->datetime_on->format('Y-m-d') }}">
                                    @error('datetime_on')
                                        <div class="small text-danger">{{ $message }}</div>
                                    @enderror
                                </div>
                                <div class="form-group">
                                    <label for="title">Title</label>
                                    <input type="text" id="title" name="title" class="form-control @error('title') is-invalid @enderror" value="{{ old('title') ?? $post->title }}">
                                    @error('title')
                                        <div class="small text-danger">{{ $message }}</div>
                                    @enderror
                                </div>
                                <div class="form-group">
                                    <label for="description">Opis</label>
                                    <input type="text" id="description" name="description" class="form-control @error('description') is-invalid @enderror" value="{{ old('description') ?? $post->description }}">
                                    @error('description')
                                        <div class="small text-danger">{{ $message }}</div>
                                    @enderror
                                </div>
                                <div class="form-group">
                                    <label for="page_id">Prikaži na stranici</label>
                                    <select name="page_id" id="page_id" class="form-control @error('page_id') is-invalid @enderror">
                                        <option value="">-- izaberi --</option>
                                        @foreach($pages as $page)
                                            <option value="{{ $page->id }}" @if((int)old('page_id') === (int)$page->id || (!old('page_id') && (int)$post->page_id === (int)$page->id)) selected @endif>{{ $page->name }}</option>
                                        @endforeach
                                    </select>
                                    @error('page_id')
                                        <div class="small text-danger">{{ $message }}</div>
                                    @enderror
                                </div>
                            </div>
                            <!-- drugi stupac -->
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="slug">Slug/SEO naslov</label>
                                    <input type="text" id="slug" name="slug" readonly class="form-control @error('slug') is-invalid @enderror" value="{{ old('slug') ?? $post->slug }}">
                                    @error('slug')
                                        <div class="small text-danger">{{ $message }}</div>
                                    @enderror
                                </div>
                                <div class="form-group">
                                    <label for="text">Sadržaj</label>
                                    <textarea id="text" name="text" class="form-control @error('text') is-invalid @enderror">
                                        {{ old('text') ?? $post->text }}
                                    </textarea>
                                    @error('text')
                                        <div class="small text-danger">{{ $message }}</div>
                                    @enderror
                                </div>
                                <div class="form-group">
                                    <label for="datetime_off">Datum završetka</label>
                                    <input type="text" id="datetime_off" name="datetime_off" class="form-control @error('datetime_off') is-invalid @enderror" value="{{ old('datetime_off') ?? $post->datetime_off->format('Y-m-d') }}">
                                    @error('datetime_off')
                                        <div class="small text-danger">{{ $message }}</div>
                                    @enderror
                                </div>
                                <div class="form-group">
                                    <label for="keywords">Ključne riječi/Tagovi</label>
                                    <small class="small text-info">Tagove odvajati zarezom (,)</small>
                                    <input type="text" id="keywords" name="keywords" class="form-control @error('keywords') is-invalid @enderror" value="{{ old('keywords') ?? $post->keywords }}">
                                    @error('keywords')
                                        <div class="small text-danger">{{ $message }}</div>
                                    @enderror
                                </div>
                                <div class="form-group position-relative">
                                    <label for="image">Priloži sliku</label>
                                    <small class="small text-info">Slika ne smije biti dimenzija manjih od 1035x286px (omjer 1 : 3,62)</small>
                                    <i class="fa fa-info-circle text-warning float-right" title="Spremanjem nove slike, postojeća slika će biti obrisana."></i>
                                    <input type="file" id="image" name="image" class="form-control @error('image') is-invalid @enderror" value="">
                                    @if($post->image_name && file_exists('storage/images/news/thumb/' . $post->image_name))
                                        <a href="{{ asset('storage/images/news/' . $post->image_name) }}" target="_blank" rel="noopener noreferrer">
                                            <img src="{{ asset('storage/images/news/thumb/' . $post->image_name) }}" 
                                                height="36" alt="{{ $post->image_name }}" 
                                                class="img-preview" title="Pogledaj u novom prozoru"
                                            >
                                        </a>
                                    @endif
                                    @error('image')
                                        <div class="small text-danger">{{ $message }}</div>
                                    @enderror
                                </div>
                                <div class="form-group">
                                    <label for="is_valid">Objavi</label><br>
                                    <input type="checkbox" id="is_valid" @if(old('is_valid') || (int)$post->is_valid === 1) checked @endif name="is_valid" class="js-switch" data-switchery="true" value="1">
                                    @error('is_valid')
                                        <div class="small text-danger">{{ $message }}</div>
                                    @enderror
                                </div>
                            </div>
                        </div>
                        
                        <!--  -->
                        <div class="form-group">
                            <button type="submit" class="btn btn-primary float-right">Spremi</button>
                        </div>
                    </form>
                    <!--  -->
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('js') 
<script>
$(document).ready(function() {
    // ck editors init
    let editorText = CKEDITOR.replace('text', {
        language: 'hr',
        toolbarGroups: [
            { name: 'clipboard',   groups: [ 'clipboard', 'undo' ] },
            { name: 'links' },
            { name: 'others' },
            { name: 'basicstyles', groups: [ 'basicstyles', 'cleanup' ] },
            { name: 'paragraph',   groups: [ 'list', 'indent', 'blocks', 'align' ] },
            { name: 'colors' },
            { name: 'insert', groups: ['image', 'table'] },
            { name: 'document', groups: [ 'mode', 'document', 'doctools' ] },
            { name: 'styles', groups: ['styles'] },
        ] 
    });
    CKFinder.setupCKEditor( editorText );

    let editorPreview = CKEDITOR.replace('preview', {
        language: 'hr',
        toolbarGroups: [
            { name: 'clipboard',   groups: [ 'clipboard', 'undo' ] },
            { name: 'links' },
            { name: 'others' },
            { name: 'basicstyles', groups: [ 'basicstyles', 'cleanup' ] },
            { name: 'paragraph',   groups: [ 'list', 'indent', 'blocks', 'align' ] },
            { name: 'colors' },
            { name: 'insert', groups: ['image', 'table'] },
            { name: 'document', groups: [ 'mode', 'document', 'doctools' ] },
            { name: 'styles', groups: ['styles'] },
        ] 
    });
    CKFinder.setupCKEditor( editorPreview );
    
    // switchery init
    let elem = document.querySelector('.js-switch');
    options = {
        color: "#FAA51A",
        size: "normal"
    };
    let init = new Switchery(elem, options);

    // create slug from post title
    $("#name").on('blur', function(e) {
            
        let title = $(this).val();
        let modelId = "{{ $post->id }}";

        if (title != '') {            
            $.ajax({
                method: 'GET',
                url: "{{ route('create-slug', app()->getLocale()) }}",
                data: {title, modelId},
                cache: false,
                timeout: 10000,
                success: function(data, textStatus, xhr){
                    if(xhr.status === 200){
                        $("#slug").val(data);
                    } else {
                        console.log(data.message);
                    }
                },
                error: function(xhr, statusText, error){
                    console.log('Error creating slug ' + xhr.status + ' ' + error);
                }
            });
        }
    });
});

// init datepickers
$("#datetime_on, #datetime_off").daterangepicker({
    singleDatePicker: true,
    locale: {
        format: 'YYYY-MM-DD',
        daysOfWeek: ['Ne', 'Po', 'Ut', 'Sr', 'Če', 'Pe', 'Su'],
        monthNames: ['Siječanj', 'Veljača', 'Ožujak', 'Travanj', 'Svibanj', 'Lipanj', 'Srpanj', 'Kolovoz', 'Rujan', 'Listopad', 'Studeni', 'Prosinac'],
        firstDay: 1
    }
});
</script>
@endsection