@extends('layouts.app')
@section('title', 'Novosti')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    <h3 class="card-title">Novosti</h3>
                    <span class="float-right">
                        <a href="{{ route('admin-post-create', app()->getLocale()) }}">
                            <button class="btn btn-primary">Nova novost</button>
                        </a>
                    </span>
                </div>

                <div class="card-body">
                    <table id="posts-table" class="table table-bordered table-striped">
                        <thead>
                            <th>#</th>
                            <th>Naslov</th>
                            <th>Pregled</th>
                            <th>Datum kreiranja</th>
                            <th>Objavljeno</th>
                            <th>Akcije</th>
                        </thead>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('js') 
<script>
$(document).ready(function() {
    let usersTable = $('#posts-table').DataTable({
        processing: true,
        serverSide: true,
        responsive: true,
        deferRender: true,
        stateSave: true,
        lengthMenu: [[10, 25, 50, -1], [10, 25, 50, "Sve"]],
        order: [[0, 'desc']],
        ajax: {
            url: "{{ route('admin-post-list', app()->getLocale()) }}"
        },
        columns: [
            { data: 'id', name: 'id' },
            { data: 'name', name: 'name', width: '330px'},
            { 
                data: 'preview', 
                name: 'preview',
                render: function(data, type, row) {
                    return $("<div/>").html(data).text();
                }
            },
            { 
                data: 'created_at', 
                name: 'created_at',
                orderable: false,
                width: '125px'
            },
            { 
                data: 'is_valid', 
                name: 'is_valid',
                orderable: false
            },
            {
                data: 'action',
                name: 'action',
                orderable: false,
                width: '195px'
            }
        ],
        language: {
            url: "{{ asset('datatable_languages/' . config('app.locale') .'.json') }}"
        }
    });
    
    //
    $("#posts-table").on('change', '.custom-control-input', function(e) {

        let publish = $(this).is(':checked') ? 1:0;
        let post_id = $(this).data('post-id');
        let route = $(this).data('route');
        $(".alert-messages").empty();

        $.ajax({
            method: "PATCH",
            url: route,
            data: {post_id, publish},
            cache: false,
            timeout: 30000,
            success: function(data) {
                if (data.error === false) {
                    let alert = `
                        <div class="alert alert-success alert-dismissible">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                            ${data.message}
                        </div>
                    `;
                    $(".alert-messages").append(alert);
                }
            },
            error: function(xhr, statusText, error) {
                $(".alert-messages").append(`<div class="alert alert-danger">${xhr.status} ${error}</div>`);
            }
        });
        
    });
});
</script>
@endsection