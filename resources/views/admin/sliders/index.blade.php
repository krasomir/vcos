@extends('layouts.app')
@section('title', 'Slider')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    <h3 class="card-title">Slider</h3>
                    <span class="float-right">
                        <a href="{{ route('sliders.create', app()->getLocale()) }}">
                            <button class="btn btn-primary">Novi slider</button>
                        </a>
                    </span>
                </div>

                <div class="card-body">
                    <table id="sliders-table" class="table table-bordered table-striped">
                        <thead>
                            <th>#</th>
                            <th>Naziv</th>
                            <th>Slika</th>
                            <th>Pozicija*</th>
                            <th>Link na stranicu</th>
                            <th>Akcije</th>
                        </thead>
                    </table>
                    <small class="text-info">* Klikom u polje pozicije nekog slidera omogućujemo upis vrijednosti i klikom izvan polja ili pomoću tipke enter potvrđujemo unos</small>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('js') 
<script>
$(document).ready(function() {
    let slidersTable = $('#sliders-table').DataTable({
        processing: true,
        serverSide: true,
        responsive: true,
        deferRender: true,
        stateSave: true,
        lengthMenu: [[10, 25, 50, -1], [10, 25, 50, "Sve"]],
        order: [[3, 'asc'], [0, 'asc']],
        ajax: {
            url: "{{ route('slider.list', app()->getLocale()) }}"
        },
        columns: [
            { data: 'id', name: 'id' },
            { data: 'title', name: 'title'},
            { 
                data: 'image_name', 
                name: 'image_name',
                orderable: false,
                width: '350px'
            },
            {
                data: 'position',
                name: 'position',
                className: 'slider-position'
            },
            {
                data: 'link_to_page',
                name: 'link_to_page'
            },
            {
                data: 'action',
                name: 'action',
                orderable: false,
                width: '135px'
            }
        ],
        language: {
            url: "{{ asset('datatable_languages/' . config('app.locale') .'.json') }}"
        }
    });

    // change position element attributes
    $("#sliders-table").on("click", "tbody .slider-position", function(e) {
        
        let sliderId = $(this).parent().attr("id");
        $(this).attr({
            "contenteditable": true,
            "data-id": sliderId,
            "data-old-position": $(this).text(),
        }).focus();
    });

    // send request to change position value on blur event
    $("#sliders-table").on("blur", "tbody .slider-position", function(e) {
                
        let sliderId = parseInt($(this).data("id"));
        let oldPosition = parseInt($(this).data("old-position"));
        let position = parseInt($(this).text());
        if (oldPosition != position) {
            updatePosition(sliderId, position);
        }        
    });
    // trigger blur event when user press on key enter
    $("#sliders-table").on("keydown", "tbody .slider-position", function(e) {
        
        let keycode = (e.keyCode) ? e.keyCode : e.which;
        if (keycode == 13) {
            e.preventDefault();
            $(this).blur();
        }
    });

    // function for sending update request
    function updatePosition(sliderId, position) {
        
        $(".alert-messages").empty();

        $.ajax({
            method: 'PATCH',
            url: "{{ route('update-slider-position', app()->getLocale()) }}",
            data: {sliderId, position},
            cache: false,
            timeout: 10000,
            success: function(data, textStatus, xhr){
                if (xhr.status === 200) {
                    let alert = `
                        <div class="alert alert-success alert-dismissible">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                            ${data.message}
                        </div>
                    `;
                    $(".alert-messages").append(alert);
                    slidersTable.ajax.reload();
                }
            },
            error: function(xhr, statusText, error){
                handleAjaxError(xhr);
                console.log('Error updating slider position: ' + xhr.status + ' ' + error);
            }
        });
    }
});
</script>
@endsection