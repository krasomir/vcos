@extends('layouts.app')
@section('title', 'Novi slider')


@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    <h3 class="card-title">Novi slider</h3>
                    <span class="float-right">
                        <a href="{{ route('sliders.index', app()->getLocale()) }}">
                            <button class="btn btn-primary">
                                <span class="fa fa-arrow-left"></span>
                            </button>
                        </a>
                    </span>
                </div>

                <div class="card-body">
                    
                    <form action="{{ route('sliders.store', app()->getLocale()) }}" id="storeSliderForm" method="post" enctype="multipart/form-data">
                        @csrf
                        <!--  -->
                        <div class="row">
                            <!--  -->
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="title">Naziv slidera</label>
                                    <input type="text" id="title" name="title" class="form-control @error('title') is-invalid @enderror" value="{{ old('title') }}">
                                    @error('title')
                                        <div class="small text-danger">{{ $message }}</div>
                                    @enderror
                                </div>
                                <div class="form-group">
                                    <label for="slug">Slug/SEO naziva</label>
                                    <input type="text" id="slug" name="slug" readonly class="form-control @error('slug') is-invalid @enderror" value="{{ old('slug') }}">
                                    @error('slug')
                                        <div class="small text-danger">{{ $message }}</div>
                                    @enderror
                                </div>
                                <div class="form-group">
                                    <label for="image">Priloži sliku</label>
                                    <small class="small text-info">Slika ne smije biti dimenzija manjih od 2031x953px (omjer 1 : 2,13)</small>
                                    <input type="file" id="image" name="image" class="form-control @error('image') is-invalid @enderror" value="">
                                    @error('image')
                                        <div class="small text-danger">{{ $message }}</div>
                                    @enderror
                                </div>
                                <div class="form-group">
                                    <label for="link_to_page">Link na stranicu</label>
                                    <select name="link_to_page" id="link_to_page" class="form-control @error('link_to_page') is-invalid @enderror" value="{{ old('link_to_page') }}">
                                        <option value="">-- izaberi stranicu --</option>
                                        @foreach ($routes as $route)
                                            <option 
                                                value="{{ $route['uri'] }}" 
                                                @if(old('link_to_page') == $route['uri']) selected @endif
                                            >{{ $route['name'] }}</option>
                                        @endforeach
                                    </select>
                                    @error('link_to_page')
                                        <div class="small text-danger">{{ $message }}</div>
                                    @enderror
                                </div>
                                <div class="form-group">
                                    <label for="title">Proizvoljni link</label>
                                    <input type="url" id="custom_link" name="custom_link" placeholder="https://www.google.hr" class="form-control @error('custom_link') is-invalid @enderror" value="{{ old('custom_link') }}">
                                    @error('custom_link')
                                        <div class="small text-danger">{{ $message }}</div>
                                    @enderror
                                </div>
                                <small class="small text-info">U slučaju da postoje oba linka upisana/izabrana, prednost ima 'Link na stranicu'</small>
                            </div>
                        </div>
                        
                        <!--  -->
                        <div class="form-group">
                            <button type="submit" class="btn btn-primary float-right">Spremi</button>
                        </div>
                    </form>

                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('js')
<script>
$(document).ready(function() {

    // create slug from slider title
    $("#title").on('blur', function(e) {
            
        let title = $(this).val();
        let modelId = 0;
        let type = 'slider';

        if (title != '') {            
            $.ajax({
                method: 'GET',
                url: "{{ route('create-slug', app()->getLocale()) }}",
                data: {title, modelId, type},
                cache: false,
                timeout: 10000,
                success: function(data, textStatus, xhr){
                    if(xhr.status === 200){
                        $("#slug").val(data);
                    } else {
                        console.log(data.message);
                    }
                },
                error: function(xhr, statusText, error){
                    console.log('Error creating slug ' + xhr.status + ' ' + error);
                }
            });
        }
    });
});
</script>
@endsection