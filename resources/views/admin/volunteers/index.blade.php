@extends('layouts.app')
@section('title', 'Volonteri')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    <h3 class="card-title">Volonteri</h3>
                </div>

                <div class="card-body">
                    <table id="volunteers-table" class="table table-bordered table-striped">
                        <thead>
                            <th>#</th>
                            <th>Ime i prezime</th>
                            <th>Email</th>
                            <th>Telefon</th>
                            <th>Mjesto</th>
                            <th>Akcije</th>
                        </thead>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('js') 
<script>
$(document).ready(function() {
    let volunteersTable = $('#volunteers-table').DataTable({
        processing: true,
        serverSide: true,
        responsive: true,
        deferRender: true,
        stateSave: true,
        lengthMenu: [[10, 25, 50, -1], [10, 25, 50, "Sve"]],
        order: [[1, 'asc']],
        ajax: {
            url: "{{ route('volunteers-list', app()->getLocale()) }}"
        },
        columns: [
            { data: 'id', name: 'id' },
            { data: 'first_last_name', name: 'first_last_name'},
            { data: 'email', name: 'email'},
            { data: 'telephone', name: 'telephone'},
            { data: 'city', name: 'city'},
            {
                data: 'action',
                name: 'action',
                orderable: false,
                width: '135px'
            }
        ],
        language: {
            url: "{{ asset('datatable_languages/' . config('app.locale') .'.json') }}"
        }
    });
});
</script>
@endsection