@extends('layouts.app')
@section('title', 'MailChimp liste')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    <h3 class="card-title">MailChimp lista - {{ $listName }}</h3>
                </div>

                <div class="card-body">
                    <div class="alert alert-{{ $alertType }}">
                        {{ $message }}
                    </div>
                    <div class="d-flex justify-content-between">
                        @if($status)
                            <h5>Status: {{ $status }}</h5>
                        @endif
                        @if($downloadResultURL)
                            <a href="{{ $downloadResultURL }}" class="btn btn-primary">Preuzmi rezultat importa</a>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection