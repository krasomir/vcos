@extends('layouts.app-pdf')
@section('title', $organisation->organisation_name)

@section('content')
    <x-volunteer-organisation-sop-details :organisation="$organisation" /> 
@endsection