@extends('layouts.app-pdf')
@section('title', $organisation->organisation_name)

@section('content')
    <x-volunteer-organisation-details :organisation="$organisation" /> 
@endsection