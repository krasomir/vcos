@extends('layouts.app-pdf')
@section('title', $volunteer->first_last_name)

@section('content')
    <x-volunteer-details :volunteer="$volunteer" /> 
@endsection