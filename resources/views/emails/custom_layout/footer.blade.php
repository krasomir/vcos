                            <!-- FOOTER -->
                            <tr>
                                <td align="center" valign="top" id="templateFooter">
                                    <!--[if (gte mso 9)|(IE)]>
                                    <table align="center" border="0" cellspacing="0" cellpadding="0" width="600" style="width:600px;">
                                    <tr>
                                    <td align="center" valign="top" width="600" style="width:600px;">
                                    <![endif]-->
                                    <table align="center" border="0" cellpadding="0" cellspacing="0" width="100%" class="templateContainer">
                                        <tr>
                                            <td valign="top" class="footerContainer">
                                                <table border="0" cellpadding="0" cellspacing="0" width="100%" class="mcnBoxedTextBlock" style="min-width:100%;">
                                                    <!--[if gte mso 9]>
                                                    <table align="center" border="0" cellspacing="0" cellpadding="0" width="100%">
                                                    <![endif]-->
                                                    <tbody class="mcnBoxedTextBlockOuter">
                                                        <tr>
                                                            <td valign="top" class="mcnBoxedTextBlockInner">
                                                                <!--[if gte mso 9]>
                                                                    <td align="center" valign="top"">
                                                                <![endif]-->
                                                                <table
                                                                    align="left"
                                                                    border="0"
                                                                    cellpadding="0"
                                                                    cellspacing="0"
                                                                    width="100%"
                                                                    style="min-width:100%;"
                                                                    class="mcnBoxedTextContentContainer"
                                                                    >
                                                                    <tbody>
                                                                        <tr>
                                                                            <td
                                                                                style="padding-top: 20px;padding-left:18px;padding-bottom: 20px;padding-right:18px;"
                                                                            >
                                                                                <table
                                                                                    border="0"
                                                                                    cellspacing="0"
                                                                                    class="mcnTextContentContainer"
                                                                                    width="100%"
                                                                                    style="min-width:100% !important;"
                                                                                >
                                                                                    <tbody>
                                                                                        <tr>
                                                                                            <td
                                                                                                valign="top"
                                                                                                class="mcnTextContent"
                                                                                                style="color: #000000;font-size: 14px;font-weight: normal;text-align: left;"
                                                                                            >
                                                                                                <div
                                                                                                    style="text-align: left;"
                                                                                                >
                                                                                                    <span
                                                                                                        style="font-size:16px"
                                                                                                        ><strong
                                                                                                            >DKolektiv
                                                                                                            -
                                                                                                            organizacija
                                                                                                            za
                                                                                                            društveni
                                                                                                            razvoj<br />
                                                                                                            Volonterski
                                                                                                            centar
                                                                                                            Osijek</strong
                                                                                                        ><br />
                                                                                                        Stjepana
                                                                                                        Radića
                                                                                                        16<br />
                                                                                                        31000
                                                                                                        Osijek<br />
                                                                                                        tel.
                                                                                                        031
                                                                                                        211
                                                                                                        306<br />
                                                                                                        fax.
                                                                                                        031
                                                                                                        200
                                                                                                        457<br />
                                                                                                        e-mail:
                                                                                                        <a
                                                                                                            href="mailto:dkolektiv@dkolektiv.hr"
                                                                                                            target="_blank"
                                                                                                            >
                                                                                                                <span>dkolektiv@dkolektiv.hr</span>
                                                                                                            </a
                                                                                                        ></span
                                                                                                    >
                                                                                                </div>

                                                                                                <div
                                                                                                    style="text-align: left;"
                                                                                                >
                                                                                                    <span
                                                                                                        style="font-size:16px"
                                                                                                        ><a
                                                                                                            href="https://www.dkolektiv.hr"
                                                                                                            target="_blank"
                                                                                                            >
                                                                                                                <span>https://www.dkolektiv.hr</span>
                                                                                                            </a
                                                                                                        ></span
                                                                                                    >
                                                                                                </div>
                                                                                            </td>
                                                                                        </tr>
                                                                                    </tbody>
                                                                                </table>
                                                                            </td>
                                                                        </tr>
                                                                    </tbody>
                                                                </table>
                                                                <!--[if gte mso 9]>
                                                                </td>
                                                                <![endif]-->

                                                                <!--[if gte mso 9]>
                                                                </tr>
                                                                </table>
                                                                <![endif]-->
                                                            </td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>

                                    <!--[if (gte mso 9)|(IE)]>
                                    </td>
                                    </tr>
                                    </table>
                                    <![endif]-->
                                </td>
                            </tr>