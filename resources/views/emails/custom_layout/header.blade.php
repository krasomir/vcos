                            <!-- HEADER -->
                            <tr>
                                <td
                                    align="center"
                                    valign="top"
                                    id="templateHeader"
                                    >
                                    <!--[if (gte mso 9)|(IE)]>
                                    <table align="center" border="0" cellspacing="0" cellpadding="0" width="600" style="width:600px;">
                                    <tr>
                                    <td align="center" valign="top" width="600" style="width:600px;">
                                    <![endif]-->
                                    <table
                                        align="center"
                                        border="0"
                                        cellpadding="0"
                                        cellspacing="0"
                                        width="100%"
                                        class="templateContainer"
                                        >
                                        <tr>
                                            <td
                                                valign="top"
                                                class="headerContainer"
                                                >
                                                <table
                                                    border="0"
                                                    cellpadding="0"
                                                    cellspacing="0"
                                                    width="100%"
                                                    class="mcnImageBlock"
                                                    style="min-width:100%;"
                                                    >
                                                    <tbody
                                                        class="mcnImageBlockOuter"
                                                        >
                                                        <tr>
                                                            <td
                                                                valign="top"
                                                                style="padding:0px"
                                                                class="mcnImageBlockInner"
                                                                >
                                                                <table
                                                                    align="left"
                                                                    width="100%"
                                                                    border="0"
                                                                    cellpadding="0"
                                                                    cellspacing="0"
                                                                    class="mcnImageContentContainer"
                                                                    style="min-width:100%;"
                                                                    >
                                                                    <tbody>
                                                                        <tr>
                                                                            <td
                                                                                class="mcnImageContent"
                                                                                valign="top"
                                                                                style="padding-right: 0px; padding-left: 0px; padding-top: 0; padding-bottom: 0; text-align:center;"
                                                                                >
                                                                                <a
                                                                                    href="https://dkolektiv.hr"
                                                                                    title=""
                                                                                    class=""
                                                                                    target="_blank"
                                                                                    >
                                                                                    <img
                                                                                        align="center"
                                                                                        alt="VCOS i DKolektiv logo"
                                                                                        src="https://mcusercontent.com/256150d26383ecb07fa6ef939/images/b51c91d4-17a6-9beb-254a-26a4801eada3.png"
                                                                                        width="600"
                                                                                        style="max-width: 1000px; padding-bottom: 0px; vertical-align: bottom; display: inline !important; border: 1px none;"
                                                                                        class="mcnImage"
                                                                                    />
                                                                                </a>
                                                                            </td>
                                                                        </tr>
                                                                    </tbody>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>

                                    <!--[if (gte mso 9)|(IE)]>
                                    </td>
                                    </tr>
                                    </table>
                                    <![endif]-->
                                </td>
                            </tr>