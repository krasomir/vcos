                            <!-- BODY -->
                            <tr>
                                <td align="center" valign="top" id="templateBody">
                                    <!--[if (gte mso 9)|(IE)]>
                                    <table align="center" border="0" cellspacing="0" cellpadding="0" width="600" style="width:600px;">
                                    <tr>
                                    <td align="center" valign="top" width="600" style="width:600px;">
                                    <![endif]-->
                                    <table
                                        align="center"
                                        border="0"
                                        cellpadding="0"
                                        cellspacing="0"
                                        width="100%"
                                        class="templateContainer"
                                        >
                                        <tr>
                                            <td
                                                valign="top"
                                                class="bodyContainer"
                                                >
                                                <table
                                                    border="0"
                                                    cellpadding="0"
                                                    cellspacing="0"
                                                    width="100%"
                                                    class="mcnTextBlock"
                                                    style="min-width:100%;"
                                                    >
                                                    <tbody
                                                        class="mcnTextBlockOuter"
                                                    >
                                                        <tr>
                                                            <td
                                                                valign="top"
                                                                class="mcnTextBlockInner"
                                                                style="padding-top:9px;"
                                                            >
                                                                <!--[if mso]>
                                                                <table align="left" border="0" cellspacing="0" cellpadding="0" width="100%" style="width:100%;">
                                                                <tr>
                                                                <![endif]-->

                                                                <!--[if mso]>
                                                                <td valign="top" width="600" style="width:600px;">
                                                                <![endif]-->
                                                                <table
                                                                    align="left"
                                                                    border="0"
                                                                    cellpadding="0"
                                                                    cellspacing="0"
                                                                    style="max-width:100%; min-width:100%;"
                                                                    width="100%"
                                                                    class="mcnTextContentContainer"
                                                                >
                                                                    <tbody>
                                                                        <tr>
                                                                            <td
                                                                                valign="top"
                                                                                class="mcnTextContent"
                                                                                style="padding-top:0; padding-right:18px; padding-bottom:9px; padding-left:18px;"
                                                                                >
                                                                                @yield('content')
                                                                            </td>
                                                                        </tr>
                                                                    </tbody>
                                                                </table>
                                                                <!--[if mso]>
                                                                </td>
                                                                <![endif]-->

                                                                <!--[if mso]>
                                                                </tr>
                                                                </table>
                                                                <![endif]-->
                                                            </td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                                <table
                                                    border="0"
                                                    cellpadding="0"
                                                    cellspacing="0"
                                                    width="100%"
                                                    class="mcnImageBlock"
                                                    style="min-width:100%;"
                                                    >
                                                    <tbody
                                                        class="mcnImageBlockOuter"
                                                    >
                                                        <tr>
                                                            <td
                                                                valign="top"
                                                                style="padding:9px"
                                                                class="mcnImageBlockInner"
                                                            >
                                                                <table
                                                                    align="left"
                                                                    width="100%"
                                                                    border="0"
                                                                    cellpadding="0"
                                                                    cellspacing="0"
                                                                    class="mcnImageContentContainer"
                                                                    style="min-width:100%;"
                                                                >
                                                                    <tbody>
                                                                        <tr>
                                                                            <td
                                                                                class="mcnImageContent"
                                                                                valign="top"
                                                                                style="padding-right: 9px; padding-left: 9px; padding-top: 0; padding-bottom: 0; text-align:center;"
                                                                            >
                                                                                <a
                                                                                    href="https::/dkolektiv.hr"
                                                                                    title=""
                                                                                    class=""
                                                                                    target="_blank"
                                                                                >
                                                                                    <img
                                                                                        align="center"
                                                                                        alt="Volonterski centar Osijek, Razvoj civilnog društva, Demo akademija, Društveni atelje - logo"
                                                                                        src="https://mcusercontent.com/256150d26383ecb07fa6ef939/images/22dc0372-16a3-a185-97ee-744b2f33e0e2.png"
                                                                                        width="564"
                                                                                        style="max-width:1000px; padding-bottom: 0; display: inline !important; vertical-align: bottom;"
                                                                                        class="mcnImage"
                                                                                    />
                                                                                </a>
                                                                            </td>
                                                                        </tr>
                                                                    </tbody>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>

                                    <!--[if (gte mso 9)|(IE)]>
                                    </td>
                                    </tr>
                                    </table>
                                    <![endif]-->
                                </td>
                            </tr>
                            