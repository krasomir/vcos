                                                                                <h1
                                                                                    class="null"
                                                                                    style="text-align: center;"
                                                                                     >
                                                                                    <span style="font-size:16px">
                                                                                        <span style="color:#73be47" >
                                                                                            <strong>HeadOnEast 2020.: Udruga Breza poziva volontere za pomoć pri organizaciji&nbsp;događaja "Hvatači snova"</strong>
                                                                                        </span>
                                                                                    </span>
                                                                                </h1>

                                                                                <p>
                                                                                    <br />
                                                                                    Drage volonterke i volonteri,
                                                                                </p>

                                                                                <p
                                                                                    style="text-align: left;"
                                                                                    >
                                                                                    Udruga
                                                                                    za
                                                                                    rad
                                                                                    s
                                                                                    mladima
                                                                                    Breza
                                                                                    u
                                                                                    sklopu
                                                                                    manifestacije
                                                                                    <strong
                                                                                        >HeadOnEast</strong
                                                                                    >
                                                                                    organizira
                                                                                    događaj
                                                                                    pod&nbsp;nazivom
                                                                                    "<strong
                                                                                        >Hvatači
                                                                                        snova</strong
                                                                                    >",
                                                                                    gdje
                                                                                    pripremaju
                                                                                    pregršt
                                                                                    radionica,
                                                                                    performansa
                                                                                    i
                                                                                    koncerata
                                                                                    za&nbsp;djecu,
                                                                                    mlade
                                                                                    i
                                                                                    sve
                                                                                    zainteresirane
                                                                                    građane.<br />
                                                                                    <br />
                                                                                    Događaj
                                                                                    će
                                                                                    započeti
                                                                                    u
                                                                                    <strong
                                                                                        >petak,
                                                                                        2.
                                                                                        listopada,
                                                                                        u
                                                                                        17:00
                                                                                        sati,
                                                                                        a
                                                                                        završava
                                                                                        u
                                                                                        nedjelju,
                                                                                        4.
                                                                                        listopada,
                                                                                        u&nbsp;13:00</strong
                                                                                    >.
                                                                                    Zbog
                                                                                    povećanog
                                                                                    obujma
                                                                                    posla
                                                                                    za
                                                                                    vrijeme
                                                                                    događaja,
                                                                                    potrebna
                                                                                    im
                                                                                    je
                                                                                    vaša
                                                                                    pomoć!<br />
                                                                                    <br />
                                                                                    Pozivaju
                                                                                    sve
                                                                                    zainteresirane
                                                                                    građane
                                                                                    da
                                                                                    se
                                                                                    prijave
                                                                                    za
                                                                                    volontiranje
                                                                                    na
                                                                                    način
                                                                                    da
                                                                                    ispune
                                                                                    ovaj&nbsp;prijavni
                                                                                    obrazac:
                                                                                    <a
                                                                                        href="https://docs.google.com/forms/d/e/1FAIpQLSfgbw7U4Wg5vTZld25nwv3leAcZYzY7ZcCtyWcJft99Yg54mQ/viewform"
                                                                                        target="_blank"
                                                                                        ><strong
                                                                                            ><span
                                                                                                style="color:#0000FF"
                                                                                                ><u
                                                                                                    >OBRAZAC</u
                                                                                                ></span
                                                                                            ></strong
                                                                                        ></a
                                                                                    ><br />
                                                                                    <br />
                                                                                    Svi
                                                                                    volonteri
                                                                                    koji
                                                                                    ispune
                                                                                    ovaj
                                                                                    obrazac
                                                                                    bit
                                                                                    će
                                                                                    pozvani
                                                                                    na
                                                                                    inicijalni
                                                                                    sastanak
                                                                                    prije
                                                                                    samog&nbsp;događaja.<br />
                                                                                    <br />
                                                                                    &nbsp;
                                                                                </p>