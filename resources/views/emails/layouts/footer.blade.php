<tr>
    <td>
        <table class="email-footer" align="center" width="570" cellpadding="0" cellspacing="0" role="presentation">
            <tr>
                <td class="content-cell" align="center">
                    <p class="f-fallback sub align-center">&copy; DKolektiv.hr - All rights reserved.</p>
                    <a href="https://www.dkolektiv.hr" class="email-masthead_name">
                        <img src="https://dkolektiv.hr/public/storage/images/dkolektiv_logo_sml.png" alt="{{ config('app.name') }}" class="logo" style="max-width:200px;">
                    </a>
                </td>
            </tr>
        </table>
    </td>
</tr>