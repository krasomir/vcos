<tr class="email-masthead_row">
    <td class="email-masthead">
        <a href="https://www.dkolektiv.hr" class="f-fallback email-masthead_name">
            <img src="https://dkolektiv.hr/public/storage/images/dkolektiv_logo_sml.png" alt="{{ config('app.name') }}" class="logo" style="max-width:200px">
        </a>
    </td>
</tr>