<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    @include('emails.layouts.head')
</head>

<body>
    <table class="email-wrapper" width="100%" cellpadding="0" cellspacing="0" role="presentation">
        <tr>
            <td align="center">
                <table class="email-content" width="100%" cellpadding="0" cellspacing="0" role="presentation">
                    
                    @include('emails.layouts.masthead')
                    <tr>
                        <td class="email-body" width="100%" cellpadding="0" cellspacing="0">
                            <table class="email-body_inner" align="center" width="570" cellpadding="0" cellspacing="0" role="presentation">
                                
                                <tr>
                                    <td class="content-cell">
                                        <div class="f-fallback">
                                            @yield('content')
                                        </div>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    @include('emails.layouts.footer')

                </table>
            </td>
        </tr>
    </table>
</body>

</html>