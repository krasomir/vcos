<header>
    <div class="top_bar">
        <div class="container">
            <div class="top_header_content">
                <div class="text-center">
                    <a href="https://www.dkolektiv.hr" title="{{ config('app.name') }}" class="logo">
                        <img src="https://dkolektiv.hr/public/storage/images/dkolektiv_logo_sml.png" alt="{{ config('app.name') }}" class="logo">
                    </a>
                </div><!--menu_logo end-->
                <div class="clearfix"></div>
            </div><!--top_header_content end-->
        </div>
    </div><!--header_content end-->
</header><!--header end-->