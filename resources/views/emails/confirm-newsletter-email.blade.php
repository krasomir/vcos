@extends('emails.custom_layout.emails')

@section('content')

<h2>
    @lang('Poštovani/a') {{ $model->first_last_name ?? $model->contact_person }},
</h2>
<br>

<p>@lang('Klikom na dugme niže potvrdite Vašu e-mail adresu.')</p>

<br>
<a href="{{ route('verify', ['token' => $token, 'lang' => config('app.locale')]) }}" class="button button--blue" target="_blank">@lang('Potvrdi e-mail')</a>
<br>

<br>
<br>
<hr>
<p>@lang('Ako, iz nekog razloga, ne možete koristiti dugme, onda kopirajte link niže i zaljepite ga u adresnu traku vašeg pretraživača.')</p>
<p class="small-link">{{ route('verify', ['token' => $token, 'lang' => config('app.locale')]) }}</p>

@endsection