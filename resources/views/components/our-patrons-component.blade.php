
<div class="section-patron">
    <div class="container">
        <div class="row">
            <div class="patron-network col-sm-12 col-md-4 mb-4">
                <div class="row section cta">
                    <div class="col-sm-12 text-center">
                        <p class="m-2">{{ __('Članstvo u mrežama') }}:</p>
                    </div>
                </div>
                <div class="row gutter-5 text-center p-3 mt-2">
                    <div class="col-12">
                        <a href="https://www.hcrv.hr/" target="_blank">
                            <img class="mxh-200 img-fluid" src="{{ asset('storage/images/hcrv_logo_400.png') }}" alt="HCRV">
                        </a>
                    </div>
                    <div class="col-12">
                        <a href="https://www.mmh.hr" target="_blank">
                            <img class="mxh-200 img-fluid" src="{{ asset('storage/images/clanstvo-u-mrezama-mmh_410.jpg') }}" alt="zaklada-za-razvoj">
                        </a>
                    </div>
                    <div class="col-12">
                        <a href="https://goo.hr/good-inicijativa/" target="_blank">
                            <img class="mxh-200 img-fluid" src="{{ asset('storage/images/clanstvo-u-mrezama-good_410.jpg') }}" alt="zaklada-za-razvoj">
                        </a>
                    </div>
                </div>
            </div>
            <!-- upravljanje kvalitetom -->
            <div class="patron-network col-sm-12 col-md-4 mb-4">
                <div class="row section cta">
                    <div class="col-sm-12 text-center">
                        <p class="m-2">{{ __('Upravljanje kvalitetom') }}:</p>
                    </div>
                </div>
                <div class="row gutter-5 text-center p-3 mt-2">
                    <div class="col-12 pb-3">
                        <a href="https://skockano.goo.hr/" target="_blank">
                            <img class="mxh-200 img-fluid" src="{{ asset('storage/images/skockano_organizacija.png') }}" alt="skockano organizacija logo">
                        </a>
                    </div>
                    <div class="col-12 pb-3">
                        <a href="https://dkolektiv.hr/public/hr/volonterski-centar-osijek" target="_blank">
                            <img class="mxh-200 img-fluid" src="{{ asset('storage/images/volonterska_oznaka_kvalitete.jpg') }}" alt="volonterska oznaka kvalitete">
                        </a>
                    </div>
                    <div class="col-12 pb-3">
                        <a href="https://dkolektiv.hr/public/hr/ambasadori-europskih-snaga-solidarnosti" target="_blank">
                            <img class="mxh-200 img-fluid" src="{{ asset('storage/images/ess_oznaka_kvalitete.png') }}" alt="snaga solidarnosti logo">
                        </a>
                    </div>
                    <div class="col-12 pb-3">
                        <a href="https://dkolektiv.hr/public/hr/posts/drustveni-atelje-u-becu-dobio-nagradu-sozialmarie-za-drustvenu-inovaciju" target="_blank">
                            <img class="mxh-200 img-fluid" src="{{ asset('storage/images/ogn2_sml.png') }}" alt="sozialmarie logo">
                        </a>
                    </div>
                    <div class="col-12 pb-3">
                        <a href="https://dkolektiv.hr/public/hr/posts/osvojili-smo-nagradu-european-social-services-award" target="_blank">
                            <img class="mxh-200 img-fluid" src="{{ asset('storage/images/ess_award_200.jpg') }}" alt="ESS Awards">
                        </a>
                    </div>
                </div>
            </div>
            <!-- financijski podržavaju -->
            <div class="patron-finance col-sm-12 col-md-4 mb-4">
                <div class="row section cta">
                    <div class="col-sm-12 text-center">
                        <p class="m-2">{{ __('Financijski podržavaju') }}:</p>
                    </div>
                </div>
                <div class="row gutter-5 text-center p-3 mt-2">
                    <div class="col-12">
                        <a href="https://acfcroatia.hr" target="_blank">
                            <img class="mxh-350 img-fluid" src="{{ asset('storage/images/financijski-podrzavaju-acf.jpg') }}" alt="ured-za-udruge">
                        </a>
                        <div class="text-center mb-3">DKolektiv je korisnik organizacijske podrške u razdoblju 1.9.2022. - 30.4.2024.</div>
                    </div>
                    <div class="col-6">
                        <a href="https://mrosp.gov.hr" target="_blank">
                            <img class="mxh-200 img-fluid" src="{{ asset('storage/images/financijski-podrzavaju-mrmsosp.jpg') }}" alt="ured-za-udruge">
                        </a>
                    </div>
                    <div class="col-6">
                        <a href="https://zaklada.civilnodrustvo.hr" target="_blank">
                            <img class="mxh-200 img-fluid" src="{{ asset('storage/images/financijski-podrzavaju-nzrcd.jpg') }}" alt="eu">
                        </a>
                    </div>
                    <div class="col-6">
                        <a href="https://udruge.gov.hr/" target="_blank">
                            <img class="mxh-200 img-fluid" src="{{ asset('storage/images/financijski-podrzavaju-uzu.jpg') }}" alt="eu">
                        </a>
                    </div>
                    <div class="col-6">
                        <a href="https://ec.europa.eu/info/index_hr" target="_blank">
                            <img class="mxh-200 img-fluid" src="{{ asset('storage/images/financijski-podrzavaju-ek.jpg') }}" alt="eu">
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
