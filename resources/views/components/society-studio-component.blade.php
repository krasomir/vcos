<!-- društveni atelje -->
<div class="other-activities society-studio h-150">
    <ul class="ml-4 mb-0 other-activities-sngle h-150">
        <div class="activity-title">
            <a href="{{ route('social-atelier', app()->getLocale()) }}">
                Društveni atelje
            </a>
        </div>
        <li>
            <a href="{{ route('social-atelier', app()->getLocale()) }}">
                <span class="font-italic">Mjesto susreta različitosti i osnaživanja za doprinos uključivoj zajednici</span>
            </a>
        </li>
    </ul>
</div>