<!-- banner component -->
<div class="section banner-page"
    @if($banner && $banner->image_name)
        data-background="{{ asset('storage/images/banners/' . $banner->image_name) }}"
    @endif
    >
    <div class="content-wrap pos-relative">
        <div class="d-flex justify-content-center bd-highlight mb-3">
            <div class="title-page">{{ $name }}</div>
        </div>
        <div class="d-flex justify-content-center bd-highlight mb-3">
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb ">
                    <li class="breadcrumb-item">
                        <a href="{{ route('home', app()->getLocale()) }}">
                            {{ __('Naslovnica') }}
                        </a>
                    </li>
                    @if($extraBreadcrumb)
                        <li class="breadcrumb-item">
                            <a href="{{ $extraBreadcrumb }}">
                                {{ $extraBreadcrumbName }}
                            </a>
                        </li>
                    @endif
                    <li class="breadcrumb-item active" aria-current="page">{{ $name }}</li>
                </ol>
            </nav>
        </div>
    </div>
</div>
@if ($banner)
    <script>
        document.getElementsByClassName("banner-page")[0].style.backgroundColor = "{{ $banner->bkg_color }}";
    </script>
@endif
