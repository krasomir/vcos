<div class="col-sm-6 col-md-4 col-lg-3">
    <div class="box-fundraising">
        <div class="body-content">
            <div class="meta text-right mb-1">
                <div class="date-archive">
                    <span>{{ __($category) }}</span>
                </div>
            </div>
            <p class="title">
                <a href="{{ $link }}">
                    {{ $title }}
                </a>
            </p>
            <div class="meta">
                <div class="date-archive">
                    <i class="fa fa-clock-o" aria-hidden="true"></i> {{ $date }}
                </div>
            </div>
            <div class="text font-italic global-search-result-text">
                <p>{!! $text !!}</p>
            </div>
        </div>
    </div>
</div>
