<!-- voluntering CTA component -->
<div class="section cta">
    <div class="content-wrap-20">
        <div class="container">
            <div class="row">
                <div class="col-sm-12 col-md-12">
                    <div class="cta-1">
                        <div class="body-text">
                            <h3>{{ __('Pridružite nam se za bolji život i lijepu budućnost.') }}</h3>
                        </div>
                        <div class="body-action">
                            <a href="#" class="btn btn-secondary">{{ strtoupper(__('Prijavite se za volontiranje')) }}</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>