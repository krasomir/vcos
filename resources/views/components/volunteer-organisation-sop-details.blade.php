<table id="volunteers-table" class="table table-bordered table-striped">
    <tbody>
        <tr>
            <td width="50%">Naziv organizacije</td>
            <td width="50%">{{ $organisation->organisation_name }}</td>
        </tr>
        <tr>
            <td>Vrsta organizacije</td>
            <td>{{ $organisation->organisation_type }}</td>
        </tr>
        <tr>
            <td>Godina osnutka</td>
            <td>{{ $organisation->foundation_year }}</td>
        </tr>
        <tr>
            <td>Ulica i kućni broj</td>
            <td>{{ $organisation->address }}</td>
        </tr>
        <tr>
            <td>Pošanski broj</td>
            <td>{{ $organisation->zip_number }}</td>
        </tr>
        <tr>
            <td>Mjesto</td>
            <td>{{ $organisation->city }}</td>
        </tr>
        <tr>
            <td>Kontakt osoba</td>
            <td>{{ $organisation->contact_person }}</td>
        </tr>
        <tr>
            <td>Telefon</td>
            <td>{{ $organisation->telephone }}</td>
        </tr>
        <tr>
            <td>E-mail</td>
            <td>{{ $organisation->email }}</td>
        </tr>
        <tr>
            <td>Web stranica</td>
            <td>
                @if($organisation->web)
                    <a href="https://{{ $organisation->web }}" target="_blank">
                        {{ $organisation->web }}
                    </a>
                @endif
            </td>
        </tr>
        <tr>
            <td>Misija i ciljevi organizacije</td>
            <td>{{ $organisation->mission_goals }}</td>
        </tr>
        <tr>
            <td>Područja djelovanja</td>
            <td>
                <ul>
                    @foreach($organisation->activity_area as $area)
                        <li>{{ $area }}</li>
                    @endforeach
                </ul>
            </td>
        </tr>
        <tr>
            <td>Ciljna skupina/korisnici organizacije</td>
            <td>{{ $organisation->target_group }}</td>
        </tr>
        <tr>
            <td>Zemljopisno područje djelovanja</td>
            <td>
                <ul>
                    @foreach($organisation->geo_area as $area)
                        <li>{{ $area }}</li>
                    @endforeach
                </ul>
            </td>
        </tr>
        <tr>
            <td>Newsletter</td>
            <td>{{ (int) $organisation->newsletter === 1 ? 'Da': 'Ne' }}</td>
        </tr>
    </tbody>
</table> 