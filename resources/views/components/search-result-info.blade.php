<!-- meta info -->
<div class="row mb-3">
    <div class="col-sm-12 col-md-12">
        <nav aria-label="Page navigation">
            {!! __('Za traženi pojam <span class="bold">:searchTerm</span> pronađeno je rezultata: :count', [
                'count' => $searchResultsCount,
                'searchTerm' => $searchTerm
            ]) !!}
        </nav>
    </div>
</div>
