<table id="volunteers-table" class="table table-bordered table-striped">
    <tbody>
        <tr>
            <td width="50%">Naziv organizacije</td>
            <td width="50%">{{ $organisation->organisation_name }}</td>
        </tr>
        <tr>
            <td>Vrsta organizacije</td>
            <td>{{ $organisation->organisation_type }}</td>
        </tr>
        <tr>
            <td>Sjedište organizacije (ulica i kućni broj)</td>
            <td>{{ $organisation->address }}</td>
        </tr>
        <tr>
            <td>Mjesto</td>
            <td>{{ $organisation->city }}</td>
        </tr>
        <tr>
            <td>Telefon organizacije</td>
            <td>{{ $organisation->telephone }}</td>
        </tr>
        <tr>
            <td>Kontakt e-mail</td>
            <td>{{ $organisation->contact_email }}</td>
        </tr>
        <tr>
            <td>Kontakt telefon</td>
            <td>{{ $organisation->contact_telephone }}</td>
        </tr>
        <tr>
            <td>Kontakt osoba</td>
            <td>{{ $organisation->contact_person }}</td>
        </tr>
        <tr>
            <td>Volontere trebate</td>
            <td>{{ $organisation->volunteer_engagement }}</td>
        </tr>
        <tr>
            <td>Broj potrebnih volontera</td>
            <td>{{ $organisation->volunteer_number }}</td>
        </tr>
        <tr>
            <td>Naziv volonterske pozicije</td>
            <td>{{ $organisation->position_name }}</td>
        </tr>
        <tr>
            <td>Svrha volonterske pozicije</td>
            <td>{{ $organisation->purpose_of_position }}</td>
        </tr>
        <tr>
            <td>Opis zadatka koji bi volonter obavljao</td>
            <td>{{ $organisation->tasks_description }}</td>
        </tr>
        <tr>
            <td>Kvalifikacije, znanje, vještine volontera?</td>
            <td>{{ $organisation->volunteer_qualifications }}</td>
        </tr>
        <tr>
            <td>Razdoblje volontiranja</td>
            <td>{{ $organisation->volunteer_period }}</td>
        </tr>
        <tr>
            <td>Broj sati</td>
            <td>{{ $organisation->hour_number }}</td>
        </tr>
        <tr>
            <td>Mjesto volontiranja</td>
            <td>{{ $organisation->volunteering_place }}</td>
        </tr>
        <tr>
            <td>Pružanje dodatnog obrazovanja (poduke) za obavljanje volonterskog angažmana</td>
            <td>{{ $organisation->additional_education }}</td>
        </tr>
        <tr>
            <td>Osiguravanje pokrivanja troškova (prijevoza, hrane, smještaja)</td>
            <td>{{ $organisation->expense_refund }}</td>
        </tr>
        <tr>
            <td>Rok za prijavu</td>
            <td>{{ $organisation->application_deadline->format('d.m.Y') }}</td>
        </tr>
        <tr>
            <td>Članovi organizacije završili edukaciju za koordinatora volontera</td>
            <td>{{ $organisation->volunteer_menagement }}</td>
        </tr>
        <tr>
            <td>Zainteresirani za treninge o upravljanju kvalitetnim volonterskim programima</td>
            <td>{{ $organisation->leadership_training }}</td>
        </tr>
        <tr>
            <td>Pomoć pri organizaciji</td>
            <td>{{ $organisation->organisation_help }}</td>
        </tr>
        <tr>
            <td>Zainteresirani primati informacije te info bilten Volonterskog centra Osijek?</td>
            <td>{{ $organisation->newsletter }}</td>
        </tr>
    </tbody>
</table>