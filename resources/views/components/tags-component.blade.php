<!-- tags component -->
<div class="tagcloud">
    @foreach($tags as $tag)
        <a href="#" title="{{ $tag }}">{{ $tag }}</a>
    @endforeach
</div>