<!-- podcast player component -->
<div class="pcast-player">
    <div class="pcast-player-controls">
        <div class="pcast-control-btns">
            <button class="pcast-play">
                <i class="fa fa-play"></i>
                <span>Play</span>
            </button>
            <button class="pcast-pause">
                <i class="fa fa-pause"></i>
                <span>Pause</span>
            </button>
            <button class="pcast-rewind">
                <i class="fa fa-fast-backward"></i>
                <span>Rewind</span>
            </button>
            <button class="pcast-speed">1x</button>
            <button class="pcast-mute">
                <i class="fa fa-volume-up"></i>
                <span>Mute/Unmute</span>
            </button>
            <span class="pcast-title">
                {{ $title }}
            </span>
        </div>
        <div class="pcast-timings">
            <span class="pcast-currenttime pcast-time">00:00</span>
            <span class="pcast-duration pcast-time">00:00</span>
        </div>
        <div class="pcast-progress-wrapper">
            <progress class="pcast-progress" value="0"></progress>
        </div>
    </div>
    <audio>
        <source src="{{ asset('storage/files/podcasts/' . $fileName) }}" type="audio/mpeg">
        <p>Your browser doesn't support HTML5 audio.</p>
    </audio>
</div>