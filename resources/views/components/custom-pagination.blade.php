@if ($paginator->hasPages())
<!-- visible on md and up -->
<ul class="d-none d-md-block-pagination pagination">
    <!-- first page -->
    <li class="page-item @if($paginator->onFirstPage()) active disabled @endif">
        <a href="?page=1" class="page-link" rel="prev">Prva</a>
    </li>

    @if (!$paginator->onFirstPage())
        <li class="page-item">
            <a href="{{ $paginator->previousPageUrl() }}" class="page-link" rel="prev">Predhodna</a>
        </li>
    @endif

    @foreach ($elements as $element)

        @if (is_string($element))
            <li class="page-item disabled">
                <a class="page-link" href="#">{{ $element }}</a>
            </li>
        @endif

        @if (is_array($element))
            @foreach ($element as $page => $url)
                @if(
                    ($page - 1) <= $paginator->currentPage() && 
                    ($page + 1) >= $paginator->currentPage()
                )
                    @if ($page == $paginator->currentPage())
                    <li class="page-item active disabled">
                        <a class="page-link" href="#">{{ $page }}</a>
                    </li>
                    @else
                    <li class="page-item ">
                        <a href="{{ $url }}" class="page-link">{{ $page }}</a>
                    </li>
                    @endif
                @endif
            @endforeach
        @endif
    @endforeach

    @if ($paginator->hasMorePages())
        <li class="page-item">
            <a href="{{ $paginator->nextPageUrl() }}" class="page-link" rel="next">Slijedeća</a>
        </li>
    @endif
    <!-- last page -->
    <li class="page-item @if(!$paginator->hasMorePages()) active disabled @endif">
        <a href="?page={{ $paginator->lastPage() }}" class="page-link" rel="prev">Zadnja</a>
    </li>
</ul>

<!-- visible on mobile -->
<ul class="d-sm-block-pagination d-md-none pagination">
    <!-- first page -->
    <li class="page-item @if($paginator->onFirstPage()) active disabled @endif">
        <a href="?page=1" class="page-link" rel="prev">
            <i class="fa fa-angle-double-left"></i>
        </a>
    </li>

    @if (!$paginator->onFirstPage())
        <li class="page-item">
            <a href="{{ $paginator->previousPageUrl() }}" class="page-link" rel="prev">
                <i class="fa fa-angle-left"></i>
            </a>
        </li>
    @endif

    @foreach ($elements as $element)
        @if (is_array($element))
            @foreach ($element as $page => $url)
                @if ($page == $paginator->currentPage())
                    <li class="page-item active disabled">
                        <a class="page-link" href="#">{{ $page }}</a>
                    </li>
                @endif
            @endforeach
        @endif
    @endforeach

    @if ($paginator->hasMorePages())
        <li class="page-item">
            <a href="{{ $paginator->nextPageUrl() }}" class="page-link" rel="next">
                <i class="fa fa-angle-right"></i>
            </a>
        </li>
    @endif
    <!-- last page -->
    <li class="page-item @if(!$paginator->hasMorePages()) active disabled @endif">
        <a href="?page={{ $paginator->lastPage() }}" class="page-link" rel="prev">
            <i class="fa fa-angle-double-right"></i>
        </a>
    </li>


</ul>
@endif