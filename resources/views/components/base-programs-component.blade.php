<!-- base programs component -->
<div>
    <select name="program_page" id="program_page" class="form-control @error('program_page') is-invalid @enderror">
        <option value="">-- izaberi stranicu programa --</option>
        @foreach ($basePrograms as $key => $program)
            <option value="{{ $key }}" 
                @if($key === $selected) selected @endif
                @if(in_array($key, $disabledPrograms)) disabled @endif
                >
                {{ $program }}
            </option>
        @endforeach
    </select>
</div>