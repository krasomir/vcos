<!-- volunteer detail component -->
<table id="volunteers-table" class="table table-bordered table-striped">
    <tbody>
        <tr>
            <td>Ime i prezime</td>
            <td>{{ $volunteer->first_last_name }}</td>
        </tr>
        <tr>
            <td>Spol</td>
            <td>{{ (int)$volunteer->gender === 1 ? 'M' : 'Ž' }}</td>
        </tr>
        <tr>
            <td>Godina rođenja</td>
            <td>{{ $volunteer->birth_year }}</td>
        </tr>
        <tr>
            <td>Mjesto stanovanja</td>
            <td>{{ $volunteer->city }}</td>
        </tr>
        <tr>
            <td>Telefon</td>
            <td>{{ $volunteer->telephone }}</td>
        </tr>
        <tr>
            <td>E-mail adresa</td>
            <td>{{ $volunteer->email }}</td>
        </tr>
        <tr>
            <td>Zanimanje</td>
            <td>{{ $volunteer->profession }}</td>
        </tr>
        <tr>
            <td>Dodatne vještine, interesi, hobiji</td>
            <td>{{ $volunteer->additional_skills }}</td>
        </tr>
        <tr>
            <td>Dosadašnje volontersko iskustvo</td>
            <td>{{ $volunteer->volunteer_experience }}</td>
        </tr>
        <tr>
            <td>Područja volontiranja</td>
            <td>
                <ul>
                    @foreach($volunteer->work_area as $area)
                        <li>{{ $area }}</li>
                    @endforeach
                <ul>
            </td>
        </tr>
        <tr>
            <td>Volonterski angažman</td>
            <td>{{ $volunteer->volunteer_engagement }}</td>
        </tr>
        <tr>
            <td>Na kojem geografskom području želite volontirati</td>
            <td>
                <ul>
                    @foreach($volunteer->geo_area as $area)
                        <li>{{ $area }}</li>
                    @endforeach
                </ul>
            </td>
        </tr>
        <tr>
            <td>Slavonija i Baranje pobliže</td>
            <td>{{ $volunteer->geo_area_sib }}</td>
        </tr>
        <tr>
            <td>Ograničenja/napomene</td>
            <td>{{ $volunteer->exclude_area }}</td>
        </tr>
        <tr>
            <td>Kako ste saznali za Volonterski centar Osijek</td>
            <td>
                <ul>
                    @foreach($volunteer->source_info as $info)
                        <li>{{ $info }}</li>
                    @endforeach
                    <li>{{ $volunteer->source_info_other }}</li>
                </ul>
            </td>
        </tr>
        <tr>
            <td>Newsletter</td>
            <td>{{ (int) $volunteer->newsletter === 1 ? 'Da' : 'Ne' }}</td>
        </tr>
    </tbody>
</table>