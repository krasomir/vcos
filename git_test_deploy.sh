#!/bin/sh

GREEN='\033[0;32m'
YELLOW='\033[1;33m'
NC='\033[0m' # No Color

for branch in test; do
    git checkout "${branch}"
    echo -e "-> ${YELLOW}Checked out to ${branch}${NC}"
    git merge develop
    echo -e "-> ${YELLOW}Merged develop${NC}"
    git push
    echo -e "-> ${YELLOW}Pushed to ${branch}${NC}"
    git push deploy "${branch}"
    echo -e "-> ${GREEN}Deployed to ${branch}${NC}"
    echo -e "----------------------\n"
done

git checkout develop-admin