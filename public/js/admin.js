$(function() {
    $.ajaxSetup({
        headers: {
            "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content"),
        },
    });
    // on show delete modal
    $("#confirmDelete").on('show.bs.modal', function(e) {
        let button = $(e.relatedTarget);
        let modal = $(this);
        let title = button.data('title');
        let message = button.data('message');
        let link = button.data('delete-route');
        let id = button.data('model-id');
        let mediaPath = button.data('media-path');
        let mediaType = button.data('media-type');

        modal.find("#confirmDeleteLabel").text(title);
        modal.find(".modal-body").text(message);
        modal.find("#deleteForm").attr('action', link);
        modal.find("#id").val(id);
        modal.find("#mediaPath").val(mediaPath);
        if (mediaType === 'image') {
            modal.find(".modal-body").append(`<img src="../../${mediaPath}" />`);
        }
    });
});

/**
 * handle ajax errors
 * @param {*} xhr 
 */
 function handleAjaxError(xhr) {
    if(xhr.status === 422) {
        $.each(xhr.responseJSON.errors, (_key, val) => {
            $(".alert-messages").append(`<div class="alert alert-danger">${val}</div>`);
        });
    } else if(xhr.status >= 400) {
        $(".alert-messages").append(`<div class="alert alert-danger">${xhr.responseJSON.message}</div>`);
    } else {
        $.each(xhr.responseJSON.errors, (_key, val) => {
            $(".alert-messages").append(`<div class="alert alert-danger">${val}</div>`);
        });
    }
}