$(document).ready(function(){
    // dynamic change of text
    let settings = {
        type: 'html',
        animationSpeed: [400,300],
        delay: 5000
    };
    $('.dynamic-slogans').atrotating(settings);

    handleTeamMemberDescription();
});

// scroll to element
function scrollToElement(element) {

    $('html, body').stop().animate({
        scrollTop: $(element).offset().top - 150 + 'px'
    }, 1000);
}

/**
 * handle ajax errors
 * @param {*} xhr
 */
function handleAjaxError(xhr) {
    if (xhr.status >= 500) {
        //toastr.error(xhr.responseJSON.message);
    } else if(xhr.status === 422) {
        $.each(xhr.responseJSON.errors, (_key, val) => {
            console.log(_key + ' - log - ' + val);
            //toastr.error(val);
        });
    } else if(xhr.status >= 400 && xhr.status < 500) {
        //toastr.warning(xhr.responseJSON.message);
    } else {
        $.each(xhr.responseJSON.errors, (_key, val) => {
            //toastr.error(val);
        });
    }
}

function handleTeamMemberDescription() {
    // Ensure descriptions start hidden
    $('.team-member-container .media').hover(
        function() {
            $('.team-member-container').css('min-height', '');
            $('.team-member-description').css('min-height', '');
            $('.vertical-centered').css('min-height', '');
            $(this).closest('.team-member-container').addClass('hover-active');

            const $container = $(this).closest('.team-member-container');
            const $description = $container.find('.team-member-description');
            const $verticalCentered = $container.find('.vertical-centered');

            // Position the description properly on hover
            const containerHeight = $container.height();
            const descriptionHeight = $description.outerHeight();

            // Adjust position if description is taller than container
            if (descriptionHeight > containerHeight) {
                $description.css('top', '0');
                $container.css('min-height', descriptionHeight);
                $verticalCentered.css('min-height', descriptionHeight);
            } else {
                $description.css('top', '50%').css('transform', 'translateY(-50%)');
                $description.css('min-height', containerHeight);
                $verticalCentered.css('min-height', containerHeight);
            }
        },
        function() {
            $(this).closest('.team-member-container').removeClass('hover-active');
        }
    );

    // Also handle direct hover on the description to maintain visibility
    $('.team-member-description').hover(
        function() {
            $(this).closest('.team-member-container').addClass('hover-active');
        },
        function() {
            $(this).closest('.team-member-container').removeClass('hover-active');
        }
    );
}
