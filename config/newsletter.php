<?php

return [

    /*
     * The driver to use to interact with MailChimp API.
     * You may use "log" or "null" to prevent calling the
     * API directly from your environment.
     */
    'driver' => env('MAILCHIMP_DRIVER', 'api'),

    /*
     * The API key of a MailChimp account. You can find yours at
     * https://us10.admin.mailchimp.com/account/api-key-popup/.
     */
    'apiKey' => env('MAILCHIMP_APIKEY'),
    'vcos_apikey' => env('MAILCHIMP_VCOS_APIKEY'),
    'ss_apikey' => env('MAILCHIMP_SS_APIKEY'),

    /*
     * The listName to use when no listName has been specified in a method.
     */
    'defaultListName' => 'subscribers',
    'defaultSSListName' => 'druge_zupanije',

    /*
     * Here you can define properties of the lists.
     */
    'lists' => [

        /*
         * This key is used to identify this list. It can be used
         * as the listName parameter provided in the various methods.
         *
         * You can set it to any string you want and you can add
         * as many lists as you want.
         */
        'subscribers' => [

            /*
             * A MailChimp list id. Check the MailChimp docs if you don't know
             * how to get this value:
             * http://kb.mailchimp.com/lists/managing-subscribers/find-your-list-id.
             */
            'id' => env('MAILCHIMP_VCOS_LIST_ID'),

            /*
             * The GDPR marketing permissions of this audience.
             * You can get a list of your permissions with this command: "php artisan newsletter:permissions"
             */
            'marketing_permissions' => [
                // 'email' => '2a4819ebc7',
                // 'customized_online_advertising' => '4256fc7dc5',
            ],

        ],
        /**
         * Lists for Snaga sudjelovanja
         */
        'druge_zupanije' => [
            'id' => env('MAILCHIMP_DRUGE_ZUPANIJE_LIST_ID'),
            'marketing_permissions' => [
                // 'email' => '2a4819ebc7',
                // 'customized_online_advertising' => '4256fc7dc5',
            ],
        ],
        'osjecko_baranjska' => [
            'id' => env('MAILCHIMP_OSJECKO_BARANJSKA_LIST_ID'),
            'marketing_permissions' => [
                // 'email' => '2a4819ebc7',
                // 'customized_online_advertising' => '4256fc7dc5',
            ],
        ],
        'vukovarsko_srijemska' => [
            'id' => env('MAILCHIMP_VUKOVARSKO_SRIJEMSKA_LIST_ID'),
            'marketing_permissions' => [
                // 'email' => '2a4819ebc7',
                // 'customized_online_advertising' => '4256fc7dc5',
            ],
        ],
        'brodsko_posavska' => [
            'id' => env('MAILCHIMP_BRODSKO_POSAVSKA_LIST_ID'),
            'marketing_permissions' => [
                // 'email' => '2a4819ebc7',
                // 'customized_online_advertising' => '4256fc7dc5',
            ],
        ],
        'pozesko_slavonska' => [
            'id' => env('MAILCHIMP_POZESKO_SLAVONSKA_LIST_ID'),
            'marketing_permissions' => [
                // 'email' => '2a4819ebc7',
                // 'customized_online_advertising' => '4256fc7dc5',
            ],
        ],
    ],

    /*
     * If you're having trouble with https connections, set this to false.
     */
    'ssl' => true,

];
