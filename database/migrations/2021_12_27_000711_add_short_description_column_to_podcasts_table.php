<?php

use App\Podcast;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddShortDescriptionColumnToPodcastsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('podcasts', function (Blueprint $table) {
            $table->mediumText('short_description')->after('slug')->nullable();
            $table->dateTime('published_at')->after('file_name')->nullable();
        });

        $podcasts = Podcast::all();
        foreach ($podcasts as $podcast) {
            $podcast->published_at = $podcast->created_at;
            $podcast->save();
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('podcasts', function (Blueprint $table) {
            $table->dropColumn('short_description');
            $table->dropColumn('published_at');
        });
    }
}
