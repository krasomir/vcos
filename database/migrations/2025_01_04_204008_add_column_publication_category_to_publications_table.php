<?php

use App\Enums\PublicationCategoriesEnum;
use App\Publication;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::table('publications', function (Blueprint $table) {
            $table->enum('publication_category', PublicationCategoriesEnum::values())
                ->after('slug')
                ->default(PublicationCategoriesEnum::PROFESSIONAL_MANUALS);
        });

        // mapiran redoslijed od najstarijeg (id,asc) [1,3,13,25,28,29,30,32,36] u ID-eve
        $researchAndAnalysisIds = [2,4,14,31,34,35,36,38,42];
        $publications = Publication::whereIn('id', $researchAndAnalysisIds)->get();

        foreach ($publications as $publication) {
            $publication->publication_category = PublicationCategoriesEnum::RESEARCH_AND_ANALYSIS->value;
            $publication->save();
        }
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::table('publications', function (Blueprint $table) {
            $table->dropColumn('publication_category');
        });
    }
};
