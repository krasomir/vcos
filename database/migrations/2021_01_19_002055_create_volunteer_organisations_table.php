<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateVolunteerOrganisationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('volunteer_organisations', function (Blueprint $table) {
            $table->id();
            $table->string('organisation_name');
            $table->string('organisation_type');
            $table->string('address');
            $table->string('city');
            $table->string('telephone');
            $table->string('contact_telephone')->nullable();
            $table->string('contact_person')->nullable();
            $table->string('contact_email')->nullable();
            $table->string('volunteer_engagement');
            $table->integer('volunteer_number');
            $table->string('position_name');
            $table->string('purpose_of_position');
            $table->string('tasks_description');
            $table->string('volunteer_qualifications');
            $table->string('volunteer_period');
            $table->integer('hour_number');
            $table->string('volunteering_place');
            $table->tinyInteger('additional_education')->default(0);
            $table->tinyInteger('expense_refund')->default(0);
            $table->date('application_deadline');
            $table->tinyInteger('volunteer_menagement')->default(0);
            $table->tinyInteger('leadership_training')->default(0);
            $table->tinyInteger('organisation_help')->default(0);
            $table->tinyInteger('newsletter')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('volunteer_organisations');
    }
}
