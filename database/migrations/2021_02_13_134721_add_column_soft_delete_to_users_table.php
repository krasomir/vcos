<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddColumnSoftDeleteToUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->softDeletes()->after('remember_token');
        });

        Schema::table('volunteers', function (Blueprint $table) {
            $table->softDeletes()->after('newsletter');
        });

        Schema::table('volunteer_organisations', function (Blueprint $table) {
            $table->softDeletes()->after('newsletter');
        });

        Schema::table('volunteer_organisation_strength_of_participations', function (Blueprint $table) {
            $table->softDeletes()->after('geo_area');
        });

        Schema::table('blogs', function (Blueprint $table) {
            $table->softDeletes()->after('is_valid');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->dropSoftDeletes();
        });

        Schema::table('volunteers', function (Blueprint $table) {
            $table->dropSoftDeletes();
        });

        Schema::table('volunteer_organisations', function (Blueprint $table) {
            $table->dropSoftDeletes();
        });

        Schema::table('volunteer_organisation_strength_of_participations', function (Blueprint $table) {
            $table->dropSoftDeletes();
        });

        Schema::table('blogs', function (Blueprint $table) {
            $table->dropSoftDeletes();
        });
    }
}
