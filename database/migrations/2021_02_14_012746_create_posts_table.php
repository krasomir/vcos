<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePostsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('posts', function (Blueprint $table) {
            $table->id();
			$table->foreignId('author_id')->nullable()->constrained('users')->nullOnDelete();
			$table->string('slug', 255);
			$table->string('name', 255);
			$table->dateTime('datetime_on');
			$table->dateTime('datetime_off');
			$table->text('preview');
			$table->text('text');
			$table->string('title', 255)->nullable();
			$table->text('keywords')->nullable();
			$table->text('description')->nullable();
			$table->tinyInteger('is_valid')->default(0);
            $table->text('image_name')->nullable();
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('posts');
    }
}
