<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class ChangeExpenseRefundColumnToVolunteerOrganisationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('volunteer_organisations', function (Blueprint $table) {
            $table->string('expense_refund')->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('volunteer_organisations', function (Blueprint $table) {
            $table->tinyInteger('expense_refund')->change();
        });
    }
}
