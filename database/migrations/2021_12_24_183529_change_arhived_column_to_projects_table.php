<?php

use App\Project;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

class ChangeArhivedColumnToProjectsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        // get archived project
        $archivedProjects = $this->getArchivedProject();

        Schema::table('projects', function (Blueprint $table) {
            $table->dropColumn('archived');
        });

        Schema::table('projects', function (Blueprint $table) {
            $table->dateTime('archived')->nullable()->after('published');
        });

        // set archive column to project archive date
        foreach ($archivedProjects as $project) {
            $project->archived = $project->updated_at;
            $project->save();
        }
        
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('projects', function (Blueprint $table) {
            $table->tinyInteger('archived')->default(0)->change();
        });
    }

    protected function getArchivedProject()
    {
        return Project::select('id', 'updated_at')->where('archived', '1')->get();
    }
}
