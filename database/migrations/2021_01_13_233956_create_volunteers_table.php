<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateVolunteersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('volunteers', function (Blueprint $table) {
            $table->id();
            $table->string('first_last_name');
            $table->tinyInteger('gender')->comment('male = 1, female = 2');
            $table->integer('birth_year');
            $table->string('city');
            $table->string('telephone')->nullable();
            $table->string('email');
            $table->string('profession');
            $table->text('additional_skills')->nullable();
            $table->text('volunteer_experience')->nullable();
            $table->text('volunteer_engagement');
            $table->text('work_area');
            $table->text('geo_area');
            $table->text('geo_area_sib')->nullable();
            $table->text('exclude_area')->nullable();
            $table->text('source_info');
            $table->text('source_info_other')->nullable();
            $table->tinyInteger('newsletter')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('volunteers');
    }
}
