<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddVerifiedColumnToVolunteerOrganisationStrengthOfParticipationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('volunteer_organisation_strength_of_participations', function (Blueprint $table) {
            $table->tinyInteger('newsletter')->default(0)->after('geo_area');
            $table->dateTime('verified_at')->after('newsletter')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('volunteer_organisation_strength_of_participations', function (Blueprint $table) {
            $table->dropColumn('newsletter');
            $table->dropColumn('verified_at');
        });
    }
}
