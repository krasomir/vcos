<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBlogsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('blogs', function (Blueprint $table) {
            $table->id();
			$table->foreignId('author_id')->nullable()->constrained('users')->nullOnDelete();
			$table->string('slug', 255);
			$table->string('name', 255);
			$table->date('datetime_on');
			$table->date('datetime_off');
			$table->text('preview');
			$table->text('text');
			$table->string('title', 255);
			$table->text('keywords')->nullable();
			$table->text('description')->nullable();
			$table->tinyInteger('is_valid')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('blogs');
    }
}
