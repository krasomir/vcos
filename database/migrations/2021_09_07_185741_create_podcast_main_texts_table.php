<?php

use App\PodcastMainText;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePodcastMainTextsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('podcast_main_texts', function (Blueprint $table) {
            $table->id();
            $table->mediumText('content')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });

        PodcastMainText::create([
            'content' => 'PoDcast Kolektiv predstavlja podcast civilnog društva o civilnom društvu, u kojem ćemo s raznim gostima iz područja sektora, rada s mladima, obrazovanja, politike, mirovnjaštva, otvarati teme koje se dotiču svakodnevice, potreba, izazova i trendova vezanih za civilni sektor, promicanja aktivnog građanstva, građanskog odgoja i obrazovanja, volonterstva i brojnih drugih tema.'
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('podcast_main_texts');
    }
}
