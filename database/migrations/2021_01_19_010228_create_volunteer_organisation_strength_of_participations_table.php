<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateVolunteerOrganisationStrengthOfParticipationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('volunteer_organisation_strength_of_participations', function (Blueprint $table) {
            $table->id();
            $table->string('organisation_name');
            $table->string('organisation_type');
            $table->integer('foundation_year');
            $table->string('address');
            $table->integer('zip_number');
            $table->string('city');
            $table->string('contact_person')->nullable();
            $table->string('telephone')->nullable();
            $table->string('email')->nullable();
            $table->string('web')->nullable();
            $table->text('mission_goals');
            $table->text('activity_area');
            $table->text('target_group');
            $table->text('geo_area');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('volunteer_organisation_strength_of_participations');
    }
}
