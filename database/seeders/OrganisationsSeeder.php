<?php

namespace Database\Seeders;

use App\VolunteerOrganisation;
use Carbon\Carbon;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;

class OrganisationsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('volunteer_organisations')->truncate();

        $json = File::get("database/vcos_organizacije.json");
        $data = json_decode($json);

        foreach ($data[2]->data as $obj) {
            VolunteerOrganisation::create([
                'id' => $obj->id,
                'organisation_name' => $obj->naziv_organizacije,
                'organisation_type' => $obj->vrsta_organizacije,
                'address' => $obj->adresa,
                'city' => $obj->mjesto,
                'telephone' => $obj->telefon,
                'contact_telephone' => $obj->kontakt_telefon,
                'contact_person' => $obj->kontakt_osoba,
                'contact_email' => $obj->kontakt_email,
                'volunteer_engagement' => $obj->potreba_volontera,
                'volunteer_number' => $obj->broj_osoba,
                'position_name' => $obj->naziv_pozicije,
                'purpose_of_position' => $obj->svrha_pozicije,
                'tasks_description' => $obj->naziv_pozicije,
                'volunteer_qualifications' => $obj->kvalifikacije,
                'volunteer_period' => $obj->razdoblje_volontiranja,
                'hour_number' => $obj->broj_sati,
                'volunteering_place' => $obj->mjesto_volontiranja,
                'additional_education' => ($obj->dodatno_obrazovanje == 'da') ? 1:0,
                'expense_refund' => ($obj->pokrivanje_troskova == 'da') ? 1:0,
                'application_deadline' => Carbon::parse($obj->rok_prijave)->format('Y-m-d'),
                'volunteer_menagement' => ($obj->menadzment_volontera == 'da') ? 1:0,
                'leadership_training' => ($obj->trening_vodjenja == 'da') ? 1:0,
                'organisation_help' => ($obj->organizacija_pomoc == 'da') ? 1:0,
                'newsletter' => ($obj->newsletter == 'da') ? 1:0,
                'created_at' => $obj->created_at,
                'updated_at' => $obj->updated_at,
            ]);
        }
    }
}