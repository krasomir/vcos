<?php

namespace Database\Seeders;

use App\Page;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class PagesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS=0;');
        DB::table('pages')->truncate();

        $pages = ['novosti', 'otvorene mogućnosti za volontiranje'];

        foreach ($pages as $page) {
            Page::create([
                'name' => $page
            ]);
        }

        DB::statement('SET FOREIGN_KEY_CHECKS=1;');
            
    }
}
