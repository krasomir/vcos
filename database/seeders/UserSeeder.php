<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Schema;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Schema::disableForeignKeyConstraints();

        if (Schema::hasTable('permissions')) {
            DB::table('users')->truncate();
            DB::table('users')->insert([
                [
                    'name' => 'administrator',
                    'email' => 'admin@dkolektiv.hr',
                    'email_verified_at' => now(),
                    'password' => Hash::make('admin//.'),
                    'created_at' => now(),
                    'updated_at' => now()
                ],
                [
                    'name' => 'Krasomir',
                    'email' => 'kmioc@inet.hr',
                    'email_verified_at' => now(),
                    'password' => Hash::make('admin//.'),
                    'created_at' => now(),
                    'updated_at' => now()
                ],
                [
                    'name' => 'Jelena',
                    'email' => 'jelena@dkolektiv.hr',
                    'email_verified_at' => now(),
                    'password' => Hash::make('admin//.'),
                    'created_at' => now(),
                    'updated_at' => now()
                ],
                [
                    'name' => 'Mirta',
                    'email' => 'mirta@dkolektiv.hr',
                    'email_verified_at' => now(),
                    'password' => Hash::make('admin//.'),
                    'created_at' => now(),
                    'updated_at' => now()
                ],
            ]);
        }

        Schema::enableForeignKeyConstraints();
    }
}
