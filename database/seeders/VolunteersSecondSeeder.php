<?php

namespace Database\Seeders;

use App\Volunteer;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;

class VolunteersSecondSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {        
        $json = File::get("database/vcos_volonteri_second.json");
        $data = json_decode($json);

        foreach ($data[2]->data as $obj) {
            Volunteer::create([
                'id' => $obj->id,
                'first_last_name' => $obj->ime_prezime,
                'gender' => ($obj->spol == 'M') ? 1 : 2,
                'birth_year' => $obj->godina_rodjenja,
                'city' => $obj->mjesto,
                'telephone' => $obj->telefon,
                'email' => $obj->email,
                'profession' => $obj->zanimanje,
                'additional_skills' => $obj->interesi,
                'volunteer_experience' => $obj->volontersko_iskustvo,
                'volunteer_engagement' => $obj->angazman,
                'work_area' => $obj->podrucja_rada,
                'geo_area' => $obj->geo_podrucje,
                'exclude_area' => $obj->ogranicenja,
                'source_info' => $obj->izvor_info, 
                'created_at' => $obj->created_at,               
                'updated_at' => $obj->updated_at,               
            ]);
        }
    }
}
