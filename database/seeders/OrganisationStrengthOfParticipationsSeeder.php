<?php

namespace Database\Seeders;

use App\VolunteerOrganisationStrengthOfParticipation;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;

class OrganisationStrengthOfParticipationsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('volunteer_organisation_strength_of_participations')->truncate();

        $json = File::get("database/vcos_organizacije_snagasudjelovanja.json");
        $data = json_decode($json);

        foreach ($data[2]->data as $obj) {
            VolunteerOrganisationStrengthOfParticipation::create([
                'id' => $obj->id,
                'organisation_name' => $obj->naziv_organizacije,
                'organisation_type' => $obj->vrsta_organizacije,
                'foundation_year' => $obj->godina_osnutka,
                'address' => $obj->adresa,
                'zip_number' => $obj->postanski_broj,
                'city' => $obj->mjesto,
                'contact_person' => $obj->kontakt_osoba,
                'telephone' => $obj->telefon,
                'email' => $obj->email,
                'web' => $obj->web,
                'mission_goals' => $obj->misija_ciljevi,
                'activity_area' => $obj->podrucje_djelovanja,
                'target_group' => $obj->ciljna_skupina,
                'geo_area' => $obj->geo_podrucje,
                'created_at' => $obj->created_at,
                'updated_at' => $obj->updated_at,
            ]);
        }
    }
}
