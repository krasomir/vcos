<?php

namespace Database\Seeders;

use App\Post;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;

class BlogsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $jsonExchangeNewsIds = File::get("database/id_novosti_burze_volontera.json");
        $dataExchangeNewsIds = json_decode($jsonExchangeNewsIds);
        $postIds = [];
        foreach ($dataExchangeNewsIds[2]->data as $value) {
            $postIds[] = $value->cmsblog_id;
        }

        DB::table('posts')->truncate();
        
        $json = File::get("database/vcos_blogs.json");
        $data = json_decode($json);

        foreach ($data[2]->data as $obj) {
            Post::create([
                'id' => $obj->id,
                'author_id' => $obj->author_id,
                'slug' => str_replace(['\\', '/'], '', $obj->slug),
                'name' => $obj->name,
                'datetime_on' => $obj->datetime_on,
                'datetime_off' => $obj->datetime_off,
                'preview' => $obj->preview,
                'text' => $obj->text,
                'title' => $obj->title,
                'keywords' => $obj->keyw,
                'description' => $obj->descr,
                'is_valid' => $obj->is_valid,
                'page_id' => in_array($obj->id, $postIds) ? 2 : 1,
                'created_at' => $obj->created_at,
                'updated_at' => $obj->updated_at,
            ]);
        }
    }
}
