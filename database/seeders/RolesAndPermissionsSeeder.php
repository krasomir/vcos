<?php

namespace Database\Seeders;

use App\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Spatie\Permission\Models\Role;
use Illuminate\Support\Facades\Schema;
use Spatie\Permission\Models\Permission;

class RolesAndPermissionsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        Schema::disableForeignKeyConstraints();

        if (Schema::hasTable('permissions')) {

            DB::table('permissions')->truncate();
            app()->make(\Spatie\Permission\PermissionRegistrar::class)->forgetCachedPermissions();

            Permission::create(['name' => 'create posts']);
            Permission::create(['name' => 'edit posts']);
            Permission::create(['name' => 'delete posts']);
            Permission::create(['name' => 'publish posts']);
            Permission::create(['name' => 'unpublish posts']);

            Permission::create(['name' => 'create users']);
            Permission::create(['name' => 'edit users']);
            Permission::create(['name' => 'delete users']);

            Permission::create(['name' => 'edit volunteers']);
            Permission::create(['name' => 'delete volunteers']);

            Permission::create(['name' => 'edit organisations']);
            Permission::create(['name' => 'delete organisations']);

            Permission::create(['name' => 'edit organisations strength of participation']);
            Permission::create(['name' => 'delete organisations strength of participation']);

            Permission::create(['name' => 'create projects']);
            Permission::create(['name' => 'edit projects']);
            Permission::create(['name' => 'delete projects']);
            Permission::create(['name' => 'publish projects']);
            Permission::create(['name' => 'unpublish projects']);
            Permission::create(['name' => 'archive projects']);
            Permission::create(['name' => 'unarchive projects']);

            Permission::create(['name' => 'create reports']);
            Permission::create(['name' => 'edit reports']);
            Permission::create(['name' => 'delete reports']);
            Permission::create(['name' => 'publish reports']);
            Permission::create(['name' => 'unpublish reports']);

            Permission::create(['name' => 'create banners']);
            Permission::create(['name' => 'edit banners']);
            Permission::create(['name' => 'delete banners']);

            Permission::create(['name' => 'create publications']);
            Permission::create(['name' => 'edit publications']);
            Permission::create(['name' => 'delete publications']);

            Permission::create(['name' => 'create sliders']);
            Permission::create(['name' => 'edit sliders']);
            Permission::create(['name' => 'delete sliders']);

            Permission::create(['name' => 'create podcasts']);
            Permission::create(['name' => 'edit podcasts']);
            Permission::create(['name' => 'delete podcasts']);

            Permission::create(['name' => 'create programs']);
            Permission::create(['name' => 'edit programs']);
            Permission::create(['name' => 'delete programs']);
        }

        if (Schema::hasTable('roles') && Schema::hasTable('permissions')) {

            DB::table('roles')->truncate();
            DB::table('role_has_permissions')->truncate();

            $roleAdmin = Role::create(['name' => 'admin']);
            $roleAdmin->givePermissionTo(Permission::all());

            $roleWriter = Role::create(['name' => 'writer']);
            $roleWriter->givePermissionTo([
                'create posts', 'edit posts', 'publish posts', 'unpublish posts'
            ]);

            $user = User::find(1);
            $user->givePermissionTo(Permission::all());
        }

        Schema::enableForeignKeyConstraints();
    }
}
