<?php

namespace Database\Seeders;

use App\User;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Schema;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;

class AddTeamPermissionsAndRolesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        Schema::disableForeignKeyConstraints();

        if (Schema::hasTable('permissions')) {
            Permission::findOrCreate('create teams');
            Permission::findOrCreate('edit teams');
            Permission::findOrCreate('delete teams');
        }

        if (Schema::hasTable('roles') && Schema::hasTable('permissions') && Schema::hasTable('role_has_permissions')) {

            $adminRole = Role::where('name', '=', 'admin')->first();
            $adminRole->givePermissionTo(['create teams', 'edit teams', 'delete teams']);

            $user = User::find(1);
            $user->givePermissionTo([
                'create teams', 'edit teams', 'delete teams'
            ]);
        }
    }
}
