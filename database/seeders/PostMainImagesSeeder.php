<?php

namespace Database\Seeders;

use App\PostMainImage;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;

class PostMainImagesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('post_main_images')->truncate();
        
        $json = File::get("database/vcos_post_main_images.json");
        $data = json_decode($json);

        foreach ($data[2]->data as $obj) {
            PostMainImage::create([
                'id' => $obj->id,
                'file_id' => $obj->file_id, 
                'post_id' => $obj->post_id, 
                'post_type' => $obj->post_type, 
                'width' => $obj->w, 
                'height' => $obj->h, 
                'crop' => $obj->crop, 
                'crop_thumb' => $obj->crop_thumb,
            ]);
        }
    }
}
