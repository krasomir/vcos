<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Database\Seeders\UserSeeder;
use Database\Seeders\VolunteersSeeder;
use Database\Seeders\OrganisationsSeeder;
use Database\Seeders\PostMainImagesSeeder;
use Database\Seeders\RolesAndPermissionsSeeder;
use Database\Seeders\OrganisationStrengthOfParticipationsSeeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(OrganisationStrengthOfParticipationsSeeder::class);
        $this->call(OrganisationsSeeder::class);
        $this->call(VolunteersSeeder::class);
        $this->call(UserSeeder::class);
        $this->call(RolesAndPermissionsSeeder::class);
        $this->call(PagesSeeder::class);
        $this->call(PostMainImagesSeeder::class);
        $this->call(BlogsSeeder::class);
        $this->call(AddTeamPermissionsAndRolesSeeder::class);
    }
}
