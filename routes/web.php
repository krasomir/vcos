<?php

use App\Http\Controllers\BannerController;
use App\Http\Controllers\FailedSubscriptionController;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\MediaLibraryController;
use App\Http\Controllers\PodcastController;
use App\Http\Controllers\PostController;
use App\Http\Controllers\ProgramController;
use App\Http\Controllers\ProgramPagesController;
use App\Http\Controllers\ProjectController;
use App\Http\Controllers\PublicationController;
use App\Http\Controllers\ReportController;
use App\Http\Controllers\SearchController;
use App\Http\Controllers\SliderController;
use App\Http\Controllers\SubscribeToMailChimpController;
use App\Http\Controllers\TeamController;
use App\Http\Controllers\TokenController;
use App\Http\Controllers\UserController;
use App\Http\Controllers\VolunteerController;
use App\Http\Controllers\VolunteerOrganisationController;
use App\Http\Controllers\VolunteerOrganisationStrengthOfParticipationController;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

Route::redirect('/', '../hr');

// ckFinder routes
Route::any('/ckfinder/connector', '\CKSource\CKFinderBridge\Controller\CKFinderController@requestAction')
    ->name('ckfinder_connector');
Route::any('/ckfinder/browser', '\CKSource\CKFinderBridge\Controller\CKFinderController@browserAction')
    ->name('ckfinder_browser');
// rss feed route
Route::feeds();

Route::group(['prefix' => '{lang}'], function ($lang) {

    /* VCOS frontend routes */
    Route::get('/', [HomeController::class, 'frontIndex'])->name('home');

    // mision vision goals
    Route::view('/mision-vision-goals', 'pages.mision-vision-goals')->name('mision-vision-goals');
    // our team
    Route::get('/our-team', [TeamController::class, 'ourTeam'])->name('our-team');
    // board of directors
    Route::view('/board-of-directors', 'pages.board-of-directors')->name('board-of-directors');
    // reports
    Route::get('/reports', [ReportController::class, 'frontIndex'])->name('reports');
    // sources of funding
    Route::view('/sources-of-funding', 'pages.sources-of-funding')->name('sources-of-funding');
    // projects
    Route::get('/active-projects', [ProjectController::class, 'activeProjects'])->name('active-projects');
    Route::get('/project/{project:slug}', [ProjectController::class, 'projectDetail'])->name('project');
    Route::get('/archive-of-projects', [ProjectController::class, 'archiveProjects'])->name('archive-of-projects');
    // publications
    Route::get('/publications', [PublicationController::class, 'frontPublications'])->name('publications');

    // news
    Route::get('/news', [PostController::class, 'newsList'])->name('news');
    // news detail
    Route::get('/posts/{post:slug}', [PostController::class, 'show'])->name('news-details');

    // hot tiles routes
    Route::get('/volonterski-centar-osijek', [ProgramPagesController::class, 'vcos'])->name('vcos');
    Route::get('/korisni-dokumenti', [ProgramPagesController::class, 'kd'])->name('kd');
    Route::get('/uciniti-nevidljivo-vidljivim', [ProgramPagesController::class, 'unv'])->name('unv');
    Route::get('/ambasadori-europskih-snaga-solidarnosti', [ProgramPagesController::class, 'ess'])->name('ess');
    Route::get('/want-to-volunteer', function () {
        return view('pages.want-to-volunteer', [
            'validation_step_routes' => [
                'step_1' => route('validate_step_one', app()->getLocale()),
                'step_2' => route('validate_step_two', app()->getLocale()),
                'step_3' => route('validate_step_three', app()->getLocale()),
            ],
        ]);
    })->name('want-to-volunteer');
    Route::post('/save-volunteer-application', [VolunteerController::class, 'save'])->name('save-volunteer-application');
    Route::post('/validate_step_one', [VolunteerController::class, 'validateStepOne'])->name('validate_step_one');
    Route::post('/validate_step_two', [VolunteerController::class, 'validateStepTwo'])->name('validate_step_two');
    Route::post('/validate_step_three', [VolunteerController::class, 'validateStepThree'])->name('validate_step_three');

    Route::get('/verify/{token:token}', [TokenController::class, 'index'])->name('verify');

    Route::get('/need-volunteers', [VolunteerOrganisationController::class, 'frontendIndex'])->name('need-volunteers');
    Route::post('/need_volunteers_validate_step_one', [VolunteerOrganisationController::class, 'validationStepOne'])->name('need_volunteers_validate_step_one');
    Route::post('/need_volunteers_validate_step_two', [VolunteerOrganisationController::class, 'validationStepTwo'])->name('need_volunteers_validate_step_two');
    Route::post('/need_volunteers_validate_step_three', [VolunteerOrganisationController::class, 'validationStepThree'])->name('need_volunteers_validate_step_three');
    Route::post('/save-need-volunteers', [VolunteerOrganisationController::class, 'store'])->name('save-need-volunteers');

    Route::get('/open-volunteering-opportunities', [PostController::class, 'openVolunteeringNewsList'])->name('open-volunteering-opportunities');
    Route::get('/open-volunteering-opportunities/{post:slug}', [PostController::class, 'showOpenVolunteeringNews'])->name('open-volunteering-opportunities-details');
    Route::get('/social-atelier', [ProgramPagesController::class, 'socialAtelier'])->name('social-atelier');
    Route::get('/strength-of-participation', [ProgramPagesController::class, 'strengthOfParticipation'])->name('strength-of-participation');
    Route::get('/strength-of-participation-application', [VolunteerOrganisationStrengthOfParticipationController::class, 'showApplication'])->name('strength-of-participation-application');
    Route::post('/strength_of_participation_validate_step_one', [VolunteerOrganisationStrengthOfParticipationController::class, 'validationStepOne'])->name('strength_of_participation_validate_step_one');
    Route::post('/strength_of_participation_validate_step_two', [VolunteerOrganisationStrengthOfParticipationController::class, 'validationStepTwo'])->name('strength_of_participation_validate_step_two');
    Route::post('save-strength-of-participation-application', [VolunteerOrganisationStrengthOfParticipationController::class, 'store'])->name('save-strength-of-participation-application');

    Route::get('/demo-academy', [PublicationController::class, 'demoAcademy'])->name('demo-academy');
    Route::view('/demo-academy-instructors', 'pages.demo-academy-instructors')
        ->name('demo-academy-instructors');
    Route::get('/osijek-to-goo', [ProgramPagesController::class, 'osijekToGoo'])->name('osijek-to-goo');
    Route::get('/podcasts/{podcast:slug}', [PodcastController::class, 'podcastIndex'])->name('podcast-show');
    Route::get('/podcasts', [PodcastController::class, 'frontPodcasts'])->name('podcasts');
    Route::view('/radius-v', 'pages.radius-v')->name('radius-v');

    Route::get('/search', SearchController::class)->name('search');

    /* administration routes */
    Auth::routes(['register' => false]);
    /* VCOS admin routes */
    Route::group(['middleware' => 'auth', 'prefix' => 'admin'], function () {

        Route::get('/home', [HomeController::class, 'index'])->name('admin-home');

        /* user */
        Route::get('/user', [UserController::class, 'index'])->name('admin-users');
        Route::get('/user-list', [UserController::class, 'usersList'])->name('admin-users-list');
        Route::get('/user/create', [UserController::class, 'create'])->name('admin-user-create');
        Route::post('/user', [UserController::class, 'store'])->name('admin-user-store');
        Route::get('/user/{user}', [UserController::class, 'show'])->name('admin-user-show');
        Route::get('/user/{user}/edit', [UserController::class, 'edit'])->name('admin-user-edit');
        Route::patch('/user/{user}', [UserController::class, 'update'])->name('admin-user-update');
        Route::delete('/user/{user}', [UserController::class, 'destroy'])->name('admin-user-delete');
        Route::get('/clear-cache', function () {
            Artisan::call('cache:clear');

            return back();
        })->name('clear-cache');
        Route::get('/user-profile', [UserController::class, 'userProfile'])->name('admin-user-profile');

        /* volunteers */
        Route::get('/sync-subscribers', [VolunteerController::class, 'syncSubscribers'])->name('sync');
        Route::get('/volunteers', [VolunteerController::class, 'index'])->name('volunteers');
        Route::get(
            '/volunteers-list',
            [VolunteerController::class, 'volunteersList']
        )->name('volunteers-list');
        Route::get(
            '/volunteers/{volunteer}',
            [VolunteerController::class, 'show']
        )->name('volunteers-show');
        Route::delete(
            '/volunteers-delete/{volunteer}',
            [VolunteerController::class, 'destroy']
        )->name('volunteers-delete');
        Route::get('/volunteer-pdf/{volunteer}', [VolunteerController::class, 'volunteerPdf'])->name('volunteer-pdf');

        /* organisations */
        Route::get(
            '/organisations',
            [VolunteerOrganisationController::class, 'index']
        )->name('organisations');
        Route::get(
            '/organisations-list',
            [VolunteerOrganisationController::class, 'organisationsList']
        )->name('organisations-list');
        Route::get(
            '/organisations/{organisation}',
            [VolunteerOrganisationController::class, 'show']
        )->name('organisations-show');
        Route::delete(
            '/organisations-delete/{organisation}',
            [VolunteerOrganisationController::class, 'destroy']
        )->name('organisations-delete');
        Route::get(
            '/organisation-pdf/{organisation}',
            [VolunteerOrganisationController::class,
                'organisationPdf']
        )->name('organisation-pdf');

        /* organisations strength of participations */
        Route::get(
            '/organisation-strength-of-participations',
            [VolunteerOrganisationStrengthOfParticipationController::class, 'index']
        )->name('organisation-strength-of-participations');
        Route::get(
            '/organisation-strength-of-participations-list',
            [VolunteerOrganisationStrengthOfParticipationController::class, 'volunteersList']
        )->name('organisation-strength-of-participations-list');
        Route::get(
            '/organisation-strength-of-participations/{organisation}',
            [VolunteerOrganisationStrengthOfParticipationController::class, 'show']
        )->name('organisation-strength-of-participations-show');
        Route::delete(
            '/organisation-strength-of-participations-delete/{organisation}',
            [VolunteerOrganisationStrengthOfParticipationController::class, 'destroy']
        )->name('organisation-strength-of-participations-delete');
        Route::get(
            'organisation-sop-pdf/{organisation}',
            [VolunteerOrganisationStrengthOfParticipationController::class, 'organisationSopPdf']
        )->name('organisation-sop-pdf');

        /* blog / posts */
        Route::get('/post', [PostController::class, 'index'])->name('admin-posts');
        Route::get('/post-list', [PostController::class, 'postsList'])->name('admin-post-list');
        Route::get('/post/create', [PostController::class, 'create'])->name('admin-post-create');
        Route::post('/post', [PostController::class, 'store'])->name('admin-post-store');
        Route::get('/post/{post}/edit', [PostController::class, 'edit'])->name('admin-post-edit');
        Route::patch('/post/{post}/update', [PostController::class, 'updatePost'])->name('admin-post-update');
        Route::delete('/post/{post}', [PostController::class, 'destroy'])->name('admin-post-delete');
        Route::get('/create-slug', [PostController::class, 'createSlug'])->name('create-slug');
        Route::patch('/post-publish', [PostController::class, 'postPublish'])->name('post-publish');

        /* reports */
        Route::get('/reports', [ReportController::class, 'index'])->name('admin-reports');
        Route::get('/report-list', [ReportController::class, 'reportList'])->name('report-list');
        Route::get('/report/create', [ReportController::class, 'create'])->name('report-create');
        Route::post('/reports', [ReportController::class, 'store'])->name('report-store');
        Route::get('/report/{report}/edit', [ReportController::class, 'edit'])->name('report-edit');
        Route::patch('/report/{report}/update', [ReportController::class, 'update'])->name('report-update');
        Route::delete('/report/{report}', [ReportController::class, 'destroy'])->name('report-delete');
        Route::patch('/report-publish/{report}', [ReportController::class, 'reportPublish'])->name('report-publish');

        /* projects */
        Route::get('/projects', [ProjectController::class, 'adminIndex'])->name('admin-projects');
        Route::get('/project-list', [ProjectController::class, 'projectList'])->name('project-list');
        Route::get('/project/create', [ProjectController::class, 'create'])->name('project-create');
        Route::post('/project', [ProjectController::class, 'store'])->name('project-store');
        Route::get('/project/{project}/edit', [ProjectController::class, 'edit'])->name('project-edit');
        Route::patch('/project/{project}/update', [ProjectController::class, 'update'])->name('project-update');
        Route::delete('/project/{project}', [ProjectController::class, 'destroy'])->name('project-delete');
        Route::patch('/project-publish/{project}', [ProjectController::class, 'projectPublish'])->name('project-publish');
        Route::patch('/project-archive/{project}', [ProjectController::class, 'projectArchive'])->name('project-archive');

        /* banners */
        Route::get('/banners/list', [BannerController::class, 'list'])->name('banner.list');
        Route::resource('banners', BannerController::class);

        /* publications */
        Route::get('/publications/list', [PublicationController::class, 'list'])->name('publication.list');
        Route::resource('publications', PublicationController::class);

        /* sliders */
        Route::get('/sliders/list', [SliderController::class, 'list'])->name('slider.list');
        Route::patch('/sliders/update-position', [SliderController::class, 'updateSliderPosition'])->name('update-slider-position');
        Route::resource('sliders', SliderController::class);

        /* podcasts */
        Route::get('/podcasts/list', [PodcastController::class, 'list'])->name('podcast.list');
        Route::resource('podcasts', PodcastController::class)->except('show');

        /* podcast main text */
        Route::post('/podcast/main/{podcastMainText}', [PodcastController::class, 'saveMainText'])->name('save-main-podcast-text');

        /* media library */
        Route::get('/media-library', [MediaLibraryController::class, 'index'])->name('media-library');
        Route::delete('/media-library/{media_name}', [MediaLibraryController::class, 'delete'])->name('media-delete');

        /* programs */
        Route::get('/programs/list', [ProgramController::class, 'list'])->name('program-list');
        Route::resource('programs', ProgramController::class)->except('show');

        /* failed subscriptions */
        Route::get('/failed-subscriptions/list', [FailedSubscriptionController::class, 'list'])->name('failed-subscriptions-list');
        Route::get('/failed-subscriptions/retry/{failedSubscription}', [FailedSubscriptionController::class, 'retry'])->name('failed-subscriptions-retry');
        Route::get('/failed-subscriptions/retry/all', [FailedSubscriptionController::class, 'retryAll'])->name('failed-subscriptions-retry-all');
        Route::resource('/failed-subscriptions', FailedSubscriptionController::class);

        /* import subscribers from old vcos site newsletter */
        Route::get('/subscribe-to-subscribers-list', [SubscribeToMailChimpController::class, 'subscribersList'])->name('subscribe-to-subscribers-list');
        Route::get('/subscribe-to-osjecko-baranjska-list', [SubscribeToMailChimpController::class, 'osjeckoBaranjskaList'])->name('subscribe-to-osjecko-baranjska-list');
        Route::get('/subscribe-to-vukovarsko-srijemska-list', [SubscribeToMailChimpController::class, 'vukovarskoSrijemskaList'])->name('subscribe-to-vukovarsko-srijemska-list');
        Route::get('/subscribe-to-pozesko-slavonska-list', [SubscribeToMailChimpController::class, 'pozeskoSlavonskaList'])->name('subscribe-to-pozesko-slavonska-list');
        Route::get('/subscribe-to-brodsko-posavska-list', [SubscribeToMailChimpController::class, 'brodskoPosavskaList'])->name('subscribe-to-brodsko-posavska-list');
        Route::get('/subscribe-to-druge-zupanije-list', [SubscribeToMailChimpController::class, 'drugeZupanijeList'])->name('subscribe-to-druge-zupanije-list');

        /* dKolektiv tim */
        Route::resource('/teams', TeamController::class);
        Route::get('/teams-list', [TeamController::class, 'teamsList'])->name('admin-team-list');

    });

});
