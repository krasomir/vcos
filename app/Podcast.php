<?php

namespace App;

use App\Http\Requests\PodcastStoreRequest;
use App\Http\Requests\PodcastUpdateRequest;
use App\Traits\FileUploadTrait;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
use Laravel\Scout\Searchable;
use Spatie\Feed\Feedable;
use Spatie\Feed\FeedItem;

class Podcast extends Model implements Feedable
{
    use FileUploadTrait, Searchable;

    protected $fillable = ['title', 'season', 'slug', 'short_description', 'description', 'file_name', 'published_at'];

    protected $casts = [
        'published_at' => 'datetime',
    ];

    public function toSearchableArray(): array
    {
        return [
            'title' => strip_tags($this->title),
            'description' => strip_tags($this->description),
        ];
    }

    public function shortenDescription(): string
    {
        $bigText = $this->description;
        $bigText = strip_tags($bigText);

        return Str::words($bigText, 30, ' ...');
    }

    public function toFeedItem(): FeedItem
    {
        return FeedItem::create()
            ->id(app()->getLocale().'/podcasts/'.$this->slug)
            ->title($this->title)
            ->updated($this->published_at)
            ->summary($this->short_description ?? '')
            ->link(route('podcast-show', [
                'lang' => app()->getLocale(),
                'podcast' => $this->slug,
            ]))
            ->authorName('DKolektiv')
            ->authorEmail(config('app.podcast_email'));
    }

    public static function getFeedItems(): Collection
    {
        return Podcast::where('published_at', '!=', null)->latest('published_at')->get();
    }

    public function isPublished(): bool
    {
        return ($this->published_at !== null) ? true : false;
    }

    /**
     * Undocumented function
     *
     * @return bool|\Exception
     */
    public static function storePodcast(PodcastStoreRequest $request)
    {
        try {
            if ($request->hasFile('file')) {
                $fileName = self::handleFileUpload($request->file('file'), $request->slug);
            }

            $request->request->add(['file_name' => $fileName ?? null]);

            if ($request->published) {
                $request->request->add(['published_at' => now()]);
            }

            $podcast = Podcast::create($request->except(['_token', 'file', 'published']));

            return $podcast;

        } catch (\Exception $e) {
            return $e;
        }
    }

    /**
     * Undocumented function
     *
     * @return bool|\Exception
     */
    public static function updatePodcast(PodcastUpdateRequest $request, Podcast $podcast)
    {
        try {
            if ($request->hasFile('file')) {
                $fileName = self::handleFileUpload($request->file('file'), $request->slug);
                $request->file_name = $fileName;
            }

            if ($request->slug != $podcast->slug) {
                $newName = self::handleRenameFile($podcast->file_name, $request->slug);

                if ($podcast->file_name !== $newName) {
                    $podcast->file_name = $newName;
                }
            }

            $podcast->title = $request->title;
            $podcast->slug = $request->slug;
            $podcast->short_description = $request->short_description;
            $podcast->description = $request->description;
            $podcast->season = $request->season;

            if (! $request->published) {
                $podcast->published_at = null;
            } elseif ($podcast->published_at === null) {
                $podcast->published_at = now();
            }

            return $podcast->save();

        } catch (\Exception $e) {
            //dd($e->getMessage());
            return $e;
        }
    }

    /**
     * Undocumented function
     *
     * @return bool|\Exception
     */
    public static function deletePodcast(Podcast $podcast)
    {
        try {
            $path = 'public/files/podcasts/'.$podcast->file_name;
            if (Storage::exists($path)) {
                if (self::handleDeleteFile($path)) {
                    return $podcast->delete();
                } else {
                    return false;
                }
            }

            return $podcast->delete();
        } catch (\Exception $e) {
            return $e;
        }
    }

    public static function getSeasonYears($season): string
    {
        $podcasts = Podcast::where('season', $season)->get();

        $years = [];
        foreach ($podcasts as $podcast) {
            $years[] = $podcast->created_at->format('Y');
        }

        if (! $years) {
            return now()->format('Y').'./.'.(now()->addYear()->format('Y'));
        }

        return $years[array_key_first($years)].'./'.($years[array_key_first($years)] + 1).'.';
    }
}
