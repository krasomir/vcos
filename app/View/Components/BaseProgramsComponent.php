<?php

namespace App\View\Components;

use App\Program;
use Illuminate\View\Component;

class BaseProgramsComponent extends Component
{
    public $basePrograms;

    public $disabledPrograms;

    public $selected;

    /**
     * Create a new component instance.
     *
     * @param String $selected
     */
    public function __construct(String $selected = null)
    {
        $this->selected = $selected;
        $this->basePrograms = Program::$basePrograms;
        $this->disabledPrograms = ($selected) ? Program::where('program_page', '!=', $selected)->get()->pluck('program_page')->toArray() : Program::get()->pluck('program_page')->toArray();
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\Contracts\View\View|string
     */
    public function render()
    {
        return view('components.base-programs-component');
    }
}
