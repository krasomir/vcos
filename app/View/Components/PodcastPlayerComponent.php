<?php

namespace App\View\Components;

use Illuminate\View\Component;

class PodcastPlayerComponent extends Component
{
    public $fileName;

    public $title;

    /**
     * Create a new component instance.
     *
     * @return void
     */
    public function __construct(String $fileName, String $title)
    {
        $this->fileName = $fileName;
        $this->title = $title;
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\Contracts\View\View|string
     */
    public function render()
    {
        return view('components.podcast-player-component');
    }
}
