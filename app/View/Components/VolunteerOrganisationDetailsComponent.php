<?php

namespace App\View\Components;

use Illuminate\View\Component;

class VolunteerOrganisationDetailsComponent extends Component
{
    public $organisation;

    /**
     * Undocumented function
     *
     * @param [type] $organisation
     */
    public function __construct($organisation)
    {
        $this->organisation = $organisation;
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\Contracts\View\View|string
     */
    public function render()
    {
        return view('components.volunteer-organisation-details');
    }
}
