<?php

namespace App\View\Components;

use App\Banner;
use Illuminate\View\Component;

class BannerComponent extends Component
{
    public $name;

    public $extraBreadcrumb;

    public $extraBreadcrumbName;

    public $requestPath;

    public $banner;

    /**
     * Undocumented function
     *
     * @param String $name
     * @param String $extraBreadcrumb
     * @param String $extraBreadcrumbName
     * @param String $requestPath
     */
    public function __construct(
        String $name, 
        String $extraBreadcrumb = null, 
        String $extraBreadcrumbName = null,
        String $requestPath = null
        )
    {
        $this->name = $name;
        $this->extraBreadcrumb = $extraBreadcrumb;
        $this->extraBreadcrumbName = $extraBreadcrumbName;
        $this->requestPath = $requestPath;
        $this->banner = $this->getBanner();
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\Contracts\View\View|string
     */
    public function render()
    {
        return view('components.banner-component');
    }

    /**
     * Undocumented function
     *
     * @return Banner
     */
    private function getBanner()
    {
        $path = $this->parsePath();
        return ($path) ? 
            Banner::where('show_on_page', $path)->orWhere('show_on_page', '/all')->latest()->first() :
            Banner::where('show_on_page', '/all')->first();
    }

    /**
     * Undocumented function
     *
     * @return String|null
     */
    private function parsePath()
    {
        if (!$this->requestPath) {
            return null;
        }

        $path = explode('/', $this->requestPath);
        return '/' . $path[1];
    }
}
