<?php

namespace App\View\Components;

use App\Volunteer;
use Illuminate\View\Component;

class VolunteerDetails extends Component
{
    public $volunteer;

    /**
     * Undocumented function
     *
     * @param [type] $volunteer
     */
    public function __construct($volunteer)
    {
        $this->volunteer = $volunteer;
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\Contracts\View\View|string
     */
    public function render()
    {
        return view('components.volunteer-details');
    }
}
