<?php

namespace App\View\Components;

use Illuminate\View\Component;

class VolunteerOrganisationSopDetailsComponent extends Component
{
    public $organisation;

    /**
     * Create a new component instance.
     *
     * @return void
     */
    public function __construct($organisation)
    {
        $this->organisation = $organisation;
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\Contracts\View\View|string
     */
    public function render()
    {
        return view('components.volunteer-organisation-sop-details');
    }
}
