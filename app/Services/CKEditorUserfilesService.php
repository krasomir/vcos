<?php

namespace App\Services;


use App\Services\FormatSizeService;
use Carbon\Carbon;
use Illuminate\Support\Facades\File;

class CKEditorUserfilesService
{
    private static $userfilesPath = 'userfiles';
    private static $documentExtension = ['pdf', 'txt', 'doc', 'docx', 'xls', 'xlsx', 'ppt', 'pptx'];
    private static $imageExtension = ['jpg', 'jpeg', 'png', 'webp', 'gif', 'svg'];

    /**
     * Undocumented function
     *
     * @return object
     */
    public static function getUserfiles()
    {
        $documents = [];
        $images = [];

        $files = File::allFiles(public_path(self::$userfilesPath));

        foreach ($files as $file) {

            if (in_array($file->getExtension(), self::$documentExtension)) {
                $documents[] = self::getFileInfo($file);
            }

            if (in_array($file->getExtension(), self::$imageExtension) && strpos($file->getPathname(), '__thumbs') === false) {
                $images[] = self::getFileInfo($file);
            }
            
        }

        return (object) [
            'documents' => $documents,
            'images' => $images
        ];
    }

    /**
     * Undocumented function
     *
     * @param [type] $file
     * @return object
     */
    private static function getFileInfo($file)
    {
        $filePath = $file->getPathname();
        $path = explode('userfiles', $filePath);
        
        return (object) [
            'name' => $file->getFilenameWithoutExtension(),
            'filename' => $file->getFilename(),
            'formated_filesize' => FormatSizeService::formatSize($file->getSize()),
            'filesize' => $file->getSize(),
            'created_at' => Carbon::parse($file->getMTime())->format('d.m.Y H:i:s'),
            'file_path' => asset('userfiles' . $path[1]),
            'media_path' => 'userfiles' . $path[1],
        ];
    }
}