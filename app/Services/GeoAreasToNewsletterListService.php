<?php

namespace App\Services;

class GeoAreasToNewsletterListService
{
    public static function convert(string $geoArea): string
    {
        if ($geoArea === 'subscribers') {
            return $geoArea;
        }
        
        $areas = [
            "Osječko-baranjska županija" => "osjecko_baranjska",
            "Vukovarsko-srijemska županija" => "vukovarsko_srijemska",
            "Brodsko-posavska županija" => "brodsko_posavska",
            "Požeško-slavonska županija" => "pozesko_slavonska",
            "Druge županije" => "druge_zupanije",
        ];

        return $areas[$geoArea];
    }
}