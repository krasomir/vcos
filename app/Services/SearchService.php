<?php

namespace App\Services;

use App\Podcast;
use App\Post;
use App\Project;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;

class SearchService
{
    public function search(string $searchTerm): Collection|\Illuminate\Support\Collection
    {
        $posts = Post::search($searchTerm)->whereIn('is_valid', [1])->get();
        $projects = Project::search($searchTerm)->where('published', 1)->get();
        $podcasts = Podcast::search($searchTerm)->query(function (Builder $query) {
            $query->whereNotNull('published_at');
        })->get();

        return $posts->concat($projects)
            ->concat($podcasts)
            ->sortBy('created_at', 0, 'desc');
    }
}
