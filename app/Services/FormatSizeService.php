<?php

namespace App\Services;

class FormatSizeService
{
    /**
     * Undocumented function
     *
     * @param integer $size
     * @param integer $precision
     * @return void
     */
    public static function formatSize(int $size, $precision = 2)
    {
        if ($size < 1) {
            return 0 .' K';
        }
        $base = log($size, 1024);
        $suffixes = array('', 'KB', 'MB', 'GB', 'TB');   
    
        return round(pow(1024, $base - floor($base)), $precision) .' '. $suffixes[floor($base)];
    }
}