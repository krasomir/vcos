<?php

namespace App\Services;

use App\Post;
use Illuminate\Support\Str;

class Slug
{
    /**
     * @param string $title
     * @param int $id
     * @param string $type
     * @return String
     * @throws \Exception
     */
    public static function createSlug(string $title, $id = 0, $type = 'post')
    {
        $allSlugs = collect();

        // Normalize the title
        $slug = Str::slug($title);

        // Get any that could possibly be related.
        // This cuts the queries down by doing it once.
        $allSlugs = self::getRelatedSlugs($slug, $id, $type);

        // If we haven't used it before then we are all good.
        if (!$allSlugs->contains('slug', $slug)){
            return $slug;
        }

        // append numbers until we find not used
        for ($i = 1; $i <= 10; $i++) {
            $newSlug = $slug.'-'.$i;
            if (!$allSlugs->contains('slug', $newSlug)) {
                return $newSlug;
            }
        }

        throw new \Exception('Can not create a unique slug');
    }

    /**
     * Undocumented function
     *
     * @param String $slug
     * @param integer $id
     * @param String $type
     * @return Model|null
     */
    protected static function getRelatedSlugs(String $slug, int $id, String $type)
    {
        $model = 'App\\' . ucfirst($type);
        return $model::select('slug')->where('slug', 'like', $slug . '%')
            ->where('id', '!=', $id)
            ->get();
    }
}
