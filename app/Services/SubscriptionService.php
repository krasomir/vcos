<?php

namespace App\Services;

use App\Services\GeoAreasToNewsletterListService;
use Spatie\Newsletter\NewsletterFacade as Newsletter;

class SubscriptionService
{
    public static function subscribe(object $newsletterData, string $modelType): array
    {
        if ($modelType === 'App\Volunteer') {
            if (Newsletter::isSubscribed($newsletterData->email)) {
                return $response = [
                    'message' => 'You already subscribed.',
                    'alertType' => 'info'
                ];
            }
            $response = self::handleVolunteerSubscription($newsletterData);
        }

        if ($modelType === 'App\VolunteerOrganisationStrengthOfParticipation') {
            $response = self::handleStrengthOfParticipationSubscrition($newsletterData);
        }

        return $response;
    }

    private static function handleVolunteerSubscription(object $newsletterData): array
    {
        $response = [
            'message' => trans('Došlo je do greške pri spremanju pretplate. Pokušajte ponovno.'),
            'alertType' => 'danger'
        ];
        $failedListSubscription = [];

        if (Newsletter::subscribeOrUpdate($newsletterData->email, $newsletterData->names, $newsletterData->subscriptions)) {
                
            $response = [
                'message' => trans('Uspješno ste potvrdili vašu e-mail adresu.'),
                'alertType' => 'success'
            ];
        } else {
            $response = [
                'message' => 'MailChimp Error: ' . Newsletter::getLastError(),
                'alertType' => 'danger'
            ];
            $failedListSubscription['failedListSubscription']['subscriptions'][] = $newsletterData->subscriptions;
            $failedListSubscription['failedListSubscription']['email'] = $newsletterData->email;
            $failedListSubscription['failedListSubscription']['names'] = serialize($newsletterData->names);
            $failedListSubscription['failedListSubscription']['comment'] = 'MailChimp Error: ' . Newsletter::getLastError() ?? '';
        }

        return array_merge($response, $failedListSubscription);
    }

    private static function handleStrengthOfParticipationSubscrition(object $newsletterData): array
    {
        $response = [
            'message' => trans('Došlo je do greške pri spremanju pretplate. Pokušajte ponovno.'),
            'alertType' => 'danger'
        ];
        $failedListSubscription = [];

        foreach ($newsletterData->subscriptions as $key => $subscription) {
            $convertedSubscription = self::convertGeoAreasToNewsletterLists($subscription);
            if (Newsletter::subscribeOrUpdate($newsletterData->email, $newsletterData->names, $convertedSubscription)) {
                $response = [
                    'message' => trans('Uspješno ste potvrdili vašu e-mail adresu.'),
                    'alertType' => 'success'
                ];
            } else {
                $response = [
                    'message' => 'MailChimp Error: ' . Newsletter::getLastError(),
                    'alertType' => 'danger'
                ];
                $failedListSubscription['failedListSubscription']['subscriptions'][] = $subscription;
                $failedListSubscription['failedListSubscription']['email'] = $newsletterData->email;
                $failedListSubscription['failedListSubscription']['names'] = serialize($newsletterData->names);
                $failedListSubscription['failedListSubscription']['comment'] = 'MailChimp Error: ' . Newsletter::getLastError() ?? '';
            }
        }

        return array_merge($response, $failedListSubscription);
    }

    private static function convertGeoAreasToNewsletterLists(string $geoArea): string
    {
        return GeoAreasToNewsletterListService::convert($geoArea);
    }

    public static function getNewsletterData(string $modelType, string $model): object
    {
        $modelDecoded = json_decode($model);

        if ($modelType === 'App\Volunteer') {

            config(['newsletter.apiKey' => config('newsletter.vcos_apikey')]);
            config(['newsletter.defaultListName' => config('newsletter.defaultListName')]);
            $name = explode(' ', $modelDecoded->first_last_name);
            $names = ['FNAME' => $name[0] ?? '', 'LNAME' => $name[1] ?? ''];
            $subscriptions = 'subscribers';                
        }
        
        if ($modelType === 'App\VolunteerOrganisationStrengthOfParticipation') {

            config(['newsletter.apiKey' => config('newsletter.ss_apikey')]);
            config(['newsletter.defaultListName' => config('newsletter.defaultSSListName')]);
            $name = explode(' ', $modelDecoded->contact_person);
            $names = ['FNAME' => $name[0] ?? '', 'LNAME' => $name[1] ?? ''];            
            $subscriptions = $modelDecoded->geo_area;
        }

        return (object) [
            'model_id' => $modelDecoded->id,
            'names' => $names,
            'email' => $modelDecoded->email,
            'subscriptions' => $subscriptions,
        ];
    }
}