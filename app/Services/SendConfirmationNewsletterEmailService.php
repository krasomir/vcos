<?php

namespace App\Services;

use App\Token;
use App\Mail\ConfirmNewsletterEmail;
use Illuminate\Support\Facades\Mail;
use Illuminate\Database\Eloquent\Model;

class SendConfirmationNewsletterEmailService
{
    public $model;
    public $email;
    public $type;

    public function __construct(Model $model, string $email, string $type)
    {
        $this->model = $model;
        $this->email = $email;
        $this->type = $type;
    }

    public function send()
    {
        $token = $this->createToken();

        if ($token) {
            Mail::to($this->email)
                ->queue(new ConfirmNewsletterEmail($token->token, $this->model));
        }
    }

    /**
     * Undocumented function
     *
     * @return Token|Exception
     */
    private function createToken()
    {
        try {
            $token = bin2hex(openssl_random_pseudo_bytes(64));
            return Token::create([
                'token' => $token,
                'email' => $this->email,
                'model' => $this->model,
                'model_type' => $this->type,
            ]);
        } catch (\Exception $e) {
            return $e->getMessage();
        }
    }
}