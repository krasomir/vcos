<?php

namespace App\Services;

use App\FailedSubscription;

class FailedSubscriptionService
{
    public static function store(array $failedSubscriptions): bool
    {
        $failedSubscriptionData = [];
        foreach ($failedSubscriptions['subscriptions'] as $subscription) {
            $failedSubscriptionData[] = [
                'email' => $failedSubscriptions['email'],
                'mailing_list_name' => $subscription,
                'names' => $failedSubscriptions['names'],
                'comment' => $failedSubscriptions['comment'],
                'created_at' => now(),
                'updated_at' => now(),
            ];
        }

        return FailedSubscription::insert($failedSubscriptionData);
    }
}