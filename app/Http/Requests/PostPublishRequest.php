<?php

namespace App\Http\Requests;

use App\Post;
use Illuminate\Validation\Rule;
use Illuminate\Foundation\Http\FormRequest;

class PostPublishRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return (
            auth()->user()->hasPermissionTo('publish posts') ||
            auth()->user()->hasPermissionTo('unpublish posts')
        ) ? true : false;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'post_id' => 'required|' . Rule::in(Post::all()->pluck('id')),
            'publish' => 'required|numeric|in:0,1'
        ];
    }
}
