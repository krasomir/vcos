<?php

namespace App\Http\Requests;

use App\Rules\IsFileExists;
use Illuminate\Foundation\Http\FormRequest;

class PodcastUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return (auth()->user()->hasPermissionTo('edit podcasts')) ? true : false;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'season' => 'required|numeric', 
            'title' => 'required',
            'slug' => 'required',
            'short_description' => 'required',
            'description' => 'required',
            'file' => 'nullable|file|mimes:mp3|max:51200',
            'existing_file' => ['nullable', new IsFileExists],
        ];
    }

    /**
     * Undocumented function
     *
     * @return array
     */
    public function attributes()
    {
        return [
            'season' => 'sezona',
            'title' => 'naziv epizode',
            'slug' => 'slug/SEO naziva',
            'short_description' => 'kratki opis epizode',
            'description' => 'opis epizode',
            'file' => 'priloži datoteku',
            'existing_file' => 'Postojeća datoteka',
        ];
    }
}
