<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StrengthOfParticipationStepOneRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            "organisation_type" => "required",
            "organisation_name" => "required",
            "foundation_year" => "required|numeric",
            "address" => "required",
            "zip_number" => "required|numeric",
            "city" => "required",
            "contact_person" => "required",
            "telephone" => "required|regex:/^[0-9\-\(\)\/\+\s]*$/",
            "email" => "required|email",
            "web" => "nullable",
        ];
    }

    /**
     * Undocumented function
     *
     * @return array
     */
    public function attributes()
    {
        return [
            "organisation_type" => mb_strtolower(trans('Vrsta organizacije')),
            "organisation_name" => mb_strtolower(trans('Naziv organizacije')),
            "foundation_year" => mb_strtolower(trans('Godina osnutka')),
            "address" => mb_strtolower(trans('Ulica i kućni broj')),
            "zip_number" => mb_strtolower(trans('Poštanski broj')),
            "city" => mb_strtolower(trans('Mjesto')),
            "contact_person" => mb_strtolower(trans('Kontakt osoba')),
            "telephone" => mb_strtolower(trans('Telefon')),
            "email" => mb_strtolower(trans('E-mail')),
            "web" => mb_strtolower(trans('Web stranica')),
        ];
    }
}
