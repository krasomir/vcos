<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ValidateVolunteerApplicationStepTwoRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'profession' => 'required',
            'additional_skills' => 'nullable',
            'volunteer_experience' => 'nullable',
        ];
    }

    public function attributes()
    {
        return [
            'profession' => 'zanimanje',
            'additional_skills' => 'dodatne vještine, interesi, hobiji',
            'volunteer_experience' => 'volontersko iskustvo',
        ];
    }
}
