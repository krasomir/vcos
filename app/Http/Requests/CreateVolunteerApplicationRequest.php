<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CreateVolunteerApplicationRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'first_last_name' => 'required',
            'gender' => 'required',
            'birth_year' => 'required',
            'city' => 'required',
            'telephone' => 'nullable',
            'email' => 'required',
            'profession' => 'required',
            'additional_skills' => 'nullable',
            'volunteer_experience' => 'nullable',
            'volunteer_engagement' => 'required',
            'work_area' => 'required',
            'geo_area' => 'required',
            'exclude_area' => 'nullable',
            'source_info' => 'required_without:source_info_other',
            'source_info_other' => 'required_without:source_info'
        ];
    }

    public function attributes()
    {
        return [
            'first_last_name' => 'ime i prezime',
            'gender' => 'spol',
            'birth_year' => 'godina rođenja',
            'city' => 'mjesto stanovanja',
            'telephone' => 'telefon',
            'profession' => 'zanimanje',
            'additional_skills' => 'dodatne vještine, interesi, hobiji',
            'volunteer_experience' => 'volontersko iskustvo',
            'volunteer_engagement' => 'volonterski angažman',
            'work_area' => 'područje volontiranja',
            'geo_area' => 'geografsko područje',
            'exclude_area' => 'područje gdje ne biste volontirali',
            'source_info' => 'kako ste saznali',
        ];
    }
}
