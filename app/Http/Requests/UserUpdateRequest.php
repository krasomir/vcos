<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UserUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return (auth()->user()->hasPermissionTo('edit users')) ? true : false;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required', 
            'email' => 'required|email|unique:users,email,'. $this->user->id,
            'password' => 'nullable|min:8|confirmed',
            'permission_ids' => 'required',
        ];
    }

    /**
     * Undocumented function
     *
     * @return void
     */
    public function attributes()
    {
        return [
            'name' => 'ime',
            'email' => 'e-mail adresa',
            'password' => 'lozinka',
            'permission_ids' => 'dozvole/dopuštenja'
        ];
    }
}
