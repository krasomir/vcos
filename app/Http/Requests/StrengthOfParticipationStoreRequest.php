<?php

namespace App\Http\Requests;

use App\VolunteerOrganisationStrengthOfParticipation;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class StrengthOfParticipationStoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            "target_group" => "required",
            "geo_area" => "required|array",
            "geo_area.*" => [Rule::in(VolunteerOrganisationStrengthOfParticipation::$geo_areas)],
            "newsletter" => "required|in:0,1",
        ];
    }

    public function attributes(): array
    {
        return [
            "target_group" => mb_strtolower(trans('Ciljna skupina/korisnici organizacije')),
            "geo_area" => mb_strtolower(trans('Zemljopisno područje djelovanja')),
            "newsletter" => mb_strtolower(trans('Newsletter DKolektiva')),
        ];
    }

    protected function prepareForValidation(): void
    {
        $this->merge([
            "newsletter" => $this->newsletter ? 1:0,
        ]);
    }
}
