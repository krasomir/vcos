<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ReportUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return (auth()->user()->hasPermissionTo('edit reports')) ? true : false;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title' => 'required',
            'report_type' => 'nullable|in:pdf,piktochart',
            'report_url' => 'required_with:report_type',
            'published' => 'required',
        ];
    }

    /**
     * Undocumented function
     *
     * @return void
     */
    protected function prepareForValidation()
    {
        $this->merge([
            'published' => ($this->published) ? 1 : 0,
        ]);
    }

    /**
     * Undocumented function
     *
     * @return array
     */
    public function attributes()
    {
        return [
            'report' => 'izvještaj',
            'report_type' => 'vrsta izvještaja',
            'report_url' => 'link ili file izvještaja',
            'publish' => 'objavi'
        ];
    }
}
