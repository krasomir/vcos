<?php

namespace App\Http\Requests;

use App\Slider;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class SliderUpdatePositionRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return (
            auth()->user()->hasPermissionTo('edit sliders') || 
            auth()->user()->hasPermissionTo('create sliders')
        ) ? true :false;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'sliderId' => 'required|numeric|' . Rule::in(Slider::pluck('id')->all()),
            'position' => 'required|numeric'
        ];
    }

    /**
     * Undocumented function
     *
     * @return array
     */
    public function attributes()
    {
        return [
            'sliderId' => 'slider ID',
            'position' => 'pozicija'
        ];
    }
}
