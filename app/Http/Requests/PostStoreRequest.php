<?php

namespace App\Http\Requests;

use App\User;
use Illuminate\Validation\Rule;
use Illuminate\Foundation\Http\FormRequest;

class PostStoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return (
                auth()->user()->hasPermissionTo('create posts') || 
                auth()->user()->hasPermissionTo('edit posts')
            ) ? true : false;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|max:250',
            'preview' => 'required|max:1000',
            'datetime_on' => 'required|date_format:Y-m-d',
            'title' => 'nullable',
            'description' => 'nullable',
            'slug' => 'required',
            'text' => 'required',
            'datetime_off' => 'required|date_format:Y-m-d|after_or_equal:datetime_on',
            'keywords' => 'nullable',
            'image' => 'nullable|image|mimes:jpg,jpeg,png,svg,webp,bmp|dimensions:min_width=1032,min_height=286|max:5120',
            'page_id' => 'required|in:1,2'
        ];
    }

    /**
     * Undocumented function
     *
     * @return array
     */
    public function attributes()
    {
        return [
            'name' => 'naslov',
            'preview' => 'pregled/kratki sadržaj/uvod',
            'datetime_on' => 'datum početka',
            'description' => 'opis',
            'text' => 'sadržaj',
            'datetime_off' => 'datum završetka',
            'keywords' => 'klučne riječi/tagovi',
            'image' => 'priložena slika',
            'page_id' => 'prikaži na stranici'
        ];
    }

    /**
     * Undocumented function
     *
     * @return array
     */
    public function messages()
    {
        return [
            'image.max' => 'Priložena slika mora biti manja od 5MB.'
        ];
    }
}
