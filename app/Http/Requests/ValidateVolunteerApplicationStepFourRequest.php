<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ValidateVolunteerApplicationStepFourRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function rules()
    {
        return [
            'geo_area' => 'required',
            'exclude_area' => 'nullable',
            'source_info' => 'required',
        ];
    }

    public function attributes()
    {
        return [
            'geo_area' => 'geografsko područje',
            'exclude_area' => 'područje gdje ne biste volontirali',
            'source_info' => 'kako ste saznali',
        ];
    }
}
