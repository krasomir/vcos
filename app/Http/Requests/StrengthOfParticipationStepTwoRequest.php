<?php

namespace App\Http\Requests;

use App\VolunteerOrganisationStrengthOfParticipation;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class StrengthOfParticipationStepTwoRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            "mission_goals" => "required|max:250",
            "activity_area" => "required|array",
            "activity_area.*" => [Rule::in(VolunteerOrganisationStrengthOfParticipation::$activity_areas)],
        ];
    }

    public function attributes()
    {
        return [
            "mission_goals" => mb_strtolower(trans('Misija i ciljevi')),
            "activity_area" => mb_strtolower(trans('Područje djelovanja')),
            "activity_area.*" => mb_strtolower(trans('Područje djelovanja')),
        ];
    }
}
