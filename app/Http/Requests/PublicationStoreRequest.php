<?php

namespace App\Http\Requests;

use App\Enums\PublicationCategoriesEnum;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class PublicationStoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return (auth()->user()->hasPermissionTo('create publications')) ? true : false;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title' => 'required',
            'slug' => 'required',
            'publication_category' => ['required', Rule::enum(PublicationCategoriesEnum::class)],
            'image' => 'required|image|dimensions:min-width=576,min-height=800|max:5120|mimes:jpg,jpeg,png,svg,webp,bmp',
            'publication' => 'required|file|max:20480|mimetypes:application/pdf',
            'is_demo_academy' => 'required|in:0,1'
        ];
    }

    /**
     * Undocumented function
     *
     * @return array
     */
    public function attributes()
    {
        return [
            'title' => 'naziv publikacije',
            'slug' => 'slug/SEO naziva',
            'publication_category' => 'kategorija publikacije',
            'image' => 'priloži sliku',
            'publication' => 'priloži publikaciju',
            'is_demo_academy' => 'publikacija za demo akademiju',
        ];
    }

    /**
     * Undocumented function
     *
     * @return void
     */
    public function prepareForValidation()
    {
        $this->merge([
            'is_demo_academy' => ($this->is_demo_academy) ? 1 : 0,
        ]);
    }
}
