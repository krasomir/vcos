<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class SearchRequest extends FormRequest
{
    public function authorize(): bool
    {
        return true;
    }

    public function rules(): array
    {
        return [
            'search_term' => ['required', 'min:3'],
        ];
    }

    public function attributes(): array
    {
        return [
            'search_term' => strtolower(__('Traži')),
        ];
    }

    public function messages()
    {
        return [
            'search_term.min' => __('Upišite minimalno :min znaka.'),
        ];
    }
}
