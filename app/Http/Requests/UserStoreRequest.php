<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UserStoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return (auth()->user()->hasPermissionTo('create users')) ? true : false;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required', 
            'email' => 'required|email|unique:users,email',
            'password' => 'required|min:8|confirmed',
            'permission_ids' => 'required',
            'email_verified_at' => 'date_format:Y-m-d H:i:s'
        ];
    }

    /**
     * Undocumented function
     *
     * @return void
     */
    public function attributes()
    {
        return [
            'name' => 'ime',
            'email' => 'e-mail adresa',
            'password' => 'lozinka',
            'permission_ids' => 'dozvole/dopuštenja'
        ];
    }
}
