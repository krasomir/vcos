<?php

namespace App\Http\Requests;

use App\VolunteerOrganisation;
use Carbon\Carbon;
use Illuminate\Foundation\Http\FormRequest;

class NeedVolunteersStepFour extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            "volunteer_menagement" => "required|in:0,1,2",
            "leadership_training" => "nullable|in:0,1",
            "newsletter" => "required|in:0,1",
        ];
    }

    public function attributes()
    {
        return [
            "volunteer_menagement" => mb_strtolower(trans('Jesu li članovi vaše organizacije završili edukaciju za koordinatora volontera (menadžment volontera)')),
            "leadership_training" => mb_strtolower(trans('')),
            "newsletter" => mb_strtolower(trans('Jeste li zainteresirani redovito primati informacije putem info biltena DKolektiva - organizacije za društveni razvoj')),
        ];
    }

    public function prepareForValidation(): void
    {
        $this->merge([
            'volunteer_period' => $this->volunteer_period_from .' do '. $this->volunteer_period_to,
            'application_deadline' => Carbon::createFromFormat('d.m.Y', $this->application_deadline)->format('Y-m-d'),
            'expense_refund' => implode(',', $this->expense_refund),
        ]);
    }
}
