<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ProjectUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return (auth()->user()->hasPermissionTo('edit projects')) ? true : false;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title' => 'required|max:190',
            'slug' => 'required|max:190',
            'preview' => 'required',
            'description' => 'required',
            'start_date' => 'required|date',
            'end_date' => 'required|date|after_or_equal:start_date',
            'published' => 'required|in:0,1',
            'image' => 'nullable|image|mimes:jpg,jpeg,png,svg,webp,bmp|dimensions:min_width=1032,min_height=286|max:5120',
        ];
    }

    /**
     * Prepare the data for validation.
     *
     * @return void
     */
    protected function prepareForValidation()
    {
        $this->merge([
            'published' => $this->published ? 1:0,
        ]);
    }

    /**
     * Get custom attributes for validator errors.
     *
     * @return array
     */
    public function attributes()
    {
        return [
            'title' => 'naslov',
            'slug' => 'SEO naslov',
            'preview' => 'pregled/kratki sadržaj/uvod',
            'description' => 'opis',
            'start_date' => 'datum početka',
            'end_date' => 'datum završetka',
            'published' => 'objavi',
            'image' => 'priložena slika'
        ];
    }
}
