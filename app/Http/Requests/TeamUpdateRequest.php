<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class TeamUpdateRequest extends FormRequest
{
    public function authorize(): bool
    {
        return (bool)auth()->user()->hasPermissionTo('edit teams');
    }

    public function rules(): array
    {
        return [
            'first_name' => ['required', 'string', 'max:255'],
            'last_name' => ['required', 'string', 'max:255'],
            'position' => ['required', 'string', 'max:255'],
            'email' => ['nullable', 'string', 'email', 'max:255', Rule::unique('teams', 'email')->ignore($this->team->id)],
            'linked_in' => ['nullable', 'string', 'max:255'],
            'description' => ['nullable', 'string', 'max:40000'],
            'image' => ['nullable', 'image', 'mimes:jpeg,jpg,png,webp', 'dimensions:min_width=200, min_height=200', 'max:2048'],
        ];
    }

    public function attributes(): array
    {
        return [
            'first_name' => 'ime',
            'last_name' => 'prezime',
            'position' => 'pozicija',
            'email' => 'e-mail',
            'linked_in' => 'linkedIn',
            'description' => 'kratki tekst',
            'image' => 'priloži sliku',
        ];
    }
}
