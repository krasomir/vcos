<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class TeamDeleteRequest extends FormRequest
{
    public function authorize(): bool
    {
        return (bool)auth()->user()->hasPermissionTo('delete teams');
    }

    public function rules(): array
    {
        return [
            //
        ];
    }
}
