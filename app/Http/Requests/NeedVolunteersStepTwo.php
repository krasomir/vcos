<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class NeedVolunteersStepTwo extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            "contact_email" => "nullable|email",
            "contact_telephone" => "nullable|regex:/^[0-9\-\(\)\/\+\s]*$/",
            "contact_person" => "nullable",
        ];
    }

    public function attributes(): array
    {
        return [
            "contact_email" => mb_strtolower(trans('Kontakt e-mail')),
            "contact_telephone" => mb_strtolower(trans('Kontakt telefon')),
            "contact_person" => mb_strtolower(trans('Kontakt osoba')),
        ];
    }
}
