<?php

namespace App\Http\Requests;

use App\Program;
use Illuminate\Foundation\Http\FormRequest;

class ProgramStoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return (
            auth()->user()->hasPermissionTo('create programs') ||
            auth()->user()->hasPermissionTo('edit programs')
        ) ? true : false;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title' => 'required',
            'slug' => 'required',
            'program_page' => 'required|in:' . Program::getBaseProgramList(),
            'content' => 'required',
            'link_to_pdf' => 'nullable|url',
            'link_title' => 'nullable|required_with:link_to_pdf'
        ];
    }

    /**
     * Undocumented function
     *
     * @return array
     */
    public function attributes()
    {
        return [
            'title' => 'naslov',
            'slug' => 'slug/SEO naslova',
            'program_page' => 'stranica programa',
            'content' => 'sadržaj',
            'link_to_pdf' => 'link to pdf dokument volonterskih nagrada',
            'link_title' => 'naziv linka'
        ];
    }
}
