<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ValidateVolunteerApplicationStepOneRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'first_last_name' => 'required',
            'gender' => 'required',
            'birth_year' => 'required|integer',
            'city' => 'required',
            'telephone' => 'nullable',
            'email' => 'required|email|unique:volunteers,email'
        ];
    }

    public function attributes()
    {
        return [
            'first_last_name' => 'ime i prezime',
            'gender' => 'spol',
            'birth_year' => 'godina rođenja',
            'city' => 'mjesto stanovanja',
            'telephone' => 'telefon',
        ];
    }

    public function messages()
    {
        return [
            'email.unique' => 'Prijava za korisnika s tom e-mail adresom već postoji.',
        ];
    }
}
