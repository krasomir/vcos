<?php

namespace App\Http\Requests;

use App\Banner;
use Illuminate\Foundation\Http\FormRequest;

class BannerUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return (auth()->user()->hasPermissionTo('edit banners')) ? true : false;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title' => 'required',
            'slug' => 'required',
            'image' => 'nullable|image|mimes:jpg,jpeg,png,svg,webp,bmp|max:5120|dimensions:min_width=2030,min_height=190',
            'show_on_page' => 'nullable|in:' . Banner::getBannerUrisString(),
            'bkg_color' => 'required|regex:/^(#[a-fA-F0-9]{6})$/',
        ];
    }

    /**
     * Undocumented function
     *
     * @return void
     */
    public function attributes()
    {
        return [
            'title' => 'naziv bannera',
            'slug' => 'slug/SEO naziva',
            'image' => 'priložena slika',
            'show_on_page' => 'prikaži na stranici',
            'bkg_color' => 'pozadinska boja',
        ];
    }
}
