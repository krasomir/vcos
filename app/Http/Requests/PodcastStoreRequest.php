<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class PodcastStoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return (auth()->user()->hasPermissionTo('create podcasts')) ? true : false;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title' => 'required',
            'season' => 'required|numeric',
            'slug' => 'required',
            'short_description' => 'required',
            'description' => 'required',
            'file' => 'required|file|mimes:mp3|max:204800'
        ];
    }

    /**
     * Undocumented function
     *
     * @return array
     */
    public function attributes()
    {
        return [
            'title' => 'naziv epizode',
            'season' => 'sezona',
            'slug' => 'slug/SEO naziva',
            'short_description' => 'kratki opis epizode',
            'description' => 'opis epizode',
            'file' => 'priloži datoteku'
        ];
    }

    /**
     * Undocumented function
     *
     * @return array
     */
    public function messages()
    {
        return [
            'file.max' => 'Datoteka koju prilažete mora biti manja od 200MB.'
        ];
    }
}
