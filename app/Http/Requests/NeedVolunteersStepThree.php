<?php

namespace App\Http\Requests;

use App\VolunteerOrganisation;
use Illuminate\Foundation\Http\FormRequest;

class NeedVolunteersStepThree extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            "volunteer_engagement" => "required",
            "volunteer_number" => "required|integer",
            "position_name" => "required|max:190",
            "purpose_of_position" => "required|max:190",
            "tasks_description" => "required|max:190",
            "volunteer_qualifications" => "required|max:190",
            "volunteer_period" => "required",
            "hour_number" => "required|integer",
            "volunteering_place" => "required|max:190",
            "additional_education" => "required",
            "expense_refund" => "required|array",
            "expense_refund.*" => "required|in:0,1,2,3",
            "volunteer_period_from" => "required|date_format:d.m.Y",
            "volunteer_period_to" => "required|date_format:d.m.Y|after:volunteer_period_from",
            "application_deadline" => "required|date_format:d.m.Y|before:volunteer_period_from",
        ];
    }

    public function attributes(): array
    {
        return [
            "volunteer_engagement" => mb_strtolower(trans('Volontere trebate')),
            "volunteer_number" => mb_strtolower(trans('Broj potrebnih volontera')),
            "position_name" => mb_strtolower(trans('Naziv volonterske pozicije')),
            "purpose_of_position" => mb_strtolower(trans('Svrha volonterske pozicije')),
            "tasks_description" => mb_strtolower(trans('Opis zadataka koje bi volonter obavljao')),
            "volunteer_qualifications" => mb_strtolower(trans('Koje kvalifikacije, vještine očekujete od volontera')),
            "volunteer_period" => mb_strtolower(trans('Razdoblje volontiranja')),
            "volunteer_period_from" => mb_strtolower(trans('Razdoblje volontiranja - od')),
            "volunteer_period_to" => mb_strtolower(trans('Razdoblje volontiranja - do')),
            "hour_number" => mb_strtolower(trans('Broj sati')),
            "volunteering_place" => mb_strtolower(trans('Mjesto volontiranja (pobliže odredite prostor/lokaciju)')),
            "application_deadline" => mb_strtolower(trans('Rok za prijavu')),
            "additional_education" => mb_strtolower(trans('Može li organizacija pružiti specifično dodatno obrazovanje (poduku) za obavljanje volonterskog angažmana')),
            "expense_refund" => mb_strtolower(trans('Može li organizacija volonteru osigurati pokrivanje određenih troškova ako je potrebno')),
        ];
    }

    public function prepareForValidation(): void
    {
        $this->merge([
            'volunteer_period' => $this->volunteer_period_from .' do '. $this->volunteer_period_to,
        ]);
    }
}
