<?php

namespace App\Http\Requests;

use App\Slider;
use Illuminate\Foundation\Http\FormRequest;

class SliderUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return (auth()->user()->hasPermissionTo('edit sliders')) ? true : false;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title' => 'required',
            'slug' => 'required',
            'image' => 'nullable|image|dimensions:min-width=2031,min-height=953|max:2500|mimes:jpg,jpeg,png,svg,webp,bmp,gif',
            'link_to_page' => 'nullable|in:' . Slider::getAvailablePageUrlsString(),
            'custom_link' => 'nullable|active_url'
        ];
    }

    /**
     * Undocumented function
     *
     * @return array
     */
    public function attributes()
    {
        return [
            'title' => 'naziv slidera',
            'slug' => 'slug/SEO naziva',
            'image' => 'priloži sliku',
            'link_to_page' => 'link na stranicu',
            'custom_link' => 'proizvoljni link',
        ];
    }
}
