<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ValidateVolunteerApplicationStepThreeRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function rules()
    {
        return [
            'volunteer_engagement' => 'required',
            'work_area' => 'required',
        ];
    }

    public function attributes()
    {
        return [
            'volunteer_engagement' => 'volonterski angažman',
            'work_area' => 'područje volontiranja',
        ];
    }    
}
