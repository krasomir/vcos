<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class NeedVolunteersStepOne extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            "organisation_type" => "required",
            "organisation_name" => "required",
            "address" => "required",
            "city" => "required",
            "telephone" => "required|regex:/^[0-9\-\(\)\/\+\s]*$/",
        ];
    }

    public function attributes(): array
    {
        return [
            "organisation_type" => mb_strtolower(trans('Vrsta organizacije')),
            "organisation_name" => mb_strtolower(trans('Naziv organizacije')),
            "address" => mb_strtolower(trans('Sjedište organizacije (ulica i kućni broj)')),
            "city" => mb_strtolower(trans('Mjesto')),
            "telephone" => mb_strtolower(trans('Telefon')),
        ];
    }
}
