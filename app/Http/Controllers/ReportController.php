<?php

namespace App\Http\Controllers;

use App\Http\Requests\ReportDeleteRequest;
use App\Http\Requests\ReportStoreRequest;
use App\Http\Requests\ReportUpdateRequest;
use App\Report;
use Illuminate\Http\Request;
use Yajra\DataTables\Facades\DataTables;

class ReportController extends Controller
{
    public function frontIndex()
    {
        return view('pages.reports', [
            'reports' => Report::latest()->where('published', 1)->get(),
        ]);
    }

    /**
     * Undocumented function
     *
     * @return view
     */
    public function index()
    {
        return view('admin.reports.index');
    }

    /**
     * Undocumented function
     *
     * @param Request $request
     * @return DataTables
     */
    public function reportList(Request $request)
    {
        if ($request->ajax()) {
            $data = Report::latest()->get();
            return DataTables::of($data)
                ->addIndexColumn()
                ->addColumn('action', function($row){
                    $actionBtn = '
                        <a href="'. route('report-edit', [$row, 'lang' => app()->getLocale()]) .'" class="edit btn btn-success btn-sm">Uredi</a> 
                        <a href="#" 
                            data-toggle="modal" 
                            data-target="#confirmDelete" 
                            data-title="Brisanje izvještaja"
                            data-model-id="'. $row->id .'"
                            data-message="Da li zaista želiš obrisati izvještaj - '. $row->title .'?"
                            data-delete-route="'. route('report-delete', [$row, 'lang' => app()->getLocale()]) .'"
                            class="delete btn btn-danger btn-sm"
                        >
                            Obriši
                        </a>
                    ';
                    return $actionBtn;
                })
                ->addColumn('published', function($row) {
                    $checked = ((int)$row->published === 1) ? 'checked': '';
                    $switchBtn = '
                        <div class="custom-control custom-checkbox">
                            <input data-route="'. route('report-publish', [app()->getLocale(), $row]) .'" class="custom-control-input" value="1" '. $checked .' name="published" type="checkbox" id="customCheckbox_'.$row->id.'">
                            <label for="customCheckbox_'.$row->id.'" class="custom-control-label">&nbsp;</label>
                        </div>
                    ';

                    return $switchBtn;
                })
                ->addColumn('report_url', function($row) {
                    return '<a href="'. (($row->report_type === 'pdf') ? asset($row->report_url) : $row->report_url) .'" target="_blank">'. asset($row->report_url) . '</a>';
                })
                ->rawColumns(['action', 'published', 'report_url'])
                ->make(true);
        }
    }

    /**
     * Undocumented function
     *
     * @return view
     */
    public function create()
    {
        return view('admin.reports.create');
    }

    /**
     * Undocumented function
     *
     * @param ReportStoreRequest $request
     * @return Redirect
     */
    public function store(ReportStoreRequest $request)
    {
        $report = Report::store($request);
        if ($report) {
            return redirect()->route('admin-reports', app()->getLocale())->withSuccess('Izvještaj je uspješno spremljen.');
        }
        return redirect()->route('admin-reports', app()->getLocale())->withErrors('Došlo je do greške prilikom spremanja.');
    }

    /**
     * Undocumented function
     *
     * @param string $lang
     * @param Report $report
     * @return view
     */
    public function edit(string $lang, Report $report)
    {
        return view('admin.reports.edit', [
            'report' => $report,
        ]);
    }

    /**
     * Undocumented function
     *
     * @param ReportUpdateRequest $request
     * @param string $lang
     * @param Report $report
     * @return redirect
     */
    public function update(ReportUpdateRequest $request, string $lang, Report $report)
    {
        $updated = $report::updateReport($request, $report);
        if ($updated) {
            return redirect()->route('admin-reports', app()->getLocale())->withSuccess('Izvještaj je uspješno spremljen.');
        }
        return redirect()->route('admin-reports', app()->getLocale())->withErrors('Došlo je do greške prilikom spremanja.');
    }

    /**
     * Undocumented function
     *
     * @param ReportDeleteRequest $request
     * @param string $lang
     * @param Report $report
     * @return Redirect
     */
    public function destroy(ReportDeleteRequest $request, string $lang, Report $report)
    {
        $deleted = $report::deleteReport($report);
        if ($deleted) {
            return redirect()->route('admin-reports', app()->getLocale())->withSuccess('Izvještaj je uspješno obrisan.');
        }
        return redirect()->route('admin-reports', app()->getLocale())->withErrors('Greška prilikom brisanja izvještaja.');
    }

    /**
     * Undocumented function
     *
     * @param Request $request
     * @param string $lang
     * @param Report $report
     * @return Response
     */
    public function reportPublish(Request $request, string $lang, Report $report)
    {
        $report->published = $request->published;
        
        if ($report->save()) {
            return response()->json([
                'error' => false,
                'message' => 'Promjena uspješno spremljena.'
            ], 200);
        }
        return response()->json([
            'error' => true,
            'message' => 'Došlo je do grške prilikom spremanja promjene.'
        ], 500);
    }
}
