<?php

namespace App\Http\Controllers;

use App\Http\Requests\PodcastDeleteRequest;
use App\Http\Requests\PodcastMainTextStoreRequest;
use App\Podcast;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use Yajra\DataTables\Facades\DataTables;
use App\Http\Requests\PodcastStoreRequest;
use App\Http\Requests\PodcastUpdateRequest;
use App\PodcastMainText;
use Intervention\Image\Exception\NotFoundException;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class PodcastController extends Controller
{
    /**
     * Display a listing of the resource in admin panel
     *
     * @return \View
     */
    public function index()
    {
        return view('admin.podcasts.index', [
            'mainPodcastText' => PodcastMainText::first(),
        ]);
    }

    /**
     * Display a listing of the resource on front
     *
     * @return \View
     */
    public function frontPodcasts()
    {
        $podcasts = Podcast::whereNotNull('published_at')->latest('published_at')->get();
        $podcastsGroupedBy = $podcasts->groupBy('season');
        $seasonCount = $podcastsGroupedBy->count();
        $seasons = range(1, $seasonCount);
        arsort($seasons);
        
        return view('pages.podcasts', [
            'podcasts' => $podcasts,
            'seasons' => $seasons,
            'mainPodcastText' => PodcastMainText::first(),
        ]);
    }

    /**
     * Undocumented function
     *
     * @param String $lang
     * @param Podcast $podcast
     * @return \View
     */
    public function podcastIndex(String $lang, Podcast $podcast)
    {
        abort_if(!$podcast->isPublished(), Response::HTTP_NOT_FOUND);

        return view('pages.podcast', [
            'podcast' => $podcast,
        ]);
    }

    /**
     * Undocumented function
     *
     * @param Request $request
     * @return DataTables
     */
    public function list(Request $request)
    {
        if ($request->ajax()) {
            $podcasts = Podcast::latest()->get();

            return DataTables::of($podcasts)
                ->addIndexColumn()                
                ->addColumn('action', function($row){
                    $actionBtn = '
                        <a href="'. route('podcasts.edit', [$row, 'lang' => app()->getLocale()]) .'" class="edit btn btn-success btn-sm">Uredi</a> 
                        <a href="#" 
                            data-toggle="modal" 
                            data-target="#confirmDelete" 
                            data-title="Brisanje epizode"
                            data-model-id="'. $row->id .'"
                            data-message="Da li zaista želiš obrisati epizodu - '. $row->title .'?"
                            data-delete-route="'. route('podcasts.destroy', [$row, 'lang' => app()->getLocale()]) .'"
                            class="delete btn btn-danger btn-sm"
                        >
                            Obriši
                        </a>
                    ';
                    return $actionBtn;
                })
                ->editColumn('description', function($row) {
                    return Str::words(strip_tags($row->description), 20, '...');
                })
                ->setRowId(function($row) {
                    return $row->id;
                })
                ->rawColumns(['action'])
                ->make(true);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \View
     */
    public function create()
    {
        $lastSeason = Podcast::select('season')->latest()->take(1)->first();
        
        return view('admin.podcasts.create', [
            'lastSeason' => $lastSeason->season,
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(PodcastStoreRequest $request)
    {
        $saved = Podcast::storePodcast($request);

        if ($saved) {
            return redirect()->route('podcasts.index', app()->getLocale())->withSuccess('Uspješno spremljena epizoda.');
        }
        return redirect()->route('podcasts.index', app()->getLocale())->withErrors('Greška prilikom spremanja epizode.');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Podcast  $podcast
     * @return \View
     */
    public function edit(Request $request, String $lang, Podcast $podcast)
    {
        return view('admin.podcasts.edit', [
            'podcast' => $podcast,
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  String  $lang
     * @param  \App\Podcast  $podcast
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(PodcastUpdateRequest $request, String $lang, Podcast $podcast)
    {
        $updated = Podcast::updatePodcast($request, $podcast);
        if ($updated) {
            return redirect()->route('podcasts.index', app()->getLocale())->withSuccess('Uspješno spremljeno.');
        }
        return redirect()->route('podcasts.index', app()->getLocale())->withErrors('Greška prilikom spremanja.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  String $lang
     * @param  \App\Podcast  $podcast
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy(PodcastDeleteRequest $request, String $lang, Podcast $podcast)
    {
        $deleted = Podcast::deletePodcast($podcast);
        if ($deleted) {
            return redirect()->route('podcasts.index', app()->getLocale())->withSuccess('Uspješno obrisano.');
        }
        return redirect()->route('podcasts.index', app()->getLocale())->withErrors('Greška prilikom brisanja.');
    }

    /**
     * Undocumented function
     *
     * @param PodcastMainTextStoreRequest $request
     * @param String $lang
     * @param PodcastMainText $podcastMainText
     * @return \Illuminate\Http\RedirectResponse
     */
    public function saveMainText(PodcastMainTextStoreRequest $request, String $lang, PodcastMainText $podcastMainText)
    {
        if ($podcastMainText->update($request->validated())) {
            return redirect()->back()->withSuccess('Uspješno spremljena promjena.');
        }
        return redirect()->back();
    }
}
