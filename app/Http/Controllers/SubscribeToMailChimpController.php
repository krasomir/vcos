<?php

namespace App\Http\Controllers;

use App\SubscriptionBatch;
use App\SubscriptionChunck;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Spatie\Newsletter\NewsletterFacade as Newsletter;

class SubscribeToMailChimpController extends Controller
{
    public function subscribersList()
    {
        config(['newsletter.apiKey' => config('newsletter.vcos_apikey')]);
        config(['newsletter.defaultListName' => config('newsletter.defaultListName')]);

        $mailChimpApi = Newsletter::getApi();
        $batch = $mailChimpApi->new_batch();

        if (SubscriptionChunck::where('list_name', 'subscribers')->count() > 0) {
            
            $savedBatch = SubscriptionBatch::where('list_name', 'subscribers')->first();
            if ($savedBatch->count() > 0) {
                $mailChimpApi->new_batch($savedBatch->batch_id);
                $checkStatus = $batch->check_status();
                $resultIndex = array_search($savedBatch->batch_id, array_column($checkStatus['batches'], 'id'));
                if (count($checkStatus['batches']) > 0) {
                    $checkStatusResult = $checkStatus['batches'][$resultIndex];

                    $savedBatch->update([
                        'status' => $checkStatusResult['status'],
                        'total_operations' => $checkStatusResult['total_operations'],
                        'finished_operations' => $checkStatusResult['finished_operations'],
                        'errored_operations' => $checkStatusResult['errored_operations'],
                        'response_body_url' => $checkStatusResult['response_body_url'],
                        'completed_at' => Carbon::parse($checkStatusResult['completed_at'])->format('Y-m-d H:i:s'),
                    ]);
                }
            }
                

            return view('admin.newsletters.index', [
                'alertType' => 'info',
                'message' => 'Pretplate su poslane na MailChimp API. Rezultat importa možete preuzeti klikom na dugme.',
                'downloadResultURL' => $savedBatch->response_body_url ?? null,
                'status' => $savedBatch->status ?? null,
                'listName' => 'subscribers/volonteri',
            ]);
        }

        /* TODO
        - https://mailchimp.com/developer/marketing/api/lists/batch-subscribe-or-unsubscribe/
        - https://github.com/drewm/mailchimp-api
        - https://mailchimp.com/developer/marketing/guides/run-async-requests-batch-endpoint/
        */

        $subscribers = [
            'bornabutkovic@gmail.com',
            'doramajic1008@gmail.com',
            'jug.leon28@gmail.com',
            'antonia.skoliber@gmail.com',
            'anaagatic@gmail.com',
            'isabelljanjanin@gmail.com',
            'zeljka.damir.k@gmail.com',
            'marija.roksandic00@gmail.com',
            'iskramatijanic@gmail.com',
            'dorisr346@gmail.com',
            'luka.maric1911@hotmail.com',
            'roberta.cvijetovic99@gmail.com',
            'tonivkcl140@gmail.com',
            'sofijasitar@gmail.com',
            'osijek@step.hr',
            'andrej.aladrovic43@gmail.com',
            'martina2431998@gmail.com',
            'matea.stelcar@gmail.com',
            'tanjaberlancic@gmail.com',
            'leonardapataca@gmail.com',
            'eva.getos@gmail.com',
            'mattteeeeatr@gmail.com',
            'lucijacuric75@gmail.com',
            'anamisev80@gmail.com',
            'martapavlovic2006@gmail.com',
            'korana.sibila55@gmail.com',
            'ana.skudar7@gmail.com',
            'katjaburic0812@gmail.com',
            'marko.ivkovic26@gmail.com',
            'tia.galjer@gmail.com',
            'vanesa.marincel@gmail.com',
            'karla.vojedilov@gmail.com',
            'kerievelyn9@gmail.com',
            'brkicdino1@gmail.com',
            'bermihaela@gmail.com',
            'dekijurko@gmail.com',
            'tamara.spoljaric1@gmail.com',
            'iva.sapina5@gmail.com',
            'mihicm632@gmail.com',
            'chendinamin@gmail.com',
            'tomislava.cavar00@gmail.com',
            'zrinko.leko.posao@gmail.com',
            'tihanapavokovicosk@gmail.com',
            'asuvaljko@mefos.hr',
            'matea.takus@gmail.com',
            'romer123@net.hr',
            'ivona.albertt@gmail.com',
            'silvija.dmejhal@gmail.com',
            'lucija.majetic2@gmail.com',
            'ljubicicmarij@gmail.com',
            'leastrasser06@gmail.com',
            'lucija.hunjadi@gmail.com',
            'lananikolic33@gmail.com',
            'sarasencar11@gmail.com',
            'milospoz13@gmail.com',
            'poznanovicmiljana@gmail.com',
            'ema.padavic@gmail.com',
            'lucija.vukovic2@gmail.com',
            'brunomedic51@gmail.com',
            'leda.perisa98@gmail.com',
            'tomaz.gabrijela@gmail.com',
            'bilic.magdalena99@gmail.com',
            'tomislav.njari@krnjak.eu',
            'jelenic.marina@gmail.com',
            'ivaskola10@gmail.com',
            'zara.bicvic@gmail.com',
            'zelic.kristian22@gmail.com',
            'jmiskovic85@gmail.com',
            'sanja.skugor@gmail.com',
            'tadicruzica3@gmail.com',
            'suncica.alduk@gmail.com',
            'izabela.efos@gmail.com',
            'tonka.budic1@gmail.com',
            'laura.mandic@gmail.com',
            'mirta.kovacevic@dkolektiv.hr',
            'jelena.stefanovic@dkolektiv.hr',
            'dajanapavl23@gmail.com',
            'rgajic123@gmail.com',
            'faktor.laura@gmail.com',
            'facee.martina@gmail.com',
            'anamariaoned@gmail.com',
            'jklepac9@gmail.com',
            'helena.ekstajn@gmail.com',
            'anja.knezevic2@gmail.com',
            'agorup97@gmail.com',
            'bbrekalo6@gmail.com',
            'teajanus997@gmail.com',
            'brigitaknezevic96@gmail.com',
            'dunjasimic3@gmail.com',
            'stilindora567@gmail.com',
            'opg_maletic@yahoo.com',
            'sfaletar@gmail.com',
            'baric.magdalena2@gmail.com',
            'zana.weg@gmail.com',
            'valentina.kresic1@gmail.com',
            'mirta.glavasic@gmail.com',
            'anadrda75@gmail.com',
            'kljubic7@gmail.com',
            'ivan.antukic@gmail.com',
            'dragak7@gmail.com',
            'farkasdominik100@gmail.com',
            'roko.gj@hotmail.com',
            'sekulic.tajnik@gmail.com',
            'leonarda.fabing@gmail.com',
            'lokinlucija@gmail.com',
            'vulesima@gmail.com',
            'dora.stojcic2002@gmail.com',
            'helenasogoric54@gmail.com',
            'patricia.andric2@gmail.com',
            'lorenamarkasovic@gmail.com',
            'mjeger@gmail.com',
            'klara.valentekovic@gmail.com',
            'vanja.basaric@gmail.com',
            'doraidanijela@gmail.com',
            'nikolina212zar@gmail.com',
            'sanja.raca@yahoo.com',
            'loncar.v18@gmail.com',
            'dea.rajlic@gmail.com',
            'monika.jovanovic30@gmail.com',
            'romana.marusic55@gmail.com',
            'luka.palaversic@gmail.com',
            'vampaticmartina@gmail.com',
            'ivanasb94@outlook.com',
            'm.stanic97@gmail.com',
            'ruzica.trdin@gmail.com',
            'ana.vugrinec7@gmail.com',
            'anabetlach@gmail.com',
            'ivanica.jagodic@gmail.com',
            'izabela.stosic6@gmail.com',
            'anaperic969@gmail.com',
            'barbzivaljic@gmail.com',
            'vedranavedricc@gmail.com',
            'majakopc@gmail.com',
            'anamiskovic11@gmail.com',
            'skatarp@gmail.com',
            'vedrana.hvizdak15@gmail.com',
            'noragrevinger00@gmail.com',
            'dragicevic.ena@gmail.com',
            'jurkovic.gabriela2000@gmail.com',
            'ena.osonjacki2110@gmail.com',
            'klarajurisic1294@gmail.com',
            'nika.skugor123@gmail.com',
            'martinagelencir98@gmail.com',
            'petrarosandic88@gmail.com',
            'ema.ilic031@gmail.com',
            'anita.krstanovic@gmail.com',
            'enyaoshr@gmail.com',
            'Nikolinasimic1008@gmail.com',
            'zvonkec2002@gmail.com',
            'ivan.dragicevic095@gmail.com',
            'tina.brodanac@gmail.com',
            'danielapecolaj@gmail.com',
            'majkic.referent@gmail.com',
            'janadolezal10@gmail.com',
            'radanoviceva@gmail.com',
            'ana_maria0555@hotmail.com',
            'papuciclara@gmail.com',
            'persinovic.mihael@gmail.com',
            'anab70907@gmail.com',
            'ttolic2000@gmail.com',
            'klarasvetic@gmail.com',
            'anna.jurkovic1012@hotmail.com',
            'sarahrcka51@gmail.com',
            'dunjacaleta01@gmail.com',
            'learadic5@gmail.com',
            'igor_sovic@hotmail.com',
            'tomas.katarina88@gmail.com',
            'hana.zajkic@gmail.com',
            'yatriika@outlook.com',
            'marko.1953.1959@gmail.com',
            'vladimirljubicic99@gmail.com',
            'Filipmudrovcic2311@gmail.com',
            'matildalazovic@gmail.com',
            'tanjatvrdavica@gmail.com',
            'renata.jaksic36@gmail.com',
            'martinadasovi@gmail.com',
            'teamaric995@gmail.com',
            'mihalje995@gmail.com',
            'hanna.tripolski12345@gmail.com',
            'matea.novak89@gmail.com',
            'katarinacavar02@gmail.co',
            'ena.popovic1310@gmail.com',
            'cvitkovic.monika@gmail.com',
            'elenajaric@gmail.com',
            'plackovic.jelena199@gmail.com',
            'mbozicka@gmail.com',
            'Marijamilic893@gmail.com',
            'tmikic90@gmail.com',
            'mateapernar031@gmail.com',
            'klescek.tea@gmail.com',
            'ana.bardic32@gmail.com',
            'petra.sekulic17@gmail.com',
            'simic.anja00@gmail.com',
            'sara_posavec@hotmail.com',
            'bajraktarevicleonarda@gmail.com',
            'marta.pav14@gmail.com',
            'sabina.novoselic@gmail.com',
            'dijanapandurevic@gmail.com',
            'valentina.sinko123@gmail.com',
            'tena.vulic@gmail.com',
            'tvrtko.horvat@pliva.com',
            'ramona.salha@gmail.com',
            'twyxy17@gmail.com',
            'ivona1805@gmail.com',
            'georgije.bogojevic@gmail.com',
            'ivanalisnic2903@gmail.com',
            'christensen.aaron@missionary.org',
            'amkelic@gmail.com',
            'mihaceva9@gmail.com',
            'iva.pralija@gmail.com',
            'bruno.erzic@gmail.com',
            'tea1.zagar@gmail.com',
            'teamarkovic535@gmail.com',
            'mihaela.korman@gmail.com',
            'knollica@gmail.com',
            'lidijapaulic07@gmail.com',
            'majksnerkristina@gmail.com',
            'majksneranamarija@gmail.com',
            'ivana.kraus94@gmail.com',
            'kprpic@hotmail.com',
            'laura.marticc@gmail.com',
            'zv.milanovic@gmail.com',
            'ivana.madarevic@hotmail.com',
            'smedimorec@yahoo.com',
            'ivan.maras.et4@gmail.com',
            'tmutnjak@gmail.com',
            'kristina-jurcic1@hotmail.com',
            'klaudija.zdravac@gmail.com',
            'anao79979@gmail.com',
            'karlabubalo2@gmail.com',
            'i.jankovi@yahoo.com',
            'anaaa784@gmail.com',
            'borna.stiglec@gmail.com',
            'sarasamardzic11@gmail.com',
            'gazda0110@hotmail.com',
            'rorsolic@gmail.com',
            'itvujic@gmail.com',
            'andrejinic@gmail.com',
            'sambolicmarta@gmail.com',
            'ivansnijeg96@gmail.com',
            'magdalenaorlovic77@gmail.com',
            'iva.maric.os@gmail.com',
            'teazubovic998@gmail.com',
            'tea.ravnjak98@gmail.com',
            'lucija.bukvich@hotmail.com',
            'tamarabolic@gmail.com',
            'anasladoja7@gmail.com',
            'gorankab@hotmail.com',
            'leonardabosiljkov1456789@gmail.com',
            'julijana.kizivat@gmail.com',
            'nikolinaa1.kusturic@gmail.com',
            'laurabradaric@gmail.com',
            'galovic7@gmail.com',
            'valentina.blazinkov@gmail.com',
            'kristina.pinjusic@gmail.com',
            'leenaomi4@gmail.com',
            'leonora.repinac@gmail.com',
            'klara.djambic97@gmail.com',
            'ena.babic.vk@gmail.com',
            'tamara.kajtar@hotmail.com',
            'narancaa123@gmail.co',
            'grozdanicvlatka@gmail.com',
            'ruzicamar7@gmail.com',
            'elenavuceta@gmail.com',
            'klaudijakaby@gmail.com',
            'wizehood@gmail.com',
            'ramona.davidovic@gmail.com',
            'petra.hampovcan@gmail.com',
            'marta.rastija@gmail.com',
            'andrea.abramac@gmail.com',
            'miriam.kalic@gmail.com',
            'iradic826@gmail.com',
            'ana.knezevic1276@gmail.com',
            'laura.dujic2@gmail.com',
            'anapinotic1010@gmail.com',
            'ninadebeljak0@gmail.com',
            'rsaraga02@gmail.com',
            'andreamigles@gmail.com',
            'vanesa.tonkovac@gmail.com',
            'bubu.the.geek@gmail.com',
            'dorabalic1998@gmail.com',
            'mateomartincevic@gmail.com',
            'paula.spiranec5@gmail.com',
            'franka.vuletic11@gmail.com',
            'filipfey@gmail.com',
            'ancicagaso@gmail.com',
            'kristina.sekulic08@gmail.com',
            'barbara.hunjadi04@gmail.com',
            'drzaic.sara24@gmail.com',
            'doris.andrijevic.da@gmail.com',
            'mfbabok@gmail.com',
            'elizabeta.konjusak@gmail.com',
            'majatominac21@gmail.com',
            'marija.zivaljic13@gmail.com',
            'mpavlov654@gmail.com',
            'sarah.cupoo@gmail.com',
            'lana.lukadinovic@gmail.com',
            'elena.corba@gmail.com',
            'sarasrakic4@gmail.com',
            'ivona.albert@gmail.com',
            'lusichelena@gmail.co',
            'ivanaljubas22@gmail.com',
            'jelenaloncaric@yahoo.com',
            'astulac95@gmail.com',
            'ppendic0@gmail.com',
            'karlakrolo69@gmail.com',
            'dora.glavas7@gmail.com',
            'luka@sokcic.hr',
            'roxy1502@gmail.com',
            'mia.lalic03@gmail.com',
            'luludaki1987@gmail.com',
            'serdarpnv@gmail.com',
            'ina.delic13@gmail.com',
            'dinosvalina@gmail.com',
            'anamarijap44@gmail.com',
            'juramalinovic@gmail.com',
            'karla.kurbalic19@gmail.com',
            'gabrijelvoda@gmail.com',
            'idabiskup98@gmail.com',
            'amoreta.vacka135@gmail.com',
            'samosara01@gmail.co',
            'dunja.babic8@gmail.com',
            'majatominac123@gmail.com',
            'tihana.erzic@gmail.com',
            'anica.krizanovic@gmail.com',
            'magdapoljak4@gmail.com',
            'zvjezda50@net.hr',
            'laura.stubiar42@gmail.com',
            'erzic.ivana@gmail.com',
            'petradragun0@gmail.com',
            'suzana.lena.glavas@gmail.com',
            'ivana5mrso@gmail.com',
            'martinagrbesa77@gmail.com',
            'mjaman294@gmail.com',
            'barbara.ravlic@hotmail.com',
            'cosic1905@gmail.com',
            'iva.fehir@gmail.com',
            'patrik.pokupic@gmail.com',
            'lana25fb@gmail.com',
            'vzoldin@etfos.hr',
            'sarabilje@gmail.co',
            'jakopiclarisa3@gmail.com',
            'drazicana1@live.com',
            'pavlarupa124@gmail.com',
            'Kjaki218@gmail.com',
            'pesikanmarija@gmail.com',
            'jana.matusko89@gmail.com',
            'marijana_rajkovaca@hotmail.com',
            'katarinaprgomet13@gmail.com',
            'mikicleona@gmail.com',
            'mivana546@gmail.com',
            'matejopolcer@gmail.com',
            'mateabartulovic169@gmail.com',
            'lmunzfilipovic@gmail.com',
            'janja.vidovice@hotmail.com',
            'anamary43@hotmail.com',
            'vjakus00@gmail.com',
            'lukabrankovic285@gmail.com',
            'martina.begovic2409@gmail.com',
            'marija.kasapovic1@skole.hr',
            'zupaneva898@gmail.co',
            'lucijatrampus007@gmail.com',
            'lucijacuvalo14@gmail.com',
            'triplat.ana@gmail.com',
            'izidoramini@gmail.com',
            'ravlic.ena@gmail.com',
            'lanabiondic5@gmail.com',
            'lorenauranic2@gmail.com',
            'sarakucek@gmail.com',
            'tkalecantonia@gmail.com',
            'marijana.32@hotmail.com',
            'rebeka.bosnjakovic@hotmail.com',
            'marija.krizic99@gmail.com',
            'ivakovac2005@gmail.com',
            'duraniciva@gmail.com',
            'lara1krasnjak@gmail.com',
            'lucija.akmadzic9@gmail.com',
            'donatr2014@gmail.com',
            'stelaivankovic1@gmail.com',
            'danijela.madjarac@gmail.com',
            'matija_grguric@yahoo.co',
            'mbunic33@gmail.com',
            'sanela.vojvodic5@gmail.com',
            'anamarija5656@gmail.com',
            'mateaschmutz@gmail.com',
            'majakompanovic97@gmail.com',
            'klobucargabrijela@gmail.com',
            'brunocosic2016@gmail.com',
            'annastankovic77@gmail.com',
            'mia.ivancevic28@gmail.com',
            'manda15matesic@gmail.com',
            'iva.duvnjaak@gmail.com',
            'ivavrbanic193@gmail.com',
            'simic29ivana@gmail.com',
            'josko.buzina12@gmail.com',
            'lanaantunovic41@gmail.com',
            'marija.miric68@gmail.com',
            'loris.zagvozda@gmail.com',
            'lana.handanovic@gmail.com',
            'doroteajelkic@gmail.com',
            'zeljka136@gmail.co',
            'lucijanikolic175@gmail.com',
            'prohaskarea73@gmail.com',
            'levacic.stevo@gmail.com',
            'dunja.gagulic@gmail.com',
            'tonicelan2000@gmail.com',
            'mihaelasego31@gmail.com',
            'lucijamijatovic00@gmail.com',
            'marijana44@gmail.com',
            'mtrconic@gmail.com',
            'marina.kardum.efos@gmail.com',
            'ivonabogut@gmail.com',
            'klara.digi9@gmail.com',
            'kristinarajski@gmail.com',
            'majazupan92@gmail.com',
            'tena.trcovic@icloud.com',
            'vlatka.sokec153@gmail.com',
            'dorotea.horvat123@gmail.com',
            'iscicdunja@gmail.com',
            'dbogojevic68@gmail.com',
            'lucijaniki@gmail.co',
            'stella.glavic@gmail.com',
            'rados.ivona@gmail.com',
            'larabarisic12@gmail.com',
            'deyoacm@gmail.com',
            'horvatdorotea555@gmail.com',
            'dina.zafred1@gmail.com',
            'lucija.pavin7@gmail.com',
            'vuknicivan2@gmail.com',
            'anamm333@gmail.com',
            'matea.crnoja44@gmail.com',
            'lucijalaus@gmail.com',
            'vanessa.b98@gmail.com',
            'maja.popovic108@hotmail.com',
            'uzarevicccmatea94@gmail.com',
            'marin3852@gmail.com',
            'mihaelaspajic@gmail.com',
            'elazeko@gmail.com',
            'andrijana95marinic@gmail.com',
            'delta07@net.hr',
            'anitasaric2013@gmail.com',
            'zagoreni05@gmail.com',
            'valentinaastantic@gmail.com',
            'ivona.banjeglav@gmail.com',
            'ivanbogdanovic1998@gmail.com',
            'dumancic.darija@gmail.com',
            'dancejust03@gmail.com',
            'spajz3@gmail.com',
            'kraljeviclaura@yahoo.com',
            'nekict@gmail.com',
            'bbetaduga@gmail.com',
            'Le.ivona1995@gmail.com',
            'ana.pap.88@gmail.com',
            'ipjopica1@gmail.com',
            'kkrnjakovic@ffos.hr',
            'parus.majur@gmail.com',
            'javorovic.luka@gmail.com',
            'ana.itienne.njari@gmail.com',
            'isvilanovic@gmail.com',
            'saarbasaar@gmail.com',
            'gabrijelaarbanas0@gmail.co',
            'domagojkaucic2@gmail.com',
            'djinamalivuk11@gmail.com',
            'tmalivuk11@gmail.com',
            'mirela.vitman@gmail.com',
            'kisic.monika@gmail.com',
            'lidijastevic911@gmail.com',
            'kaja.grgich@gmail.com',
            'maric.klara.os@gmail.com',
            'tjurisic.kvar@gmail.com',
            'mihaa2708@gmail.com',
            'dajanabogojevic1@gmail.com',
            'stjepankocevarmat@gmail.com',
            'zivkovickatarina93@gmail.com',
            'mihaelahrnjkas@yahoo.com',
            'anamarija.buzgo@gmail.com',
            'marija.fercec003@gmail.com',
            'ana_stakic@hotmail.com',
            'gotal551@icloud.com',
            'zivancica3@gmail.com',
            'lana2002mak@gmail.com',
            'tihana01456@gmail.com',
            'ivanaa0703@gmail.com',
            'mihaela1764@gmail.com',
            'mina.kovacic1d@gmail.com',
            'npejakic@gmail.com',
            'Lara.rtf@gmail.com',
            'marija.prpic88@gmail.com',
            'Karla.dimoti2@gmail.com',
            'nikolinahorvatinovic@gmail.com',
            'sodar.tihana@gmail.com',
            'dcagalj1980@gmail.com',
            'tomineratomi.tomi@gmail.com',
            'marija93.maja@gmail.com',
            'raffaella.gudelj@gmail.com',
            'dunja.cpn@gmail.com',
            'marissa.bura10@gmail.com',
            'marko-grubjesic@hotmail.com',
            'IzabelaBorge@gmail.com',
            'vekym94@gmail.com',
            'sanja.socivica@gmail.co',
            'luka.adidas@gmail.com',
            'SuzanaPuljich955@gmail.com',
            'gagulicantonijapetra@gmail.com',
            'dorotea.vrbanec@gmail.com',
            'illusion6os@gmail.com',
            'ana.milas140@gmail.com',
            'nbacelj7@gmail.com',
            'zvonimirparac@gmail.com',
            'dajana.gaso@ptfos.hr',
            'emilycesarec23@gmail.com',
            'danijela.romic11@gmail.com',
            'marta.keglevic@gmail.com',
            'inesinera@gmail.com',
            'ebabic127@gmail.com',
            'krasnicsabrina@gmail.com',
            'valicsandra@gmail.com',
            'vcosi29@gmail.com',
            'vita.guljas@gmail.com',
            'simicicdora@gmail.com',
            'ivana.milicevic127@gmail.co',
            'karla.vujicic@gmail.com',
            'helena98miskovic39@gmail.com',
            'andrea.stefic1@gmail.com',
            'spiranec.karla@gmail.com',
            'maltasic.robert@gmail.com',
            'suzana016@gmail.com',
            'edvu@edvu.hr',
            'matej.pleskov@gmail.com',
            'laura-song@live.com',
            'petrastrbik1302@gmail.com',
            'emily1prime@gmail.com',
            'adrijan.malinovic@gmail.com',
            'pbilandzic@gmail.com',
            'ena.salaj@gmail.com',
            'm.9ciganovic@gmail.com',
            'florijancic.matko@gmail.com',
            'katerina.rajkovaca@gmail.com',
            'marina.cno7@gmail.com',
            'inesmarkulak1@gmail.com',
            'miavolmut7@gmail.co',
            'antonela.bikic98@gmail.com',
            'franciska.miletic8@gmail.com',
            'tina.glumac15@gmail.com',
            'renatadrventic@gmail.com',
            'lucija.leventic@gmail.com',
            'markomajnik@gmail.com',
            'IvanPuljich955@gmail.com',
            'nina4baum@gmail.com',
            'ivanchy.basic@gmail.com',
            'karmelasiljes1507@gmail.com',
            'martinahranic7@gmail.com',
            'rbeljan8@gmail.com',
            'teavujnovac522@gmail.com',
            'bartolovic.marija@gmail.com',
            'arnautovicdajana@gmail.com',
            'odvjetnik.hr@hotmail.com',
            'jastuk.osk@gmail.com',
            'svjetlana.simic.os@gmail.com',
            'tatjana.rigo7@gmail.com',
            'sara.vlahek1e@gmail.co',
            'karlatrampus007@gmail.com',
            'dina.kovacevic33@gmail.com',
            'antoniazlataric@gmail.com',
            'aktivni.umirovljenici@gmail.com',
            'matea.trampus15@gmail.com',
            'dunja.resetar.1995@gmail.com',
            'danielamijatovic612@gmail.com',
            'ana.abramovic97@gmail.com',
            'emaloryana@gmail.com',
            'emacosic2@gmail.com',
            'vinko746@gmail.com',
            'nemanja.stojanovic4@gmail.com',
            'zrinka.lovric1@gmail.com',
            'emamilicevic4@gmail.com',
            'bardiclucija@gmail.com',
            'filipvidosavljevic.os@gmail.com',
            'gloria.bencina@gmail.com',
            'helena.ivana.klaric@gmail.com',
            'ivanatomljenovic02@gmail.com',
            'dogy004@hotmail.com',
            'valentina.turudic@gmail.com',
            'katarinadamjanovic4@gmail.com',
            'mihaelanana12@gmail.com',
            'paula196@live.com',
            'ivapreradovic@gmail.com',
            'josipa_plesa@hotmail.com',
            'basic.sara@yahoo.com',
            'ioxol2611@gmail.com',
            'psihos.klub@gmail.com',
            'smartina907@gmail.com',
            'ana.marija.kovacevic7@gmail.com',
            'maruskica13@hotmail.com',
            'valentina.zegnal@gmail.com',
            'popovicsrdjan1108@gmail.com',
            'dorotea.zivalj@gmail.com',
            'marijazigmundic@gmail.com',
            'jelenak9@hotmail.com',
            'majakulej@gmail.com',
            'doris.priba@hotmail.com',
            'ana.gregic@gmail.com',
            'medved.magdalena@gmail.com',
            'kolegicaa@gmail.com',
            'lila.modric@gmail.com',
            'msulmajster@gmail.com',
            'dratkajec@ptfos.hr',
            'hiklmarko@gmail.com',
            'tanjawhi@hotmail.com',
            'jelena.galicz@gmail.com',
            'vanjkovac@gmail.com',
            'nevena_dalj@yahoo.com',
            'sakota.marina1@gmail.com',
            'mrkyblu28@gmail.com',
            'iva.lendic@gmail.com',
            'vlatka.tutic11@gmail.com',
            'kmikaac@gmail.com',
            'ana.veocic@gmail.com',
            'sarcheko@gmail.com',
            'beata.bosnakovski@gmail.com',
            'kristina.ivkovic00@gmail.com',
            'mdragun007@gmail.co',
            'katarina.klasic@gmail.com',
            'alma.arbanas@gmail.com',
            'gabriela.duic@gmail.com',
            'milapatkovic@yahoo.com',
            'mely.posao@gmail.com',
            'tena.sokman@gmail.com',
            'doriana.duic@gmail.com',
            'stefani.majic@gmail.com',
            'improvisione@gmail.com',
            'mislav-osk@hotmail.com',
            'rebeka.oklopcic@gmail.com',
            'raff.rebeka@gmail.com',
            'valentinarom7@gmail.com',
            'ines-vk@live.com',
            'kikica001@gmail.com',
            'marija.sostaric07@gmail.com',
            'marina.bosnjak13@gmail.com',
            'hanaiadrijana@yahoo.com',
            'jstefanovic84@gmail.com',
            'masaberg@gmail.co',
            'marogic@gmail.com',
            'vena_bok@yahoo.com',
            'ivana.kuraja@gmail.com',
            'helena_skender@yahoo.com',
            'stefaniedjurakovic@gmail.com',
            'arijana.lekic@gmail.com',
            'kovac.pravos@gmail.com',
            'valentino.fabing@gmail.com',
            'tomisslav@gmail.com',
            'majak4@gmail.com',
            'a.anaosijek@gmail.com',
        ];

        $listId = config('newsletter.lists.subscribers.id');
        $chunckedSubscribers = array_chunk($subscribers, 500);
        $insertData = [];
        
        foreach ($chunckedSubscribers as $key => $subscribers) {
            foreach ($subscribers as $subscriber) {
                $batch->post("subscribers_$key", "lists/$listId/members", [
                    'email_address' => $subscriber,
                    'status' => 'subscribed',
                ]);
                $insertData[] = [
                    'operation_id' => "subscribers_$key",
                    'list_name' => config('newsletter.defaultListName'),
                    'email' => $subscriber,
                    'created_at' => now(),
                    'updated_at' => now(),
                ];
            }
        }
        
        if(SubscriptionChunck::insert($insertData)) {
        
            $result = $batch->execute();
            
            $created = SubscriptionBatch::create([
                'list_name' => config('newsletter.defaultListName'),
                'batch_id' => $result['id'],
                'status' => $result['status'],
                'total_operations' => $result['total_operations'],
                'finished_operations' => $result['finished_operations'],
                'errored_operations' => $result['errored_operations'],
                'submitted_at' => Carbon::parse($result['submitted_at'])->format('Y-m-d H:i:s'),
                'completed_at' => Carbon::parse($result['completed_at'])->format('Y-m-d H:i:s'),
                'response_body_url' => $result['response_body_url'],
            ]);
            if ($created) {
                return view('admin.newsletters.index', [
                    'alertType' => 'success',
                    'message' => 'Pretplate su poslane na MailChimp API. Rezultat importa možete preuzeti klikom na dugme.',
                    'downloadResultURL' => $result->response_body_url ?? null,
                    'listName' => 'subscribers/volonteri',
                    'status' => $result['status'],
                ]);
            }
        }

        return view('admin.newsletters.index', [
            'alertType' => 'danger',
            'message' => 'Greška prilikom exporta u MailChimp "subscribers" listu.',
            'listName' => 'subscribers/volonteri',
        ]);
    }

    public function osjeckoBaranjskaList()
    {
        config(['newsletter.apiKey' => config('newsletter.ss_apikey')]);
        config(['newsletter.defaultListName' => config('newsletter.defaultSSListName')]);

        $mailChimpApi = Newsletter::getApi();
        $batch = $mailChimpApi->new_batch();

        if (SubscriptionChunck::where('list_name', 'osjecko_baranjska')->count() > 0) {
            
            $savedBatch = SubscriptionBatch::where('list_name', 'osjecko_baranjska')->first();
            if ($savedBatch->count() > 0) {
                $mailChimpApi->new_batch($savedBatch->batch_id);
                $checkStatus = $batch->check_status();
                $resultIndex = array_search($savedBatch->batch_id, array_column($checkStatus['batches'], 'id'));
                if (count($checkStatus['batches']) > 0) {
                    $checkStatusResult = $checkStatus['batches'][$resultIndex];

                    $savedBatch->update([
                        'status' => $checkStatusResult['status'],
                        'total_operations' => $checkStatusResult['total_operations'],
                        'finished_operations' => $checkStatusResult['finished_operations'],
                        'errored_operations' => $checkStatusResult['errored_operations'],
                        'response_body_url' => $checkStatusResult['response_body_url'],
                        'completed_at' => Carbon::parse($checkStatusResult['completed_at'])->format('Y-m-d H:i:s'),
                    ]);
                }
            }
                            

            return view('admin.newsletters.index', [
                'alertType' => 'light',
                'message' => 'Pretplate su poslane na MailChimp API. Rezultat importa možete preuzeti klikom na dugme.',
                'downloadResultURL' => $savedBatch->response_body_url ?? null,
                'status' => $savedBatch->status ?? null,
                'listName' => 'brodsko posavska',
            ]);
        }

        $subscribers = [
            'kreso.putevimilosti@gmail.com',
            'ivana.maltasic1@gmail.com',
            'mirta.kovacevic@dkolektiv.hr',
            'jelena.stefanovic@dkolektiv.hr',
            'udruga.putevimilosti@gmail.com',
            'ivan.kristijan@kulturni-centar.hr',
            'udruga.4lista@gmail.com',
            'partwish.udruga@gmail.com',
            'jesenka.ricl@mso.hr',
            'info.rutha@gmail.com',
            'info@srk-zuti-sesir.hr',
            'melita.lulic@tehnika-osijek.hr',
            'vaga@vaga-zdravlje.hr',
            'jaka.osijek@gmail.com',
            'maja@ne-ovisnost.hr',
            'nktomislavlivana@outlook.com',
            'press@osijek1784.hr',
            'projekt.grad@os.t-com.hr',
            'jelena.rebic88@gmail.com',
            'viktorija.leo@gmail.com',
            'smavrak@gmail.com',
            'drustvo.mro.osijek@gmail.com',
            'oaza_bm@yahoo.com',
            'psihorehabilitacijski.centar@gmail.com',
            'savezrusina@gmail.com',
            'udruga.devet.zivota@gmail.com',
            'udruga.slijepih.pleternice@gmail.com',
            'savez-slovaka@os.t-com.hr',
            'udrugaplantaza@gmail.com',
            'djola.darda@gmail.com',
            'edvu@edvu.hr',
            'IvanPuljich955@gmail.com',
            'aktivni.umirovljenici@gmail.com',
            'julijana.tesija@evtos.hr',
            'icm.fora@gmail.com',
            'psihos.klub@gmail.com',
            'cwwppsummer@gmail.com',
            'zdenka@radiozupanja.hr',
            'upds.slavonka@os.t-com.hr',
            'ured@os-drftudjman-beli-manastir.skole.hr',
            'slavonski.hrast@os.t-com.hr',
            'zajednozazajednicu@hotmail.com',
            'pedagoginja@vrtic-nasice.hr',
            'sinisa.glavas@xnet.hr',
            'rameuzrame@net.hr',
            'ikjavor@gmail.com',
            'jadranka.kokolari@gmail.com',
            'tijana.gvozdenovic@gmail.com',
            'likovnaudrugacepin@gmail.com',
            'vladomarsonia@gmail.com',
            'dlan.osijek@gmail.com',
            'kbabic2373@gmail.com',
            'dms_osijek@sdmsh.hr',
            'laura.plantak@kultakt.hr',
            'baranja@inet.hr',
            'ceip.info@gmail.com',
            'udruga.mogu@gmail.com',
            'pobjede@gmail.com',
            'podrska.zrtvama.i.svjedocima@gmail.com',
            'zenska.udruga.izvor@gmail.com',
            'martina.cangajst@optinet.hr',
            'uir.vukovar@gmail.com',
            'uzposmr@gmail.com',
            'hils@hils.hr',
            'zeljko.kajtar2@os.t-com.hr',
            'ewob.croatia@gmail.com',
            'mirta.faktor@gmail.com',
            'alekrtinic@gmail.com',
            'sonja@medijacija.net',
            'veronikin-rubac@net.hr',
            'medijacija@medijacija.net',
            'mladi.protiv.gladi@gmail.com',
            'baranjski.leptirici@gmail.com',
            'udruga.umirovljenika.osijek@hi.ht.hr',
            'hkdizvor.dm@mail.inet.hr',
            'udruga.novidan@gmail.com',
            'babic.milena@gmail.com',
            'ugso.os@gmail.com',
            'dubravka.spancic@pakrac.hr',
            'sunce@os.t-com.hr',
            'kristina@cnzd.org',
        ];

        $chunckedSubscribers = array_chunk($subscribers, 500);
        $insertData = [];
        $listId = config('newsletter.lists.osjecko_baranjska.id');
        
        foreach ($chunckedSubscribers as $key => $subscribers) {
            foreach ($subscribers as $subscriber) {
                $batch->post("osjecko_baranjska_$key", "lists/$listId/members", [
                    'email_address' => $subscriber,
                    'status' => 'subscribed',
                ]);
                $insertData[] = [
                    'operation_id' => "osjecko_baranjska_$key",
                    'list_name' => 'osjecko_baranjska',
                    'email' => $subscriber,
                    'created_at' => now(),
                    'updated_at' => now(),
                ];
            }
        }
        
        if(SubscriptionChunck::insert($insertData)) {
        
            $result = $batch->execute();
            
            $created = SubscriptionBatch::create([
                'list_name' => 'osjecko_baranjska',
                'batch_id' => $result['id'],
                'status' => $result['status'],
                'total_operations' => $result['total_operations'],
                'finished_operations' => $result['finished_operations'],
                'errored_operations' => $result['errored_operations'],
                'submitted_at' => Carbon::parse($result['submitted_at'])->format('Y-m-d H:i:s'),
                'completed_at' => Carbon::parse($result['completed_at'])->format('Y-m-d H:i:s'),
                'response_body_url' => $result['response_body_url'],
            ]);

            if ($created) {
                return view('admin.newsletters.index', [
                    'alertType' => 'success',
                    'message' => 'Pretplate su poslane na MailChimp API. Rezultat importa možete preuzeti klikom na dugme.',
                    'downloadResultURL' => $result->response_body_url ?? null,
                    'listName' => 'osječko baranjska',
                    'status' => $result['status'],
                ]);
            }
        }

        return view('admin.newsletters.index', [
            'alertType' => 'danger',
            'message' => 'Greška prilikom exporta u MailChimp "osjecko-baranjska" listu.',
            'listName' => 'osječko baranjska',
        ]);
    }

    public function vukovarskoSrijemskaList()
    {
        config(['newsletter.apiKey' => config('newsletter.ss_apikey')]);
        config(['newsletter.defaultListName' => config('newsletter.defaultSSListName')]);

        $mailChimpApi = Newsletter::getApi();
        $batch = $mailChimpApi->new_batch();

        if (SubscriptionChunck::where('list_name', 'vukovarsko_srijemska')->count() > 0) {
            
            $savedBatch = SubscriptionBatch::where('list_name', 'vukovarsko_srijemska')->first();
            if ($savedBatch->count() > 0) {
                $mailChimpApi->new_batch($savedBatch->batch_id);
                $checkStatus = $batch->check_status();
                $resultIndex = array_search($savedBatch->batch_id, array_column($checkStatus['batches'], 'id'));
                if (count($checkStatus['batches']) > 0) {
                    $checkStatusResult = $checkStatus['batches'][$resultIndex];

                    $savedBatch->update([
                        'status' => $checkStatusResult['status'],
                        'total_operations' => $checkStatusResult['total_operations'],
                        'finished_operations' => $checkStatusResult['finished_operations'],
                        'errored_operations' => $checkStatusResult['errored_operations'],
                        'response_body_url' => $checkStatusResult['response_body_url'],
                        'completed_at' => Carbon::parse($checkStatusResult['completed_at'])->format('Y-m-d H:i:s'),
                    ]);
                }
            }                            

            return view('admin.newsletters.index', [
                'alertType' => 'light',
                'message' => 'Pretplate su poslane na MailChimp API. Rezultat importa možete preuzeti klikom na dugme.',
                'downloadResultURL' => $savedBatch->response_body_url ?? null,
                'status' => $savedBatch->status ?? null,
                'listName' => 'vukovarsko srijemska',
            ]);
        }

        $subscribers = [
            'kreso.putevimilosti@gmail.com',
            'tkosebojisutrajos@gmail.com',
            'ivana.maltasic1@gmail.com',
            'mirta.kovacevic@dkolektiv.hr',
            'jelena.stefanovic@dkolektiv.hr',
            'vladimir.knezevic7@gmail.com',
            'volonterskicentarvsz@gmail.com',
            'udruga.putevimilosti@gmail.com',
            'europski.dom.vukovar@gmail.com',
            'helena.crnjak@gmail.com',
            'dnd.vkci@gmail.com',
            'svetlanaostojic13@gmail.com',
            'info.rutha@gmail.com',
            'info@srk-zuti-sesir.hr',
            'jaka.osijek@gmail.com',
            'udrugadugavu@gmail.com',
            'viktorija.leo@gmail.com',
            'smavrak@gmail.com',
            'psihorehabilitacijski.centar@gmail.com',
            'savezrusina@gmail.com',
            'udruga.slijepih.pleternice@gmail.com',
            'savez-slovaka@os.t-com.hr',
            'kk.satir@gmail.com',
            'edvu@edvu.hr',
            'tina.dominkovic@gmail.com',
            'cwwppsummer@gmail.com',
            'zdenka@radiozupanja.hr',
            'golubica@uzosio-golubica.com',
            'dlan.osijek@gmail.com',
            'laura.plantak@kultakt.hr',
            'baranja@inet.hr',
            'podrska.zrtvama.i.svjedocima@gmail.com',
            'uir.vukovar@gmail.com',
            'uzposmr@gmail.com',
            'salter-vsz@proni.hr',
            'alekrtinic@gmail.com',
            'medijacija@medijacija.net',
            'mladi.protiv.gladi@gmail.com',
            'babic.milena@gmail.com',
            'daniela.cukelj@zsvu.pravosudje.hr',
            'sunce@os.t-com.hr',
        ];

        $chunckedSubscribers = array_chunk($subscribers, 500);
        $insertData = [];
        $listId = config('newsletter.lists.vukovarsko_srijemska.id');
        
        foreach ($chunckedSubscribers as $key => $subscribers) {
            foreach ($subscribers as $subscriber) {
                $batch->post("vukovarsko_srijemska_$key", "lists/$listId/members", [
                    'email_address' => $subscriber,
                    'status' => 'subscribed',
                ]);
                $insertData[] = [
                    'operation_id' => "vukovarsko_srijemska_$key",
                    'list_name' => 'vukovarsko_srijemska',
                    'email' => $subscriber,
                    'created_at' => now(),
                    'updated_at' => now(),
                ];
            }
        }
        
        if(SubscriptionChunck::insert($insertData)) {
        
            $result = $batch->execute();
            
            $created = SubscriptionBatch::create([
                'list_name' => 'vukovarsko_srijemska',
                'batch_id' => $result['id'],
                'status' => $result['status'],
                'total_operations' => $result['total_operations'],
                'finished_operations' => $result['finished_operations'],
                'errored_operations' => $result['errored_operations'],
                'submitted_at' => Carbon::parse($result['submitted_at'])->format('Y-m-d H:i:s'),
                'completed_at' => Carbon::parse($result['completed_at'])->format('Y-m-d H:i:s'),
                'response_body_url' => $result['response_body_url'],
            ]);
            if ($created) {
                return view('admin.newsletters.index', [
                    'alertType' => 'success',
                    'message' => 'Pretplate su poslane na MailChimp API. Rezultat importa možete preuzeti klikom na dugme.',
                    'downloadResultURL' => $result->response_body_url ?? null,
                    'listName' => 'vukovarsko srijemska',
                    'status' => $result['status'],
                ]);
            }
        }

        return view('admin.newsletters.index', [
            'alertType' => 'danger',
            'message' => 'Greška prilikom exporta u MailChimp "vukovarsko-srijemska" listu.',
            'listName' => 'vukovarsko srijemska',
        ]);
    }

    public function pozeskoSlavonskaList()
    {
        config(['newsletter.apiKey' => config('newsletter.ss_apikey')]);
        config(['newsletter.defaultListName' => config('newsletter.defaultSSListName')]);

        $mailChimpApi = Newsletter::getApi();
        $batch = $mailChimpApi->new_batch();

        if (SubscriptionChunck::where('list_name', 'pozesko_slavonska')->count() > 0) {
            
            $savedBatch = SubscriptionBatch::where('list_name', 'pozesko_slavonska')->first();
            if ($savedBatch->count() > 0) {
                $mailChimpApi->new_batch($savedBatch->batch_id);
                $checkStatus = $batch->check_status();
                $resultIndex = array_search($savedBatch->batch_id, array_column($checkStatus['batches'], 'id'));
                if (count($checkStatus['batches']) > 0) {
                    $checkStatusResult = $checkStatus['batches'][$resultIndex];

                    $savedBatch->update([
                        'status' => $checkStatusResult['status'],
                        'total_operations' => $checkStatusResult['total_operations'],
                        'finished_operations' => $checkStatusResult['finished_operations'],
                        'errored_operations' => $checkStatusResult['errored_operations'],
                        'response_body_url' => $checkStatusResult['response_body_url'],
                        'completed_at' => Carbon::parse($checkStatusResult['completed_at'])->format('Y-m-d H:i:s'),
                    ]);
                }
            }
                            

            return view('admin.newsletters.index', [
                'alertType' => 'light',
                'message' => 'Pretplate su poslane na MailChimp API. Rezultat importa možete preuzeti klikom na dugme.',
                'downloadResultURL' => $savedBatch->response_body_url ?? null,
                'status' => $savedBatch->status ?? null,
                'listName' => 'požeško slavonska',
            ]);
        }

        $subscribers = [
            'mirta.kovacevic@dkolektiv.hr',
            'jelena.stefanovic@dkolektiv.hr	',
            'jaka.osijek@gmail.com',
            'radost.pleternica@gmail.com',
            'ekocentarlatinovac@gmail.com',
            'kutjevackaudrugamladih@gmail.com',
            'delfin.zamir@gmail.com',
            'cwwppsummer@gmail.com',
            'vladomarsonia@gmail.com',
            'dlan.osijek@gmail.com',
            'laura.plantak@kultakt.hr',
            'baranja@inet.hr',
            'maja.jankovi@gmail.com',
            'uir.vukovar@gmail.com',
            'uzposmr@gmail.com',
            'udruga.slijepih.pakrac.lipik@gmail.com',
            'udrugaosmijeh1@gmail.com',
            'medijacija@medijacija.net',
            'mladi.protiv.gladi@gmail.com',
            'babic.milena@gmail.com',
            'dubravka.spancic@pakrac.hr',
            'info@oppidum.hr',
        ];

        $chunckedSubscribers = array_chunk($subscribers, 500);
        $insertData = [];
        $listId = config('newsletter.lists.pozesko_slavonska.id');
        
        foreach ($chunckedSubscribers as $key => $subscribers) {
            foreach ($subscribers as $subscriber) {
                $batch->post("pozesko_slavonska_$key", "lists/$listId/members", [
                    'email_address' => $subscriber,
                    'status' => 'subscribed',
                ]);
                $insertData[] = [
                    'operation_id' => "pozesko_slavonska_$key",
                    'list_name' => 'pozesko_slavonska',
                    'email' => $subscriber,
                    'created_at' => now(),
                    'updated_at' => now(),
                ];
            }
        }
        
        if(SubscriptionChunck::insert($insertData)) {
        
            $result = $batch->execute();
            
            $created = SubscriptionBatch::create([
                'list_name' => 'pozesko_slavonska',
                'batch_id' => $result['id'],
                'status' => $result['status'],
                'total_operations' => $result['total_operations'],
                'finished_operations' => $result['finished_operations'],
                'errored_operations' => $result['errored_operations'],
                'submitted_at' => Carbon::parse($result['submitted_at'])->format('Y-m-d H:i:s'),
                'completed_at' => Carbon::parse($result['completed_at'])->format('Y-m-d H:i:s'),
                'response_body_url' => $result['response_body_url'],
            ]);
            if ($created) {
                return view('admin.newsletters.index', [
                    'alertType' => 'success',
                    'message' => 'Pretplate su poslane na MailChimp API. Rezultat importa možete preuzeti klikom na dugme.',
                    'downloadResultURL' => $result->response_body_url ?? null,
                    'listName' => 'požeško slavonska',
                    'status' => $result['status'],
                ]);
            }
        }

        return view('admin.newsletters.index', [
            'alertType' => 'danger',
            'message' => 'Greška prilikom exporta u MailChimp "pozesko-slavonska" listu.',
            'listName' => 'požeško slavonska',
        ]);
    }

    public function brodskoPosavskaList()
    {
        config(['newsletter.apiKey' => config('newsletter.ss_apikey')]);
        config(['newsletter.defaultListName' => config('newsletter.defaultSSListName')]);

        $mailChimpApi = Newsletter::getApi();
        $batch = $mailChimpApi->new_batch();

        if (SubscriptionChunck::where('list_name', 'brodsko_posavska')->count() > 0) {
            
            $savedBatch = SubscriptionBatch::where('list_name', 'brodsko_posavska')->first();
            if ($savedBatch->count() > 0) {
                $mailChimpApi->new_batch($savedBatch->batch_id);
                $checkStatus = $batch->check_status();
                $resultIndex = array_search($savedBatch->batch_id, array_column($checkStatus['batches'], 'id'));
                if (count($checkStatus['batches']) > 0) {
                    $checkStatusResult = $checkStatus['batches'][$resultIndex];

                    $savedBatch->update([
                        'status' => $checkStatusResult['status'],
                        'total_operations' => $checkStatusResult['total_operations'],
                        'finished_operations' => $checkStatusResult['finished_operations'],
                        'errored_operations' => $checkStatusResult['errored_operations'],
                        'response_body_url' => $checkStatusResult['response_body_url'],
                        'completed_at' => Carbon::parse($checkStatusResult['completed_at'])->format('Y-m-d H:i:s'),
                    ]);
                }
            }
                            

            return view('admin.newsletters.index', [
                'alertType' => 'light',
                'message' => 'Pretplate su poslane na MailChimp API. Rezultat importa možete preuzeti klikom na dugme.',
                'downloadResultURL' => $savedBatch->response_body_url ?? null,
                'status' => $savedBatch->status ?? null,
                'listName' => 'brodsko posavska',
            ]);
        }

        $subscribers = [
            'mirta.kovacevic@dkolektiv.hr',
            'jelena.stefanovic@dkolektiv.hr',
            'udruga.putevimilosti@gmail.com',
            'info.rutha@gmail.com',
            'jaka.osijek@gmail.com',
            'info@mi-udruga.hr',
            'udrugavivak@gmail.com',
            'udruga.loco.moto@gmail.com',
            'udruga.prevencija@gmail.com',
            'smavrak@gmail.com',
            'psihorehabilitacijski.centar@gmail.com',
            'udruga.slijepih.pleternice@gmail.com',
            'snsibinj@gmail.com',
            'udruga-slijepih-bpz@sb.t-com.hr',
            'eko.brezna.info@gmail.com',
            'ngbuntovnici@ngbuntovnici.hr',
            'tanja.cupicstevic@gmail.com',
            'ured@os-sradic-oprisavci.skole.hr',
            'cwwppsummer@gmail.com',
            'kresimir.dokuzovic@sb.t-com.hr',
            'bzspuz@gmail.com',
            'info@ipc.com.hr',
            'vladomarsonia@gmail.com',
            'dlan.osijek@gmail.com',
            'baranja@inet.hr',
            'uir.vukovar@gmail.com',
            'uzposmr@gmail.com',
            'bebrinska.udruga.mladih@gmail.com',
            'iris@bed.hr',
            'udrugaomrsb@gmail.com',
            'vandric@bpz.hr',
            'medijacija@medijacija.net',
            'mladi.protiv.gladi@gmail.com',
            'udruga.vrapcici@gmail.com',
            'babic.milena@gmail.com',
            'info@oppidum.hr',
        ];

        $chunckedSubscribers = array_chunk($subscribers, 500);
        $insertData = [];
        $listId = config('newsletter.lists.brodsko_posavska.id');
        
        foreach ($chunckedSubscribers as $key => $subscribers) {
            foreach ($subscribers as $subscriber) {
                $batch->post("brodsko_posavska_$key", "lists/$listId/members", [
                    'email_address' => $subscriber,
                    'status' => 'subscribed',
                ]);
                $insertData[] = [
                    'operation_id' => "brodsko_posavska_$key",
                    'list_name' => 'brodsko_posavska',
                    'email' => $subscriber,
                    'created_at' => now(),
                    'updated_at' => now(),
                ];
            }
        }
        
        if(SubscriptionChunck::insert($insertData)) {
        
            $result = $batch->execute();
            
            $created = SubscriptionBatch::create([
                'list_name' => 'brodsko_posavska',
                'batch_id' => $result['id'],
                'status' => $result['status'],
                'total_operations' => $result['total_operations'],
                'finished_operations' => $result['finished_operations'],
                'errored_operations' => $result['errored_operations'],
                'submitted_at' => Carbon::parse($result['submitted_at'])->format('Y-m-d H:i:s'),
                'completed_at' => Carbon::parse($result['completed_at'])->format('Y-m-d H:i:s'),
                'response_body_url' => $result['response_body_url'],
            ]);
            if ($created) {
                return view('admin.newsletters.index', [
                    'alertType' => 'success',
                    'message' => 'Pretplate su poslane na MailChimp API. Rezultat importa možete preuzeti klikom na dugme.',
                    'downloadResultURL' => $result->response_body_url ?? null,
                    'listName' => 'brodsko posavska',
                    'status' => $result['status'],
                ]);
            }
        }

        return view('admin.newsletters.index', [
            'alertType' => 'danger',
            'message' => 'Greška prilikom exporta u MailChimp "brodsko-posavska" listu.',
            'listName' => 'brodsko posavska',
        ]);
    }

    public function drugeZupanijeList()
    {
        config(['newsletter.apiKey' => config('newsletter.ss_apikey')]);
        config(['newsletter.defaultListName' => config('newsletter.defaultSSListName')]);

        $mailChimpApi = Newsletter::getApi();
        $batch = $mailChimpApi->new_batch();

        if (SubscriptionChunck::where('list_name', 'druge_zupanije')->count() > 0) {
            
            $savedBatch = SubscriptionBatch::where('list_name', 'druge_zupanije')->first();
            if ($savedBatch->count() > 0) {
                $mailChimpApi->new_batch($savedBatch->batch_id);
                $checkStatus = $batch->check_status();
                $resultIndex = array_search($savedBatch->batch_id, array_column($checkStatus['batches'], 'id'));
                if (count($checkStatus['batches']) > 0) {
                    $checkStatusResult = $checkStatus['batches'][$resultIndex];

                    $savedBatch->update([
                        'status' => $checkStatusResult['status'],
                        'total_operations' => $checkStatusResult['total_operations'],
                        'finished_operations' => $checkStatusResult['finished_operations'],
                        'errored_operations' => $checkStatusResult['errored_operations'],
                        'response_body_url' => $checkStatusResult['response_body_url'],
                        'completed_at' => Carbon::parse($checkStatusResult['completed_at'])->format('Y-m-d H:i:s'),
                    ]);
                }                    
            }
                            

            return view('admin.newsletters.index', [
                'alertType' => 'light',
                'message' => 'Pretplate su poslane na MailChimp API. Rezultat importa možete preuzeti klikom na dugme.',
                'downloadResultURL' => $savedBatch->response_body_url ?? null,
                'status' => $savedBatch->status ?? null,
                'listName' => 'druge županije',
            ]);
        }

        $subscribers = [
            'krasomir.mioc@gmail.com'
        ];

        $chunckedSubscribers = array_chunk($subscribers, 500);
        $insertData = [];
        $listId = config('newsletter.lists.druge_zupanije.id');
        
        foreach ($chunckedSubscribers as $key => $subscribers) {
            foreach ($subscribers as $subscriber) {
                $batch->post("druge_zupanije_$key", "lists/$listId/members", [
                    'email_address' => $subscriber,
                    'status' => 'subscribed',
                ]);
                $insertData[] = [
                    'operation_id' => "druge_zupanije_$key",
                    'list_name' => 'druge_zupanije',
                    'email' => $subscriber,
                    'created_at' => now(),
                    'updated_at' => now(),
                ];
            }
        }
        
        if(SubscriptionChunck::insert($insertData)) {
        
            $result = $batch->execute();
            
            $created = SubscriptionBatch::create([
                'list_name' => config('newsletter.defaultListName'),
                'batch_id' => $result['id'],
                'status' => $result['status'],
                'total_operations' => $result['total_operations'],
                'finished_operations' => $result['finished_operations'],
                'errored_operations' => $result['errored_operations'],
                'submitted_at' => Carbon::parse($result['submitted_at'])->format('Y-m-d H:i:s'),
                'completed_at' => Carbon::parse($result['completed_at'])->format('Y-m-d H:i:s'),
                'response_body_url' => $result['response_body_url'],
            ]);
            if ($created) {
                return view('admin.newsletters.index', [
                    'alertType' => 'success',
                    'message' => 'Pretplate su poslane na MailChimp API.',
                    'downloadResultURL' => $result->response_body_url ?? null,
                    'listName' => 'druge županije',
                    'status' => $result['status'],
                ]);
            }
        }

        return view('admin.newsletters.index', [
            'alertType' => 'danger',
            'message' => 'Greška prilikom exporta u MailChimp "druge-zupanije" listu.',
            'listName' => 'druge županije',
        ]);
    }
}
