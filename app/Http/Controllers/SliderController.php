<?php

namespace App\Http\Controllers;

use App\Http\Requests\SliderDeleteRequest;
use App\Http\Requests\SliderStoreRequest;
use App\Http\Requests\SliderUpdatePositionRequest;
use App\Http\Requests\SliderUpdateRequest;
use App\Slider;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Yajra\DataTables\Facades\DataTables;

class SliderController extends Controller
{
    /**
     * Undocumented function
     *
     * @return View
     */
    public function index()
    {
        return view('admin.sliders.index');
    }

    /**
     * Undocumented function
     *
     * @param Request $request
     * @return DataTables
     */
    public function list(Request $request)
    {
        if ($request->ajax()) {
            $sliders = Slider::latest()->get();
            return DataTables::of($sliders)
                ->addIndexColumn()                
                ->addColumn('action', function($row){
                    $actionBtn = '
                        <a href="'. route('sliders.edit', [$row, 'lang' => app()->getLocale()]) .'" class="edit btn btn-success btn-sm">Uredi</a> 
                        <a href="#" 
                            data-toggle="modal" 
                            data-target="#confirmDelete" 
                            data-title="Brisanje slidera"
                            data-model-id="'. $row->id .'"
                            data-message="Da li zaista želiš obrisati slider - '. $row->title .'?"
                            data-delete-route="'. route('sliders.destroy', [$row, 'lang' => app()->getLocale()]) .'"
                            class="delete btn btn-danger btn-sm"
                        >
                            Obriši
                        </a>
                    ';
                    return $actionBtn;
                })
                ->editColumn('image_name', function($row) {
                    if (!$row->image_name || Storage::missing('public/images/slider/' . $row->image_name)) {
                        return null;
                    }
                    $img = '<img src="../..'. Storage::url('images/slider/' . $row->image_name) .'" height="30" />';
                    
                    return $img;
                })
                ->addColumn('position', function($row) {
                    return $row->position;
                })
                ->addColumn('link_to_page', function($row) {
                    return $row->link_to_page .'<br>'. $row->custom_link;
                })
                ->setRowId(function($row) {
                    return $row->id;
                })
                ->rawColumns(['action', 'position', 'image_name', 'link_to_page'])
                ->make(true);
        }
    }

    /**
     * Undocumented function
     *
     * @return View
     */
    public function create()
    {
        return view('admin.sliders.create', [
            'routes' => Slider::$availablePageUrls,
        ]);
    }

    /**
     * Undocumented function
     *
     * @param SliderStoreRequest $request
     * @return Redirect
     */
    public function store(SliderStoreRequest $request)
    {
        $saved = Slider::store($request);
        if ($saved) {
            return redirect()->route('sliders.index', app()->getLocale())->withSuccess('Uspješno spremljen slider.');
        }
        return redirect()->route('sliders.index', app()->getLocale())->withErrors('Greška prilikom spremanja slidera. ' . $saved);
    }

    /**
     * Undocumented function
     *
     * @param String $lang
     * @param Slider $slider
     * @return View
     */
    public function edit(String $lang, Slider $slider)
    {
        return view('admin.sliders.edit', [
            'slider' => $slider,
            'routes' => Slider::$availablePageUrls,
        ]);
    }

    /**
     * Undocumented function
     *
     * @param Request $request
     * @param String $lang
     * @param Slider $slider
     * @return Redirect
     */
    public function update(SliderUpdateRequest $request, String $lang, Slider $slider)
    {
        $updated = Slider::updateSlider($request, $slider);
        if ($updated) {
            return redirect()->route('sliders.index', app()->getLocale())->withSuccess('Uspješno spremljena promjena.');
        }
        return redirect()->route('sliders.index', app()->getLocale())->withErrors('Greška prilikom spremanja promjene. ' . $updated);
    }

    /**
     * Undocumented function
     *
     * @param Request $request
     * @param String $lang
     * @param Slider $slider
     * @return Redirect
     */
    public function destroy(SliderDeleteRequest $request, String $lang, Slider $slider)
    {
        $deleted = Slider::deleteSlider($slider);
        if ($deleted) {
            return redirect()->route('sliders.index', app()->getLocale())->withSuccess('Uspješno obrisan slider.');
        }
        return redirect()->route('sliders.index', app()->getLocale())->withErrors('Greška prilikom brisanja slidera.');
    }

    /**
     * Undocumented function
     *
     * @param SliderUpdatePositionRequest $request
     * @return Response
     */
    public function updateSliderPosition(SliderUpdatePositionRequest $request)
    {
        $slider = Slider::find($request->sliderId);
        $slider->position = $request->position;

        if ($slider->update()) {
            return response()->json([
                'error' => false,
                'message' => 'Uspješno spremljena pozicija.'
            ], 200);
        }
        return response()->json([
            'error' => true,
            'message' => 'Greška prilikom spremanja pozicije.'
        ], 500);
    }
}
