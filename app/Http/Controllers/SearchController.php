<?php

namespace App\Http\Controllers;

use App\Http\Requests\SearchRequest;
use App\Services\SearchService;

class SearchController extends Controller
{
    public function __construct(private readonly SearchService $searchService)
    {
    }

    public function __invoke(SearchRequest $request)
    {
        return view('pages.search.index', [
            'searchResults' => $this->searchService->search($request->validated('search_term')),
            'searchTerm' => $request->validated('search_term'),
        ]);
    }
}
