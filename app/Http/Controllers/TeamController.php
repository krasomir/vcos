<?php

namespace App\Http\Controllers;

use App\Http\Requests\TeamDeleteRequest;
use App\Http\Requests\TeamStoreRequest;
use App\Http\Requests\TeamUpdateRequest;
use App\Team;
use DataTables;
use Illuminate\Http\Request;

class TeamController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        return view('admin.teams.index', [
            'teams' => Team::all()
        ]);
    }

    public function teamsList(Request $request)
    {
        if ($request->ajax()) {

            $teams = Team::latest()->get();

            return DataTables::of($teams)
                ->addIndexColumn()
                ->addColumn('action', function($row){
                    return '
                        <a href="'. route('teams.edit', [$row, 'lang' => app()->getLocale()]) .'"
                            class="edit btn btn-success btn-sm"
                            >
                            Uredi
                        </a>
                        <a href="#"
                            data-toggle="modal"
                            data-target="#confirmDelete"
                            data-title="Brisanje člana tima"
                            data-model-id="'. $row->id .'"
                            data-message="Da li zaista želiš obrisati člana tima - '. $row->first_name . ' ' . $row->last_name .'?"
                            data-delete-route="'. route('teams.destroy', [$row, 'lang' => app()->getLocale()]) .'"
                            class="delete btn btn-danger btn-sm"
                            >
                            Obriši
                        </a>
                    ';
                })
                ->editColumn('created_at', function($row) {
                    return $row->created_at->format('d.m.Y');
                })
                ->rawColumns(['action'])
                ->make(true);
        }
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        return view('admin.teams.create');
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(TeamStoreRequest $request)
    {
        $saved = Team::store($request);
        if ($saved) {
            return redirect()->route('teams.index', app()->getLocale())->withSuccess('Uspješno spremljen član tima.');
        }
        return redirect()->route('teams.index', app()->getLocale())->withErrors('Greška prilikom spremanja člana tima.');
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $lang, Team $team)
    {
        return view('admin.teams.edit', [
            'team' => $team
        ]);
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(TeamUpdateRequest $request, string $lang, Team $team)
    {
        $updated = Team::updateTeam($request, $team);
        if ($updated) {
            return redirect()->route('teams.index', app()->getLocale())->withSuccess('Uspješno spremljen član tima.');
        }
        return redirect()->route('teams.index', app()->getLocale())->withErrors('Greška prilikom spremanja člana tima.');
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(TeamDeleteRequest $request, string $lang, Team $team)
    {
        $deleted = Team::deleteTeam($team);
        if ($deleted) {
            return redirect()->route('teams.index', app()->getLocale())->withSuccess('Uspješno spremljen član tima.');
        }
        return redirect()->route('teams.index', app()->getLocale())->withErrors('Greška prilikom spremanja člana tima.');
    }

    public function ourTeam()
    {
        return view('pages.our-team', [
            'teams' => Team::all()
        ]);
    }
}
