<?php

namespace App\Http\Controllers;

use App\Program;
use Illuminate\Http\Request;

class ProgramPagesController extends Controller
{
    /**
     * Undocumented function
     *
     * @return View
     */
    public function vcos()
    {
        return view('pages.vcos', [
            'program' => $this->getProgramForPage('vcos'),
        ]);
    }

    /**
     * Undocumented function
     *
     * @return View
     */
    public function kd()
    {
        return view('pages.kd', [
            'program' => $this->getProgramForPage('kd'),
        ]);
    }

    /**
     * Undocumented function
     *
     * @return View
     */
    public function unv()
    {
        return view('pages.unv', [
            'program' => $this->getProgramForPage('unv'),
        ]);
    }

    /**
     * Undocumented function
     *
     * @return View
     */
    public function ess()
    {
        return view('pages.ess', [
            'program' => $this->getProgramForPage('aess'),
        ]);
    }

    /**
     * Undocumented function
     *
     * @return View
     */
    public function socialAtelier()
    {
        return view('pages.social_atelier', [
            'program' => $this->getProgramForPage('social_atelier'),
        ]);
    }

    /**
     * Undocumented function
     *
     * @return View
     */
    public function strengthOfParticipation()
    {
        return view('pages.strength-of-participation', [
            'program' => $this->getProgramForPage('rcd'),
        ]);
    }

    /**
     * Undocumented function
     *
     * @return View
     */
    public function osijekToGoo()
    {
        return view('pages.osijek-to-goo', [
            'program' => $this->getProgramForPage('otgoo'),
        ]);
    }

    /**
     * Undocumented function
     *
     * @param String $page
     * @return Program
     */
    public function getProgramForPage(String $page)
    {
        return Program::where('program_page', $page)->first();
    }
}
