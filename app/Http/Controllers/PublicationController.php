<?php

namespace App\Http\Controllers;

use App\Enums\PublicationCategoriesEnum;
use App\Http\Requests\PublicationDeleteRequest;
use App\Http\Requests\PublicationStoreRequest;
use App\Http\Requests\PublicationUpdateRequest;
use App\Program;
use App\Publication;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Yajra\DataTables\Facades\DataTables;

class PublicationController extends Controller
{
    /**
     * Undocumented function
     *
     * @return View
     */
    public function frontPublications()
    {
        $publications = Publication::where('is_demo_academy', 0)->latest()->get();
        $professionalManualsPublications = [];
        $researchAndAnalysisPublications = [];

        foreach ($publications as $publication) {
            if ($publication->publication_category === PublicationCategoriesEnum::PROFESSIONAL_MANUALS->value) {
                $professionalManualsPublications[] = $publication;
            }
            if ($publication->publication_category === PublicationCategoriesEnum::RESEARCH_AND_ANALYSIS->value) {
                $researchAndAnalysisPublications[] = $publication;
            }
        }

        return view('pages.publications', [
            'professionalManualsPublications' => $professionalManualsPublications,
            'researchAndAnalysisPublications' => $researchAndAnalysisPublications,
        ]);
    }

    /**
     * Undocumented function
     *
     * @return View
     */
    public function demoAcademy()
    {
        return view('pages.demo-academy', [
            'publications' => Publication::where('is_demo_academy', 1)->latest()->get(),
            'program' => Program::where('program_page', 'demo_academy')->first(),
        ]);
    }

    /**
     * Undocumented function
     *
     * @return View
     */
    public function index()
    {
        return view('admin.publications.index');
    }

    /**
     * Undocumented function
     *
     * @param Request $request
     * @return DataTables
     * @throws \Exception
     */
    public function list(Request $request)
    {
        if ($request->ajax()) {
            $publications = Publication::latest()->get();
            return DataTables::of($publications)
                ->addIndexColumn()
                ->addColumn('action', function($row){
                    $actionBtn = '
                        <a href="'. route('publications.edit', [$row, 'lang' => app()->getLocale()]) .'" class="edit btn btn-success btn-sm">Uredi</a>
                        <a href="#"
                            data-toggle="modal"
                            data-target="#confirmDelete"
                            data-title="Brisanje publikacije"
                            data-model-id="'. $row->id .'"
                            data-message="Da li zaista želiš obrisati publikaciju - '. htmlentities($row->title) .'?"
                            data-delete-route="'. route('publications.destroy', [$row, 'lang' => app()->getLocale()]) .'"
                            class="delete btn btn-danger btn-sm"
                        >
                            Obriši
                        </a>
                    ';
                    return $actionBtn;
                })
                ->editColumn('image_name', function($row) {
                    if((int)$row->is_demo_academy === 1) {
                        $demoAcademy = '(Demo Akademija)';
                        $imagePath = Storage::url('images/publications/demo_academy/' . $row->image_name);
                        $isImageMissing = Storage::missing('public/images/publications/demo_academy/' . $row->image_name);
                    } else {
                        $demoAcademy = '';
                        $imagePath = Storage::url('images/publications/' . $row->image_name);
                        $isImageMissing = Storage::missing('public/images/publications/' . $row->image_name);
                    }
                    if (!$row->image_name || $isImageMissing) {
                        return null;
                    }

                    return [
                        'image' => $imagePath,
                        'demo_academy' => $demoAcademy,
                    ];
                })
                ->editColumn('publication_category', function($row) {
                    return __($row->publication_category);
                })
                ->rawColumns(['action'])
                ->make(true);
        }
    }

    /**
     * Undocumented function
     *
     * @return View
     */
    public function create()
    {
        return view('admin.publications.create');
    }

    /**
     * Undocumented function
     *
     * @param PublicationStoreRequest $request
     * @return Redirect
     */
    public function store(PublicationStoreRequest $request)
    {
        $saved = Publication::store($request);
        if ($saved) {
            return redirect()->route('publications.index', app()->getLocale())->withSuccess('Uspješno spremljena publikacija.');
        }
        return redirect()->route('publications.index', app()->getLocale())->withErrors('Greška prilikom spremanja publikacije.');
    }

    /**
     * Undocumented function
     *
     * @param String $lang
     * @param Publication $publication
     * @return View
     */
    public function edit(String $lang, Publication $publication)
    {
        return view('admin.publications.edit', [
            'publication' => $publication,
        ]);
    }

    /**
     * Undocumented function
     *
     * @param PublicationUpdateRequest $request
     * @param String $lang
     * @param Publication $publication
     * @return Redirect
     */
    public function update(PublicationUpdateRequest $request, String $lang, Publication $publication)
    {
        $updated = Publication::updatePublication($request, $publication);
        if ($updated) {
            return redirect()->route('publications.index', app()->getLocale())->withSuccess('Uspješno spremljeno.');
        }
        return redirect()->route('publications.index', app()->getLocale())->withErrors('Greška prilikom spremanja.');
    }

    /**
     * Undocumented function
     *
     * @param PublicationDeleteRequest $request
     * @param String $lang
     * @param Publication $publication
     * @return Redirect
     */
    public function destroy(PublicationDeleteRequest $request, String $lang, Publication $publication)
    {
        $deleted = Publication::deletePublication($publication);
        if ($deleted) {
            return  redirect()->route('publications.index', app()->getLocale())->withSuccess('Uspješno obrisano.');
        }
        return redirect()->route('publications.index', app()->getLocale())->withErrors('Greška prilikomm brisanja.');
    }
}
