<?php

namespace App\Http\Controllers;

use PDF;
use Illuminate\Http\Request;
use App\Mail\UserRegisterEmail;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;
use Yajra\DataTables\Facades\DataTables;
use App\VolunteerOrganisationStrengthOfParticipation;
use App\Services\SendConfirmationNewsletterEmailService;
use App\Http\Requests\StrengthOfParticipationStoreRequest;
use App\Http\Requests\StrengthOfParticipationStepOneRequest;
use App\Http\Requests\StrengthOfParticipationStepTwoRequest;
use App\Http\Requests\OrganisationStrengthOfParticipationDeleteRequest;

class VolunteerOrganisationStrengthOfParticipationController extends Controller
{
    /**
     * Undocumented function
     *
     * @return view
     */
    public function index()
    {
        return view('admin.organisation_strength_of_participations.index');
    }

    /**
     * Undocumented function
     *
     * @param Request $request
     * @return DataTables
     */
    public function volunteersList(Request $request)
    {
        if ($request->ajax()) {
            $data = VolunteerOrganisationStrengthOfParticipation::latest()->get();
            return DataTables::of($data)
                ->addIndexColumn()
                ->addColumn('action', function($row){
                    $actionBtn = '
                        <a href="'. route('organisation-strength-of-participations-show', [$row, 'lang' => app()->getLocale()]) .'" class="edit btn btn-info btn-sm">Detalji</a> 
                        <a href="#" 
                            data-toggle="modal" 
                            data-target="#confirmDelete" 
                            data-title="Brisanje organizacije Snaga sudjelovanja"
                            data-model-id="'. $row->id .'"
                            data-message="Da li zaista želiš obrisati organizaciju - '. $row->organisation_name .'?"
                            data-delete-route="'. route('organisation-strength-of-participations-delete', [$row, 'lang' => app()->getLocale()]) .'"
                            class="delete btn btn-danger btn-sm"
                        >
                            Obriši
                        </a>
                    ';
                    return $actionBtn;
                })
                ->addColumn('organisation_name', function($row) {
                    $name = $row->organisation_name;
                    $newsIcon = ((int)$row->newsletter === 1) ? ' <i class="float-right mt-2 fab fa-mailchimp"></i>' : '';

                    return $name . $newsIcon;
                })
                ->rawColumns(['action', 'organisation_name'])
                ->make(true);
        }
    }

    /**
     * Undocumented function
     *
     * @param Request $request
     * @param [type] $lang
     * @param VolunteerOrganisationStrengthOfParticipation $organisation
     * @return view
     */
    public function show(Request $request, $lang, VolunteerOrganisationStrengthOfParticipation $organisation)
    {
        return view('admin.organisation_strength_of_participations.show', [
            'organisation' => $organisation,
        ]);
    }

    /**
     * Undocumented function
     *
     * @param Request $request
     * @param String $lang
     * @param VolunteerOrganisationStrengthOfParticipation $organisation
     * @return PDF
     */
    public function organisationSopPdf(Request $request, String $lang, VolunteerOrganisationStrengthOfParticipation $organisation)
    {
        view()->share('organisation', $organisation);
        $pdf = PDF::loadView('admin.pdf.volunteer-organisation-sop', $organisation);

        return $pdf->stream();
    }

    /**
     * Undocumented function
     *
     * @param Request $request
     * @param [type] $lang
     * @param VolunteerOrganisationStrengthOfParticipation $organisation
     * @return redirect
     */
    public function destroy(OrganisationStrengthOfParticipationDeleteRequest $request, $lang, VolunteerOrganisationStrengthOfParticipation $organisation)
    {
        $organisation->delete();

        return redirect()->route('organisation-strength-of-participations', app()->getLocale())->withSuccess('Organizacija je uspješno obrisana.');
    }

    /**
     * Undocumented function
     *
     * @param Request $request
     * @return View
     */
    public function showApplication(Request $request)
    {
        return view('pages.strength-of-participation-application', [
            'validation_step_routes' => [
                'step_1' => route('strength_of_participation_validate_step_one', app()->getLocale()),
                'step_2' => route('strength_of_participation_validate_step_two', app()->getLocale()),
            ],
            'organisation' => session('organisation'),
            'acticvity_areas' => VolunteerOrganisationStrengthOfParticipation::$activity_areas,
            'geo_areas' => VolunteerOrganisationStrengthOfParticipation::$geo_areas,
        ]);
    }

    public function validationStepOne(StrengthOfParticipationStepOneRequest $request): JsonResponse
    {
        if ($request->validated()) {
            return response()->json([
                'errors' => false,
            ], 200);
        }
    }

    public function validationStepTwo(StrengthOfParticipationStepTwoRequest $request): JsonResponse
    {
        if ($request->validated()) {
            return response()->json([
                'errors' => false,
            ], 200);
        }
    }

    public function store(StrengthOfParticipationStoreRequest $request): JsonResponse
    {
        $organisation = VolunteerOrganisationStrengthOfParticipation::create($request->except(['_token']));

        if ($organisation) {

            Mail::to(config('app.organisation_strength_of_participation_to_address'))
                    ->send(new UserRegisterEmail($organisation, 'organisation_strength_of_participation'));
                    
            if ($request->newsletter == 1) {
                (new SendConfirmationNewsletterEmailService($organisation, $organisation->email, get_class($organisation)))->send();                
            }
            session()->forget('organisation');

            return response()->json([
                'errors' => false,
                'message' => trans('Podaci su uspješno spremljeni.'),
            ], 200);
        }
        return response()->json([
            'errors' => true,
            'message' => trans('Došlo je do greške prilikom spremanja. Podaci nisu spremljeni.'),
        ], 500);                            
    }
}
