<?php

namespace App\Http\Controllers;

use App\FailedSubscription;
use App\Services\GeoAreasToNewsletterListService;
use Illuminate\Http\Request;
use Spatie\Newsletter\NewsletterFacade as Newsletter;
use Yajra\DataTables\Facades\DataTables;

class FailedSubscriptionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.failed_subscriptions.index');
    }

    public function list(Request $request)
    {
        if ($request->ajax()) {
            
            $failedSubscriptions = FailedSubscription::latest()->get();

            return DataTables::of($failedSubscriptions)
                ->addIndexColumn()
                ->addColumn('action', function($row){
                    $actionBtn = '
                        <a href="#"
                            id="subscription_retry_'. $row->id .'"
                            class="subscription-retry btn btn-success btn-sm"
                            data-id="'. $row->id .'"
                            data-retry-route="'. route('failed-subscriptions-retry', [$row, 'lang' => app()->getLocale()]) .'"
                        >
                            <span class="button">Pretplati</span>
                            <span class="spinner d-none">
                                <i class="fas fa-spinner fa-spin"></i>
                            </span>
                        </a> 
                        <a href="#" 
                            data-toggle="modal" 
                            data-target="#confirmDelete" 
                            data-title="Brisanje neuspjele pretplate"
                            data-model-id="'. $row->id .'"
                            data-message="Da li zaista želiš obrisati neuspjelu pretplatu za - '. $row->email .'?"
                            data-delete-route="'. route('failed-subscriptions.destroy', [$row, 'lang' => app()->getLocale()]) .'"
                            class="delete btn btn-danger btn-sm"
                        >
                            Obriši
                        </a>
                    ';
                    return $actionBtn;
                })
                ->addColumn('names', function($row) {
                    $names = unserialize($row->names);
                    return $names['FNAME'] .' '. $names['LNAME'];
                })
                ->setRowId(function($row) {
                    return $row->id;
                })
                ->rawColumns(['action'])
                ->make(true);
        }
    }

    public function retry(string $lang, string $id)
    {
        $failedSubscription = FailedSubscription::find($id);
        $mailingList = GeoAreasToNewsletterListService::convert($failedSubscription->mailing_list_name);
        
        if ($failedSubscription->mailing_list_name === 'subscribers') {
            config(['newsletter.apiKey' => config('newsletter.vcos_apikey')]);
            config(['newsletter.defaultListName' => config('newsletter.defaultListName')]);
        } else {
            config(['newsletter.apiKey' => config('newsletter.ss_apikey')]);
            config(['newsletter.defaultListName' => config('newsletter.defaultSSListName')]);
        }
        
        if (Newsletter::subscribeOrUpdate($failedSubscription->email, unserialize($failedSubscription->names), $mailingList)) {
            return response()->json([
                'error' => false,
                'message' => trans('Uspješno izvršena pretplata'),
            ], 200);
        }
        return response()->json([
            'error' => true,
            'message' => 'MailChimp Error ' . Newsletter::getLastError(),
        ], 400);
    }

    public function retryAll()
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, string $lang, FailedSubscription $failedSubscription)
    {
        if ($failedSubscription->delete()) {
            return redirect()->route('failed-subscriptions.index', app()->getLocale())->withSuccess('Neuspjela pretplata je uspješno obrisana.');
        }
        return redirect()->route('failed-subscriptions.index', app()->getLocale())->withErrors('Greška prilikom brisanja neuspjele pretplate.');
    }
}
