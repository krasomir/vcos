<?php

namespace App\Http\Controllers;

use App\Post;
use App\Services\Slug;
use Illuminate\Http\Request;
use App\Http\Requests\PostDeleteRequest;
use App\Http\Requests\PostPublishRequest;
use App\Http\Requests\PostStoreRequest;
use App\Page;
use Jorenvh\Share\Share;
use Yajra\DataTables\Facades\DataTables;

class PostController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \View
     */
    public function index()
    {
        return view('admin.posts.index');
    }

    /**
     * Undocumented function
     *
     * @return \View
     */
    public function newsList()
    {
        return view('pages.news', [
            'posts' => Post::where([
                ['is_valid', 1],
                ['page_id', 1]
            ])->latest('datetime_on')->paginate(12),
        ]);
    }

    /**
     * Undocumented function
     *
     * @return \View
     */
    public function openVolunteeringNewsList()
    {
        return view('pages.open-volunteering-opportunities', [
            'posts' => Post::where([
                ['is_valid', 1],
                ['page_id', 2]
            ])->latest('datetime_on')->paginate(12),
        ]);
    }

    /**
     * Undocumented function
     *
     * @param String $lang
     * @param Post $post
     * @return \View
     */
    public function showOpenVolunteeringNews(String $lang, Post $post, Request $request)
    {
        if (!$post->is_valid && !$this->isPreviewTokenValid($request, $post)) {
            abort(404);
        }
        
        return view('pages.open-volunteering-opportunities-details', [
            'post' => $post,
            'oldPostImagePath' => $post->getOldPostImagePath(),
        ]);
    }

    /**
     * Undocumented function
     *
     * @param Request $request
     * @param Post $post
     * @return boolean
     */
    public function isPreviewTokenValid(Request $request, Post $post)
    {
        if (!$request->token || !auth()->check()) {
            return false;
        }

        if (md5(auth()->user()->email . $post->id) === $request->token) {
            return true;
        }

        return false;
    }

    /**
     * Undocumented function
     *
     * @param Request $request
     * @return DataTables
     */
    public function postsList(Request $request)
    {
        if ($request->ajax()) {

            $posts = Post::latest()->get();

            return DataTables::of($posts)
                ->addIndexColumn()
                ->addColumn('action', function($row){
                    $previewToken = md5(auth()->user()->email . $row->id);
                    $route = ($row->page_id === 1) ? 
                        route('news-details', [
                            $row, 
                            'lang' => app()->getLocale(), 
                            'token' => $previewToken
                        ]) : 
                        route('open-volunteering-opportunities-details', [
                            $row, 
                            'lang' => app()->getLocale(), 
                            'token' => $previewToken
                        ]);
                    $actionBtn = '
                        <a href="'. route('admin-post-edit', [$row, 'lang' => app()->getLocale()]) .'" 
                            class="edit btn btn-success btn-sm"
                            >
                            Uredi
                        </a> 
                        <a href="#" 
                            data-toggle="modal" 
                            data-target="#confirmDelete" 
                            data-title="Brisanje posta"
                            data-model-id="'. $row->id .'"
                            data-message="Da li zaista želiš obrisati post - '. $row->name .'?"
                            data-delete-route="'. route('admin-post-delete', [$row, 'lang' => app()->getLocale()]) .'"
                            class="delete btn btn-danger btn-sm"
                            >
                            Obriši
                        </a>
                        <a href="'. $route .'" target="_blank"
                            class="preview btn btn-info btn-sm"
                            >
                            Pregledaj
                        </a> 
                    ';
                    return $actionBtn;
                })
                ->editColumn('created_at', function($row) {
                    return $row->created_at->format('d.m.Y');
                })
                ->editColumn('preview', function($row) {
                    return $row->shortenPreview();
                })
                ->addColumn('is_valid', function($row) {
                    $checked = ((int)$row->is_valid === 1) ? 'checked': '';
                    $switchBtn = '
                        <div class="custom-control custom-checkbox">
                            <input data-post-id="'. $row->id .'" data-route="'. route('post-publish', app()->getLocale()) .'" class="custom-control-input" value="1" '. $checked .' name="is_valid" type="checkbox" id="customCheckbox_'.$row->id.'">
                            <label for="customCheckbox_'.$row->id.'" class="custom-control-label">&nbsp;</label>
                        </div>
                    ';

                    return $switchBtn;
                })
                ->rawColumns(['action', 'is_valid'])
                ->make(true);
        } 
    }

    /** 
     * Show the form for creating a new resource.
     *
     * @return \View
     */
    public function create()
    {
        return view('admin.posts.create', [
            'pages' => Page::all(),
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  PostStoreRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(PostStoreRequest $request)
    {
        $post = Post::store($request);
        if ($post) {
            return redirect()->route('admin-posts', app()->getLocale())->withSuccess('Uspješno spremljen post.');
        }
        return redirect()->route('admin-posts', app()->getLocale())->withErrors('Greška prilikom spremanja.');
    }

    /**
     * Display the specified resource.
     *
     * @param Post $post
     * @return \View
     */
    public function show(String $lang, Post $post, Request $request)
    {
        if (!$post->is_valid && !$this->isPreviewTokenValid($request, $post)) {
            abort(404);
        }
        
        return view('pages.news-details', [
            'post' => $post,
            'oldPostImagePath' => $post->getOldPostImagePath(),
            'socialShare' => (new Share)->page(route('news-details', [app()->getLocale(), $post->slug]), $post->title)
                ->facebook()
                ->twitter()
                ->linkedin(),
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  $lang
     * @param  \App\Post  $post
     * @return \View
     */
    public function edit(String $lang, Post $post)
    {
        return view('admin.posts.edit', [
            'post' => $post,
            'pages' => Page::all(),
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param PostStoreRequest $request
     * @param String $lang
     * @param Post $post
     * @return \Illuminate\Http\Response
     */
    public function updatePost(PostStoreRequest $request, String $lang, Post $post)
    {
        $updated = $post::updatePost($request, $post);
        if ($updated) {
            return redirect()->route('admin-posts', app()->getLocale())->withSuccess('Promjene su uspješno spremljene.');
        }
        return redirect()->route('admin-posts', app()->getLocale())->withErrors('Greška prilikom spremanja promjena.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param PostStoreRequest $request  
     * @param String $lang
     * @param  \App\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function destroy(PostDeleteRequest $request, String $lang, Post $post)
    {
        $deleted = $post::deletePost($post);
        if ($deleted) {
            return redirect()->route('admin-posts', app()->getLocale())->withSuccess('Post je uspješno obrisan.');
        }
        return redirect()->route('admin-posts', app()->getLocale())->withErrors('Greška prilikom brisanja posta.');        
    }

    /**
     * Undocumented function
     *
     * @param Request $request
     * @return String
     */
    public function createSlug(Request $request)
    {
        $modelId = ($request->modelId > 0)? $request->modelId: 0;

        return Slug::createSlug($request->title, $modelId, $request->has('type') ? $request->type : 'post');
    }

    /**
     * Undocumented function
     *
     * @param PostPublishRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function postPublish(PostPublishRequest $request)
    {
        $post = Post::find($request->post_id);
        $post->is_valid = $request->publish;
        
        if ($post->save()) {
            return response()->json([
                'error' => false,
                'message' => 'Promijena uspješno spremljena.'
            ], 200);
        }
        return response()->json([
            'error' => true,
            'message' => 'Došlo je do greške prilikom spremanja promjene. Pokušajte ponovno.'
        ], 500);
    }
}
