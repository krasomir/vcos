<?php

namespace App\Http\Controllers;

use App\Post;
use App\Slider;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Show the application dashboard.
     *
     * @return \View
     */
    public function index()
    {
        return view('admin.index');
    }

    /**
     * Undocumented function
     * @return \View
     *
     */
    public function frontIndex()
    {
        return view('pages.home', [
            'sliders' => Slider::orderBy('position')->get(),
            'latestPosts' => Post::where([
                ['is_valid', 1],
                ['page_id', 1]
            ])->orderBy('datetime_on', 'desc')->take(2)->get()
        ]);
    }
}
