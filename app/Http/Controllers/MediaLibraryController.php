<?php

namespace App\Http\Controllers;

use App\Services\CKEditorUserfilesService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;

class MediaLibraryController extends Controller
{
    /**
     * Undocumented function
     *
     * @return View
     */
    public function index()
    {
        $files = CKEditorUserfilesService::getUserfiles();

        return view('admin.media_libraries.index', [
            'documents' => $files->documents,
            'images' => $files->images,
        ]);
    }

    /**
     * Undocumented function
     *
     * @param Request $request
     * @return Redirect
     */
    public function delete(Request $request)
    {
        if ($this->handleDeleting($request)) {
            return redirect()->route('media-library', app()->getLocale())->withSuccess('Uspješno obisana datoteka.');
        }
        return redirect()->route('media-library', app()->getLocale())->withErrors('Greška prilikom brisanja datoteke.');
    }

    /**
     * Undocumented function
     *
     * @param Request $request
     * @return boolean
     */
    private function handleDeleting(Request $request)
    {
        $mediaPath = $request->mediaPath;
        $thumbTemp = explode(DIRECTORY_SEPARATOR, $mediaPath);
        $thumbFolder = $thumbTemp[(count($thumbTemp) - 1)];
        $mediaType = $thumbTemp[1];
        $thumbPath = null;

        for ($i=0; $i < (count($thumbTemp) - 1); $i++) { 
            $thumbPath .= $thumbTemp[$i] .'\\';
        }

        $thumbPath .= '__thumbs\\' . $thumbFolder; 

        if (
            (
                $mediaType === 'images' &&
                File::exists(public_path($request->mediaPath)) && 
                File::delete(public_path($request->mediaPath)) 
            ) || (
                File::exists(public_path($request->mediaPath)) && 
                File::delete(public_path($request->mediaPath))
            )
        ) {
            if (File::isDirectory(public_path($thumbPath))) {
                File::deleteDirectory(public_path($thumbPath));
            }
            return true;
        }
        return false;
    }
}
