<?php

namespace App\Http\Controllers;

use App\Http\Requests\ProjectArchiveRequest;
use App\Http\Requests\ProjectDeleteRequest;
use App\Http\Requests\ProjectPublishRequest;
use App\Project;
use Illuminate\Http\Request;
use Yajra\DataTables\Facades\DataTables;
use App\Http\Requests\ProjectStoreRequest;
use App\Http\Requests\ProjectUpdateRequest;
use Carbon\Carbon;

class ProjectController extends Controller
{
    /**
     * Undocumented function
     *
     * @return view
     */
    public function activeProjects()
    {
        return view('pages.active-projects', [
            'projects' => Project::where([
                ['published', 1],
                ['archived', null]
            ])->latest()->paginate(12),
        ]);
    }

    /**
     * Undocumented function
     *
     * @return view
     */
    public function archiveProjects()
    {
        return view('pages.archive-projects', [
            'projects' => Project::where([
                ['archived', '!=', null]
            ])->latest('archived')->paginate(12),
        ]);
    }

    /**
     * Undocumented function
     *
     * @param Request $request
     * @param String $lang
     * @param Project $project
     * @return void
     */
    public function projectDetail(Request $request, String $lang, Project $project)
    {
        if (!$project->published) {
            abort(404);
        }
        
        return view('pages.project', [
            'project' => $project
        ]);
    }

    /**
     * Undocumented function
     *
     * @return view
     */
    public function adminIndex()
    {
        return view('admin.projects.index');
    }

    /**
     * Undocumented function
     *
     * @param Request $request
     * @return DataTables
     */
    public function projectList(Request $request)
    {
        if ($request->ajax()) {
            $data = Project::latest()->get();
            return DataTables::of($data)
                ->addIndexColumn()
                ->addColumn('action', function($row){
                    $actionBtn = '
                        <a href="'. route('project-edit', [$row, 'lang' => app()->getLocale()]) .'" class="edit btn btn-success btn-sm">Uredi</a> 
                        <a href="#" 
                            data-toggle="modal" 
                            data-target="#confirmDelete" 
                            data-title="Brisanje izvještaja"
                            data-model-id="'. $row->id .'"
                            data-message="Da li zaista želiš obrisati izvještaj - '. $row->title .'?"
                            data-delete-route="'. route('project-delete', [$row, 'lang' => app()->getLocale()]) .'"
                            class="delete btn btn-danger btn-sm"
                        >
                            Obriši
                        </a>
                    ';
                    return $actionBtn;
                })
                ->addColumn('published', function($row) {
                    $checked = ((int)$row->published === 1) ? 'checked': '';
                    $switchBtn = '
                        <div class="custom-control custom-checkbox">
                            <input data-route="'. route('project-publish', [app()->getLocale(), $row]) .'" class="custom-control-input" value="1" '. $checked .' name="published" type="checkbox" id="customCheckbox_'.$row->id.'">
                            <label for="customCheckbox_'.$row->id.'" class="custom-control-label">&nbsp;</label>
                        </div>
                    ';

                    return $switchBtn;
                })
                ->addColumn('archived', function($row) {
                    $checked = ($row->archived !== null) ? 'checked': '';
                    $title = ($row->archived) ? 'title="'. Carbon::parse($row->archived)->format('d.m.Y H:i:s') .'"' : '';
                    $switchBtn = '
                        <div class="custom-control custom-checkbox">
                            <input 
                                data-route="'. route('project-archive', [app()->getLocale(), $row]) .'" 
                                class="custom-control-input-archive" 
                                value="1" '. $checked .' 
                                name="archived" 
                                type="checkbox" 
                                id="archiveCustomCheckbox_'.$row->id.'"
                                data-id="'. $row->id .'"
                                >
                            <label 
                                for="archiveCustomCheckbox_'.$row->id.'" 
                                class="custom-control-archive-label" '. $title .'
                                >&nbsp;
                            </label>
                        </div>
                    ';

                    return $switchBtn;
                })
                ->addColumn('archived_date', function($row) {
                    return ($row->archived) ? Carbon::parse($row->archived)->timestamp : null;
                })
                ->addColumn('DT_RowId', function($row) {
                    return 'archive_' . $row->id;
                })
                ->rawColumns(['action', 'published', 'archived'])
                ->make(true);
        }
    }

    /**
     * Undocumented function
     *
     * @return void
     */
    public function create()
    {
        return view('admin.projects.create');
    }

    /**
     * Undocumented function
     *
     * @param ProjectStoreRequest $request
     * @return Redirect
     */
    public function store(ProjectStoreRequest $request)
    {
        $saved = Project::createProject($request);
        if ($saved) {
            return redirect()->route('admin-projects', app()->getLocale())->withSuccess('Uspješno spremljeno.');
        }
        return redirect()->route('admin-projects', app()->getLocale())->withErrors('Greška prilikom spremanja.');
    }

    /**
     * Undocumented function
     *
     * @param Request $request
     * @param String $lang
     * @param Project $project
     * @return view
     */
    public function edit(Request $request, String $lang, Project $project)
    {
        return view('admin.projects.edit', [
            'project' => $project,
        ]);
    }

    /**
     * Undocumented function
     *
     * @param Request $request
     * @param String $lang
     * @param Project $project
     * @return Redirect
     */
    public function update(ProjectUpdateRequest $request, String $lang, Project $project)
    {
        if ($project->image_name) {
            Project::handleOldImage($project->image_name, 'projects');
        } 

        if ($request->hasFile('image')) {
            $imageName = Project::handleUploadImage(
                $request->file('image'), 
                $request->slug, 
                'projects', 
                Project::getProjectImageWidth(), 
                Project::getProjectThumbImageWidth()
            );
        }

        $project->title = $request->title;
        $project->slug = $request->slug;
        $project->preview = $request->preview;
        $project->description = $request->description;
        $project->start_date = $request->start_date;
        $project->end_date = $request->end_date;
        $project->published = $request->published;
        $project->image_name = $imageName ?? null;

        if ($project->save()) {
            return redirect()->route('admin-projects', app()->getLocale())->withSuccess('Uspješno spremljeno.');
        }
        return redirect()->route('admin-projects', app()->getLocale())->withErrors('Greška prilikom spremanja.');
    }

    /**
     * Undocumented function
     *
     * @param ProjectPublishRequest $request
     * @param String $lang
     * @param Project $project
     * @return Response
     */
    public function projectPublish(ProjectPublishRequest $request, String $lang, Project $project)
    {
        $project->published = $request->published;
        
        if ($project->save()) {
            return response()->json([
                'error' => false,
                'message' => 'Promijena uspješno spremljena.'
            ], 200);
        }
        return response()->json([
            'error' => true,
            'message' => 'Došlo je do greške prilikom spremanja promjene. Pokušajte ponovno.'
        ], 500);
    }

    /**
     * Undocumented function
     *
     * @param ProjectArchiveRequest $request
     * @param String $lang
     * @param Project $project
     * @return Response
     */
    public function projectArchive(ProjectArchiveRequest $request, String $lang, Project $project)
    {
        $project->archived = ($request->archived === '1') ? now() : null;
        
        if ($project->save()) {
            return response()->json([
                'error' => false,
                'message' => 'Promijena uspješno spremljena.',
                'archivedDate' => ($project->archived) ? $project->archived->format('d.m.Y H:i:s') : null,
            ], 200);
        }
        return response()->json([
            'error' => true,
            'message' => 'Došlo je do greške prilikom spremanja promjene. Pokušajte ponovno.'
        ], 500);
    }

    /**
     * Undocumented function
     *
     * @param Request $request
     * @param String $lang
     * @param Project $project
     * @return Response
     */
    public function destroy(ProjectDeleteRequest $request, String $lang, Project $project)
    {
        if ($project->delete()) {
            return redirect()->route('admin-projects', app()->getLocale())->withSuccess('Projekt je uspješno obrisan.');
        }
        return redirect()->route('admin-projects', app()->getLocale())->withErrors('Greška prilikom brisanja projekta.');
    }
}
