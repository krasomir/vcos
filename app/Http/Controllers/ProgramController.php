<?php

namespace App\Http\Controllers;

use App\Http\Requests\ProgramDeleteRequest;
use App\Http\Requests\ProgramStoreRequest;
use App\Program;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use Yajra\DataTables\Facades\DataTables;

class ProgramController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return View
     */
    public function index()
    {
        return view('admin.programs.index');
    }

    /**
     * Undocumented function
     *
     * @param Request $request
     * @return DataTables
     */
    public function list(Request $request)
    {
        if ($request->ajax()) {
            
            $programs = Program::latest()->get();

            return DataTables::of($programs)
                ->addIndexColumn()
                ->addColumn('action', function($row){
                    $actionBtn = '
                        <a href="'. route('programs.edit', [$row, 'lang' => app()->getLocale()]) .'" class="edit btn btn-success btn-sm">Uredi</a> 
                        <a href="#" 
                            data-toggle="modal" 
                            data-target="#confirmDelete" 
                            data-title="Brisanje programa"
                            data-model-id="'. $row->id .'"
                            data-message="Da li zaista želiš obrisati program - '. $row->title .'?"
                            data-delete-route="'. route('programs.destroy', [$row, 'lang' => app()->getLocale()]) .'"
                            class="delete btn btn-danger btn-sm"
                        >
                            Obriši
                        </a>
                    ';
                    return $actionBtn;
                })
                ->editColumn('content', function($row) {
                    return Str::words(strip_tags($row->content), 20, '...');
                })
                ->setRowId(function($row) {
                    return $row->id;
                })
                ->rawColumns(['action'])
                ->make(true);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return View
     */
    public function create()
    {
        return view('admin.programs.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ProgramStoreRequest $request)
    {
        $saved = Program::create($request->validated());

        if ($saved) {
            return redirect()->route('programs.index', app()->getLocale())->withSuccess('Uspješno spremljen program.');
        }
        return redirect()->route('programs.index', app()->getLocale())->withErrors('Greška prilikom spremanja programa.');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  String  $lang
     * @param  \App\Program  $program
     * @return \Illuminate\Http\Response
     */
    public function edit(String $lang, Program $program)
    {
        return view('admin.programs.edit', [
            'program' => $program
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  String  $lang
     * @param  \App\Program  $program
     * @return \Illuminate\Http\Response
     */
    public function update(ProgramStoreRequest $request, String $lang, Program $program)
    {
        if ($program->update($request->validated())) {
            return redirect()->route('programs.index', app()->getLocale())->withSuccess('Uspješno spremljene promjene.');
        }
        return redirect()->route('programs.index', app()->getLocale())->withErrors('Greška prilikom spremanja promjena.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  ProgramDeleteRequest  $request
     * @param  String  $lang
     * @param  \App\Program  $program
     * @return Redirect
     */
    public function destroy(ProgramDeleteRequest $request, String $lang, Program $program)
    {
        if ($program->delete()) {
            return redirect()->route('programs.index', app()->getLocale())->withSuccess('Uspješno obrisan program.');
        }
        return redirect()->route('programs.index', app()->getLocale())->withErrors('Greška prilikom brisanja programa.');
    }
}
