<?php

namespace App\Http\Controllers;

use App\Banner;
use App\Http\Requests\BannerDeleteRequest;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Storage;
use Yajra\DataTables\Facades\DataTables;
use App\Http\Requests\BannerStoreRequest;
use App\Http\Requests\BannerUpdateRequest;
use Illuminate\Support\Facades\Artisan;

class BannerController extends Controller
{
    private $existOnPages;

    public function __construct()
    {
        $this->existOnPages = Banner::all()->pluck('show_on_page')->toArray();
    }
    /**
     * Undocumented function
     *
     * @return view
     */
    public function index(Request $request)
    {        
        return view('admin.banners.index');
    }

    /**
     * Undocumented function
     *
     * @param Request $request
     * @return DataTables
     */
    public function list(Request $request)
    {
        if ($request->ajax()) {
            $banners = Banner::latest();
            return DataTables::of($banners)
                ->addIndexColumn()                
                ->addColumn('action', function($row){
                    $actionBtn = '
                        <a href="'. route('banners.edit', [$row, 'lang' => app()->getLocale()]) .'" class="edit btn btn-success btn-sm">Uredi</a> 
                        <a href="#" 
                            data-toggle="modal" 
                            data-target="#confirmDelete" 
                            data-title="Brisanje bannera"
                            data-model-id="'. $row->id .'"
                            data-message="Da li zaista želiš obrisati banner - '. $row->title .'?"
                            data-delete-route="'. route('banners.destroy', [$row, 'lang' => app()->getLocale()]) .'"
                            class="delete btn btn-danger btn-sm"
                        >
                            Obriši
                        </a>
                    ';
                    return $actionBtn;
                })
                ->editColumn('image_name', function($row) {
                    if (!$row->image_name || Storage::missing('public/images/banners/' . $row->image_name)) {
                        return null;
                    }
                    return Storage::url('images/banners/' . $row->image_name);
                })
                ->make(true);
        }
    }

    /**
     * Undocumented function
     *
     * @return view
     */
    public function create()
    {
        return view('admin.banners.create', [
            'routes' => Banner::$bannerUris,
            'exist_on_pages' => $this->existOnPages,
        ]);
    }

    /**
     * Undocumented function
     *
     * @param BannerStoreRequest $request
     * @return Redirect
     */
    public function store(BannerStoreRequest $request)
    {
        $saved = Banner::store($request);
        if ($saved) {
            return redirect()->route('banners.index', app()->getLocale())->withSuccess('Uspješno spremljeno.');
        }
        return redirect()->route('banners.index', app()->getLocale())->withErrors('Greška prilikom spremanja.');
    }

    /**
     * Undocumented function
     *
     * @param String $lang
     * @param Banner $banner
     * @return View
     */
    public function edit(String $lang, Banner $banner)
    {
        return view('admin.banners.edit', [
            'banner' => $banner,
            'routes' => $banner::$bannerUris,
            'exist_on_pages' => $this->existOnPages,
        ]);
    }

    /**
     * Undocumented function
     *
     * @param BannerUpdateRequest $request
     * @param String $lang
     * @param Banner $banner
     * @return Redirect
     */
    public function update(BannerUpdateRequest $request, String $lang, Banner $banner)
    {
        $updated = Banner::updateBanner($request, $banner);
        if ($updated) {
            return redirect()->route('banners.index', app()->getLocale())->withSuccess('Banner je uspješno spremljen.');
        }
        return redirect()->route('banners.index', app()->getLocale())->withErrors('Greška prilikom spremanja bannera.');
    }

    /**
     * Undocumented function
     *
     * @param BannerDeleteRequest $request
     * @param String $lang
     * @param Banner $banner
     * @return Redirect
     */
    public function destroy(BannerDeleteRequest $request, String $lang, Banner $banner)
    {
        $deleted = Banner::deleteBanner($banner);
        if ($deleted) {
            return redirect()->route('banners.index', app()->getLocale())->withSuccess('Banner je uspješno obrisan.');
        }
        return redirect()->route('banners.index', app()->getLocale())->withErrors('Greška prilikom brisanja bannera.');
    }
}
