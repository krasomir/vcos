<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cookie;
use App\Http\Requests\UserStoreRequest;
use App\Http\Requests\UserDeleteRequest;
use App\Http\Requests\UserUpdateRequest;
use Spatie\Permission\Models\Permission;
use Yajra\DataTables\Facades\DataTables;

class UserController extends Controller
{
    /**
     * Undocumented function
     *
     * @return view
     */
    public function index()
    {
        return view('admin.users.users');
    }

    /**
     * Undocumented function
     *
     * @param Request $request
     * @return DataTables
     */
    public function usersList(Request $request)
    {
        if ($request->ajax()) {
            $data = User::latest()->get();
            return DataTables::of($data)
                ->addIndexColumn()
                ->addColumn('action', function($row){
                    $actionBtn = '
                        <a href="'. route('admin-user-show', [$row, 'lang' => app()->getLocale()]) .'" class="edit btn btn-success btn-sm">Uredi</a> 
                        <a href="#" 
                            data-toggle="modal" 
                            data-target="#confirmDelete" 
                            data-title="Brisanje korisnika"
                            data-model-id="'. $row->id .'"
                            data-message="Da li zaista želiš obrisati korisnika - '. $row->name .'?"
                            data-delete-route="'. route('admin-user-delete', [$row, 'lang' => app()->getLocale()]) .'"
                            class="delete btn btn-danger btn-sm"
                        >
                            Obriši
                        </a>
                    ';
                    return $actionBtn;
                })
                ->editColumn('created_at', function($row) {
                    return $row->created_at->format('d.m.Y.');
                })
                ->rawColumns(['action'])
                ->make(true);
        }
    }

    /**
     * Undocumented function
     *
     * @return view
     */
    public function create()
    {
        return view('admin.users.user', [
            'permissions' => Permission::all(),
        ]);
    }

    /**
     * Undocumented function
     *
     * @param UserStoreRequest $request
     * @return redirect
     */
    public function store(UserStoreRequest $request)
    {
        $request->request->add(['email_verified_at' => now()]);
        $user = User::create($request->except(['password_confirmation', '_token']));
        $user->givePermissionTo($request->permission_ids);        

        return redirect()->route('admin-users', app()->getLocale())->withSuccess('Korisnik je uspješno kreiran.');
    }

    /**
     * Undocumented function
     *
     * @param [type] $lang
     * @param User $user
     * @return view
     */
    public function show($lang, User $user)
    {
        return view('admin.users.show', [
            'permissions' => Permission::all(),
            'user' => $user,
        ]);
    }

    /**
     * Undocumented function
     *
     * @param UserUpdateRequest $request
     * @param [type] $lang
     * @param User $user
     * @return void
     */
    public function update(UserUpdateRequest $request, $lang, User $user)
    {
        $user->name = $request->name;
        $user->email = $request->email;
        if ($request->filled('password')) {
            $user->password = $request->password;
        }
        $user->syncPermissions($request->permission_ids);
        $user->save();
        
        return redirect()
            ->route('admin-users', ['lang' => app()->getLocale(), 'user' => $user])
            ->withSuccess('Podaci su uspješno spremljeni.');
    }

    /**
     * Undocumented function
     *
     * @param UserDeleteRequest $request
     * @param [type] $lang
     * @param User $user
     * @return void
     */
    public function destroy(UserDeleteRequest $request, $lang, User $user)
    {
        $user->delete();

        return redirect()->route('admin-users', app()->getLocale())->withSuccess('Korisnik je uspješno obrisan.');
    }

    /**
     * Undocumented function
     *
     * @return void
     */
    public function userProfile()
    {
        return view('admin.users.profile');
    }
}
