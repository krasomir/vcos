<?php

namespace App\Http\Controllers;

use PDF;
use Illuminate\Http\Request;
use App\VolunteerOrganisation;
use App\Mail\UserRegisterEmail;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\Mail;
use Illuminate\Http\RedirectResponse;
use Yajra\DataTables\Facades\DataTables;
use App\Http\Requests\NeedVolunteersStepOne;
use App\Http\Requests\NeedVolunteersStepTwo;
use App\Http\Requests\NeedVolunteersStepFour;
use App\Http\Requests\NeedVolunteersStepThree;
use App\Http\Requests\OrganisationDeleteRequest;

class VolunteerOrganisationController extends Controller
{
    /**
     * Undocumented function
     *
     * @return view
     */
    public function index()
    {
        return view('admin.organisations.index');
    }

    /**
     * Undocumented function
     *
     * @param Request $request
     * @return DataTables
     */
    public function organisationsList(Request $request)
    {
        if ($request->ajax()) {
            $data = VolunteerOrganisation::latest()->get();
            return DataTables::of($data)
                ->addIndexColumn()
                ->addColumn('action', function($row){
                    $actionBtn = '
                        <a href="'. route('organisations-show', [$row, 'lang' => app()->getLocale()]) .'" class="edit btn btn-info btn-sm">Detalji</a> 
                        <a href="#" 
                            data-toggle="modal" 
                            data-target="#confirmDelete" 
                            data-title="Brisanje organizacije"
                            data-model-id="'. $row->id .'"
                            data-message="Da li zaista želiš obrisati organizaciju - '. $row->organisation_name .'?"
                            data-delete-route="'. route('organisations-delete', [$row, 'lang' => app()->getLocale()]) .'"
                            class="delete btn btn-danger btn-sm"
                        >
                            Obriši
                        </a>
                    ';
                    return $actionBtn;
                })
                ->rawColumns(['action'])
                ->make(true);
        }
    }

    /**
     * Undocumented function
     *
     * @param Request $request
     * @param String $lang
     * @param VolunteerOrganisation $organisation
     * @return view
     */
    public function show(Request $request, $lang, VolunteerOrganisation $organisation)
    {
        return view('admin.organisations.show', [
            'organisation' => $organisation,
        ]);
    }

    /**
     * Undocumented function
     *
     * @param Request $request
     * @param String $lang
     * @param VolunteerOrganisation $organisation
     * @return PDF
     */
    public function organisationPdf(Request $request, String $lang, VolunteerOrganisation $organisation)
    {
        view()->share('organisation', $organisation);
        $pdf = PDF::loadView('admin.pdf.volunteer-organisation', $organisation);

        return $pdf->stream();
    }

    /**
     * Undocumented function
     *
     * @param OrganisationDeleteRequest $request
     * @param [type] $lang
     * @param VolunteerOrganisation $organisation
     * @return redirect
     */
    public function destroy(OrganisationDeleteRequest $request, $lang, VolunteerOrganisation $organisation)
    {
        $organisation->delete();

        return redirect()->route('organisations', app()->getLocale())->withSuccess('Organizacija je uspješno obrisana.');
    }

    /**
     * Undocumented function
     *
     * @return View
     */
    public function frontendIndex()
    {
        return view('pages.need-volunteers', [
            'validation_step_routes' => [
                'step_1' => route('need_volunteers_validate_step_one', app()->getLocale()),
                'step_2' => route('need_volunteers_validate_step_two', app()->getLocale()),
                'step_3' => route('need_volunteers_validate_step_three', app()->getLocale()),
            ]
        ]);
    }

    public function validationStepOne(NeedVolunteersStepOne $request): JsonResponse
    {
        if ($request->validated()) {
            return response()->json([
                'errors' => false,
            ], 200);
        }
    }

    public function validationStepTwo(NeedVolunteersStepTwo $request): JsonResponse
    {
        if ($request->validated()) {
            return response()->json([
                'errors' => false,
            ], 200);
        }
    }

    public function validationStepThree(NeedVolunteersStepThree $request): JsonResponse
    {
        if ($request->validated()) {
            return response()->json([
                'errors' => false,
            ], 200);
        }
    }

    /**
     * Undocumented function
     *
     * @param NeedVolunteersStepFour $request
     * @return RedirectResponse|JsonResponse
     */
    public function store(NeedVolunteersStepFour $request): JsonResponse
    {
        $organisation = VolunteerOrganisation::create($request->except(['_token', 'volunteer_period_from', 'volunteer_period_to']));

        if ($organisation) {

            Mail::to(config('app.organisation_need_volunteer_to_address'))
                    ->send(new UserRegisterEmail($organisation, 'organisation_need_volunteer'));
                
            if ($request->newsletter == 1) {
                session(['organisation' => $organisation->fresh()]);
                return response()->json([
                    'errors' => false,
                    'redirect' => route('strength-of-participation-application', ['lang' => app()->getLocale()]), 
                ], 200);
            }
            return response()->json([
                'errors' => false,
                'message' => trans('Podaci su uspješno spremljeni.'),
            ], 200);
        }
        return response()->json([
            'errors' => true,
            'message' => trans('Došlo je do greške prilikom spremanja. Podaci nisu spremljeni.'),
        ], 500);
    }
}
