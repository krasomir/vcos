<?php

namespace App\Http\Controllers;

use App\Jobs\ProcessFailedSubscriptionJob;
use App\Token;
use Illuminate\View\View;
use Illuminate\Http\Request;
use App\Services\SubscriptionService;
use App\Services\FailedSubscriptionService;

class TokenController extends Controller
{
    /**
     * Undocumented function
     *
     * @param String $lang
     * @param Token $token
     * @param Request $request
     * @return View
     */
    public function index(String $lang, Token $token, Request $request)
    {
        $newsletterData = SubscriptionService::getNewsletterData($token->model_type, $token->model);
        $model = $token->model_type::findOrFail($newsletterData->model_id);
        
        if ($model) {
            $subscription = SubscriptionService::subscribe($newsletterData, $token->model_type);

            if ($subscription['alertType'] === 'success') {
                $token->delete();
                $model->update(['verified_at' => now()]);
            }

            if (isset($subscription['failedListSubscription']) && $subscription['failedListSubscription']) {
                ProcessFailedSubscriptionJob::dispatch($subscription['failedListSubscription']);            
            }
        }

        return view('pages.verify-token', [
            'verificationMessage' => $subscription['message'],
            'alertType' => $subscription['alertType'],
        ]);
    }
}
