<?php

namespace App\Http\Middleware;

use Closure;

class SetLanguage
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $lang = (!in_array($request->lang, config('languages.possible_languages'))) ? 'hr': $request->lang;
        \App::setLocale($lang);
        return $next($request);
    }
}
