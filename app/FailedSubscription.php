<?php

namespace App;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class FailedSubscription extends Model
{
    use HasFactory;

    protected $filled = [
        'email',
        'mailing_list_name',
        'names',
        'comment',
        'created_at',
        'updated_at'
    ];
}
