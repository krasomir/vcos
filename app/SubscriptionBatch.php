<?php

namespace App;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class SubscriptionBatch extends Model
{
    use HasFactory;

    protected $fillable = [
        'list_name',
        'batch_id',
        'status',
        'total_operations',
        'finished_operations',
        'errored_operations',
        'submitted_at',
        'completed_at',
        'response_body_url',
    ];

    protected $casts = [
        'submitted_at' => 'datetime',
        'completed_at' => 'datetime'
    ];
}
