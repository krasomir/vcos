<?php

namespace App\Rules;

use Illuminate\Support\Facades\File;
use Illuminate\Contracts\Validation\Rule;

class IsFileExists implements Rule
{
    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        return File::exists(public_path('storage/files/podcasts/' . $value));
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return __('validation.is_file_exists');
    }
}
