<?php

namespace App;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class VolunteerOrganisationStrengthOfParticipation extends Model
{
    use HasFactory, SoftDeletes;

    protected $fillable = [
        "organisation_type", 
        "organisation_name", 
        "foundation_year", 
        "address", 
        "zip_number", 
        "city", 
        "contact_person", 
        "telephone", 
        "email", 
        "web", 
        "mission_goals", 
        "activity_area", 
        "target_group", 
        "geo_area", 
        "newsletter",
        "verified_at",
    ];

    public static $activity_areas = [
        'razvoj civilnog društva',
        'razvoj demokracije',
        'ljudska prava i vladavina prava',
        'izgradnja mira',
        'razvoj lokalne zajednice',
        'obrazovanje',
        'istraživanje i znanost',
        'filantropija',
        'volonterstvo',
        'socijalna politika, socijalna skrb i usluge',
        'socijalno poduzetništvo',
        'zdravlje',
        'zaštita okoliša i održivi razvoj',
        'zaštita životinja',
        'kultura i umjetnost',
        'međunarodna suradnja',
        'religija',
        'sport',
        'tehnička kultura',
        'strukovne i poslovne organizacije'
    ];

    public static $geo_areas = [
        'Osječko-baranjska županija', 
        'Vukovarsko-srijemska županija', 
        'Brodsko-posavska županija', 
        'Požeško-slavonska županija', 
        'Druge županije', 
    ];

    /**
     * Undocumented function
     *
     * @param array|string $value
     * @return void
     */
    public function setActivityAreaAttribute($value): void
    {
        $this->attributes['activity_area'] = serialize($value);
    }

    /**
     * Undocumented function
     *
     * @param String $value
     * @return String
     */
    public function getActivityAreaAttribute(String $value)
    {
        $activityArea = [];
        $tmpActivityArea = unserialize($value);

        foreach ($tmpActivityArea as $area) {
            if ($area != '') {
                $activityArea[] = $this->sanitizeString($area);
            }            
        }

        return $activityArea;
    }

    /**
     * Undocumented function
     *
     * @param array|string $value
     * @return void
     */
    public function setGeoAreaAttribute($value): void
    {
        $this->attributes['geo_area'] = serialize($value);
    }

    /**
     * Undocumented function
     *
     * @param String $value
     * @return String
     */
    public function getGeoAreaAttribute(String $value)
    {
        $geoArea = [];
        $tmpGeoArea = unserialize($value);

        foreach ($tmpGeoArea as $area) {
            if ($area != '') {
                $geoArea[] = $this->sanitizeString($area);
            }            
        }

        return $geoArea;
    }

    /**
     * Undocumented function
     *
     * @param String $string
     * @return string
     */
    private function sanitizeString(String $string)
    {
        return preg_replace('/[\x00-\x1F\x7F]/u', '', $string);
    }
}
