<?php

namespace App;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PostMainImage extends Model
{
    use HasFactory;

    protected $fillable = [
        'file_id', 'post_id', 'post_type', 'width', 'height', 'crop', 'crop_thumb', 
    ];
}
