<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Database\Eloquent\Model;

class UserRegisterEmail extends Mailable implements ShouldQueue
{
    use Queueable, SerializesModels;

    public $registrant;

    public $type;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(Model $registrant, string $type)
    {
        $this->registrant = $registrant;
        $this->type = $type;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->subject(trans('Nova registracija'))->view('emails.new-registration');
    }
}
