<?php

namespace App\Mail;

use App\Volunteer;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Database\Eloquent\Model;

class ConfirmNewsletterEmail extends Mailable implements ShouldQueue
{
    use Queueable, SerializesModels;

    public $token;

    public $model;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(string $token, Model $model)
    {
        $this->token = $token;
        $this->model = $model;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->subject(trans('Potvrda e-mail adrese za newsletter'))->view('emails.confirm-mailchimp-newsletter-email');
    }
}
