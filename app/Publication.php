<?php

namespace App;

use App\Traits\ImageUploadTrait;
use Exception;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;
use Illuminate\Foundation\Http\FormRequest;
use App\Http\Requests\PublicationStoreRequest;
use App\Http\Requests\PublicationUpdateRequest;

class Publication extends Model
{
    use ImageUploadTrait;

    protected $fillable = ['title', 'slug', 'publication_category', 'image_name', 'publication_name', 'is_demo_academy'];

    /**
     * Undocumented variable
     *
     * @var integer
     */
    protected static int $imageWidth = 576;

    /**
     * Undocumented function
     *
     * @param PublicationStoreRequest $request
     * @return boolean|Exception
     */
    public static function store(PublicationStoreRequest $request)
    {
        try {
            if ($request->hasFile('image')) {
                $imageName = self::handleUploadImage(
                    $request->file('image'),
                    $request->slug,
                    ($request->is_demo_academy) ? 'publications/demo_academy' : 'publications',
                    self::$imageWidth,
                    0
                );
            }

            if ($request->hasFile('publication')) {
                $publicationName = self::handleUploadPublication($request);
            }

            $request->request->add([
                'image_name' => $imageName ?? null,
                'publication_name' => $publicationName ?? null,
            ]);

            $publication =  Publication::create($request->except(['_token', 'image', 'publication']));

            return $publication;

        } catch (\Exception $e) {
            return $e->getMessage();
        }
    }

    /**
     * Undocumented function
     *
     * @param PublicationStoreRequest $request
     * @return String
     */
    public static function handleUploadPublication(FormRequest $request)
    {
        $extension = $request->file('publication')->extension();
        $name = $request->slug . '.' . $extension;

        $path = $request->file('publication')->storeAs(
            'documents/publications', $name, 'public'
        );

        return $name;
    }

    /**
     * Undocumented function
     *
     * @param PublicationUpdateRequest $request
     * @param Publication $publication
     * @return bool|Exception
     */
    public static function updatePublication(PublicationUpdateRequest $request, Publication $publication): Exception|bool
    {
        try {
            if ($request->hasFile('image')) {
                self::handleOldImage($publication->image_name, 'publications');
                $imageName = self::handleUploadImage(
                    $request->file('image'),
                    $request->slug,
                    ($request->is_demo_academy) ? 'publications/demo_academy' : 'publications',
                    self::$imageWidth,
                    0
                );
                $publication->image_name = $imageName;
            }

            if ($request->hasFile('publication')) {
                self::handleOldPublication($publication->publication_name);

                $publicationName = self::handleUploadPublication($request);
                $publication->publication_name = $publicationName;
            }

            if ($publication->slug != $request->slug) {
                // handle rename of image
                $publication->image_name = self::handleImageRename($publication->image_name, $request->slug, 'publications');

                // handle rename of publication
                $publication->publication_name = self::handlePublicationRename($publication->publication_name, $request->slug);
            }

            $publication->title = $request->title;
            $publication->slug = $request->slug;
            $publication->publication_category = $request->publication_category;
            $publication->is_demo_academy = $request->is_demo_academy;

            return $publication->save();

        } catch (\Exception $e) {
            return $e->getMessage();
        }
    }

    /**
     * Undocumented function
     *
     * @param String $name
     * @return void
     */
    public static function handleOldPublication(String $name)
    {
        Storage::delete('public/documents/publications/' . $name);
    }

    /**
     * Undocumented function
     *
     * @param String $oldName
     * @param String $slug
     * @return String|boolean
     */
    public static function handlePublicationRename(String $oldName, String $slug)
    {
        $newNameExt = pathinfo(storage_path('documents/publications/'. $oldName), PATHINFO_EXTENSION);
        $newName = $slug . '.' . $newNameExt;

        if (Storage::move('public/documents/publications/'. $oldName, 'public/documents/publications/'. $newName)) {
            return $newName;
        }

        return false;
    }

    /**
     * Undocumented function
     *
     * @param Publication $publication
     * @return boolean|Exception
     */
    public static function deletePublication(Publication $publication)
    {
        try {
            if ($publication->image_name) {
                $folder = ((int)$publication->is_demo_academy === 1) ? 'publications/demo_academy': 'publications';
                self::handleOldImage($publication->image_name, $folder);
            }
            if ($publication->publication_name) {
                self::handleOldPublication($publication->publication_name);
            }
            return $publication->delete();
        } catch (\Exception $e) {
            return $e->getMessage();
        }
    }
}
