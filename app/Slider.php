<?php

namespace App;

use App\Traits\ImageUploadTrait;
use Illuminate\Database\Eloquent\Model;
use App\Http\Requests\SliderStoreRequest;
use App\Http\Requests\SliderUpdateRequest;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Slider extends Model
{
    use HasFactory, ImageUploadTrait;

    protected $fillable = ['title', 'slug', 'link_to_page', 'image_name', 'position', 'custom_link'];

    /**
     * Undocumented variable
     *
     * @var integer
     */
    protected static $imageWidth = 2031;

    /**
     * Undocumented variable
     *
     * @var array
     */
    public static $availablePageUrls = [
        [
            "name" => "Misija, vizija, ciljevi",
            "uri" => "/mision-vision-goals"
        ],
        [
            "name" => "Tko čini DKolektiv?",
            "uri" => "/our-team"
        ],
        [
            "name" => "Upravni odbor",
            "uri" => "/board-of-directors"
        ],
        [
            "name" => "Izvještaji",
            "uri" => "/reports"
        ],
        [
            "name" => "Izvori financiranja",
            "uri" => "/sources-of-funding"
        ],
        [
            "name" => "Aktivni projekti",
            "uri" => "/active-projects"
        ],
        [
            "name" => "Arhiva projekata",
            "uri" => "/archive-of-projects"
        ],
        [
            "name" => "Publikacije",
            "uri" => "/publications"
        ],
        [
            "name" => "Novosti",
            "uri" => "/news"
        ],
        [
            "name" => "Volonterski centar Osijek",
            "uri" => "/volonterski-centar-osijek"
        ],
        [
            "name" => "Korisni dokumenti",
            "uri" => "/korisni-dokumenti"
        ],
        [
            "name" => "Učiniti nevidljivo vidljivim",
            "uri" => "/uciniti-nevidljivo-vidljivim"
        ],
        [
            "name" => "Ambasadori europskih snaga solidarnosti",
            "uri" => "/ambasadori-europskih-snaga-solidarnosti"
        ],
        [
            "name" => "Želite volontirati",
            "uri" => "/want-to-volunteer"
        ],
        [
            "name" => "Trebate volontere",
            "uri" => "/need-volunteers"
        ],
        [
            "name" => "Otvorene mogućnosti za volontiranje",
            "uri" => "/open-volunteering-opportunities"
        ],
        [
            "name" => "Društveni atelje",
            "uri" => "/social-atelier"
        ],
        [
            "name" => "Snaga sudjelovanja",
            "uri" => "/strength-of-participation"
        ],
        [
            "name" => "Demo akademija",
            "uri" => "/demo-academy"
        ],
        [
            "name" => "Voditelji kolegija i predavači",
            "uri" => "/demo-academy-instructors"
        ],
        [
            "name" => "Osijek to GOO",
            "uri" => "/osijek-to-goo"
        ],
        [
            "name" => "PoDcast Kolektiv",
            "uri" => "/podcasts"
        ],
        [
            "name" => "Radius V",
            "uri" => "/project/radius-v"
        ],
    ];

    /**
     * Undocumented function
     *
     * @return String
     */
    public static function getAvailablePageUrlsString()
    {
        $string = '';

        foreach (self::$availablePageUrls as $uri) {
            $string .= $uri['uri'] . ',';
        }

        return rtrim($string, ',');
    }

    /**
     * Undocumented function
     *
     * @param SliderStoreRequest $request
     * @return boolean|Exception
     */
    public static function store(SliderStoreRequest $request)
    {
        try {
            if ($request->hasFile('image')) {
                $imageName = self::handleUploadImage(
                    $request->file('image'), 
                    $request->slug, 
                    'slider', 
                    self::$imageWidth, 
                    0
                );
            }

            $request->request->add([
                'image_name' => $imageName ?? null,
            ]);

            $slider = Slider::create($request->except(['_token', 'image']));

            return $slider;

        } catch (\Exception $e) {
            return $e->getMessage();
        }
    }

    /**
     * Undocumented function
     *
     * @param SliderUpdateRequest $request
     * @param Slider $slider
     * @return boolean|Exception
     */
    public static function updateSlider(SliderUpdateRequest $request, Slider $slider)
    {
        try {
            if ($request->hasFile('image')) {
                self::handleOldImage($slider->image_name, 'slider');
                $imageName = self::handleUploadImage(
                    $request->file('image'), 
                    $request->slug, 
                    'slider', 
                    self::$imageWidth, 
                    0
                );
                $slider->image_name = $imageName;
            }

            if ($slider->slug != $request->slug) {
                // handle rename of image
                $slider->image_name = self::handleImageRename($slider->image_name, $request->slug, 'slider');
            }

            $slider->title = $request->title;
            $slider->slug = $request->slug;
            $slider->link_to_page = $request->link_to_page;
            $slider->custom_link = $request->custom_link;      

            return $slider->save();
        } catch (\Exception $e) {
            return $e->getMessage();
        }
    }

    /**
     * Undocumented function
     *
     * @param Slider $slider
     * @return boolean|Exception
     */
    public static function deleteSlider(Slider $slider)
    {
        try {
            if ($slider->image_name) {
                self::handleOldImage($slider->image_name, 'slider');
            }

            return $slider->delete();
        } catch (\Exception $e) {
            return $e->getMessage();
        }
    }
}
