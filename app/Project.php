<?php

namespace App;

use App\Http\Requests\ProjectStoreRequest;
use App\Traits\ImageUploadTrait;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;
use Laravel\Scout\Searchable;

class Project extends Model
{
    use ImageUploadTrait, Searchable, SoftDeletes;

    protected $fillable = [
        'title', 'slug', 'description', 'preview', 'start_date', 'end_date', 'published', 'archived', 'image_name',
    ];

    /**
     * The attributes that should be cast.
     *
     * @var array
     */
    protected $casts = [
        'start_date' => 'date:d.m.Y',
        'end_date' => 'date:d.m.Y',
    ];

    public function toSearchableArray(): array
    {
        return [
            'title' => strip_tags($this->title),
            'preview' => strip_tags($this->preview),
            'description' => strip_tags($this->description),
        ];
    }

    public function shortenDescription(): string
    {
        $bigText = $this->description;
        $bigText = strip_tags($bigText);

        return Str::words($bigText, 30, ' ...');
    }

    /**
     * Undocumented variable
     *
     * @var int
     */
    protected static $projectImageWidth = 1035;

    /**
     * Undocumented variable
     *
     * @var int
     */
    protected static $projectThumbImageWidth = 520;

    /**
     * Undocumented function
     *
     * @return int
     */
    public static function getProjectImageWidth()
    {
        return self::$projectImageWidth;
    }

    /**
     * Undocumented function
     *
     * @return int
     */
    public static function getProjectThumbImageWidth()
    {
        return self::$projectThumbImageWidth;
    }

    /**
     * Undocumented function
     *
     * @return Project|Exception
     */
    public static function createProject(ProjectStoreRequest $request)
    {
        $transaction = DB::transaction(function () use ($request) {

            try {

                if ($request->hasfile('image')) {

                    $imageName = self::handleUploadImage(
                        $request->file('image'),
                        $request->slug,
                        'projects',
                        self::$projectImageWidth,
                        self::$projectThumbImageWidth
                    );
                }

                $request->request->add(['image_name' => $imageName ?? null]);

                return Project::create($request->except(['_token', 'image']));

            } catch (\Exception $e) {
                return $e->getMessage();
            }
        });

        return $transaction;
    }
}
