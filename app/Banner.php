<?php

namespace App;

use App\Http\Requests\BannerDeleteRequest;
use App\Http\Requests\BannerStoreRequest;
use App\Http\Requests\BannerUpdateRequest;
use App\Traits\ImageUploadTrait;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Banner extends Model
{
    use HasFactory, ImageUploadTrait;

    protected $fillable = ['title', 'slug', 'image_name', 'show_on_page', 'bkg_color'];

    /**
     * Undocumented variable
     *
     * @var integer
     */
    protected static $bannerImageWidth = 2030;

    /**
     * Undocumented variable
     *
     * @var integer
     */
    protected static $bannerThumbImageWidth = 0;

    /**
     * Undocumented variable
     *
     * @var array
     */
    public static $bannerUris = [
        [
            "name" => "Sve stranice",
            "uri" => "/all"
        ],
        [
            "name" => "Misija, vizija, ciljevi",
            "uri" => "/mision-vision-goals"
        ],
        [
            "name" => "Tko čini DKolektiv?",
            "uri" => "/our-team"
        ],
        [
            "name" => "Upravni odbor",
            "uri" => "/board-of-directors"
        ],
        [
            "name" => "Izvještaji",
            "uri" => "/reports"
        ],
        [
            "name" => "Izvori financiranja",
            "uri" => "/sources-of-funding"
        ],
        [
            "name" => "Aktivni projekti",
            "uri" => "/active-projects"
        ],
        [
            "name" => "Arhiva projekata",
            "uri" => "/archive-of-projects"
        ],
        [
            "name" => "Publikacije",
            "uri" => "/publications"
        ],
        [
            "name" => "Novosti",
            "uri" => "/news"
        ],
        [
            "name" => "Postovi/Članci",
            "uri" => "/posts"
        ],
        [
            "name" => "Volonterski centar Osijek",
            "uri" => "/volonterski-centar-osijek"
        ],
        [
            "name" => "Korisni dokumenti",
            "uri" => "/korisni-dokumenti"
        ],
        [
            "name" => "Učiniti nevidljivo vidljivim",
            "uri" => "/uciniti-nevidljivo-vidljivim"
        ],
        [
            "name" => "Ambasadori europskih snaga solidarnosti",
            "uri" => "/ambasadori-europskih-snaga-solidarnosti"
        ],
        [
            "name" => "Želite volontirati",
            "uri" => "/want-to-volunteer"
        ],
        [
            "name" => "Trebate volontere",
            "uri" => "/need-volunteers"
        ],
        [
            "name" => "Otvorene mogućnosti za volontiranje",
            "uri" => "/open-volunteering-opportunities"
        ],
        [
            "name" => "Društveni atelje",
            "uri" => "/social-atelier"
        ],
        [
            "name" => "Snaga sudjelovanja",
            "uri" => "/strength-of-participation"
        ],
        [
            "name" => "Demo akademija",
            "uri" => "/demo-academy"
        ],
        [
            "name" => "Voditelji kolegija i predavači",
            "uri" => "/demo-academy-instructors"
        ],
        [
            "name" => "Osijek to GOO",
            "uri" => "/osijek-to-goo"
        ],
        [
            "name" => "PoDcast Kolektiv",
            "uri" => "/podcasts"
        ],
        [
            "name" => "Radius V",
            "uri" => "/radius-v"
        ],
    ];

    /**
     * Undocumented function
     *
     * @return String
     */
    public static function getBannerUrisString()
    {
        $string = '';

        foreach (self::$bannerUris as $uri) {
            $string .= $uri['uri'] . ',';
        }

        return rtrim($string, ',');
    }

    /**
     * Undocumented function
     *
     * @param BannerStoreRequest $request
     * @return Banner|Exception
     */
    public static function store(BannerStoreRequest $request)
    {
        try {
            if ($request->hasFile('image')) {
                $imageName = self::handleUploadImage(
                    $request->file('image'), 
                    $request->slug, 
                    'banners', 
                    self::$bannerImageWidth, 
                    self::$bannerThumbImageWidth
                );

                $request->request->add(['image_name' => $imageName]);
            }

            return Banner::create($request->except(['_token']));

        } catch (\Exception $e) {
            return $e->getMessage();
        }
    }

    /**
     * Undocumented function
     *
     * @param BannerUpdateRequest $request
     * @param Banner $banner
     * @return boolean|Exception
     */
    public static function updateBanner(BannerUpdateRequest $request, Banner $banner)
    {
        try {
            if ($request->hasFile('image')) {

                if ($banner->image_name) {
                    self::handleOldImage($banner->image_name, 'banners');
                } 

                $imageName = self::handleUploadImage(
                    $request->file('image'), 
                    $request->slug, 
                    'banners', 
                    self::$bannerImageWidth, 
                    self::$bannerThumbImageWidth
                );

                $request->request->add(['image_name' => $imageName]);
            }

            return $banner->update($request->except(['_token']));
        } catch (\Exception $e) {
            return $e->getMessage();
        }
    }

    /**
     * Undocumented function
     *
     * @param Banner $banner
     * @return boolean|Exception
     */
    public static function deleteBanner(Banner $banner)
    {
        try {
            if ($banner->image_name) {
                self::handleOldImage($banner->image_name, 'banners');
            }
            return $banner->delete();
        } catch (\Exception $e) {
            return $e->getMessage();
        }
    }
}
