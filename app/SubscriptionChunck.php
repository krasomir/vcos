<?php

namespace App;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class SubscriptionChunck extends Model
{
    use HasFactory;

    protected $fillable = ['operation_id', 'email'];
}
