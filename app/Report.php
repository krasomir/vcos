<?php

namespace App;

use Illuminate\Support\Str;
use Illuminate\Http\Request;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;
use App\Http\Requests\ReportStoreRequest;
use App\Http\Requests\ReportUpdateRequest;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Support\Facades\File;

class Report extends Model
{
    use HasFactory, SoftDeletes;

    protected $fillable = [
        'title', 'report_type', 'report_url', 'published'
    ];

    /**
     * Undocumented function
     *
     * @param ReportStoreRequest $request
     * @return Report|Exception
     */
    public static function store(ReportStoreRequest $request)
    {
        try {
                
            if ($request->hasfile('report_url')) {
       
                $reportUploadUrl = self::handleUploadReport($request);
            }
            
            $report = Report::create([
                'title' => $request->title,
                'report_type' => $request->report_type,
                'report_url' => $request->hasfile('report_url') ? $reportUploadUrl : $request->report_url,
                'published' => $request->published,
            ]);

            return $report;
        } catch (\Exception $e) {
            return $e->getMessage();
        } 
    }

    /**
     * Undocumented function
     *
     * @param Request $request
     * @return String
     */
    public static function handleUploadReport(Request $request)
    {
        $extension = $request->file('report_url')->extension();
        $newName = Str::slug($request->title) . '.' . $extension;

        $path = $request->file('report_url')->storeAs(
            'files/reports', $newName, 'public'
        );

        return '/storage/' . $path;
    }

    /**
     * Undocumented function
     *
     * @param ReportUpdateRequest $request
     * @param Report $report
     * @return Report|Exception
     */
    public static function updateReport(ReportUpdateRequest $request, Report $report)
    {
        try {
            
            if (
                $report->report_type !== $request->report_type && 
                File::exists(public_path($report->report_url))
                ) 
            {
                File::delete(public_path($report->report_url));
            }
                
            if ($request->hasfile('report_url')) {       
                $reportUploadUrl = self::handleUploadReport($request);
            }
            
            $report->title = $request->title;
            if ($request->report_type) {
                $report->report_type = $request->report_type;
                $report->report_url = $request->hasfile('report_url') ? $reportUploadUrl : $request->report_url;
            }
            $report->published = $request->published;
            $report->save();

            return $report->fresh();
        } catch (\Exception $e) {
            return $e->getMessage();
        } 
    }

    /**
     * Undocumented function
     *
     * @param Report $report
     * @return boolean|Exception
     */
    public static function deleteReport(Report $report)
    {
        try {
            if (
                $report->report_type === 'pdf' && 
                File::exists(public_path($report->report_url)) && 
                File::delete(public_path($report->report_url))
                ) 
            {
                return $report->delete();
            } else {
                return $report->delete();
            }
        } catch (\Exception $e) {
            return $e->getMessage();
        }
    }
}
