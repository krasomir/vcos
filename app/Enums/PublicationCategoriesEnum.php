<?php

namespace App\Enums;

enum PublicationCategoriesEnum: string
{
    case PROFESSIONAL_MANUALS = 'professional_manuals';
    case RESEARCH_AND_ANALYSIS = 'research_and_analysis';

    public static function values(): array
    {
        return array_column(PublicationCategoriesEnum::cases(), 'value');
    }
}
