<?php

namespace App;

use App\Http\Requests\TeamStoreRequest;
use App\Http\Requests\TeamUpdateRequest;
use App\Traits\ImageUploadTrait;
use Exception;
use Illuminate\Database\Eloquent\Model;

class Team extends Model
{
    use ImageUploadTrait;

    protected $fillable = [
        'first_name',
        'last_name',
        'email',
        'linked_in',
        'image',
        'position',
        'description',
        'created_at',
        'updated_at',
    ];

    protected $casts = [
        'created_at' => 'datetime',
        'updated_at' => 'datetime',
    ];

    protected static int $imageWidth = 200;

    public static function store(TeamStoreRequest $request): Team|string
    {
        try {
            $imageName = null;
            if ($request->hasFile('image')) {
                $imageName = self::handleUploadImage(
                    uploadedFile: $request->file('image'),
                    name: md5($request->get('first_name') . ' ' . $request->get('last_name')),
                    folder: 'our_team',
                    width: self::$imageWidth,
                    thumbWidth: 0,
                );
            }

            return Team::create([
                'first_name' => $request->validated('first_name'),
                'last_name' => $request->validated('last_name'),
                'email' => $request->validated('email'),
                'linked_in' => $request->validated('linked_in'),
                'image' => $imageName,
                'position' => $request->validated('position'),
                'description' => $request->validated('description'),
            ]);
        } catch (Exception $e) {
            return $e->getMessage();
        }
    }

    public static function updateTeam(TeamUpdateRequest $request, Team $team): bool|string
    {
        try {
            if ($request->hasFile('image')) {
                self::handleOldImage(imageName: $team->image, folder: 'our_team');
                $imageName = self::handleUploadImage(
                    uploadedFile: $request->file('image'),
                    name: md5($request->get('first_name') . ' ' . $request->get('last_name')),
                    folder: 'our_team',
                    width: self::$imageWidth,
                    thumbWidth: 0,
                );
                $team->image = $imageName;
            }

            $team->first_name = $request->validated('first_name');
            $team->last_name = $request->validated('last_name');
            $team->position = $request->validated('position');
            $team->email = $request->validated('email');
            $team->linked_in = $request->validated('linked_in');
            $team->description = $request->validated('description');

            return $team->save();
        } catch (Exception $e) {
            return $e->getMessage();
        }
    }

    public static function deleteTeam(Team $team): bool|string|null
    {
        try {
            if ($team->image) {
                self::handleOldImage($team->image, 'our_team');
            }

            return $team->delete();
        } catch (Exception $e) {
            return $e->getMessage();
        }
    }
}
