<?php

namespace App;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Program extends Model
{
    use HasFactory, SoftDeletes;

    protected $fillable = ['title', 'slug', 'program_page', 'content', 'link_title', 'link_to_pdf'];

    public static $basePrograms = [
        'vcos' => 'Volonterski centar Osijek',
        'kd' => 'Korisni dokumenti',
        'unv' => 'Učiniti nevidljivo vidljivim',
        'aess' => 'Ambasadori Europskih snaga solidarnosti',
        'demo_academy' => 'Demo akademija',
        'social_atelier' => 'Društveni atelje',
        'rcd' => 'Razvoj civilnog društva',
        'otgoo' => 'Osijek to GOO',
    ];

    /**
     * Undocumented function
     *
     * @return String
     */
    public static function getBaseProgramList()
    {
        $programKeys = array_keys(self::$basePrograms);

        return implode(',', $programKeys);
    }
}
