<?php

namespace App\Constants\Volonteka;

class Volonteka
{
    public const WANT_TO_VOLUNTEER_LINK = 'https://www.volonteka.hr/osijek-i-okolica/volonteri';
    public const NEED_VOLUNTEERS_LINK = 'https://www.volonteka.hr/osijek-i-okolica/organizatori-volontiranja';
    public const OPEN_VOLUNTEERING_OPPORTUNITIES_LINK = ' https://www.volonteka.hr/osijek-i-okolica';
}
