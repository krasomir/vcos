<?php

namespace App;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class VolunteerOrganisation extends Model
{
    use HasFactory, SoftDeletes;

    protected $fillable = [
        "organisation_type", 
        "organisation_name", 
        "address", 
        "city", 
        "telephone", 
        "contact_email", 
        "contact_telephone", 
        "contact_person",
        "volunteer_engagement", 
        "volunteer_number", 
        "position_name", 
        "purpose_of_position", 
        "tasks_description", 
        "volunteer_qualifications", 
        "volunteer_period", 
        "hour_number", 
        "volunteering_place", 
        "application_deadline", 
        "additional_education", 
        "expense_refund", 
        "volunteer_period_from", 
        "volunteer_period_to", 
        "volunteer_menagement", 
        "leadership_training", 
        "newsletter", 
    ];

    public static $expenseRefund = [
        'nismo u mogućnosti pokriti navdene troškove',
        'troškovi prijevoza',
        'prehrana',
        'smještaj'
    ];

    public $casts = [
        'application_deadline' => 'date'
    ];

    /**
     * Undocumented function
     *
     * @param integer $value
     * @return string
     */
    public function getAdditionalEducationAttribute(int $value)
    {
        return (int)$value === 1 ? 'da' : 'ne';
    }

    /**
     * Undocumented function
     *
     * @param string $value
     * @return string
     */
    public function getExpenseRefundAttribute(string $value)
    {
        $expenseTitles = [];
        $expenseIds = explode(',', $value);

        foreach ($expenseIds as $expense) {
            $expenseTitles[] = self::$expenseRefund[$expense];
        }

        return implode(', ', $expenseTitles);
    }

    /**
     * Undocumented function
     *
     * @param integer $value
     * @return string
     */
    public function getVolunteerMenagementAttribute(int $value)
    {
        return (int)$value === 1 ? 'da' : 'ne';
    }

    /**
     * Undocumented function
     *
     * @param integer $value
     * @return string
     */
    public function getLeadershipTrainingAttribute(int $value)
    {
        return (int)$value === 1 ? 'da' : 'ne';
    }

    /**
     * Undocumented function
     *
     * @param integer $value
     * @return string
     */
    public function getOrganisationHelpAttribute(int $value)
    {
        return (int)$value === 1 ? 'da' : 'ne';
    }

    /**
     * Undocumented function
     *
     * @param integer $value
     * @return string
     */
    public function getNewsletterAttribute(int $value)
    {
        return (int)$value === 1 ? 'da' : 'ne';
    }
}
