<?php

namespace App;

use App\Http\Requests\CreateVolunteerApplicationRequest;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Volunteer extends Model
{
    use HasFactory, SoftDeletes;

    /**
     * The attributes that aren't mass assignable.
     *
     * @var array
     */
    protected $guarded = [];

    protected $casts = [
        'verified_at' => 'datetime',
    ];

    /**
     * Undocumented function
     *
     * @param String $value
     * @return array
     */
    public function getWorkAreaAttribute(String $value)
    {
        $workAreas = [];
        $tmpWorkAreas = unserialize($value);

        foreach($tmpWorkAreas as $area) {
            if ($area != '') {
                $workAreas[] = $this->sanitizeString($area);
            }            
        }

        return $workAreas;
    }

    /**
     * Undocumented function
     *
     * @param String $value
     * @return array
     */
    public function getGeoAreaAttribute(String $value)
    {
        $geoAreas = [];
        $tmpGeoAreas = unserialize($value);

        foreach($tmpGeoAreas as $area) {
            if ($area != '') {
                $geoAreas[] = $this->sanitizeString($area);
            }            
        }

        return $geoAreas;
    }

    /**
     * Undocumented function
     *
     * @param String $value
     * @return array
     */
    public function getSourceInfoAttribute(String $value)
    {
        $sourceInfo = [];
        $tmpSourceInfo = unserialize($value);

        if ($tmpSourceInfo) {
            foreach ($tmpSourceInfo as $info) {
                if ($info != '') {
                    $sourceInfo[] = $this->sanitizeString($info);
                }            
            }
        }        

        return $sourceInfo;
    }

    /**
     * Undocumented function
     *
     * @param String $string
     * @return string
     */
    private function sanitizeString(String $string)
    {
        return preg_replace('/[\x00-\x1F\x7F]/u', '', $string);
    }

    public function store(CreateVolunteerApplicationRequest $request)
    {
        return $this->create([
            'first_last_name' => $request->first_last_name,
            'gender' => $request->gender,
            'birth_year' => $request->birth_year,
            'city' => $request->city,
            'telephone' => $request->telephone,
            'email' => $request->email,
            'profession' => $request->profession,
            'additional_skills' => $request->additional_skills,
            'volunteer_experience' => $request->volunteer_experience,
            'volunteer_engagement' => $request->volunteer_engagement,
            'work_area' => serialize($request->work_area),
            'geo_area' => serialize($request->geo_area),
            'geo_area_sib' => $request->geo_area_sib,
            'exclude_area' => $request->exclude_area,
            'source_info' => serialize($request->source_info),
            'source_info_other' => $request->source_info_other,
            'newsletter' => $request->newsletter ? 1: 0,
        ]);
    }
}
