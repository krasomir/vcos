<?php

namespace App;

use App\Http\Requests\PostStoreRequest;
use App\Traits\ImageUploadTrait;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;
use Laravel\Scout\Attributes\SearchUsingFullText;
use Laravel\Scout\Attributes\SearchUsingPrefix;
use Laravel\Scout\Searchable;

class Post extends Model
{
    use ImageUploadTrait, Searchable, SoftDeletes;

    protected $fillable = [
        'author_id', 'slug', 'name', 'datetime_on', 'datetime_off', 'preview',
        'text', 'title', 'keywords', 'description', 'is_valid', 'page_id', 'image_name',
    ];

    protected $casts = [
        'datetime_on' => 'datetime',
        'datetime_off' => 'datetime',
    ];

    public function toSearchableArray(): array
    {
        return [
            'name' => strip_tags($this->name),
            'preview' => strip_tags($this->preview),
            'text' => strip_tags($this->text),
        ];
    }

    protected static int $postImageWidth = 1035;

    protected static int $postThumbImageWidth = 514;

    public function shortenText(): string
    {
        $bigText = $this->text;
        $bigText = strip_tags($bigText);

        return Str::words($bigText, 30, ' ...');
    }

    public function shortenPreview(): string
    {
        if (strlen($this->preview) > 400) {
            return Str::words(strip_tags($this->preview), 80, ' ...');
        }

        return $this->preview;
    }

    public function getOldPostImagePath(): ?PostMainImage
    {
        return PostMainImage::where('post_id', $this->id)->select('crop', 'crop_thumb')->first();
    }

    /**
     * Undocumented function
     *
     * @return Post|Exception
     */
    public static function store(PostStoreRequest $request)
    {
        $transaction = DB::transaction(function () use ($request) {

            try {

                if ($request->hasfile('image')) {

                    $imageName = self::handleUploadImage(
                        $request->file('image'),
                        $request->slug,
                        'news',
                        self::$postImageWidth,
                        self::$postThumbImageWidth
                    );
                }

                $post = Post::create([
                    'author_id' => auth()->user()->id,
                    'slug' => $request->slug,
                    'name' => $request->name,
                    'datetime_on' => $request->datetime_on,
                    'datetime_off' => $request->datetime_off,
                    'preview' => $request->preview,
                    'text' => $request->text,
                    'title' => $request->title,
                    'keywords' => $request->keywords,
                    'description' => $request->description,
                    'is_valid' => $request->is_valid ?? 0,
                    'image_name' => $imageName ?? null,
                    'page_id' => $request->page_id,
                ]);

                return $post;
            } catch (\Exception $e) {
                return $e->getMessage();
            }
        });

        return $transaction;
    }

    /**
     * Undocumented function
     *
     * @return Post|Exception
     */
    public static function updatePost(PostStoreRequest $request, Post $post)
    {
        $transaction = DB::transaction(function () use ($request, $post) {

            try {

                if ($request->hasfile('image')) {

                    if ($post->image_name) {
                        self::handleOldImage($post->image_name, 'news');
                    }

                    $imageName = self::handleUploadImage(
                        $request->file('image'),
                        $request->slug,
                        'news',
                        self::$postImageWidth,
                        self::$postThumbImageWidth
                    );
                    $post->image_name = $imageName;
                }

                $post->author_id = auth()->user()->id;
                $post->slug = $request->slug;
                $post->name = $request->name;
                $post->datetime_on = $request->datetime_on;
                $post->datetime_off = $request->datetime_off;
                $post->preview = $request->preview;
                $post->text = $request->text;
                $post->title = $request->title;
                $post->keywords = $request->keywords;
                $post->description = $request->description;
                $post->is_valid = $request->is_valid ?? 0;
                $post->page_id = $request->page_id;
                $post->save();

                return $post->fresh();
            } catch (\Exception $e) {
                return $e->getMessage();
            }
        });

        return $transaction;
    }

    /**
     * Undocumented function
     *
     * @return bool|Exception
     */
    public static function deletePost(Post $post)
    {
        try {
            if ($post->image_name) {
                self::handleOldImage($post->image_name, 'news');
            }

            return $post->delete();
        } catch (\Exception $e) {
            return $e->getMessage();
        }
    }
}
