<?php

namespace App\Traits;

use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\Facades\Image;

trait ImageUploadTrait 
{
    /**
     * Undocumented function
     *
     * @param UploadedFile $uploadedFile
     * @param String $name
     * @param String $folder
     * @param integer $width
     * @param string $disk
     * @return File
     */
    public static function handleUploadImage(UploadedFile $uploadedFile, String $name, String $folder, int $width, int $thumbWidth, $disk = 'public')
    {
        $extension = $uploadedFile->getClientOriginalExtension();
        $name = $name . '.' . $extension;
        $path = '/images/' . $folder;
        $thumbPath = '/thumb';
        $file = $uploadedFile->storeAs($path, $name, $disk);

        if ($extension != 'gif') {
            // make main picture
            $uploaded = self::makeMain($uploadedFile, $disk, $path, $name, $width);
        }            

        // make thumbnail picture
        $uploadedThumb = self::makeThumb($uploadedFile, $disk, $path, $thumbPath, $name, $thumbWidth);       

        return $name;
    }

    /**
     * Undocumented function
     *
     * @param UploadedFile $uploadedFile
     * @param String $disk
     * @param String $path
     * @param String $name
     * @param integer $width
     * @return File
     */
    private static function makeMain(UploadedFile $uploadedFile, String $disk, String $path, String $name, int $width)
    {
        $uploadedFile = Image::make(storage_path('app/'. $disk . $path . '/' . $name));            
        $uploadedFile->resize($width, null, function ($constraint) {
            $constraint->aspectRatio();
        });
        $uploadedFile->save();

        return $uploadedFile;
    }

    /**
     * Undocumented function
     *
     * @param UploadedFile $uploadedFile
     * @param String $disk
     * @param String $path
     * @param String $thumbPath
     * @param String $name
     * @param integer $thumbWidth
     * @return String|void
     */
    private static function makeThumb(UploadedFile $uploadedFile, String $disk, String $path, String $thumbPath, String $name, int $thumbWidth)
    {
        if ($thumbWidth === 0) {
            return;
        }

        $uploadedFile->storeAs($path . $thumbPath, $name, $disk);
        $thumbFile = Image::make(storage_path('app/' . $disk . $path . $thumbPath . '/' . $name));            
        $thumbFile->resize($thumbWidth, null, function ($constraint) {
            $constraint->aspectRatio();
        });
        $thumbFile->save();

        return $thumbPath;
    }

    /**
     * Undocumented function
     *
     * @param String $imageName
     * @param String $folder
     * @param string $disk
     * @return boolean
     */
    public static function handleOldImage(String $imageName, String $folder, $disk = 'public')
    {
        $paths = [
            $disk . '/images/' . $folder . '/',
            $disk . '/images/' . $folder . '/thumb/'
        ];
        foreach ($paths as $path) {
            self::deleteHandler($path . $imageName);
        }        

        return true;
    }

    /**
     * Undocumented function
     *
     * @param String $path
     * @param string $disk
     * @return void
     */
    public static function deleteHandler(String $path, $disk = 'public')
    {
        Storage::delete($path);
        //Storage::disk($disk)->delete($path);
    }

    /**
     * Undocumented function
     *
     * @param String $oldName
     * @param String $slug
     * @param String $folder
     * @return boolean|Sgtring
     */
    public static function handleImageRename(String $oldName, String $slug, String $folder)
    {
        $newNameExt = pathinfo(storage_path('images/'. $folder .'/'. $oldName), PATHINFO_EXTENSION);
        $newName = $slug . '.' . $newNameExt;

        if (Storage::move('public/images/'. $folder .'/'. $oldName, 'public/images/'. $folder .'/'. $newName)) {
            return $newName;
        }

        return false;
    }
}