<?php

namespace App\Traits;

use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;

trait FileUploadTrait 
{
    /**
     * Undocumented function
     *
     * @param UploadedFile $uploadedFile
     * @param String $name
     * @return String
     */
    public static function handleFileUpload(UploadedFile $uploadedFile, String $name)
    {
        $extension = $uploadedFile->extension();
        $newName = $name . '.' . $extension;

        $uploadedFile->storeAs(
            'files/podcasts', $newName, 'public'
        );

        return $newName;
    }

    /**
     * Undocumented function
     *
     * @param [type] $oldName
     * @param [type] $slug
     * @return String|boolean
     */
    public static function handleRenameFile($oldName, $slug)
    {
        $newNameExt = pathinfo(storage_path('files/podcasts/'. $oldName), PATHINFO_EXTENSION);
        $newName = $slug . '.' . $newNameExt;

        if ($oldName === $newName) {
            return $newName;
        }

        if (Storage::move('public/files/podcasts/'. $oldName, 'public/files/podcasts/'. $newName)) {
            return $newName;
        }

        return false;
    }

    /**
     * Undocumented function
     *
     * @param String $path
     * @return boolean
     */
    public static function handleDeleteFile(String $path)
    {
        return Storage::delete($path); 
    }
}